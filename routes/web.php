<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/', 'Home\IndexController@index');



Route::get('/web/index', 'Home\IndexController@index');
Route::get('/wap/register', 'Wap\IndexController@register');

Route::group(['prefix' => 'web'], function () {
    Route::get('index', 'Home\IndexController@index');
    Route::get('aboutUs', 'Home\IndexController@aboutUs');
    Route::get('left', 'Home\IndexController@left');
    Route::get('captcha', 'Home\LoginController@captcha');
    Route::get('login', 'Home\LoginController@login');
    Route::get('logout', 'Home\LoginController@logout');
    Route::post('doLogin', 'Home\LoginController@doLogin');
    Route::get('register', 'Home\LoginController@register');
    Route::post('doRegister', 'Home\LoginController@doRegister');
    Route::post('file_down/{$file_path}', 'File\FileController@getFileUrl');
});

Route::group(['prefix' => 'mobile'], function () {
    Route::get('/', 'Wap\IndexController@index');
    Route::any('/product/{cate_id?}', 'Wap\IndexController@product');
    Route::any('/product_category/{cate_id?}', 'Wap\IndexController@productCategory');
    Route::get('/product_detail/{id}/{spe_id?}', 'Wap\IndexController@productDetail');
    Route::any('/news/{cate_id?}', 'Wap\IndexController@news');
    Route::get('/news_detail/{id}', 'Wap\IndexController@newsDetail');
    Route::get('/about_us', 'Wap\IndexController@aboutUs');
    Route::get('/testing_center', 'Wap\IndexController@testing_center');
    Route::get('/testing_sub/{cate_id}/{cate_sub_id}', 'Wap\IndexController@testing_sub');
    Route::any('/search', 'Wap\IndexController@search');
    Route::any('/register', 'Wap\IndexController@register');
    Route::post('/doRegister', 'Wap\IndexController@doRegister');
});

Route::group(['prefix' => 'web/article'], function () {
    Route::get('activity', 'Home\ArticleController@activityList');
    Route::get('activityDetail/{id}', 'Home\ArticleController@activityDetail');
    Route::get('technical', 'Home\ArticleController@technical');
    Route::get('article_list/{cate_id?}', 'Home\ArticleController@article_list');
    Route::get('article_list_all/{cate_id}', 'Home\ArticleController@article_list_all');
    Route::get('aboutUs', 'Home\ArticleController@about_us');
    Route::get('contactUs', 'Home\ArticleController@contact_us');
    Route::get('jobOfficer', 'Home\ArticleController@job_officer');
    Route::get('articleDetail/{id}', 'Home\ArticleController@article_detail');
    Route::get('testing_center', 'Home\TestingController@testing_center');
    Route::get('testing_detection', 'Home\TestingController@testing_detection');
    Route::get('testing_sub/{cate_id}/{cate_sub_id}', 'Home\TestingController@testing_sub');
});

Route::group(['prefix' => 'web/goods'], function () {
    Route::any('list/{cate_id?}/{order_type?}', 'Home\ProductController@goodsList');
    Route::get('detail/{id}/{spe_id?}', 'Home\ProductController@goodsDetail');
    Route::post('addInquiry', 'Home\ProductController@addInquiry');
});

Route::group(['prefix' => 'file'], function () {
    Route::any('test', 'File\FileController@testExcel');
});

Route::group(['prefix' => 'goods'], function () {
    Route::any('test', 'Goods\GoodsController@test');
    Route::any('delGoodsBatch', 'Goods\GoodsController@delGoodsBatch');
    Route::any('goods/editGoodsBatch','Goods\GoodsController@editGoodsBatch');
    Route::any('goods/editGoodsSpecsBatch','Goods\GoodsController@editGoodsSpecBatch');
});

Auth::routes();

/** common **/

Route::get('/home', 'HomeController@index');

Route::get('/changeNaviModule/{moduleId}', 'HomeController@changeNaviModule');

Route::get('/toRenderLoginForm', 'Auth\LoginController@toRenderLoginForm');

Route::get('/backstage/api/province/getall', 'Backstage\System\AddressController@getProvinces')->name('province.queryapi.getall');

Route::get('/backstage/api/city/getbyprovince/{provinceId}', 'Backstage\System\AddressController@getCitiesByProvince')->name('city.queryapi.getbyprovince');

Route::get('/backstage/api/district/getbycity/{cityId}', 'Backstage\System\AddressController@getDistrictsByCity')->name('district.queryapi.getbycity');

Route::post('/backstage/api/upload', 'Backstage\System\BaseUploadController@doUpload')->name('backstage.uploader.api');

Route::post('/backstage/api/richeditor/upload', 'Backstage\System\BaseUploadController@doRichEditorUpload')->name('backstage.richeditor.uploader.api');

Route::get('/backstage/common/getcityselector', 'Backstage\Common\SelectorController@getCitySelector')->name('common.city.getselector');




/** end common **/

$routes = \App\Lib\Core\RouteBuilder::buildWebRoutes();
foreach ($routes as $route) {
    if ($route['method'] === \App\Route::$ROUTE_METHOD_ANY) {
        if (!empty($route['slug'])) {
            Route::any($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::any($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_GET) {
        if (!empty($route['slug'])) {
            Route::get($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::get($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_POST) {
        if (!empty($route['slug'])) {
            Route::post($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::post($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_PUT) {
        if (!empty($route['slug'])) {
            Route::put($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::put($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_DELETE) {
        if (!empty($route['slug'])) {
            Route::delete($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::delete($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_PATCH) {
        if (!empty($route['slug'])) {
            Route::patch($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::patch($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_OPTIONS) {
        if (!empty($route['slug'])) {
            Route::options($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::options($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    }
}
