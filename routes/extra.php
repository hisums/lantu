<?php

/** ==== Start Extras ==== */
if (config('sandbox.enable_verify_code_sandbox')) {
    Route::get('/extra/sandbox/verifycode-testor', 'Extra\SandBox\MobileSandBoxController@index');

    Route::post('/extra/sandbox/getverifycode', 'Extra\SandBox\MobileSandBoxController@getVerifyCode');
}
/** ==== End Extras ==== */
