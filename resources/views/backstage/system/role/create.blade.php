<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 角色名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('role_id')}}" value="{{ isset($role)?$role->id:'' }}">
            <input type="text" name="{{makeElUniqueName('role_name')}}" value="{{ isset($role)?$role->name:'' }}" required lay-verify="required" placeholder="角色的名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">描述</label>
        <div class="layui-input-block">
            <textarea name="{{makeElUniqueName('role_desc')}}" class="layui-textarea">{{ isset($role)?$role->description:'' }}</textarea>
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('role_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
    form.on('submit({{makeElUniqueName('role_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/role/savenew';
        var postParam = {
            name: data.field['{{makeElUniqueName('role_name')}}'],
            description: data.field['{{makeElUniqueName('role_desc')}}'],
        };
        var roleId = data.field['{{makeElUniqueName('role_id')}}'];
        if (roleId != '') {
            //修改
            url = '/backstage/api/role/update';
            postParam.id = roleId;
        }
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                    //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                    //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
