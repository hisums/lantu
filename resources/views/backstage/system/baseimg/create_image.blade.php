<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 图片类型</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('image_id')}}" value="{{ isset($baseImage)?$baseImage->id:'' }}">
            <select name="{{makeElUniqueName('image_type')}}" lay-verify="required">
                @foreach (\App\BaseImg::$BASE_IMAGE_TYPE_MAP as $item)
                    @if (isset($baseImage))
                        <option value='{{$item['key']}}' {{ $baseImage->img_type==$item['key']?'selected':'' }}>{{$item['text']}}</option>
                    @else
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 图片</label>
        <div class="layui-input-block">
            <div class="layui-big-upload-box">
                <img id="{{makeElUniqueName('picture')}}" src="{{ isset($baseImage)?$baseImage->getFullPicturePath():'/images/no-pic-back.png' }}">
                <input type="hidden" name="{{makeElUniqueName('picture_file_id')}}" value="{{ isset($baseImage)?$baseImage->picture:'' }}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('picture_id')}}" class="layui-upload-file" id="{{makeElUniqueName('picture_id')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">图片城市</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('image_city')}}" lay-filter="{{makeElUniqueName('image_city')}}" lay-search>
                <option value="0" {{ isset($baseImage)?(empty($baseImage->city_id)?'selected':''):'' }}>(不限城市)</option>
                @foreach ($allcities as $city)
                    @if (isset($baseImage))
                        <option value='{{$city->id}}' {{ $baseImage->city_id == $city->id?'selected':'' }}>{{ $city->name }}({{ $city->code }})</option>
                    @else
                        <option value='{{$city->id}}'>{{ $city->name }}({{ $city->code }})</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">图片链接</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('image_link')}}" value="{{ isset($baseImage)?$baseImage->link_url:'' }}" placeholder="图片对应的导航链接，可为空" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($baseImage)?$baseImage->sort_order:'' }}" required lay-verify="required|number" placeholder="同一个城市下同种类型的图片显示排序号" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('image_save')}}">保存</button>
            <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
    layui.use(['form', 'validator', 'uploadUtil'], function(){
        var form = layui.form();
        var $ = layui.jquery;
        var layer = layui.layer;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;

        form.render();

        uploadUtil.doUpload({
            success: function(fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('picture_id')}}') {
                    $('#{{makeElUniqueName('picture')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('picture_file_id')}}\']').val(fileId);
                }
            }
        });

        form.on('submit({{makeElUniqueName('image_save')}})', function(data){
            var index = layer.load(1);
            var url = '/backstage/api/base-image/savenew';
            var postParam = {
                picture: data.field['{{makeElUniqueName('picture_file_id')}}'],
                img_type: data.field['{{makeElUniqueName('image_type')}}'],
                city_id: data.field['{{makeElUniqueName('image_city')}}'],
                link_url: data.field['{{makeElUniqueName('image_link')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
            };
            var imageId = data.field['{{makeElUniqueName('image_id')}}'];
            if (imageId != '') {
                //修改
                url = '/backstage/api/route/update';
                postParam.id = imageId;
            }
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, { icon: 6 });
                        layer.close(popLayerUtil.index);
                        popLayerUtil.onClose();
                    } else {
                        layer.msg(outResult.Message, { icon: 5 });
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });
    });
</script>
