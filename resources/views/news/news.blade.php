<div class="layui-form layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">新闻标题</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="新闻标题" name="{{makeElUniqueName('subject')}}" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label">所属分类</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('news_cate_id')}}">
                    <option value="">(所有分类)</option>
                    <option value="">(所有分类)</option>
                    @if(!empty($newsCates))
                        @foreach ($newsCates as $cate)
                            <option value='{{$cate->id}}'>{{$cate->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <label class="layui-form-label">是否显示</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('display_flag')}}">
                    <option value="">(所有)</option>
                    @foreach (\App\News::$DISPLAY_FLAG_MAP as $item)
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <label class="layui-form-label" style="width:130px">创建时间</label>
        <div class="layui-input-inline layui-short-input">
            <input class="layui-input" placeholder="开始日" id="{{makeElUniqueName('start_create')}}"
                   name="{{makeElUniqueName('start_create_dt')}}">
        </div>
        <label class="layui-form-label">----到----</label>
        <div class="layui-input-inline layui-short-input">
            <input class="layui-input" placeholder="结束日" id="{{makeElUniqueName('end_create')}}"
                   name="{{makeElUniqueName('end_create_dt')}}">
        </div>
        <div class="layui-input-inline" style="width:auto">
        </div>
        <label class="layui-form-label" >排序</label>
        <div class="layui-input-inline layui-short-input">
            <select name="{{makeElUniqueName('sort_order')}}">
                <option value="">默认排序</option>
                <option value="1">从低到高</option>
                <option value="2">从高到底</option>
            </select>
        </div>
    </div>
    <br/>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px"></label>
            <div class="layui-input-inline layui-long-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_goods')}}"><i
                            class="layui-icon">
                        &#xe615;</i> 搜索
                </button>
                &nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('addNews')}}"><i
                            class="layui-icon">&#xe654; </i> 新增
                </button>
            </div>
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbGoods')}}"></div>
<script>
    layui.use(['jfTable', 'form', 'addressUtil', 'dateRangeUtil', 'laydate', 'jquery', 'validator'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var jfTable = layui.jfTable;
        var form = layui.form();
        var addressUtil = layui.addressUtil;
        var dateRangeUtil = layui.dateRangeUtil;
        var laydate = layui.laydate;

        var start = {
            elem: $('#{{makeElUniqueName('start_create')}}')[0],
            format: 'YYYY-MM-DD hh:mm:ss',
            min: '1900-01-01 00:00:00',
            max: '2099-06-16 23:59:59', //最大日期
            istime: true,
            istoday: false,
            choose: function (datas) {
                end.min = datas; //开始日选好后，重置结束日的最小日期
                end.start = datas //将结束日的初始值设定为开始日
            }
        };
        var end = {
            elem: $('#{{makeElUniqueName('end_create')}}')[0],
            format: 'YYYY-MM-DD hh:mm:ss',
            min: '1900-01-01 00:00:00',
            max: '2099-06-16 23:59:59',
            istime: true,
            istoday: false,
            choose: function (datas) {
                start.max = datas; //结束日选好后，重置开始日的最大日期
            }
        };

        $('input[name={{makeElUniqueName('start_create_dt')}}]').on('click', function () {
            laydate(start);
        });
        $('input[name={{makeElUniqueName('end_create_dt')}}]').on('click', function () {
            laydate(end);
        });

        form.render();
        layui.define(function (exports) {
            var obj = {
                doEdit: function (id) {
                    $.get('/news/editIndex/' + id, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('newsAdd')}}',
                                title: '编辑分类',
                                type: 1,
                                content: str,
                                area: ['800px', '600px']
                            }),
                            onClose: function () {
                                layui.newsFuncs.refreshTableGrid();
                            }
                        });
                    });
                },
                doDelete: function (id) {
                    layer.confirm('确定删除该商品？', {
                        btn: ['确定', '放弃'],
                        icon: 3
                    }, function () {
                        var index = layer.load(1);
                        $.ajax({
                            contentType: "application/json",
                            type: 'get',
                            url: '/news/del/' + id,
                            success: function (outResult) {
                                layer.close(index);
                                if (outResult.Success) {
                                    layer.msg(outResult.Message, {icon: 6});
                                    layui.newsFuncs.refreshTableGrid();
                                } else {
                                    layer.msg(outResult.Message, {icon: 5});
                                }
                            },
                            error: function (error) {
                                layer.close(index);
                                layui.validator.processValidateError(error);
                            }
                        });
                    }, function () {
                    });
                },
                refreshTableGrid: function () {
                    $('input[name=\'{{makeElUniqueName('name')}}\']').val('');
                    $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
                }
            };
            exports('newsFuncs', obj);
        });

        $("#{{makeElUniqueName('tbGoods')}}").jfTable({
            url: '/news/query',
            pageSize: 5,
            page: true,
            skip: true,
            first: '首页',
            last: '尾页',
            columns: [{
                text: '操作',
                name: 'id',
                width: 200,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    var html = '<a class="layui-btn layui-btn-small layui-btn-warning" onclick="layui.newsFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe618;</i> 编辑</a>';
                    html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.newsFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                    return html;
                }
            }, {
                text: '新闻标题',
                name: 'subject',
                width: 170,
                align: 'center'
            }, {
                text: '创建时间',
                name: 'create_time',
                width: 170,
                align: 'center'
            }, {
                text: '是否显示',
                name: 'display_flag_text',
                width: 150,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if (dataItem.display_flag == {{\App\News::$DISPLAY_FLAG_ON}}) {
                        return '<span style="color:green">' + value + '</span>'
                    } else if (dataItem.display_flag == {{\App\News::$DISPLAY_FLAG_OFF}}) {
                        return '<span style="color:red">' + value + '</span>'
                    }
                }
            }, {
                text: '是否推荐',
                name: 'is_recommend_text',
                width: 150,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if (dataItem.is_recommend == {{\App\News::$RECOMMEND_YES}}) {
                        return '<span style="color:green">' + value + '</span>'
                    } else if (dataItem.is_recommend == {{\App\News::$RECOMMEND_NO}}) {
                        return '<span style="color:red">' + value + '</span>'
                    }
                }
            }, {
                text: '所属分类',
                name: 'newsCategoryGet.name',
                width: 170,
                align: 'center'
            },{
                text: '排序',
                name: 'sort_order',
                width: 100,
                align: 'center'
            },
            ],
            method: 'post',
            queryParam: {
                subject: $('input[name=\'{{makeElUniqueName('subject')}}\']').val(),
                display_flag: $('select[name=\'{{makeElUniqueName('display_flag')}}\']').val(),
                news_cate_id: $('select[name=\'{{makeElUniqueName('news_cate_id')}}\']').val(),
                start_create_dt: $('input[name=\'{{makeElUniqueName('start_create_dt')}}\']').val(),
                end_create_dt: $('input[name=\'{{makeElUniqueName('end_create_dt')}}\']').val(),
                sort_order: $('select[name=\'{{makeElUniqueName('sort_order')}}\']').val(),
            },
            toolbarClass: 'layui-btn-small',
            onBeforeLoad: function (param) {
                return $.extend(param, {
                    subject: $('input[name=\'{{makeElUniqueName('subject')}}\']').val(),
                    display_flag: $('select[name=\'{{makeElUniqueName('display_flag')}}\']').val(),
                    news_cate_id: $('select[name=\'{{makeElUniqueName('news_cate_id')}}\']').val(),
                    start_create_dt: $('input[name=\'{{makeElUniqueName('start_create_dt')}}\']').val(),
                    end_create_dt: $('input[name=\'{{makeElUniqueName('end_create_dt')}}\']').val(),
                    sort_order: $('select[name=\'{{makeElUniqueName('sort_order')}}\']').val(),
                });
            },
            onLoadSuccess: function (data) {
                return data;
            },
            dataFilter: function (data) {
                return data;
            }
        });


        $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_goods')}}\']').on('click', function () {
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('addNews')}}\']').on('click', function () {
            $.get('/news/addIndex', {}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('newsAdd')}}',
                        title: '添加新闻',
                        type: 1,
                        content: str,
                        area: ['800px', '600px']
                    }),
                    onClose: function () {
                        layui.newsFuncs.refreshTableGrid();
                    }
                });
            });
        });
    });
</script>
