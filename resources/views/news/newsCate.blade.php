<div class="layui-form layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">分类名称</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="分类名称" name="{{makeElUniqueName('name')}}" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label">是否显示</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('display_flag')}}">
                    <option value="">(所有分类)</option>
                    <option value="">(所有分类)</option>
                    @foreach (\App\NewsCategory::$DISPLAY_FLAG_MAP as $item)
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px"></label>
            <div class="layui-input-inline layui-long-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_goods')}}"><i
                            class="layui-icon">
                        &#xe615;</i> 搜索
                </button>
                &nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('addCate')}}"><i
                            class="layui-icon">&#xe654; </i> 新增
                </button>
            </div>
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbGoods')}}"></div>
<script>
    layui.use(['jfTable', 'form', 'addressUtil', 'dateRangeUtil','validator'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var jfTable = layui.jfTable;
        var form = layui.form();
        var addressUtil = layui.addressUtil;
        var dateRangeUtil = layui.dateRangeUtil;

        form.render();
        layui.define(function (exports) {
            var obj = {
                doEdit: function (id) {
                    $.get('/news/category/editIndex/'+id, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('createNewsCate')}}',
                                title: '编辑分类',
                                type: 1,
                                content: str,
                                area: ['800px', '600px']
                            }),
                            onClose: function () {
                                layui.togoodsQueryFuncs.refreshTableGrid();
                            }
                        });
                    });
                },
                doDelete: function (id) {
                    layer.confirm('确定删除该商品？', {
                        btn: ['确定', '放弃'],
                        icon: 3
                    }, function () {
                        var index = layer.load(1);
                        $.ajax({
                            contentType: "application/json",
                            type: 'get',
                            url: '/news/category/del/' + id,
                            success: function (outResult) {
                                layer.close(index);
                                if (outResult.Success) {
                                    layer.msg(outResult.Message, {icon: 6});
                                    layui.togoodsQueryFuncs.refreshTableGrid();
                                } else {
                                    layer.msg(outResult.Message, {icon: 5});
                                }
                            },
                            error: function (error) {
                                layer.close(index);
                                layui.validator.processValidateError(error);
                            }
                        });
                    }, function () {
                    });
                },
                refreshTableGrid: function () {
                    $('input[name=\'{{makeElUniqueName('name')}}\']').val('');
                    $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
                }
            };
            exports('togoodsQueryFuncs', obj);
        });

        $("#{{makeElUniqueName('tbGoods')}}").jfTable({
            url: '/news/category/query',
            pageSize: 5,
            page: true,
            skip: true,
            first: '首页',
            last: '尾页',
            columns: [{
                text: '操作',
                name: 'id',
                width: 200,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    var html = '<a class="layui-btn layui-btn-small layui-btn-warning" onclick="layui.togoodsQueryFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe618;</i> 编辑</a>';
                    html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.togoodsQueryFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                    return html;
                }
            }, {
                text: '分类名称',
                name: 'name',
                width: 170,
                align: 'center'
            }, {
                text: '显示顺序',
                name: 'sort_order',
                width: 170,
                align: 'center'
            }, {
                text: '新闻分类主图片',
                name: 'news_cate_picture',
                width: 170,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    return '<img style="height: 170px;width: 170px;" src="' + dataItem.getFullPicturePath + '">';
                }
            }, {
                text: '是否显示',
                name: 'display_flag_text',
                width: 150,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if (dataItem.display_flag == {{\App\NewsCategory::$DISPLAY_FLAG_ON}}) {
                        return '<span style="color:green">' + value + '</span>'
                    } else if (dataItem.display_flag == {{\App\NewsCategory::$DISPLAY_FLAG_OFF}}) {
                        return '<span style="color:red">' + value + '</span>'
                    }
                }
            }
            ],
            method: 'post',
            queryParam: {
                name: $('input[name=\'{{makeElUniqueName('name')}}\']').val(),
                display_flag: $('select[name=\'{{makeElUniqueName('display_flag')}}\']').val(),
            },
            toolbarClass: 'layui-btn-small',
            onBeforeLoad: function (param) {
                return $.extend(param, {
                    name: $('input[name=\'{{makeElUniqueName('name')}}\']').val(),
                    display_flag: $('select[name=\'{{makeElUniqueName('display_flag')}}\']').val(),
                });
            },
            onLoadSuccess: function (data) {
                return data;
            },
            dataFilter: function (data) {
                return data;
            }
        });


        $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_goods')}}\']').on('click', function () {
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('addCate')}}\']').on('click', function () {
            $.get('/news/category/addIndex', {}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createNewsCate')}}',
                        title: '新增分类',
                        type: 1,
                        content: str,
                        area: ['800px', '600px']
                    }),
                    onClose: function () {
                        layui.togoodsQueryFuncs.refreshTableGrid();
                    }
                });
            });
        });
    });
</script>
