<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 分类名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('id')}}" value="{{isset($cate_info)?$cate_info->id:''}}" >
            <input type="text" name="{{makeElUniqueName('name')}}"
                   required lay-verify="required" value="{{isset($cate_info)?$cate_info->name:''}}"
                   placeholder="分类名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 显示顺序</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}"
                   required lay-verify="required" value="{{isset($cate_info)?$cate_info->sort_order:''}}"
                   placeholder="显示顺序，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 是否显示</label>
        <div class="layui-input-block">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}" value="{{ \App\NewsCategory::$DISPLAY_FLAG_ON }}"
                     class="layui-input" {{isset($cate_info) && $cate_info->display_flag == \App\NewsCategory::$DISPLAY_FLAG_ON?'checked':''}} {{!isset($cate_info)?'checked':''}}
                   title="是">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}" value="{{ \App\NewsCategory::$DISPLAY_FLAG_OFF }}"
                    class="layui-input" {{isset($cate_info) && $cate_info->display_flag == \App\NewsCategory::$DISPLAY_FLAG_OFF?'checked':''}}
                   title="否">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 新闻分类主图片</label>
        <div class="layui-input-block">
            <div class="layui-big-upload-box">
                <img id="{{makeElUniqueName('picture')}}" src="{{isset($cate_info)?$cate_info->getFullPicturePath():''}}"
                     >
                <input type="hidden" name="{{makeElUniqueName('news_cate_picture_file_id')}}" value="{{isset($cate_info)?$cate_info->news_cate_picture:''}}"
                       >
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('news_cate_picture')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('news_cate_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
    layui.use(['form', 'validator', 'uploadUtil', 'reditorUtil'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;
        var reditorUtil = layui.reditorUtil;
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();

        var descIndex = reditorUtil.doInitEditor({elemId: '{{makeElUniqueName('goods_content')}}'});
        uploadUtil.doUpload({
            success: function (fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('news_cate_picture')}}') {
                    $('#{{makeElUniqueName('picture')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('news_cate_picture_file_id')}}\']').val(fileId);
                }
            }
        });
        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('news_cate_save')}})', function (data) {
            var index = layer.load(1);
            var url = '/news/category/add';
            var postParam = {
                name: data.field['{{makeElUniqueName('name')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
                news_cate_picture: data.field['{{makeElUniqueName('news_cate_picture_file_id')}}'],
                display_flag: data.field['{{makeElUniqueName('display_flag')}}']
            };
            var id = data.field['{{makeElUniqueName('id')}}'];
            if (id != '') {
                //修改
                url = '/news/category/edit';
                postParam.id = id;
            }
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        layer.close(popLayerUtil.index);
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose();
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });
    });
</script>
