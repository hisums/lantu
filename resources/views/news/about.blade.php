<div class="layui-form layui-field-box">
    @foreach($list as $v)
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">{{$v->name}}</label>
            <div class="layui-input-inline layui-short-input">
                <input value="{{$v->content}}" type="text" placeholder="分类名称" lay-verify="required" data-id="{{$v->id}}" name="{{makeElUniqueName('name')}}[{{$v->id}}]" autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    @endforeach
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px"></label>
            <div class="layui-input-inline layui-long-input">
                <button style="    margin-top: 25px;" class="layui-btn" lay-filter="{{makeElUniqueName('save_about')}}"><i
                            class="layui-icon">
                        &#x1005;</i> 保存
                </button>
            </div>
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbGoods')}}"></div>
<script>
    layui.use(['form',  'dateRangeUtil','validator'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var form = layui.form();

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('save_about')}}\']').on('click', function () {
            var dataInput = $('input[type=text]');
            var param = [];
            var index = layer.load(1);
            dataInput.each(function(k,v){
                param.push({
                    content:$(v).val(),
                    id:$(v).attr('data-id')
                })
            });
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: '/about/edit',
                data: JSON.stringify(param),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
        });

    });
</script>
