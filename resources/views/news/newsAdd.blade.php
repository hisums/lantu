<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 新闻标题</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('id')}}" value="{{isset($newsInfo)?$newsInfo->id:''}}">
            <input type="text" name="{{makeElUniqueName('subject')}}"
                   required lay-verify="required" value="{{isset($newsInfo)?$newsInfo->subject:''}}"
                   placeholder="分类名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 显示顺序</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}"
                   required lay-verify="required" value="{{isset($newsInfo)?$newsInfo->sort_order:''}}"
                   placeholder="显示顺序，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 新闻分类</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('news_cate_id')}}" lay-search required lay-verify="required">
                @if($newsCates->isEmpty())
                    <option value="">请先添加分类</option>
                @endif
                @if(!$newsCates->isEmpty())
                    @foreach ($newsCates as $cate)
                        <option {{isset($newsInfo) && $newsInfo->news_cate_id == $cate->id?'selected="selected"':''}} value='{{$cate->id}}'>{{$cate->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 展示图片</label>
        <div class="layui-input-block">
            <div class="layui-big-upload-box">
                <img id="{{makeElUniqueName('picture')}}"
                     src="{{isset($newsInfo)?$newsInfo->getFullPicturePath():''}}">
                <input type="hidden" name="{{makeElUniqueName('show_img')}}"
                       value="{{isset($newsInfo)?$newsInfo->show_img:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('show_img_picture')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id')}}">
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top: 120px"  class="layui-form-item">
        <label class="layui-form-label">新闻详情</label>
        <div class="layui-input-block">
            <script id="container" name="content" type="text/plain">{!! isset($newsInfo)?$newsInfo->content:'' !!}</script>
            {{--<textarea name="{{makeElUniqueName('content')}}" id="{{makeElUniqueName('content')}}"
                      class="layui-textarea">{{isset($newsInfo)?$newsInfo->content:''}}</textarea>--}}
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 是否显示</label>
        <div class="layui-input-block">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}" value="{{ \App\News::$DISPLAY_FLAG_ON }}"
                   class="layui-input"
                   {{isset($newsInfo) && $newsInfo->display_flag == \App\News::$DISPLAY_FLAG_ON?'checked':''}} {{!isset($newsInfo)?'checked':''}}
                   title="是">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}" value="{{ \App\News::$DISPLAY_FLAG_OFF }}"
                   class="layui-input"
                   {{isset($newsInfo) && $newsInfo->display_flag == \App\News::$DISPLAY_FLAG_OFF?'checked':''}}
                   title="否">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 是否推荐(最多只显示5个)</label>
        <div class="layui-input-block">
            <input type="radio" name="{{makeElUniqueName('is_recommend')}}" value="{{ \App\News::$RECOMMEND_YES }}"
                   class="layui-input"
                   {{isset($newsInfo) && $newsInfo->is_recommend == \App\News::$RECOMMEND_YES?'checked':''}} {{!isset($newsInfo)?'checked':''}}
                   title="是">
            <input type="radio" name="{{makeElUniqueName('is_recommend')}}" value="{{ \App\News::$RECOMMEND_NO }}"
                   class="layui-input"
                   {{isset($newsInfo) && $newsInfo->is_recommend == \App\News::$RECOMMEND_NO?'checked':''}}
                   title="否">
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('goods_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>
<!-- ueditor配置文件 -->
<script type="text/javascript" src="/vendor/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/vendor/ueditor/ueditor.all.js"></script>
<script>
    var ue = UE.getEditor('container');
    layui.use(['form', 'validator', 'uploadUtil', 'reditorUtil'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;
        var reditorUtil = layui.reditorUtil;
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();

        uploadUtil.doUpload({
            success: function (fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('show_img_picture')}}') {
                    $('#{{makeElUniqueName('picture')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('show_img')}}\']').val(fileId);
                }
            }
        });

        {{--var content = reditorUtil.doInitEditor({elemId: '{{makeElUniqueName('content')}}'});--}}


        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('goods_save')}})', function (data) {
            var content = ue.getContent();
            var index = layer.load(1);
            var url = '/news/add';
            var postParam = {
                subject: data.field['{{makeElUniqueName('subject')}}'],
                content: content,//reditorUtil.getContent(content),
                news_cate_id: data.field['{{makeElUniqueName('news_cate_id')}}'],
                display_flag: data.field['{{makeElUniqueName('display_flag')}}'],
                show_img: data.field['{{makeElUniqueName('show_img')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
                is_recommend: data.field['{{makeElUniqueName('is_recommend')}}']
            };
            var id = data.field['{{makeElUniqueName('id')}}'];
            if (id != '') {
                //修改
                url = '/news/edit';
                postParam.id = id;
            }
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        layer.close(popLayerUtil.index);
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose();
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });
    });
</script>
