@extends('layouts.app')

@section('content')
<div class="layui-body">
    <div class="layui-form layui-layer-outerbox" style="margin-left:100px;margin-top:100px;margin-right:350px">
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 手机号</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('mobile')}}" required lay-verify="required" placeholder="请输入测试手机号" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 短信行为</label>
            <div class="layui-input-block">
                <input type='radio' name='{{makeElUniqueName('mobile_behavior')}}' value='1' title='新用户注册' checked>
                <input type='radio' name='{{makeElUniqueName('mobile_behavior')}}' value='2' title='用户登录' >
                <input type='radio' name='{{makeElUniqueName('mobile_behavior')}}' value='3' title='重置密码' >
                <input type='radio' name='{{makeElUniqueName('mobile_behavior')}}' value='4' title='修改用户信息' >
            </div>
        </div>
        <div class="layui-form-item layui-form-center">
            <label class="layui-form-label" style="margin-left:150px;width:200px;color:blue;font-weight:bold">手机验证码为：</label>
            <label class="layui-form-label verify-code" style="color:red;font-weight:bold;text-align:left"></label>
        </div>
        <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('btn_get_mobile')}}">获取手机验证码</button>
        </div>

    </div>
</div>
<script src="/vendor/layui/layui.js" charset="utf-8"></script>

<script>
layui.use(['form', 'element', 'layer', 'util', 'jquery'], function(){
    var form = layui.form();
    var util = layui.util;
    var layer = layui.layer;
    var element = layui.element();
    var $ = layui.jquery;

    form.render();

    form.on('submit({{makeElUniqueName('btn_get_mobile')}})', function(data){
        var index = layer.load(1);
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: '/extra/sandbox/getverifycode',
            data: JSON.stringify({
                behavior: data.field['{{makeElUniqueName('mobile_behavior')}}'],
                mobile: data.field['{{makeElUniqueName('mobile')}}'],
            }),
            success: function (outResult) {
                layer.close(index);
                $('.verify-code').text(outResult.verifyCode);
            },
            error: function (error) {
                layer.close(index);
            }
        });
    });
});
</script>
@endsection
