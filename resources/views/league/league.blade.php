<div class="layui-form layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">申请人</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="申请人姓名" name="{{makeElUniqueName('customer_name')}}" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label" >联系电话</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="申请人联系电话" name="{{makeElUniqueName('customer_tel')}}" autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <label class="layui-form-label" style="width:130px">申请时间</label>
        <div class="layui-input-inline layui-short-input">
            <input class="layui-input" placeholder="开始日" id="{{makeElUniqueName('start_create')}}"
                   name="{{makeElUniqueName('start_create_dt')}}">
        </div>
        <label class="layui-form-label">----到----</label>
        <div class="layui-input-inline layui-short-input">
            <input class="layui-input" placeholder="结束日" id="{{makeElUniqueName('end_create')}}"
                   name="{{makeElUniqueName('end_create_dt')}}">
        </div>
        <div class="layui-input-inline" style="width:auto">
        </div>
    </div>
    <br/>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px"></label>
            <div class="layui-input-inline layui-long-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_goods')}}"><i
                            class="layui-icon">
                        &#xe615;</i> 搜索
                </button>
            </div>
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbGoods')}}"></div>
<script>
    layui.use(['jfTable', 'form','validator','laydate'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var form = layui.form();
        var laydate = layui.laydate;

        var start = {
            elem: $('#{{makeElUniqueName('start_create')}}')[0],
            format: 'YYYY-MM-DD hh:mm:ss',
            min: '1900-01-01 00:00:00',
            max: '2099-06-16 23:59:59', //最大日期
            istime: true,
            istoday: false,
            choose: function (datas) {
                end.min = datas; //开始日选好后，重置结束日的最小日期
                end.start = datas //将结束日的初始值设定为开始日
            }
        };
        var end = {
            elem: $('#{{makeElUniqueName('end_create')}}')[0],
            format: 'YYYY-MM-DD hh:mm:ss',
            min: '1900-01-01 00:00:00',
            max: '2099-06-16 23:59:59',
            istime: true,
            istoday: false,
            choose: function (datas) {
                start.max = datas; //结束日选好后，重置开始日的最大日期
            }
        };

        $('input[name={{makeElUniqueName('start_create_dt')}}]').on('click', function () {
            laydate(start);
        });
        $('input[name={{makeElUniqueName('end_create_dt')}}]').on('click', function () {
            laydate(end);
        });
        form.render();
        layui.define(function (exports) {
            var obj = {
                doEdit: function (id) {
                    $.get('/news/category/editIndex/'+id, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('createNewsCate')}}',
                                title: '编辑分类',
                                type: 1,
                                content: str,
                                area: ['800px', '600px']
                            }),
                            onClose: function () {
                                layui.leagueFuncs.refreshTableGrid();
                            }
                        });
                    });
                },
                refreshTableGrid: function () {
                    $('input[name=\'{{makeElUniqueName('name')}}\']').val('');
                    $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
                }
            };
            exports('leagueFuncs', obj);
        });

        $("#{{makeElUniqueName('tbGoods')}}").jfTable({
            url: '/league/query',
            pageSize: 5,
            page: true,
            skip: true,
            first: '首页',
            last: '尾页',
            columns: [{
                text: '操作',
                name: 'id',
                width: 200,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if (dataItem.process_status == {{\App\League::$PROCESS_STATUS_UNFINISH}}) {
                        var html = '<a class="layui-btn layui-btn-small layui-btn-warning" onclick="layui.leagueFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe618;</i> 标记为已处理</a>';
                        return html;
                    } else {
                        return '<span style="color: green;">已处理</span>';
                    }
                }
            }, {
                text: '申请人姓名',
                name: 'customer_name',
                width: 170,
                align: 'center'
            }, {
                text: '申请人电话',
                name: 'customer_tel',
                width: 170,
                align: 'center'
            }, {
                text: '申请时间',
                name: 'request_time',
                width: 170,
                align: 'center'
            }, {
                text: '申请人文档',
                name: 'file_id',
                width: 150,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if(dataItem.file_id == ''){
                        return '<span style="color: gray;">无文档</span>';
                    }else{
                        return '<a href="'+value+'">文档下载</a>'
                    }
                }
            },
            ],
            method: 'post',
            queryParam: {
                customer_name: $('input[name=\'{{makeElUniqueName('customer_name')}}\']').val(),
                customer_tel: $('input[name=\'{{makeElUniqueName('customer_tel')}}\']').val(),
                start_create_dt: $('input[name=\'{{makeElUniqueName('start_create_dt')}}\']').val(),
                end_create_dt: $('input[name=\'{{makeElUniqueName('end_create_dt')}}\']').val(),
            },
            toolbarClass: 'layui-btn-small',
            onBeforeLoad: function (param) {
                return $.extend(param, {
                    customer_name: $('input[name=\'{{makeElUniqueName('customer_name')}}\']').val(),
                    customer_tel: $('input[name=\'{{makeElUniqueName('customer_tel')}}\']').val(),
                    start_create_dt: $('input[name=\'{{makeElUniqueName('start_create_dt')}}\']').val(),
                    end_create_dt: $('input[name=\'{{makeElUniqueName('end_create_dt')}}\']').val(),
                });
            },
            onLoadSuccess: function (data) {
                return data;
            },
            dataFilter: function (data) {
                return data;
            }
        });


        $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_goods')}}\']').on('click', function () {
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('addCate')}}\']').on('click', function () {
            $.get('/news/category/addIndex', {}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createNewsCate')}}',
                        title: '新增分类',
                        type: 1,
                        content: str,
                        area: ['800px', '600px']
                    }),
                    onClose: function () {
                        layui.leagueFuncs.refreshTableGrid();
                    }
                });
            });
        });
    });
</script>
