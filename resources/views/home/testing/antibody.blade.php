@extends('homeLayouts.main')
@section('content')
    <style>
        .product-box {
            width: 100%;
            height: 1450px;
            border-top: 2px solid #000000;
            background-color: #F9FAFC;
        }

        .show-img {
            float: left;
            width: 1202px;
            height: 252px;
            margin-left: 360px;
            margin-top: 60px;
        }

        .show-flow {
            float: left;
            width: 1202px;
            height: 1667px;
            margin-left: 360px;
            margin-top: 123px;
            border: 1px solid #D9D9D9;
            margin-bottom: 12px;
        }

        .img-10 {
            float: left;
            margin-top: 60px;
            margin-left: 260px;
        }

        .show-text {
            float: left;
            width: 1020px;
            height: 1130px;
            margin-left: 130px;
            margin-top: 10px;
        }

        .show-text ul li {
            position: relative;
            left: 10px;
            top: 20px;
            font-size: 12px;
            line-height: 40px;
            color: #B3B3B3;
        }

        .disc li {
            list-style-type: disc;
            margin-left: 15px;
        }

        .show-line {
            float: left;
            width: 1100px;
            height: 1px;
            margin-left: 60px;
            background-color: #0C0C0C;
        }

        .show-text-2 {
            float: left;
            width: 1020px;
            height: 200px;
            margin-left: 80px;
            margin-top: 20px;
            font-size: 15px;
            line-height: 40px;
            color: #666666;
        }

        .h2-title {
            float: left;
            width: 80px;
            height: 36px;
            font-size: 16px;
            color: #666666;
            margin-top: 20px;
        }

        .disc-2 li {
            position: relative;
            top: 80px;
            list-style-type: disc;
            padding-left: 15px;
            margin-left: 80px;
        }
    </style>
    <div class="product-box">
        <div class="show-img">
            <img src="/images/home/img9.png"/>
        </div>

        <div class="show-flow">
            <img class="img-10" src="/images/home/img10.png"/>

            <div class="show-text">
                <ul>
                    <li>[服 务 项 目] <span>抗体制备</span></li>
                    <li>
                    <li>[抗体制备简介]</li>
                    <ul class="disc">
                        <li>为了研究抗体的理化性质、分子结构与功能，以及应用抗体于临床疾病的诊断、治疗及预防都需要人工制备抗体。目前，根据制备的原理和方法可分为多克隆抗体、单克隆抗体及基因工程抗体三类。</li>
                    </ul>
                    </li>
                    <li>[多克隆抗体]</li>
                    <li>大多数抗原是由大分子 蛋白质组 成，但只是抗原上有限部位的特殊分子结构能与其相应抗体结合，称此部位为抗原决定簇（antigenic determinant）或表位(epitope)。
                            一种天然抗原性物质（如细菌或其分泌的外毒素以及各种组织成分等）往往具有多种不同的抗原决定簇，而每一决定簇都可刺激机体一种抗体形成细胞产生一种特异性抗体。
                            在机体淋巴组织内可存在千百种抗体形成细胞（即B细胞），每种抗体形成细胞只识别其相应的抗原决定簇，当受抗原刺激后可增殖分化为一种细胞群，这种由单一细胞增殖形成的细胞群体可称之为细胞克隆（clone）。同一克隆的细胞可合成和分泌在理化性质、分子结构、遗传标记以及生物学特性等方面都是完全相同的均一性抗体，亦可称之为单克隆抗体。
                            在早期传统的抗体制备方法是将一种天然抗原经各种途径免疫动物，由于抗原性物质具有多种抗原决定簇，故可刺激产生多种抗体形成细胞克隆，合成和分泌抗各种决定簇抗体分泌到血清或体液中，故在其血清中实际上是含多种抗体的混合物，称这种用体内免疫法所获得的免疫血清为多克隆抗体，也是第一代抗体。由于这种抗体是不均一的，无论是对抗体分子结构与功能的研究或是临床应用都受到很大限制，因此如何能获得均一性抗体成为关注的问题。
                    </li>
                    <li>[单克隆抗体]</li>
                    <li>体内免疫法很难获得单克隆抗体（monoclonal antibody,McAb）。如能将所需要的抗体形成细胞选出并能在体外进行培养即可获得已知特异的单克隆抗体。1975年德国学者Kohler和英国学者Milstein将小鼠骨髓瘤细胞和经绵羊红细胞（sheep rue blood cell），SRBC）免疫的小鼠脾细胞在体外进行两种细胞融合，结果发现部分形成的杂交细胞既能继续在体外培养条件下生长繁殖又能分泌抗SRBC抗体，称这种杂交细胞系为杂交瘤（hybridoma）。这种杂交瘤细胞既具有骨髓瘤细胞能大量无限生长繁殖的特性，又具有抗体形成细胞合成和分泌抗体的能力。它们是由识别一种抗原决定簇的细胞克隆所产生的均一性抗体，故称之为单克隆抗体。应用杂交瘤技术可获得几乎所有抗原的单克隆抗体，只要这种抗原能引起小鼠的抗体应答。
                            这种用杂交瘤技术制备的单克隆抗体可视为第二代抗体。
                            单克隆抗体由于纯度高、特异性强、可以提高各种血清学方法检测抗原的敏感性及特异性，但单克隆抗体多为双价抗体，与抗原结合不易交联为大分子集团，故不易出现沉淀反应。单克隆抗体的应用大促进了对各种传染病和恶性肿瘤诊断的准确性。
                            单克隆抗体亦可与核素、各种毒素（如白喉外毒素或篦麻毒素）或药物通过化学偶联或基因重组制备成导向药物（targetting drug）用于肿瘤的治疗，是一种新型免疫治疗方法，有可能提高对肿瘤的疗效。
                            单克隆抗体亦可用于对各种免疫细胞及其它组织细胞表面分子的检测，这对免疫细胞的分离、鉴定及分类及研究各种膜表面分子的结构与功能都具有重要意义。
                    </li>
                    <li>[基因工程抗体]</li>
                    <li>自1975年单克隆抗体杂交瘤技术问世以来，单克隆体在医学中被广泛地应用于痢疾的诊断及治疗。但目前绝大数单克隆抗体是鼠源的，临床重复给药时体内产生抗鼠抗体，使临床疗效减弱或消失。因此，临床应用理想的单克隆抗体应是人源的，但人-人杂交瘤技术目前尚未突破，即使研制成功，也还存在人-人杂交瘤体外传代不稳定，抗体亲合力低及产量不高等问题。目前较好的解决办未能是研制基因工程抗体，(genetically engineering antibody)以代替鼠源单克隆抗体用于临床。
                            基因工程抗体兴起于80年代早期。这一技术是将对Ig基因结构与功能的了解与DNA重组技术相结合，根据研究者的意图在基因水平对Ig分子进行切割、拼接或修饰，甚至是人工全合后导入受体细胞表达，产生新型抗体，也称为第三代抗体。
                            基因工程抗体包括嵌合抗体、重构抗体、单链抗体、单区抗体及抗体库等。其中以嵌合抗体研究的较多，也较成熟。单链抗体及单区抗体虽具有结构简单、分子小等优点但其临床应用的前景尚待证实。
                    </li>
                </ul>
            </div>
            <div class="show-line"></div>
            <div class="show-text-2">
                <span class="h2-title"><strong>相关文章</strong></span>
                <ul class="disc-2">
                    <li>坑体分类型检测产品大全</li>
                    <li>eBioscience多因子细胞和组织样本裂解处理方法</li>
                    <li>eBioscience抗体分型检测产品</li>
                    <li>eBioscience抗体分型检测产品</li>
                    <li>eBioscience抗体分型检测产品</li>
                </ul>
            </div>
        </div>
    </div>
@stop