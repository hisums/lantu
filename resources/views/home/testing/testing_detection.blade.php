@extends('homeLayouts.main')
@section('content')

    <style>
        .product-box {
            width: 100%;
            border-top: 2px solid #000000;
            background-color: #F9FAFC;
        }

        .show-flow {
            width: 1200px;
            margin-top: 123px;
            border: 1px solid #D9D9D9;
            min-height: 360px;
            box-shadow: 0 0 30px #ccc;
            margin-bottom: 30px;
        }

        .img-10 {
        }

        .show-text {
            width: 1020px;
            margin-top: 50px;
            padding-bottom: 40px;
        }

        .show-text ul li {
            position: relative;
            left: 10px;
            top: 20px;
            font-size: 12px;
            line-height: 40px;
            color: #B3B3B3;
        }

        .disc li {
            list-style-type: disc;
            margin-left: 15px;
        }

        .show-title {
            margin-top: 10px;
        }

        .show-img-2 {
            width: 1200px;
            margin-bottom: 50px;
            margin-top: 50px;
        }

        .test-img {
            min-height: 300px;
            width: 1200px;
        }
        .show-flow h1 {
            font-size: 50px;
            font-weight: bold;
            color: #e5e5e5;
            transition: all cubic-bezier(0.25, 0.1, 0.19, 0.99) .6s;
            background: url('');
        }

        .show-flow h1:hover {
            color: #d1318e;
        }

        .show-flow .lin {
            margin: 0 auto;
            border-top: solid 1px #ccc;
            text-align: center;
            width: 380px;
            height: 36px;
            margin-top: 20px;
        }

        .show-flow .lin span {
            float: left;
            display: block;
            padding: 0 5px;
            background: #fff;
            margin-top: -15px;
            margin-left: 120px;
            border: solid 0px red;
            font-size: 20px;
        }
    </style>
    <link href="/vendor/swiper/dist/css/swiper.css" rel="stylesheet" type="text/css"/>
    <div class="product-box flex flex-direction-col flex-align-items-center">
        <img style="width:1200px;height: 375px;margin-top:50px;" class="img-10"
             src="{{!empty($detection)?$detection->getFullPicturePath():''}}"/>

        <div class="show-flow flex flex-direction-col flex-align-items-center">
            <div class="show-text">
                {!! !empty($detection)?$detection->content:'' !!}
            </div>
        </div>
        <div class="show-flow" style="border:none;box-shadow:none;">
            <h1 style="text-align: center;">EXPERIMENTAL DISPLAY</h1>
            <div class="lin">
                <span>实验展示</span>
            </div>
            {{--<div class="show-title">
                <h1 style="text-align: center">实&nbsp; &nbsp;验&nbsp; &nbsp;展&nbsp; &nbsp;示</h1>
            </div>--}}
            <div class="show-img-2 flex flex-jfcontent-center">
                <div class="swiper-container test-img flex flex-jfcontent-center">
                    <div class="swiper-wrapper">
                        @foreach($test_img as $v)
                            <div style="height: auto;" class="swiper-slide flex flex-jfcontent-center flex-align-items-center">
                                <img style="width: 70%;height: 70%;" src="{{$v->getFullPicturePath()}}">
                            </div>
                        @endforeach
                    </div>
                    <!-- Add Pagination -->
                    <div style="right: 20px" class="swiper-button-next"></div>
                    <div style="left:20px;" class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </div>
    <script src="/vendor/swiper/dist/js/swiper.js" charset="utf-8"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            slidesPerView: 3,
            paginationClickable: true,
            spaceBetween: 30,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
        });
    </script>
@stop