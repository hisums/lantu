@extends('homeLayouts.main')
@section('content')
    <style>
        .product-box {
            width: 100%;
            border-top: 2px solid #000000;
            background-color: #F9FAFC;
        }

        .show-img {
            width: 1202px;
            margin-top: 60px;
        }

        .show-flow {
            width: 1202px;
            margin-top: 40px;
            border: 1px solid #D9D9D9;
            margin-bottom: 70px;
            box-shadow: 0 0 30px #ccc;
        }

        .show-text {
            width: 90%;
            margin-top: 50px;
            margin-bottom: 50px;
            min-height: 200px;
        }

        .disc li {
            list-style-type: disc;
            margin-left: 15px;
        }

        .show-line {
            width: 1100px;
            height: 1px;
            background-color: #0C0C0C;
        }

        .show-text-2 {
            width: 1020px;
            margin-left: 80px;
            margin-top: 20px;
            font-size: 15px;
            line-height: 40px;
            color: #666666;
            min-height: 300px;
        }

        .h2-title {
            width: 80px;
            font-size: 16px;
            color: #666666;
            margin-top: 20px;
        }

        .disc-2 li {
            list-style-type: disc;
            padding-left: 15px;
            margin-left: 50px;
        }

        a {
            color: #000;
        }

        .img-10 {
            width: 1200px;
            height: 375px;
        }
        .piece-navigate-box{
            width:1200px;
            margin-top:20px;
        }
    </style>
    <div class="product-box flex-direction-col flex-align-items-center">
        {{--<div class="show-img">
            <img class="img-10"
                 src="{{!empty($cate_article)?$cate_article->getFullPicturePath():'/images/no-pic-back.png'}}"/>
        </div>--}}
        <div class="piece-navigate-box">
            <a style="color: #000;" href="{{url('web/index')}}">首页</a>>><a style="color: #000;"
                                                                           href="{{url('web/article/testing_center')}}">检测中心</a>>><a href="{{url('web/article/testing_sub',[$cate_id,$cate_sub_id])}}">{{$title}}</a>
        </div>
        <div class="show-flow flex flex-align-items-center flex-direction-col">
            <div class="show-text">
                {!! !empty($cate_article)?$cate_article->content:'' !!}
            </div>
        </div>
        {{--<div class="show-flow">--}}
            {{--<div class="show-text-2">--}}
                {{--<span class="h2-title"><strong>相关文章</strong></span>--}}
                {{--<ul class="disc-2">--}}
                    {{--@foreach($testing_sub_list as $v)--}}
                        {{--<li><a style="color: #000;"--}}
                               {{--href="{{url('web/article/articleDetail',[$v->id])}}">{{$v->subject}}</a></li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
@stop