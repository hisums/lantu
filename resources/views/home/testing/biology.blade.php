@extends('homeLayouts.main')
@section('content')
    <style>
        .product-box {
            width: 100%;
            height: 1450px;
            border-top: 2px solid #000000;
            background-color: #F9FAFC;
        }

        .show-img {
            float: left;
            width: 1202px;
            height: 252px;
            margin-left: 360px;
            margin-top: 60px;
        }

        .show-flow {
            float: left;
            width: 1202px;
            height: 1200px;
            margin-left: 360px;
            margin-top: 123px;
            border: 1px solid #D9D9D9;
            margin-bottom: 10px;
        }

        .img-10 {
            float: left;
            margin-top: 60px;
            margin-left: 260px;
        }

        .show-text {
            float: left;
            width: 1020px;
            height: 650px;
            margin-left: 130px;
            margin-top: 10px;
        }

        .show-text ul li {
            position: relative;
            left: 10px;
            top: 20px;
            font-size: 12px;
            line-height: 40px;
            color: #B3B3B3;
        }

        .disc li {
            list-style-type: disc;
            margin-left: 15px;
        }

        .show-line {
            float: left;
            width: 1100px;
            height: 1px;
            margin-left: 60px;
            background-color: #0C0C0C;
        }

        .show-text-2 {
            float: left;
            width: 1020px;
            height: 200px;
            margin-left: 80px;
            margin-top: 20px;
            font-size: 15px;
            line-height: 40px;
            color: #666666;
        }

        .h2-title {
            float: left;
            width: 80px;
            height: 36px;
            font-size: 16px;
            color: #666666;
            margin-top: 20px;
        }

        .disc-2 li {
            position: relative;
            top: 80px;
            list-style-type: disc;
            padding-left: 15px;
            margin-left: 80px;
        }
    </style>
    <div class="product-box">
        <div class="show-img">
            <img src="/images/home/img9.png"/>
        </div>

        <div class="show-flow">
            <img class="img-10" src="/images/home/img10.png"/>

            <div class="show-text">
                <ul>
                    <li>[服 务 项 目] <span>生化检测</span></li>
                    <li>
                    <li>[血液生化六项注意事项]</li>
                    <ul class="disc">
                        <li>不合宜人群：有明显出血倾向的人群或者吃了药，吃了影响检测的食物等。</li>
                    </ul>
                    </li>
                    <li>[检查前禁忌]</li>
                    <li>1.血脂分析、血生化全套、空腹血糖、肝功能测定等要求早晨空腹抽血。</li>
                    <li>2.血脂测定最好抽血前三天素食。</li>
                    <li>3.大便隐血试验前三天禁食动物血、猪肝、铁剂、中药及富含叶绿素食物。</li>
                    <li>4.细菌培养标本，一定要用灭菌容器，取洁尿送检。</li>
                    <li>5.痰液标本细菌培养，最好早晨先漱口后，咳出第一口痰(来自肺部)送检。</li>
                    <li>6.内分泌激素类测定，抽血时间要按医嘱进行。</li>
                    <li>[检查时要求]</li>
                    <li>抽血时应放松心情，避免因恐惧造成血管的收缩、增加采血的困难。</li>
                    <li>[血液生化六项检查作用]</li>
                    <li>血尿素与肌酐就是尿毒素。其实严格说来，这两项仅是毒素的一部分，因为，两者同时由肾小球滤过排出，而且监测容易，因此，很早就被用来评价肾功能的主要指标。数值越高，代表肾功能越差。</li>
                </ul>
            </div>
            <div class="show-line"></div>
            <div class="show-text-2">
                <span class="h2-title"><strong>相关文章</strong></span>
                <ul class="disc-2">
                    @foreach($biology_list as $v)
                        <li><a href="{{url('web/article/articleDetail',[$v->id])}}">{{$v->subject}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@stop