@extends('homeLayouts.main')
@section('content')
    <link href="/vendor/swiper/dist/css/swiper.css" rel="stylesheet" type="text/css"/>
    <style>
        .testing-banner {
            width: 100%;
            text-align: center;
            margin-top: 30px;
        }

        .testing-container {
            width: 1200px;
            margin: 0 auto;
            margin-top: 30px;
            box-shadow: 0 0 30px #ccc;
        }

        .testing-container .list-content {
            width: 100%;
            min-height: 301px;
            margin-bottom: 50px;
            margin-top: 50px;
        }

        .list-left ul, .list-right ul {
            margin-top: 20px;
        }

        .list-left ul li, .list-right ul li {
            list-style: none;
            line-height: 50px;
            font-size: 18px;
            text-align: center;
        }

        .testing-swiper {
            width: 80%;
            text-align: center;
            margin: 0 auto;
            margin-top: 30px;
        }

        .swiper-container {
            margin-top: 30px;
            margin-bottom: 30px;
        }

        .testing-swiper h1 {
            font-size: 50px;
            font-weight: bold;
            color: #e5e5e5;
            transition: all cubic-bezier(0.25, 0.1, 0.19, 0.99) .6s;
            background: url('');
        }

        .testing-swiper h1:hover {
            color: #d1318e;
        }

        .testing-swiper .lin {
            margin: 0 auto;
            border-top: solid 1px #ccc;
            text-align: center;
            width: 380px;
            height: 36px;
            margin-top: 20px;
        }

        .testing-swiper .lin span {
            float: left;
            display: block;
            padding: 0 5px;
            background: #fff;
            margin-top: -15px;
            margin-left: 120px;
            border: solid 0px red;
            font-size: 20px;
        }

        .list-content-sub {
            width: 90%;
        }

        .piece-navigate {
            width: 100%;
            margin-top:20px;
        }

        .piece-navigate-box {
            width: 1200px;
        }
    </style>
    <div class="piece-navigate flex-jfcontent-center">
        <div class="piece-navigate-box">
            <a style="color: #000;" href="{{url('web/index')}}">首页</a>>><a style="color: #000;"
                                                                           href="{{url('web/article/testing_center')}}">检测中心</a>
        </div>
    </div>
    <div class="testing-banner">
        <img style="width: 1200px;height: 375px" src="{{!empty($center)?$center->getFullPicturePath():''}}" alt="">
    </div>
    <div class="testing-container flex flex-direction-col">
        <div class="list-content flex flex-jfcontent-center">
            <div class="list-content-sub">
                {!! !empty($center)?$center->content:'' !!}
            </div>
        </div>
    </div>
    <div class="testing-swiper">
        <h1>SERVICE ITEM</h1>
        <div class="lin">
            <span>检测服务项目</span>
        </div>
        <div class="testing-subject swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide flex flex-align-items-center flex-direction-col">
                    <a href="{{url('web/article/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_SHJC,\App\NewsCategory::$DEFAULT_CATE_SHJCLC])}}">
                        @foreach($cate_article_list as $v)
                            @if($v->id == \App\NewsCategory::$DEFAULT_CATE_SHJC)
                                <img style="width: 280px;height: 280px" src="{{$v->getFullPicturePath()}}">
                            @endif
                        @endforeach
                        <div style="margin-top: 15px;font-size: 18px;text-align: center;color: #000;">生化检测</div>
                    </a>
                </div>
                <div class="swiper-slide flex flex-align-items-center flex-direction-col">
                    <a href="{{url('web/article/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_MYJC,\App\NewsCategory::$DEFAULT_CATEMYJCLC])}}">
                        @foreach($cate_article_list as $v)
                            @if($v->id == \App\NewsCategory::$DEFAULT_CATE_MYJC)
                                <img style="width: 280px;height: 280px" src="{{$v->getFullPicturePath()}}">
                            @endif
                        @endforeach
                        <div style="margin-top: 15px;font-size: 18px;text-align: center;color: #000;">免疫检测</div>
                    </a>
                </div>
                <div class="swiper-slide flex flex-align-items-center flex-direction-col">
                    <a href="{{url('web/article/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_KTZB,\App\NewsCategory::$DEFAULT_CATEKYZBLC])}}">
                        @foreach($cate_article_list as $v)
                            @if($v->id == \App\NewsCategory::$DEFAULT_CATE_KTZB)
                                <img style="width: 280px;height: 280px" src="{{$v->getFullPicturePath()}}">
                            @endif
                        @endforeach
                        <div style="margin-top: 15px;font-size: 18px;text-align: center;color: #000;">抗体制备</div>
                    </a>
                </div>
                <div class="swiper-slide flex flex-align-items-center flex-direction-col">
                    <a href="{{url('web/article/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_SJHDZ,\App\NewsCategory::$DEFAULT_CATESJHDZLC])}}">
                        @foreach($cate_article_list as $v)
                            @if($v->id == \App\NewsCategory::$DEFAULT_CATE_SJHDZ)
                                <img style="width: 280px;height: 280px" src="{{$v->getFullPicturePath()}}">
                            @endif
                        @endforeach
                        <div style="margin-top: 15px;font-size: 18px;text-align: center;color: #000;">试剂盒定制</div>
                    </a>
                </div>
            </div>
            <div style="right: 0" class="swiper-button-next"></div>
            <div style="left:0;" class="swiper-button-prev"></div>
        </div>
    </div>
    <script src="/vendor/swiper/dist/js/swiper.js" charset="utf-8"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            slidesPerView: 3,
            paginationClickable: true,
            spaceBetween: 30,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
        });
    </script>
@stop