@extends('homeLayouts.main')
@section('content')
    <style>
        .product-box {
            width: 100%;
            height: 1450px;
            border-top: 2px solid #000000;
            background-color: #F9FAFC;
        }

        .show-img {
            float: left;
            width: 1202px;
            height: 252px;
            margin-left: 360px;
            margin-top: 60px;
        }

        .show-flow {
            float: left;
            width: 1202px;
            height: 1400px;
            margin-left: 360px;
            margin-top: 123px;
            border: 1px solid #D9D9D9;
            margin-bottom: 10px;
        }

        .img-10 {
            float: left;
            margin-top: 60px;
            margin-left: 260px;
        }

        .show-text {
            float: left;
            width: 1020px;
            height: 882px;
            margin-left: 130px;
            margin-top: 10px;
        }

        .show-text ul li {
            position: relative;
            left: 10px;
            top: 20px;
            font-size: 12px;
            line-height: 40px;
            color: #B3B3B3;
        }

        .disc li {
            list-style-type: disc;
            margin-left: 15px;
        }

        .show-line {
            float: left;
            width: 1100px;
            height: 1px;
            margin-left: 60px;
            background-color: #0C0C0C;
        }

        .show-text-2 {
            float: left;
            width: 1020px;
            height: 200px;
            margin-left: 80px;
            margin-top: 20px;
            font-size: 15px;
            line-height: 40px;
            color: #666666;
        }

        .h2-title {
            float: left;
            width: 80px;
            height: 36px;
            font-size: 16px;
            color: #666666;
            margin-top: 20px;
        }

        .disc-2 li {
            position: relative;
            top: 80px;
            list-style-type: disc;
            padding-left: 15px;
            margin-left: 80px;
        }
    </style>
    <div class="product-box">
        <div class="show-img">
            <img src="/images/home/img9.png"/>
        </div>

        <div class="show-flow">
            <img class="img-10" src="/images/home/img10.png"/>

            <div class="show-text">
                <ul>
                    <li>[服 务 项 目] <span>免疫检测</span></li>
                    <li>
                    <li>[检 测 简 介]</li>
                    <ul class="disc">
                        <li>
                            ELISA试剂盒采用双抗体夹心酶联免疫吸附检测技术。特异性抗单克隆抗体预包被在高亲和力的酶标板上。酶标板孔中加入标准品、检测样本和生物素化的单克隆抗体，经过孵育，样本中存在目的蛋白与固相抗体和检测抗体结合。洗涤去除未结合的物质后，加入辣根过氧化物酶标记的链霉亲和素（S-HRP）。洗涤后，加入显色底物TMB，避光显色。终止液终止反应，在450nm波长（参考波长570-630nm）测定吸光度值。颜色反应的深浅与样本中存在目的蛋白浓度成正比。
                        </li>
                    </ul>
                    </li>
                    <li>[ELISA检测样本类型]</li>
                    <li>1.细胞培养上清（胞外）</li>
                    <li>2.体液（血清/血浆、胸腹水、脑脊液、肺泡灌洗液等各种积液)</li>
                    <li>3.分泌物（泪液、唾液、乳汁等）</li>
                    <li>4.排泄物（尿液、粪便）</li>
                    <li>5.细胞裂解物和组织裂解物</li>
                    <li>注：均已实验验证，可进行实验。</li>
                    <li>[ELISA检测报告包含]</li>
                    <li>1.原始数据</li>
                    <li>2.结果分析：
                        排版布局说明；
                        标准曲线分析：CV分析、Recovery分析、R2分析；
                        样本浓度计算。
                    </li>
                    <li>3.样本、试剂剩余情况说明</li>
                    <li>4.PDF报告：实验环境条件、所用仪器设备型号、标准曲线分析、样本浓度计算，可直接用于论文撰写。</li>
                    <li>[服务流程]</li>
                    <li>1.填写需求表，填写所需试剂盒样本相关信息、实验要求等</li>
                    <li>2.确定无误后，签订代测协议</li>
                    <li>3.如果购买本公司产品，可提供免费代测，科研用户只需提交样本。</li>
                </ul>
            </div>
            <div class="show-line"></div>
            <div class="show-text-2">
                <span class="h2-title"><strong>相关文章</strong></span>
                <ul class="disc-2">
                    <li>坑体分类型检测产品大全</li>
                    <li>eBioscience多因子细胞和组织样本裂解处理方法</li>
                    <li>eBioscience抗体分型检测产品</li>
                </ul>
            </div>
        </div>
    </div>
@stop