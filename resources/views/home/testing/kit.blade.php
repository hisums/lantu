@extends('homeLayouts.main')
@section('content')
    <style>
        .product-box {
            width: 100%;
            height: 1450px;
            border-top: 2px solid #000000;
            background-color: #F9FAFC;
        }
        .show-img {
            float: left;
            width: 1202px;
            height: 252px;
            margin-left: 360px;
            margin-top: 60px;
        }
        .show-flow {
            float: left;
            width: 1202px;
            height: 940px;
            margin-left: 360px;
            margin-top: 123px;
            border: 1px solid #D9D9D9;
        }
        .img-10 {
            float: left;
            margin-top: 60px;
            margin-left: 260px;
        }
        .show-text {
            float: left;
            width: 1020px;
            height: 380px;
            margin-left: 130px;
            margin-top: 10px;
        }
        .show-text ul li {
            position: relative;
            left: 10px;
            top: 20px;
            font-size: 12px;
            line-height: 40px;
            color: #B3B3B3;
        }
        .disc li{
            list-style-type: disc;
            margin-left: 15px;
        }
        .show-line {
            float: left;
            width: 1100px;
            height: 1px;
            margin-left: 60px;
            background-color: #0C0C0C;
        }
        .show-text-2 {
            float: left;
            width: 1020px;
            height: 200px;
            margin-left: 80px;
            margin-top: 20px;
            font-size: 15px;
            line-height: 40px;
            color: #666666;
        }
        .h2-title{
            float: left;
            width: 80px;
            height: 36px;
            font-size: 16px;
            color:  #666666;
            margin-top: 20px;
        }
        .disc-2 li {
            position: relative;
            top: 80px;
            list-style-type: disc;
            padding-left: 15px;
            margin-left:80px;
        }
    </style>
    <div class="product-box">
        <div class="show-img">
            <img src="/images/home/img9.png" />
        </div>

        <div class="show-flow">
            <img class="img-10" src="/images/home/img10.png"  />

            <div class="show-text">
                <ul>
                    <li>[服 务 项 目] <span>试剂盒定制</span></li>
                    <li>
                    <li>[服 务 优 势]</li>
                    <ul class="disc">
                        <li>本公司可根据客户的具体需求开发试剂盒。</li>
                    </ul>
                    </li>
                    <li>[订制流程]</li>
                    <li>1.填写需求表，填写所需试剂盒样本相关信息、试剂盒要求等</li>
                    <li>2.确定无误后，签订订制协议，确保试剂盒的品质和服务质量</li>
                    <li>3.可提供免费代测，科研用户只需提交样本。</li>
                    <li>具体情况可来电咨询</li>
                </ul>
            </div>
            <div class="show-line"></div>
            <div class="show-text-2">
                <span class="h2-title"><strong>相关文章</strong></span>
                <ul class="disc-2">
                    <li>坑体分类型检测产品大全</li>
                    <li>eBioscience多因子细胞和组织样本裂解处理方法</li>
                    <li>eBioscience抗体分型检测产品</li>
                </ul>
            </div>
        </div>
    </div>
@stop