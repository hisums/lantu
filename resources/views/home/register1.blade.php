@extends('homeLayouts.main1')
@section('content')
    <style>
        .main-body {
            width: 1200px;
            position: relative;
            left: 50%;
            margin-left: -600px;
            display: flex;
            margin-bottom: 20px;
        }

        .register-form {
            position: absolute;
            width: 540px;
            height: 460px;
            left: 5%;
            top: 150px;
        }

        .register-title-content {
            margin-top: 30px;
            font-size: 20px;
            width: 80%;
            color: #2459A5;
            font-weight: bold;
        }

        .input-content {
            height: 50px;
            width: 80%;
            margin-top: 20px;
        }

        .input-content-sub {
            height: 86%;
        }

        .input-content-sub-explain {
            width: 70%;
        }

        .input-common {
            height: 28px;
            width: 90%;
        }

        .input-verify {
            height: 28px;
            width: 24%;
            margin-bottom: 20px;
        }

        .explain-detail {
            font-size: 12px;
            color: #999999;
            margin-top: 2px;
        }

        #verify {
            height: 32px;
            margin-left: 10px;
        }

        .is-correct {
            position: relative;
            left: 6px;
            top: 4px;
        }

        .is-correct-unsee {
            visibility: hidden;
        }

        .lantu-tk {
            font-size: 14px;
        }

        .login-btn {
            width: 50%;
            height: 32px;
            border: none;
            background-color: #4E90FE;
            color: #ffffff;
            font-size: 18px;
            margin-top: 45px;
        }

        .logining-status {
            background-color: #999999 !important;
        }
    </style>
    <form action="#" method="post">
        <div class="main-body">
            <img src="/images/home/register-back.png"/>
            <div class="register-form flex flex-direction-col flex-align-items-center">
                <div class="register-title-content flex">
                    注册新用户　　　10秒 , 加入蓝图
                </div>
                <div class="input-content flex flex-align-items-center">
                    <div class="input-content-sub"><span style="color: red;">*</span> 账&nbsp;&nbsp;户&nbsp;&nbsp;名：</div>
                    <div class="input-content-sub-explain">
                        <input class="input-common" type="text" name="username"/><img
                                class="is-correct is-correct-unsee"
                                src="/images/home/register-right.png">
                        <div class="explain-detail">请输入邮箱/用户名/手机号</div>
                    </div>
                </div>
                <div class="input-content flex flex-align-items-center">
                    <div class="input-content-sub"><span style="color: red;">*</span> 设置密码：</div>
                    <div class="input-content-sub-explain">
                        <input class="input-common" type="password" name="password"/><img style="left: 8px;"
                                                                                          class="is-correct is-correct-unsee"
                                                                                          src="/images/home/register-right.png">
                        <div class="explain-detail">6-20位，可以使用字母（区分大小写），数字和符号</div>
                    </div>
                </div>
                <div class="input-content flex flex-align-items-center">
                    <div class="input-content-sub"><span style="color: red;">*</span> 确认密码：</div>
                    <div class="input-content-sub-explain">
                        <input class="input-common" type="password" name="cpassword"/><img style="left: 8px;"
                                                                                           class="is-correct is-correct-unsee"
                                                                                           src="/images/home/register-right.png">
                        <div class="explain-detail">请再次确认输入密码</div>
                    </div>
                </div>
                <div class="input-content flex flex-align-items-center">
                    <div class="input-content-sub"><span style="color: red;">*</span> 验&nbsp;&nbsp;证&nbsp;&nbsp;码：</div>
                    <div class="input-content-sub-explain flex flex-jfcontent-start">
                        <input class="input-verify" type="text" name="verify"/>
                        <img id="verify" style="cursor: pointer"
                             onclick="this.src='{{ url('web/captcha') }}?r='+Math.random();"
                             src="{{url('web/captcha')}}">
                        <div style="font-size: 12px;height: 100%;margin-top: 8px;margin-left: 6px;">
                            <div style="color: #999999;cursor: pointer" onclick="changeVerify()" href="#">看不清换一张</div>
                        </div>
                    </div>
                </div>
                <div class="flex">
                    <div class="flex lantu-tk flex-align-items-center">　
                        <label class="flex flex-align-items-center"><input type="checkbox">&nbsp;我已阅读同意</label>
                        <a target="_blank" href="#">《蓝图生物服务条款》</a></div>
                </div>

                <button disabled="disabled"
                        class="login-btn flex flex-align-items-center flex-jfcontent-center logining-status">注册
                </button>

            </div>
        </div>
    </form>
    <script src="/vendor/layer/layer.js" charset="utf-8"></script>
    <script>
        function changeVerify() {
            $('#verify').trigger('click');
        }

        $('input[name=username]').blur(function () {
            var reg = /^[\x7f-\xff\dA-Za-z\.\_\@]+$/;
            if (!$('input[name=username]').val() || $('input[name=username]').val().length >20 || !reg.test($('input[name=username]').val())) {
                $('input[name=username]').next().attr('src', '/images/home/register-false.png');
                $('input[name=username]').next().css('visibility', 'visible');
            } else {
                $('input[name=username]').next().css('visibility', 'visible');
                $('input[name=username]').next().attr('src', '/images/home/register-right.png');
            }
        })

        $('input[name=password]').blur(function () {
            var pwd = $('input[name=password]').val();
            if (!pwd || pwd.length < 6 || pwd.length > 20) {
                $('input[name=password]').next().attr('src', '/images/home/register-false.png');
                $('input[name=password]').next().css('visibility', 'visible');
            } else {
                $('input[name=password]').next().css('visibility', 'visible');
                $('input[name=password]').next().attr('src', '/images/home/register-right.png');
            }
        })

        $('input[name=cpassword]').blur(function () {
            var cpwd = $('input[name=cpassword]').val();
            var pwd = $('input[name=password]').val();
            if (!cpwd || cpwd.length < 6 || cpwd.length > 20 || cpwd != pwd) {
                $('input[name=cpassword]').next().attr('src', '/images/home/register-false.png');
                $('input[name=cpassword]').next().css('visibility', 'visible');
            } else {
                $('input[name=cpassword]').next().css('visibility', 'visible');
                $('input[name=cpassword]').next().attr('src', '/images/home/register-right.png');
            }
        });

        $('input[type=checkbox]').click(function () {
            if ($(this).prop('checked')) {
                $('.login-btn').removeClass('logining-status');
                $('.login-btn').attr('disabled', false);
            } else {
                $('.login-btn').addClass('logining-status');
                $('.login-btn').attr('disabled', true);
            }
        })

        $('form').submit(function () {
            if (!$('input[name=username]').val()) {
                layer.msg('请输入用户名', function () {
                });
                return false;
            }

            var reg = /^[\x7f-\xff\dA-Za-z\.\_\@]+$/;
            if (!reg.test($('input[name=username]').val())) {
                layer.msg('用户名中存在非法字符!', function () {
                });
                return false;
            }
            if (!$('input[name=username]').val().length>20) {
                layer.msg('用户名长度不得超过20个字符!', function () {
                });
                return false;
            }
            if (!$('input[name=password]').val()) {
                layer.msg('请输入密码', function () {
                });
                return false;
            } else {
                if ($('input[name=password]').val().length < 6 || $('input[name=password]').val().length > 22) {
                    layer.msg('密码只能在6-22位之间', function () {
                    });
                    return false;
                }
            }
            if (!$('input[name=cpassword]').val()) {
                layer.msg('请输入确认密码', function () {
                });
                return false;
            }
            if ($('input[name=cpassword]').val() && $('input[name=password]').val()) {
                if ($('input[name=cpassword]').val() != $('input[name=password]').val()) {
                    layer.msg('两次密码输入不相同', function () {
                    });
                    return false;
                }
            }
            if (!$('input[name=verify]').val()) {
                layer.msg('请输入验证码!', function () {
                });
                return false;
            }
            var data = {
                name: $('input[name=username]').val(),
                password: $('input[name=password]').val(),
                verify: $('input[name=verify]').val()
            };
            $('.login-btn').attr('disabled', 'disabled');
            $('.login-btn').addClass('logining-status');
            $.ajax({
                url: "{{url('web/doRegister')}}",
                dataType: 'json',
                type: 'post',
                data: data,
                success: function (res) {
                    if(res.Success){
                        layer.msg('注册成功!',{time:1000},function () {
                            location.href="{{url('web/login')}}";
                        });
                    }
                },
                error:function(res){
                    if(res.status == 422){
                        $('.login-btn').attr('disabled',false);
                        $('.login-btn').removeClass('logining-status');
                        var errors = res.responseJSON;
                        var msg = '';
                        for (var errorcode in errors) {
                            var errorItem = errors[errorcode];
                            for (var i = 0; i < errorItem.length; i++) {
                                msg = errorItem[i];
                                break;
                            }
                        }
                        layer.msg(msg,function () {
                            $('#verify').trigger("click");
                        });
                    }
                }
            });

            return false;
        })
    </script>
@stop