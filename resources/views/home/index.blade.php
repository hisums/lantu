@extends('homeLayouts.main')
@section('content')
    <link href="/vendor/swiper/dist/css/swiper.css" rel="stylesheet" type="text/css"/>
    <style>
        .discus-p {
            position: absolute;
            left: 50%;
            margin-left: 340px;
            z-index: 1;
        }

        .discus {
            background-color: #fff;
            height: 456px;
            width: 260px;
        }

        .discus-one {
            width: 100%;
            height: 50%;
        }

        .discus-title {
            width: 80%;
        }

        .discus-title-p {
            border-bottom: 1px solid #eeeeee;
            height: 40px;
        }

        .discus-img {
            width: 100%;
            height: 181px;
        }

        .discus-img img {
            width: 90%;
        }

        .discus-sub-obj {
            width: 33%;
            height: 50%;
            color: #000;
        }

        .border-right {
            border-right: 1px solid #eeeeee;
        }

        .border-bottom {
            border-bottom: 1px solid #eeeeee;
        }

        .discus-sub {
            height: 180px;
            border-bottom: 1px solid #eeeeee;
        }

        .discus-sub-obj-name {
            text-align: center;
            height: 30px;
            line-height: 30px;
            font-size: 12px;
        }

        .main-obj {
            width: 100%;
            height: 200px;
            background-color: #F8F8F8;
        }

        .main-obj-sub {
            width: 1200px;
            height: 80%;
            background-color: #ffffff;
            border: 1px solid #eeeeee;
        }

        .sub-detail {
            width: 25%;
            height: 100%;
        }

        .sub-detail-content {
            margin-left: 10%;
            height: 70%;
        }

        .sub-detail-content-txt {
            height: 50px;
        }

        .button-multiply {
            width: 100px;
            height: 30px;
            border-radius: 10px;
            text-align: center;
            line-height: 30px;
            background-color: #375EC5;
            color: #ffffff;
            font-size: 16px;
        }

        .sub-detail-img {
            width: 100px;
        }

        .banner {
            display: flex;
        }

        .swiper-container {
            z-index: 0;
        }
    </style>
    <div class="banner">
        <div class="discus-p flex flex-jfcontent-end">
            <div class="discus flex flex-direction-col flex-jfcontent-space-between">
                <div class="discus-one flex flex-direction-col">
                    <div class="discus-title-p flex flex-jfcontent-center flex-align-items-center">
                        <div class="discus-title">
                            实验论坛
                        </div>
                    </div>
                    <a href="http://bbs.lantubio.com/"
                       class="discus-img flex flex-jfcontent-center flex-align-items-center">
                        <img src="/images/home/discus-img.png"/>
                    </a>
                </div>
                <div class="discus-one">
                    <div class="discus-title-p flex flex-jfcontent-center flex-align-items-center">
                        <div class="discus-title">
                            检测中心
                        </div>
                    </div>
                    <div class="discus-sub flex flex-wrap">
                        <a href="{{url('web/article/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_SHJC,\App\NewsCategory::$DEFAULT_CATE_SHJCLC])}}"
                           class="discus-sub-obj border-right border-bottom flex flex-direction-col flex-jfcontent-end">
                            <div class="discus-sub-obj-img flex flex-jfcontent-center flex-align-items-center">
                                <img src="/images/home/ELISA.png">
                            </div>
                            <div class="discus-sub-obj-name">生化检测</div>
                        </a>
                        <a href="{{url('web/article/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_MYJC,\App\NewsCategory::$DEFAULT_CATEMYJCLC])}}"
                           class="discus-sub-obj  border-right border-bottom flex flex-direction-col flex-jfcontent-end">
                            <div class="discus-sub-obj-img flex flex-jfcontent-center flex-align-items-center">
                                <img src="/images/home/ELISA-1.png">
                            </div>
                            <div class="discus-sub-obj-name">免疫检测</div>
                        </a>
                        <a href="{{url('web/article/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_KTZB,\App\NewsCategory::$DEFAULT_CATEKYZBLC])}}"
                           class="discus-sub-obj border-bottom flex flex-direction-col flex-jfcontent-end">
                            <div class="discus-sub-obj-img flex flex-jfcontent-center flex-align-items-center">
                                <img src="/images/home/ELISA-2.png">
                            </div>
                            <div class="discus-sub-obj-name">抗体制备</div>
                        </a>
                        <a href="{{url('web/article/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_SJHDZ,\App\NewsCategory::$DEFAULT_CATESJHDZLC])}}"
                           class="discus-sub-obj border-right flex flex-direction-col flex-jfcontent-end">
                            <div class="discus-sub-obj-img flex flex-jfcontent-center flex-align-items-center">
                                <img src="/images/home/ELISA-3.png">
                            </div>
                            <div class="discus-sub-obj-name">试剂盒定制</div>
                        </a>
                        <a href="{{url('web/article/activity')}}"
                           class="discus-sub-obj border-right flex flex-direction-col flex-jfcontent-end">
                            <div class="discus-sub-obj-img flex flex-jfcontent-center flex-align-items-center">
                                <img src="/images/home/ELISA-4.png">
                            </div>
                            <div class="discus-sub-obj-name">促销活动</div>
                        </a>
                        <a href="{{url('web/article/article_list',[\App\NewsCategory::$DEFAULT_CATE_YNJD])}}"
                           class="discus-sub-obj flex flex-direction-col flex-jfcontent-end">
                            <div class="discus-sub-obj-img flex flex-jfcontent-center flex-align-items-center">
                                <img src="/images/home/ELISA-5.png">
                            </div>
                            <div class="discus-sub-obj-name">技术文章</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 1200px;user-select: none;" class="swiper-container">
            <div class="swiper-wrapper">
                @if(empty($banner->toArray()))
                    <div class="swiper-slide">
                        <div class="flex-jfcontent-center" style="width: 100%;" href="">
                            <a href="#"><img  style="width: 740px;height: 450px;margin-right: 60px" src="/images/home/banner-1.png"/></a>
                        </div>
                    </div>
                @endif
                @foreach($banner as $k=>$v)
                    <div class="swiper-slide flex-jfcontent-center">
                        <div style="width: 100%" class="flex-jfcontent-center">
                            {{--<a href="{{url('web/article/activityDetail',[$v->id])}}"><img  style="width: 740px;height: 450px;margin-right: 60px" src="{{$v->getFullPicturePath()}}"/></a>--}}
                            <a href="{{url('web/article/activityDetail',[$v->id])}}"><img  style="width: 740px;height: 450px;margin-right: 60px" src="{{$v->getFullPicturePath()}}"/></a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
    <div class="main-obj flex flex-jfcontent-center flex-align-items-center">
        <div class="main-obj-sub flex">
            <div class="sub-detail flex flex-jfcontent-space-between flex-align-items-center">
                <div class="sub-detail-content flex flex-direction-col flex-jfcontent-space-between ">
                    <div class="sub-detail-content-txt flex flex-direction-col flex-jfcontent-space-between">
                        <img src="/images/home/download.jpg"/>
                        <div>快来看看吧</div>
                    </div>
                    <a href="#" class="button-multiply">
                        点击下载
                    </a>
                </div>
                <div class="sub-detail-img flex flex-jfcontent-space-between flex-align-items-center">
                    <img src="/images/home/download-ionc.png">
                    <div style="width: 1px;border-left: 1px solid #eeeeee;height: 80px;"></div>
                </div>
            </div>
            <div class="sub-detail flex flex-jfcontent-space-between flex-align-items-center">
                <div class="sub-detail-content flex flex-direction-col flex-jfcontent-space-between ">
                    <div class="sub-detail-content-txt flex flex-direction-col flex-jfcontent-space-between">
                        <img style="width: 73px;height: 18px" src="/images/home/dl.jpg"/>
                        <div>加入我们走上人生巅峰</div>
                    </div>
                    <a href="#" style="background-color:#4EC9B5;" class="button-multiply">
                        点击加盟
                    </a>
                </div>
                <div class="sub-detail-img flex flex-jfcontent-space-between flex-align-items-center">
                    <img src="/images/home/dl-icon.png">
                    <div style="width: 1px;border-left: 1px solid #eeeeee;height: 80px;"></div>
                </div>
            </div>
            <div class="sub-detail flex flex-jfcontent-space-between flex-align-items-center">
                <div class="sub-detail-content flex flex-direction-col flex-jfcontent-space-between ">
                    <div class="sub-detail-content-txt flex flex-direction-col flex-jfcontent-space-between">
                        <img style="width: 73px;height: 18px" src="/images/home/tec.jpg"/>
                        <div>引领生物科技新高度</div>
                    </div>
                    <a href="{{url('web/article/article_list')}}" class="button-multiply">
                        点击关注
                    </a>
                </div>
                <div class="sub-detail-img flex flex-jfcontent-space-between flex-align-items-center">
                    <img src="/images/home/tec-icon.png">
                    <div style="width: 1px;border-left: 1px solid #eeeeee;height: 80px;"></div>
                </div>
            </div>
            <div class="sub-detail flex flex-jfcontent-space-between flex-align-items-center">
                <div class="sub-detail-content flex flex-direction-col flex-jfcontent-space-between ">
                    <div class="sub-detail-content-txt flex flex-direction-col flex-jfcontent-space-between">
                        <img style="width: 73px;height: 18px" src="/images/home/new.jpg"/>
                        <div>这里有你想要的</div>
                    </div>
                    <a href="{{url('web/goods/list')}}" style="background-color:#4EC9B5;" class="button-multiply">
                        点击查看
                    </a>
                </div>
                <div style="width:140px;"
                     class="sub-detail-img flex flex-jfcontent-space-between flex-align-items-center">
                    <img src="/images/home/new-icon.png">
                    <div style="width: 1px;border-left: 0px solid #eeeeee;height: 80px;"></div>
                </div>
            </div>
        </div>
    </div>
    <script src="/vendor/swiper/dist/js/swiper.js" charset="utf-8"></script>
    <script src="/vendor/jab/jquery.adaptive-backgrounds.js" charset="utf-8"></script>

    <script>
//        $(document).ready(function () {
//            $.adaptiveBackground.run()
//        });
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            paginationClickable: true,
            spaceBetween: 30,
            centeredSlides: true,
            autoplay: 2500,
            autoplayDisableOnInteraction: false,
            loop: true
        });

    </script>
@stop