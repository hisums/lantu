@extends('homeLayouts.left')
@section('subContent')
    <style>
        .activity-title {
            height: 50px;
            font-size: 20px;
            font-weight: bold;
        }
        .activity-img{
            width:100%;
            margin-bottom: 30px;
        }
        .activity-body-detail {

        }
    </style>
    @if($act_detail->getFullPicturePath())
        <div class="activity-img"><img style="width: 100%" src="{{$act_detail->getFullPicturePath()}}"></div>
    @endif
    <div class="activity-body-detail">
        {!! $act_detail->content !!}
    </div>
    <script type="text/javascript" src="/vendor/pagination/lib/jquery.min.js"></script>
    <script type="text/javascript">

    </script>
@stop