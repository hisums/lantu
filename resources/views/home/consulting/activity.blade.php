@extends('homeLayouts.left')
@section('subContent')
    <link rel="stylesheet" href="/vendor/pagination/lib/pagination.css"/>
    <style>
        .activity-title {
            height: 50px;
            font-size: 20px;
            font-weight: bold;
        }

        .activity-body-list-sub {
            width: 100%;
            margin-bottom: 50px;
        }
    </style>
    <div class="activity-body-list"></div>
    <div id="Pagination" class="pagination flex flex-jfcontent-end"><!-- 这里显示分页 --></div>
    <div id="hiddenresult" style="display:none;">
        @foreach($act_list as $v)
            <div class="activity-body-list-sub">
                <a href="{{url('web/article/activityDetail',[$v->id])}}"><img style="width: 100%" src="{{$v->getFullPicturePath()}}"></a>
            </div>
        @endforeach
    </div>
    {{--@include('homeLayouts.page', ['pagenate' => $act_list])--}}
    {{--{{$act_list->render()}}--}}
    <script type="text/javascript" src="/vendor/pagination/lib/jquery.min.js"></script>
    <script type="text/javascript" src="/vendor/pagination/lib/jquery.pagination.js"></script>
    <script type="text/javascript">
        function getcookie(cname){//获取指定名称的cookie的值
            var arrstr = document.cookie.split("; ");
            for(var i = 0;i < arrstr.length;i ++){
                var temp = arrstr[i].split("=");
                if(temp[0] == cname) return unescape(temp[1]);
            }
        }
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }
        var pageSize = 3;
        var current_page = Number(getcookie('activityPage'))?Number(getcookie('activityPage')):0 ;
        var initPagination = function () {
            var num_entries = $("#hiddenresult div.activity-body-list-sub").length;
            // 创建分页
            $("#Pagination").pagination(num_entries, {
                num_edge_entries: 1, //边缘页数
                num_display_entries: 4, //主体页数
                callback: pageselectCallback,
                items_per_page: pageSize ,//每页显示1项
                current_page:current_page,
                prev_text:'上一页',
                next_text:'下一页'
            });
        }();
        function pageselectCallback(page_index, jq) {
            $(".activity-body-list").empty();
            setCookie('activityPage',page_index);
            var new_content = '';
            for (var i = (page_index * pageSize); i < ((page_index + 1) * pageSize); i++) {
                new_content = $("#hiddenresult div.activity-body-list-sub:eq(" + i + ")").clone();
                $(".activity-body-list").append(new_content);
            }
            return false;
        }
    </script>
@stop
