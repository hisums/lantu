@extends('homeLayouts.main')
@section('content')
    <link rel="stylesheet" href="/vendor/pagination/lib/pagination.css"/>
    <link href="/vendor/swiper/dist/css/swiper.css" rel="stylesheet" type="text/css"/>
    <style>
        .main {
            width: 1200px;
            position: relative;
            margin-left: -600px;
            left: 50%;
            margin-top: 30px;
        }

        .list_container {
            width: 900px;
        }

        .right_container {
            width: 320px;
        }

        .list_head {
            width: 100%;
            height: 30px;
        }

        .list_head .title {
            width: 150px;
            border-bottom: 3px solid #C4DB9E;
            padding-left: 20px;
            color: #4E6CAE;
            font-weight: bold;
        }

        .list_head .blank {
            width: 690px;
            border-bottom: 3px solid #3E577E;
        }

        .list {
            width: 100%;
        }

        a {
            text-decoration: none;
            color: #000;
        }

        .article_item {
            height: 150px;
            width: 100%;
            overflow: hidden;
            margin: 20px;
            font-size: 14px;
        }

        .article_item img {
            width: 150px;
            height: 150px;
        }

        .item_content {
            margin-left: 20px;
            padding: 10px 0;
            width: 630px;
        }

        .item_content_detail {
            height: 60px;
            overflow: hidden;
        }

        .left_title {
            width: 100%;
            height: 40px;
            background: #255AA8;
            color: #fff;
        }

        .category {
            margin-bottom: 50px;
            width: 100%;
        }

        .left_content {
            width: 100%;
            min-height: 150px;
            border-bottom: 1px solid #999999;
        }

        .left_content-sub {
            width: 90%;
            min-height: 100px;
        }

        .recommend {
            height: 320px;
            width: 100%;
            margin-top: 20px;
            margin-bottom: 50px;
            border-bottom: 1px solid #999999;
        }

        .rank {
            min-height: 400px;
            width: 100%;
            margin-top: 20px;
            background-color: #F5F5F5;
        }

        .left_content-sub div {
            border: 1px solid #999999;
        }

        .left_content-sub-obj {
            min-width: 72px;
            min-height: 25px;
            font-size: 14px;
            margin-bottom: 5px;
            margin-top: 5px;
            padding-left: 5px;
            padding-right: 5px;
        }

        .recommend-main-body {
            height: 280px;
            width: 100%;
        }

        .swiper-container {
            width: 90%;
            height: 90%;
        }

        .swiper-slide {
            height: 100%;
        }

        .swiper-pagination {
            width: 94% !important;
        }

        .rank {
            margin-bottom: 50px;
        }

        .rank-main-body {
            width: 100%;
        }

        .rank-main-body-sub {
            width: 90%;
            min-height: 340px;
        }

        .rank-main-body-sub-obj {
            width: 100%;
            min-height: 50px;
            max-height: 150px;
            border-bottom: 1px solid #999999;
        }

        .title-content {
            margin-left: 10px;
            overflow: hidden;
            width: 85%;
            white-space: nowrap;
        }

        .rank-main-body-sub-obj-title {
            width: 100%;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .rank-main-body-sub-obj-content {
            width: 100%;
            padding-top: 5px;
            padding-bottom: 20px;
        }

        .rank-main-body-sub-obj-content-detail {
            width: 100%;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 2;
            overflow: hidden;
            font-size: 14px;
            color: #999999;
        }

        .rank-unsee {
            display: none;
        }

        .sort-number {
            width: 24px;
            height: 24px;
            border-radius: 50%;
            background-color: #999999;
            color: #ffffff;
            font-size: 14px;
        }

        .sort-number-in {
            background-color: #2962AF !important;
        }

        .list-sub {
            width: 90%;
        }

        .article-title-info {
            width: 100%;
            border-bottom: 1px solid #999999;
        }

        .article-title-content {
            font-size: 30px;
            font-weight: bold;
            height: 100px;
        }

        .article-title-other-info {
            color: #999999;
            margin-bottom: 40px;
        }

        .article-content {
            margin-top: 40px;
            margin-bottom: 40px;
            overflow-x: hidden;
        }
        .piece-navigate-box{
        }
    </style>
    <div class="main flex flex-jfcontent-space-between">
        <div class="list_container">
            <div class="piece-navigate-box">
                <a style="color: #000;" href="{{url('web/index')}}">首页</a>>><a style="color: #000;"
                                                                               href="{{url('web/article/article_list')}}">资讯中心</a>

                @if($article_info->news_cate_id == \App\NewsCategory::$DEFAULT_CATE_YNJD)
                    >><a style="color: #000;"
                         href="{{url('web/article/article_list',[\App\NewsCategory::$DEFAULT_CATE_YNJD])}}">技术文章</a>>><a href="#">{{$article_info->subject}}</a>
                @else
                    >><a style="color: #000;" href="{{url('web/article/article_list_all',[$article_info->news_cate_id])}}">{{$title}}</a>>><a href="#">{{$article_info->subject}}</a>
                @endif
            </div>
            <div class="list_head flex">
                <div class="title"></div>
                <div class="blank"></div>
            </div>
            <div class="list flex flex-jfcontent-center">
                <div class="list-sub flex flex-direction-col ">
                    <div class="article-title-info">
                        <div class="article-title-content flex flex-jfcontent-center flex-align-items-center">{{$article_info->subject}}</div>
                        <div class="article-title-other-info flex flex-jfcontent-center flex-align-items-center">
                            发布时间：{{$article_info->create_time}}　　发布人：蓝图生物科技
                        </div>
                    </div>
                    <div class="article-content">
                        {!! $article_info->content !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="right_container">
            <div class="category flex flex-direction-col flex-align-items-center">
                <h4 class="left_title flex flex-align-items-center flex-jfcontent-center">标签分类</h4>
                <div class="left_content flex flex-align-items-center flex-jfcontent-center">
                    <div class="left_content-sub flex flex-align-items-start flex-wrap flex-jfcontent-space-between">
                        <div class="left_content-sub-obj flex flex-align-items-center flex-jfcontent-center">
                            <a href="{{url('web/article/activity')}}">促销活动</a>
                        </div>
                        <div class="left_content-sub-obj flex flex-align-items-center flex-jfcontent-center">
                            <a href="{{url('web/article/article_list_all',[\App\NewsCategory::$DEFAULT_CATE_CPFK])}}">行业动态</a>
                        </div>
                        <div class="left_content-sub-obj flex flex-align-items-center flex-jfcontent-center">
                            <a href="{{url('web/article/article_list',[\App\NewsCategory::$DEFAULT_CATE_YNJD])}}">技术文章</a>
                        </div>
                        {{--<div class="left_content-sub-obj flex flex-align-items-center flex-jfcontent-center">--}}
                            {{--<a href="{{url('web/article/article_list_all',[\App\NewsCategory::$DEFAULT_CATE_YNJD])}}">疑难解答</a>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            <div class="recommend">
                <h4 class="left_title flex flex-align-items-center flex-jfcontent-center">今日推荐</h4>
                <div class="recommend-main-body flex flex-align-items-center flex-jfcontent-center">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @if(empty($common_data['today_recommend']->toArray()))
                                <div class="swiper-slide"><a href=""><img style="width: 100%;height: 100%;"
                                                                          src="/images/home/banner-1.png"/></a></div>
                            @else
                                @foreach($common_data['today_recommend'] as $v)
                                    <div class="swiper-slide"><a
                                                href="{{$v->news_cate_id == \App\NewsCategory::$DEFAULT_CATE_CXHD?url('web/article/activityDetail',[$v->id]):url('web/article/articleDetail',[$v->id])}}"><img
                                                    style="width: 100%;height: 100%;"
                                                    src="{{$v->getFullPicturePath()}}"/></a></div>
                                @endforeach
                            @endif
                        </div>
                        <div class="swiper-pagination flex flex-jfcontent-end"></div>
                    </div>
                </div>
            </div>
            <div class="rank flex flex-direction-col">
                <h4 class="left_title flex flex-align-items-center flex-jfcontent-center">总排行</h4>
                <div class="rank-main-body flex flex-align-items-center flex-jfcontent-center">
                    <div class="rank-main-body-sub flex flex-direction-col">
                        @foreach($common_data['ranking'] as $k=>$v)
                            <div class="rank-main-body-sub-obj flex flex-align-items-center flex-direction-col flex-jfcontent-center">
                                <div class="rank-main-body-sub-obj-title flex flex-jfcontent-start flex-align-items-center">
                                    <div class="sort-number sort-number-in flex-align-items-center  flex-jfcontent-center">
                                        {{sprintf('%02s',$k+1)}}
                                    </div>
                                    <a href="{{url('web/article/articleDetail',[$v->id])}}" class="title-content">{{$v->subject}}</a>
                                </div>
                                <div class="rank-main-body-sub-obj-content flex flex-align-items-start flex-jfcontent-space-between flex-wrap {{$k+1 == 1?'':'rank-unsee'}}">
                                    <img style="width: 100px;height: 70px;" src="{{$v->getFullPicturePath()}}">
                                    <div style="width:60%;">
                                        <div class="rank-main-body-sub-obj-content-detail">{{strip_tags($v->content)}}
                                        </div>
                                        <a style="color: #495970;font-size: 14px" href="{{url('web/article/articleDetail',[$v->id])}}">[详细]</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/vendor/swiper/dist/js/swiper.js" charset="utf-8"></script>
    <script type="text/javascript" src="/vendor/pagination/lib/jquery.min.js"></script>
    <script type="text/javascript" src="/vendor/pagination/lib/jquery.pagination.js"></script>
    <script type="text/javascript">
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            autoplay: 2500,
            autoplayDisableOnInteraction: false,
            loop: true
        });
        $('.rank-main-body-sub-obj').mousemove(function () {
            $('.rank-main-body-sub-obj').each(function (k, v) {
                $(v).find('.rank-main-body-sub-obj-content').addClass('rank-unsee');
                $(v).find('.sort-number').removeClass('sort-number-in');
            });
            $(this).find('.rank-main-body-sub-obj-content').removeClass('rank-unsee');
            $(this).find('.sort-number').addClass('sort-number-in');
        })
    </script>
@stop
