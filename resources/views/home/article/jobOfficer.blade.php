@extends('homeLayouts.main')
@section('content')
    <link href="/vendor/swiper/dist/css/swiper.css" rel="stylesheet" type="text/css"/>
    <style>
        .banner-box {
            width: 1200px;
            background-color: #F9FAFC;
        }

        .banner-img {
            width: 1200px;
            height: 252px;
            margin-top: 60px;
        }

        .banner-img2 {
            width: 1200px;
            height: 850px;
            margin-top: 80px;
            background: url(/images/home/img16.png);
        }

        .title-1 {
            position: relative;
            color: white;
            top: 30px;
            left: 520px;
            font-size: 20px;
        }

        .banner-line {
            position: relative;
            top: 45px;
            left: 560px;
            width: 50px;
            height: 2px;
            background-color: white;
        }

        .title-2 {
            position: relative;
            color: white;
            top: 60px;
            left: 420px;
            font-size: 16px;
        }

        #select-card {
            position: relative;
            top: 120px;
            left: 460px;
            width: 280px;
            height: 40px;
            font-size: 16px;
        }

        .title-3 {
            position: absolute;
            width: 930px;
            height: 496px;
            top: 850px;
            left: 50%;
            background-color: white;
            margin-left: -465px;
            margin-right: 0;
        }

        .btsm {
            float: left;
            width: 100%;
            height: 100%;
        }

        .btsm-sub {
            width: 100% !important;
            height: 100%;
            overflow: auto;
        }

        .btsm h3 {
            position: relative;
            left: 25px;
            top: 10px;
        }

        .btsn {
            position: relative;
            top: 50px;
            left: 30px;
        }

        .btsm ul li {
            position: relative;
            top: 30px;
            left: 25px;
            line-height: 30px;
            font-size: 14px;
        }

        .manager-job {
            width: 40%;
            height: 35px;
            background-color: #ffffff;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            border-right: 1px solid #393937;
        }

        .tec-job {
            width: 40%;
            height: 35px;
            background-color: #ffffff;
            border-right: 1px solid #393937;
        }

        .other-job {
            width: 20%;
            height: 35px;
            background-color: #ffffff;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
        }

        .job-sub-check {
            background-color: #393937;
            color: #ffffff;
        }

        .job-about-sub-unsee {
            z-index: -1
        }

        .btsm-sub-content {
            width: 80%;
            height: 90%;
        }
    </style>
    <div style="width: 100%" class="flex flex-jfcontent-center">
        <div class="banner-box">
            <div class="banner-img">
                <img src="/images/home/img15.png"/>
            </div>
            <div class="banner-img2">
                <span class="title-1">WE WANT YOU</span>
                <div class="banner-line"></div>
                <span class="title-2">请发送您的简历至355902222@qq.com,标题请注明申请职位</span>
                <div id="select-card" class="flex">
                    <div class="manager-job flex flex-jfcontent-center flex-align-items-center job-sub-check job-about">
                        管理岗位
                    </div>
                    <div class="tec-job flex flex-jfcontent-center flex-align-items-center job-about">技术岗位</div>
                    <div class="other-job flex flex-jfcontent-center flex-align-items-center job-about">其他</div>
                </div>
                <div id="manager-job-sub" class="title-3 swiper-container">
                    <div class="btsm swiper-wrapper">
                        @foreach($manager_data as $k)
                            <div class="btsm-sub swiper-slide flex flex-align-items-end flex-jfcontent-center">
                                <div class="btsm-sub-content">
                                    {!! $k->content !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination1"></div>
                    <div class="swiper-button-next swiper-button-next1"></div>
                    <div class="swiper-button-prev swiper-button-prev1"></div>
                </div>
                <div id="tec-job-sub" class="title-3 swiper-container job-about-sub-unsee">
                    <div class="btsm swiper-wrapper">
                        @foreach($tec_data as $k)
                            <div class="btsm-sub swiper-slide flex flex-align-items-end flex-jfcontent-center">
                                <div class="btsm-sub-content">
                                    {!! $k->content !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination2"></div>
                    <div class="swiper-button-next swiper-button-next2"></div>
                    <div class="swiper-button-prev swiper-button-prev2"></div>
                </div>
                <div id="other-job" class="title-3 swiper-container job-about-sub-unsee">
                    <div class="btsm swiper-wrapper">
                        @foreach($other_data as $k)
                            <div class="btsm-sub swiper-slide flex flex-align-items-end flex-jfcontent-center">
                                <div class="btsm-sub-content">
                                    {!! $k->content !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination3"></div>
                    <div class="swiper-button-next swiper-button-next3"></div>
                    <div class="swiper-button-prev swiper-button-prev3"></div>
                </div>
            </div>
        </div>
    </div>
    <script src="/vendor/swiper/dist/js/swiper.js" charset="utf-8"></script>
    <script>

        $(function () {
            var swiper1 = new Swiper('#manager-job-sub', {
                pagination: '.swiper-pagination1',
                paginationClickable: true,
                nextButton: '.swiper-button-next1',
                prevButton: '.swiper-button-prev1',
                spaceBetween: 30
            });


            var swiper2 = new Swiper('#tec-job-sub', {
                pagination: '.swiper-pagination2',
                paginationClickable: true,
                nextButton: '.swiper-button-next2',
                prevButton: '.swiper-button-prev2',
                spaceBetween: 30
            });

            var swiper3 = new Swiper('#other-job', {
                pagination: '.swiper-pagination3',
                paginationClickable: true,
                nextButton: '.swiper-button-next3',
                prevButton: '.swiper-button-prev3',
                spaceBetween: 30
            });
        });


        $('.job-about').click(function () {
            $('.job-about').each(function (k, v) {
                $(this).removeClass('job-sub-check');
            });
            $(this).addClass('job-sub-check');
            console.log($(this).index());
            $('.title-3').each(function (k, v) {
                $(this).addClass('job-about-sub-unsee');
            });
            if ($(this).index() == 0) {
                $('#manager-job-sub').removeClass('job-about-sub-unsee');
            }
            if ($(this).index() == 1) {
                $('#tec-job-sub').removeClass('job-about-sub-unsee');
            }
            if ($(this).index() == 2) {
                $('#other-job').removeClass('job-about-sub-unsee');
            }
        })
    </script>
@stop