@extends('homeLayouts.main')
@section('content')
    <style>
        .contact-box {
            width: 1200px;
        }

        .cc-1 {
            width: 1202px;
            height: 252px;
            margin-top: 60px;
        }

        .cc-2 {
            float: left;
            width: 1200px;
            min-height: 470px;
            border: 1px solid #F9FAFC;
            background-color: white;
        }

        .cc-2-img {
            width: 406px;
            margin-left: 10px;
            margin-top: 40px;
        }

        .cc-2 h2 {
            color: #2659AA;
        }

        .cc-2 p {
            line-height: 28px;
            color: #666666;
        }
        .cc-2-content{
            margin-top: 40px;
            width: 64%;
            word-wrap: break-word;
        }
        .contact-box {
            width: 1200px;
            min-height: 520px;
            border-top: 2px solid #000000;
        }

        .cc-1 {
            width: 1202px;
            height: 252px;
            margin-top: 60px;
        }

        .cc-2 {
            width: 1200px;
            min-height: 440px;
            border: 1px solid #F9FAFC;
            background-color: white;
            margin-bottom: 70px;
        }

        .cc-3 {
            width: 100%;
            height: 60px;
        }

        .cc-3-1 {
            margin-left: 30px;
            color: #2382F0;
            height: 100%;
        }

        .cc-3-2 {
            font-size: 10px;
            height: 100%;
            color: #1D5288;
            margin-left:5px;
        }

        .cc-4 {
            width: 500px;
            height: 334px;
            margin-top: 40px;
            color: #808080;
            line-height: 40px;
            margin-left: 80px;
        }

        .cc-5 {
            width: 500px;
            height: 334px;
            margin-top: 40px;
            color: #808080;
            line-height: 40px;
        }
        .main-img{
            width: 100%;
        }
    </style>
    <div style="width: 100%" class="flex flex-jfcontent-center">
        <div class="contact-box">
            <div class="cc-2 flex flex-jfcontent-space-between">
                <div class="cc-2-img">
                    <img  style="width: 100%"  src="{{!empty($about_us)?$about_us->getFullPicturePath():'/images/no-pic-back.png'}}"/>
                </div>
                <div class="cc-2-content">
                    <h2>蓝图生物简介</h2>
                    <div style="width: 100%;margin-top: 10px;">
                        {!! !empty($about_us)?$about_us->content:'' !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="width: 100%" class="flex flex-jfcontent-center">
        <div class="contact-box">
            <div class="cc-1">
                <img src="/images/home/img12.png"/>
            </div>
            <div class="cc-2 flex flex-direction-col">
                <div class="cc-3 flex flex-align-items-center">
                    <h2 class="cc-3-1 flex flex-align-items-center">联系我们</h2>
                    <div class="cc-3-2 flex flex-align-items-center">/CONTACT US</div>
                </div>
                <div class="main-img">
                    {!! $connect_us->content !!}
                   {{-- <img style="width: 1200px" src="{{$connect_us?$connect_us->getFullPicturePath():'/images/no-pic-back.png'}}">--}}
                </div>
                {{-- <div class="cc-4">
                     <h3>泉州市蓝图生物科技有限公司</h3>
                     <ul>
                         <li>QUANZHOUSHI LANTU BIOLOGICAL TECHNOLOGY CO.LTD</li>
                         <li>福建省泉州市泉港区界山镇蓝图生物科技园A栋5层</li>
                         <li>电话 ： 0595-27971666</li>
                         <li>移动电话 ： 13559590085</li>
                         <li>传真 ： 0595-27973666</li>
                         <li>QQ :355902222</li>
                         <li>邮箱 ：355902222@qq.com</li>
                     </ul>
                 </div>
                 <div class="cc-5">
                     <h3>黄石市蓝图生物科技有限公司</h3>
                     <ul>
                         <li>HUANGSHISHI LANTU BIOLOGICAL TECHNOLOGY CO.LTD</li>
                         <li>湖北省黄石市黄石港磁湖路41号-107</li>
                         <li>电话 ：0714-6330330</li>
                         <li>移动电话 ：15072037355</li>
                         <li>QQ : 2228104692</li>
                         <li>邮箱 : 2228104692@qq.com</li>
                     </ul>
                 </div>--}}
            </div>
        </div>
    </div>
@stop