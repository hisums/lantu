@extends('homeLayouts.main')
@section('content')
    <style>
        .login-background {
            width: 1200px;
            margin-bottom: 200px;
            overflow-x: hidden;
        }

        .login-form {
            position: absolute;
            height: 250px;
            width: 340px;
            left: 51%;
            top: 394px;
        }

        .login-form-sub {
            height: 80%;
            width: 90%;
        }

        .login-form-obj {
            height: 25%;
            width: 100%;
        }

        input[name=user_name] {
            width: 80%;
            height: 24px;
            border: none;
            padding-left: 10px;
            font-size: 14px;
        }

        .login-form-obj-sub-right {
            width: 74%;
            height: 30px;
            border: solid 1px #ccc;
        }

        .login-form-obj-sub {
            letter-spacing: 8px;
        }

        input[name=pwd] {
            width: 80%;
            height: 24px;
            border: none;
            padding-left: 10px;
            font-size: 14px;
        }

        input[name=verify_code] {
            width: 80%;
            height: 24px;
            border: none;
            padding-left: 0;
            font-size: 14px;
        }

        .verify-code {
            margin-left: 20px;
        }

        input[type=checkbox] {
            padding-right: 5px;
            font-size: 0;
            border: none;
            border-radius: 0;
            height: 20px;
            margin-right: 10px;
        }

        .login-btn {
            width: 68%;
            height: 32px;
            border: none;
            background-color: #4E90FE;
            color: #ffffff;
            font-size: 18px;
        }

        .logining-status {
            background-color: #999999 !important;
        }
    </style>
    <div style="width:100%" class="flex flex-jfcontent-center">
        <div class="login-background flex">
            <img style="width:1200px;" src="/images/home/login-background.png">

            <form class="login-form flex flex-align-items-center flex-jfcontent-center" action="{{url('web/doLogin')}}"
                  method="post">
                <div class="login-form-sub">
                    <div class="login-user-name login-form-obj flex flex-align-items-center flex-jfcontent-space-between">
                        <div class="login-form-obj-sub">
                            用户名
                        </div>
                        <div class="login-form-obj-sub-right flex flex-align-items-center flex-jfcontent-center">
                            <img src="/images/home/user-icon.png"/>
                            <input type="text" name="user_name">
                        </div>
                    </div>
                    <div class="login-user-name login-form-obj flex flex-align-items-center flex-jfcontent-space-between">
                        <div style="letter-spacing: 0" class="login-form-obj-sub">
                            用户密码
                        </div>
                        <div class="login-form-obj-sub-right flex flex-align-items-center flex-jfcontent-center">
                            <img src="/images/home/pwd-icon.png"/>
                            <input type="password" name="pwd">
                        </div>
                    </div>
                    <div class="login-user-name login-form-obj flex flex-align-items-center flex-jfcontent-space-between">
                        <div class="login-form-obj-sub">
                            验证码
                        </div>
                        <div style="width: calc(74% + 2px);" class="flex flex-align-items-center">
                            <div style="width: 30%"
                                 class="login-form-obj-sub-right flex flex-align-items-center flex-jfcontent-center">
                                <input style="text-align: center" type="text" placeholder="验证码" name="verify_code">
                            </div>
                            <div class="verify-code">
                                <img id="verify" style="cursor: pointer"
                                     onclick="this.src='{{ url('web/captcha') }}?r='+Math.random();"
                                     src="{{url('web/captcha')}}">
                            </div>
                        </div>
                    </div>
                    <div class="login-user-name login-form-obj flex flex-align-items-center flex-jfcontent-space-between">
                        <label style="width: 83px" class="flex flex-align-items-center"><input type="checkbox">
                            <div style="font-size: 15px">记住密码</div>
                        </label>
                        <button class="login-btn flex flex-align-items-center flex-jfcontent-center">登录　<img
                                    src="/images/home/rightbutton.png"/></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="/vendor/layer/layer.js" charset="utf-8"></script>
    <script>

        $('form').submit(function () {
            if (!$('input[name=user_name]').val()) {
                layer.msg('请输入用户名!', function () {
                });
                return false;
            }
            if (!$('input[name=user_name]').val().length>20) {
                layer.msg('用户名长度不得超过20个字符!', function () {
                });
                return false;
            }
            if (!$('input[name=pwd]').val()) {
                layer.msg('请输入密码!', function () {
                });
                return false;
            }
            if (!$('input[name=verify_code]').val()) {
                layer.msg('请输入验证码!', function () {
                });
                return false;
            }
            var data = {
                name: $('input[name=user_name]').val(),
                password: $('input[name=pwd]').val(),
                verify: $('input[name=verify_code]').val()
            };
            $('.login-btn').attr('disabled', 'disabled');
            $('.login-btn').addClass('logining-status');
            $.ajax({
                url: "{{url('web/doLogin')}}",
                type: 'post',
                dataType: 'json',
                data: data,
                async: false,
                success: function (res) {
                    if (res.Success) {
                        location.href = "{{url('web/index')}}";
                    }
                },
                error: function (res) {
                    if (res.status == 422) {
                        $('.login-btn').attr('disabled', false);
                        $('.login-btn').removeClass('logining-status');
                        var errors = res.responseJSON;
                        var msg = '';
                        for (var errorcode in errors) {
                            var errorItem = errors[errorcode];
                            for (var i = 0; i < errorItem.length; i++) {
                                msg = errorItem[i];
                                break;
                            }
                        }
                        layer.msg(msg, function () {
                            $('#verify').trigger("click");
                        });
                    }
                }
            });
            return false;
        });
    </script>
@stop