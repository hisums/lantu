@extends('homeLayouts.left')
@section('subContent')
    <link rel="stylesheet" href="/vendor/pagination/lib/pagination.css"/>
    <style>
        .activity-title {
            height: 50px;
            font-size: 20px;
            font-weight: bold;
        }

        .activity-body-list-sub {
            width: 100%;
            margin-top: 15px;
            border: 1px solid #DAE3EA;
            box-shadow: 0px 0px 1px #DAE3EA;
            border-radius: 10px;
            padding: 10px;
            height: 200px;
        }

        .search_content {
            font-size: 14px;
            color: #2A2B2F;
            border-bottom: 1px solid #D8E3E7;
            height: 45px;
        }

        .search_content span {
            margin-left: 20px;
        }

        .search_content select {
            padding-left: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            border: none;
            background-color: #EAEDF2;
        }

        .activity-body-list {
            width: 100%;

        }

        .goods_center {
            width: 460px;
        }

        .goods_right {
            width: 240px;
        }

        .goods_center span {
            height: 19%;
        }

        .addToCard {
            width: 150px;
            height: 40px;
            background: #457AC6;
            margin-left: 50px;
            border-radius: 5px;
            color: #ffffff;
            cursor: pointer;
        }

        .addToCard img {
            width: 25px;
            height: 25px;
            background: #ffffff;
            border-radius: 5px;
            margin-right: 10px;
        }

        .goods_right span {
            margin-top: 15px;
        }

        .goods_tools img {
            width: 25px;
            height: 25px;;
            margin: 5px;
        }

        #Pagination {
            margin-top: 20px;
        }

    </style>
    <div class="search_content flex">
        <span>排序：<select id="order_type">
                <option value="2" url="{{url('web/goods/list',[$cate_id,2])}}" @if($order_type == 2) selected @endif>
                    默认
                </option>
                <option value="1" url="{{url('web/goods/list',[$cate_id,1])}}" @if($order_type == 1) selected @endif>
                    价格
                </option>
            </select></span>
        <span>每页展示数量：<select id="pageSize">
                <option value="5">5条</option>
                <option value="10">10条</option>
                <option value="20">20条</option>
                <option value="50">50条</option>
            </select></span>
    </div>
    <div class="activity-body-list">

    </div>
    <div id="Pagination" class="pagination flex flex-jfcontent-end"><!-- 这里显示分页 --></div>
    <div id="hiddenresult" style="display:none;">


    </div>
    {{--@include('homeLayouts.page', ['pagenate' => $act_list])--}}
    {{--{{$act_list->render()}}--}}
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/vendor/pagination/lib/jquery.pagination.js"></script>
    <script src="/vendor/layer/layer.js" charset="utf-8"></script>
    <script type="text/javascript">
        function getcookie(cname) {//获取指定名称的cookie的值
            var arrstr = document.cookie.split("; ");
            for (var i = 0; i < arrstr.length; i++) {
                var temp = arrstr[i].split("=");
                if (temp[0] == cname) return unescape(temp[1]);
            }
        }
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires     = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }
        function getUrlParams(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); //定义正则表达式
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return decodeURIComponent(r[2]);
            return null;
        }
        var pageSize = 5;
        if (document.cookie.indexOf('productListPageSize') != -1) {
            pageSize = getcookie('productListPageSize');
        }

        var options = $("#pageSize").find("option"); // select下所有的option
        for (var i = 0; i < options.length; i++) {
            if ((options.eq(i).val()) == pageSize) {
                $("#pageSize").children().eq(i).attr("selected", 'selected');
            }
            ; // 将所有的值赋给数组
        }
        var current_page   = Number(getcookie('productListPage')) ? Number(getcookie('productListPage')) : 0;
        var initPagination = function () {
//            var num_entries = $("#hiddenresult div.activity-body-list-sub").length;
            var keywords = getUrlParams('keywords');
            $.ajax({
                url     : "{{url('web/goods/list',[$cate_id,$order_type])}}",
                data    : {count: true,keywords:keywords},
                dataType: 'json',
                type    : 'post',
                success : function (res) {
                    $("#Pagination").pagination(res.totalPage, {
                        num_edge_entries   : 1, //边缘页数
                        num_display_entries: 4, //主体页数
                        callback           : pageselectCallback,
                        current_page       : current_page,
                        items_per_page     : pageSize,//每页显示1项
                        prev_text          : '上一页',
                        next_text          : '下一页'
                    });
                }

            });
            // 创建分页
            /* $("#Pagination").pagination(num_entries, {
             num_edge_entries   : 1, //边缘页数
             num_display_entries: 4, //主体页数
             callback           : pageselectCallback,
             current_page       : current_page,
             items_per_page     : pageSize,//每页显示1项
             prev_text          : '上一页',
             next_text          : '下一页'
             });*/
        }();
        /*function pageselectCallback(page_index, jq) {
         $(".activity-body-list").empty();
         var new_content = '';
         setCookie('productListPage',page_index);
         setCookie('productListPageSize',pageSize);
         for (var i = (page_index * pageSize); i < ((page_index + 1) * pageSize); i++) {
         new_content = $("#hiddenresult div.activity-body-list-sub:eq(" + i + ")").clone();
         $(".activity-body-list").append(new_content);
         }
         return false;
         }*/
        function spelHtml(list) {
            var html = '';
            list.forEach(function (v) {
                html += '<div class="activity-body-list-sub flex flex-jfcontent-space-between">';
                html += '<a href="{{url('web/goods/detail')}}/' + v.goods_id + '/' + v.id + '"><img class="goods_left" style="height: 190px;width: 190px" src="' + v.goods.pic + '"></a>';
                html += '<div class="goods_center flex flex-direction-col">';
                html += '<a href="{{url('web/goods/detail')}}/' + v.goods_id + '/' + v.id + '" style="font-size: 20px;color: #115560">' + v.goods.goods_name + '</a>';
                html += '<span style="border-bottom: 1px solid #E4E4E4">0个评测</span>';
                html += '<span style="margin-top: 5px;">货号：' + v.goods_number + '</span>';
                html += '<span>名称：' + v.goods.goods_name + '</span>';
                html += '<span>规格：' + v.goods_spec + '</span>';
                html += '<span><a href="{{url('web/goods/detail')}}/' + v.goods_id + '/' + v.id + '">了解更多</a></span>';
                html += '</div>';
                html += '<div class="goods_right flex flex-direction-col flex-align-items-end">';
                html += '<span style="color: #457BC5;font-size: 20px">￥' + v.price + '</span>';
               /* html += '<span class="addToCard flex flex-jfcontent-center flex-align-items-center"><img src="/images/goods/add.png">添加到购物车</span>';
                html += '<span class="goods_tools flex flex-jfcontent-center">';
                html += '<img src="/images/goods/collect.png" alt="收藏">';
                html += '<img src="/images/goods/compare.png" alt="比较">';*/
                html += '</span>';
                html += '</div>';
                html += '</div>';
            })
            return html;
        }
        function pageselectCallback(page_index) {
            var load = layer.load(1);
            //pageSize = $(this).val();
            var keywords = getUrlParams('keywords');
            $.ajax({
                url     : "{{url('web/goods/list',[$cate_id,$order_type])}}",
                data    : {pageSize: pageSize, pageNumber: page_index+1,keywords:keywords},
                dataType: 'json',
                type    : 'post',
                success : function (res) {
                    console.log(res);
                    var list     = res.list;
                    var html = spelHtml(list);
                    $(".activity-body-list").empty();
                    setCookie('productListPage', page_index );
                    setCookie('productListPageSize', pageSize);
                    $(".activity-body-list").html(html);
                    layer.close(load);
                }
            });
            return false;
        }
        $('.addToCard').click(function () {
            layer.msg('此功能暂未开放');
        });

        $("#order_type").change(function () {
            location.href = $(this).find('option:selected').attr('url');
        });
        $("#pageSize").change(function () {
            setCookie('productListPage', '', -1);
            setCookie('productListPageSize', '', -1);
            pageSize = $(this).val();
            var keywords = getUrlParams('keywords');
            $.ajax({
                url     : "{{url('web/goods/list',[$cate_id,$order_type])}}",
                data    : {pageSize: pageSize, pageNumber: current_page,keywords:keywords},
                dataType: 'json',
                type    : 'post',
                success : function (res) {
                    $("#Pagination").pagination(res.totalPage, {
                        num_edge_entries   : 1, //边缘页数
                        num_display_entries: 4, //主体页数
                        callback           : pageselectCallback,
                        current_page       : current_page,
                        items_per_page     : pageSize,//每页显示1项
                        prev_text          : '上一页',
                        next_text          : '下一页'
                    });
                }

            });

            /*var num_entries = $("#hiddenresult div.activity-body-list-sub").length;
             // 创建分页
             $("#Pagination").pagination(num_entries, {
             num_edge_entries   : 1, //边缘页数
             num_display_entries: 4, //主体页数
             callback           : pageselectCallback,
             items_per_page     : pageSize,//每页显示1项
             prev_text          : '上一页',
             next_text          : '下一页'
             });*/
        })
    </script>
@stop