@extends('homeLayouts.main')
@section('content')
    <link href="/css/home/goods_detail.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/vendor/pagination/lib/pagination.css"/>
    <style>
        .piece-navigate {
            font-size: 14px;
            text-align: left;
        }
    </style>
    <div class="main flex flex-direction-col">
        <div class="piece-navigate">
            <a style="color: #000;" href="{{url('web/index')}}">首页</a>>><a style="color: #000;"
                                                                           href="{{url('web/goods/list')}}">产品中心</a>{{!empty($goods_info)?getUpCate($goods_info['goods_category_id']):''}}
            >>{{$goods_info->goods_name}}
        </div>
        <div class="goods_head flex">
            <div class="list_container flex">
                <div class="goods_album flex flex-direction-col flex-align-items-center flex-jfcontent-center">
                    <div class="tb-booth tb-pic tb-s310">
                        <a href="{{$spec_info->getFullPicturePath1()}}"><img src="{{$spec_info->getFullPicturePath1()}}"
                                                                             rel="{{$spec_info->getFullPicturePath1()}}"
                                                                             class="jqzoom"/></a>
                    </div>
                    <ul class="tb-thumb" id="thumblist">
                        <li class="tb-selected">
                            <div class="tb-pic tb-s40"><a href="javascript:;"><img
                                            src="{{$spec_info->getFullPicturePath1()}}"
                                            mid="{{$spec_info->getFullPicturePath1()}}"
                                            big="{{$spec_info->getFullPicturePath1()}}"></a></div>
                        </li>
                        @if($spec_info->goods_spec_pic2)
                            <li>
                                <div class="tb-pic tb-s40"><a href="javascript:;"><img
                                                src="{{$spec_info->getFullPicturePath2()}}"
                                                mid="{{$spec_info->getFullPicturePath2()}}"
                                                big="{{$spec_info->getFullPicturePath2()}}"></a></div>
                            </li>
                        @endif
                        @if($spec_info->goods_spec_pic3)
                            <li>
                                <div class="tb-pic tb-s40"><a href="javascript:;"><img
                                                src="{{$spec_info->getFullPicturePath3()}}"
                                                mid="{{$spec_info->getFullPicturePath3()}}"
                                                big="{{$spec_info->getFullPicturePath3()}}"></a></div>
                            </li>
                        @endif
                        @if($spec_info->goods_spec_pic4)
                            <li>
                                <div class="tb-pic tb-s40"><a href="javascript:;"><img
                                                src="{{$spec_info->getFullPicturePath4()}}"
                                                mid="{{$spec_info->getFullPicturePath4()}}"
                                                big="{{$spec_info->getFullPicturePath4()}}"></a></div>
                            </li>
                        @endif
                        @if($spec_info->goods_spec_pic5)
                            <li>
                                <div class="tb-pic tb-s40"><a href="javascript:;"><img
                                                src="{{$spec_info->getFullPicturePath5()}}"
                                                mid="{{$spec_info->getFullPicturePath5()}}"
                                                big="{{$spec_info->getFullPicturePath5()}}"></a></div>
                            </li>
                        @endif

                    </ul>
                </div>
                <div class="goods_right flex flex-direction-col">
                    <div class="goods_info1 flex flex-direction-col flex-jfcontent-center">
                        <p>{{$goods_info->goods_name}}</p>
                        {{--<div class="evaluates"><span>@for($i=0;$i<$goods_info->goods_score;$i++)☆@endfor</span><span class="evaluates_num"><span>0</span>个评测</span></div>--}}
                        <div class="line"></div>
                    </div>
                    <div class="goods_info2 flex flex-direction-col flex-jfcontent-space-between">
                        <div class="flex"><h5>货号：</h5><span>{{$spec_info->goods_number}}</span></div>
                        <div class="flex"><h5>名称：</h5><span>{{$goods_info->goods_name}}</span></div>
                        <div class="flex"><h5>品牌：</h5><span>{{$goods_info->goods_vendor}}</span></div>
                        <div class="flex"><h5>货期：</h5>
                            <span>
                            @foreach(\App\GoodsSpecs::$GOODS_DELIVERY_TIME as $dt)
                                    @if($dt['key'] == $spec_info->delivery_time)
                                        {{$dt['text']}}
                                    @endif
                                @endforeach
                            </span>
                        </div>
                        <div class="flex"><h5>规格：</h5>
                            <span class="flex flex-wrap spec">
                                @foreach($goods_info->spec as $sp)
                                    <a style="" href="{{url('web/goods/detail',[$goods_info->id,$sp->id])}}"
                                       @if($sp->id == $spec_info->id) class="select" @endif>{{$sp->goods_spec}}</a>
                                @endforeach
                            </span>
                        </div>
                    </div>
                    <div class="goods_info3 flex flex-direction-col flex-jfcontent-center">
                        <div class="line"></div>
                        <h3 class="flex flex-align-items-center">￥{{$spec_info['price']}}</h3>
                        <div class="num_input flex flex-align-items-center">
                            <div class="num_box flex flex-jfcontent-center flex-align-items-center"><span
                                        class="minus flex flex-jfcontent-center flex-align-items-center">-</span><span
                                        class="goods_number flex flex-jfcontent-center flex-align-items-center">1</span><span
                                        class="plus flex flex-jfcontent-center flex-align-items-center">+</span></div>
                            <div class="addToCard flex flex-jfcontent-center flex-align-items-center"><img
                                        src="/images/goods/add.png">添加到购物车
                            </div>
                        </div>
                        <div class="line"></div>
                    </div>
                    <div class="goods_info4 flex flex-direction-col">
                        <div class="operate_list flex flex-align-items-center">
                            <div id="goods_qrcode_img" class="hiddenresult"><img
                                        src="{{$goods_info->getFullQRCodePath()}}"></div>
                            <span id="goods_qrcode" class="flex flex-jfcontent-center flex-align-items-center"><img
                                        src="/images/goods/qrcode.png">　QR Code</span>
                        </div>
                        @if(!empty($file_info))
                            <div class="document">
                                <span>文档说明</span>
                                @foreach($file_info as $v)
                                    <p>
                                        <a href="{{$v->getFullPath()}}">{{$v->name}}</a>
                                    </p>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="right_container flex flex-direction-col">
                <div class="contact_us flex flex-direction-col">
                    <h3 class="flex flex-jfcontent-center flex-align-items-center">联系我们</h3>
                    <p>电话：0714-6338558</p>
                    <p>QQ：355902222</p>
                    <p>邮箱：355902222@qq.com</p>
                    <p>网址：www.biolantu.com</p>
                    <p class="notes">notes:为了方便，您也可以在我们网站右侧点击在线客户直接咨询</p>
                </div>
                {{--<div class="favourable flex flex-direction-col">
                    <h3 class="flex flex-jfcontent-center flex-align-items-center">FAVOURABLE</h3>
                    <img src="/images/goods/favourate.png">
                </div>--}}
            </div>
        </div>
        <div class="goods_detail flex flex-direction-col">
            <div class="detail_head flex">
                <h3 class="current flex flex-jfcontent-center flex-align-items-center">产品描述(Description)</h3>
                <h3 class="flex flex-jfcontent-center flex-align-items-center">咨询记录(共{{count($inquiryList)}}条)</h3>
                <h3 class="flex flex-jfcontent-center flex-align-items-center">在线询价</h3>
            </div>
            <div class="detail_container">
                <div class="detail_description current flex flex-direction-col flex-align-items-center">
                    <div class="goods_params flex flex-align-items-center">
                        <div class="detail_items flex flex-jfcontent-center flex-align-items-center">产品参数</div>
                        <div class="goods_params_content flex flex-direction-col">
                            @foreach($goods_info->params as $pa)
                                <span><span>{{$pa->params_name}}：</span>{{$pa->params_value}}</span>
                            @endforeach
                        </div>
                    </div>
                    <div class="goods_describe flex flex-align-items-center">
                        <div class="detail_items flex flex-jfcontent-center flex-align-items-center">产品描述</div>
                        <div class="goods_describe_content">
                            {!! $goods_info->goods_content  !!}
                        </div>
                    </div>
                </div>
                <div class="detail_consult flex flex-direction-col unshow">
                    <div id="detail_consult_show">

                    </div>

                    <div id="Pagination" class="pagination flex flex-jfcontent-end"></div>
                    <div id="hiddenresult">
                        @foreach($inquiryList as $inquiry)
                            <div class="consult_item flex flex-direction-col">
                                <p>{{$inquiry->customer_name}}：{{$inquiry->content}}</p>
                                {{--<p class="reply"><span>回复:</span>回复内容回复内容</p>--}}
                                <span class="flex flex-jfcontent-end">{{$inquiry->inquiry_time}}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="detail_price unshow">
                    <form action="{{url('web/goods/addInquiry')}}" method="post" class="flex flex-direction-col"
                          id="inquiryForm">
                        <label class="consult_textarea flex flex-align-items-center">
                            <div class="detail_price_sub_obj">咨询内容：</div>
                            <textarea name="content"></textarea>
                        </label>
                        <label class="consult_contact flex flex-align-items-center">
                            <div class="detail_price_sub_obj">姓名：</div>
                            <input name="customer_name">
                        </label>
                        <label class="consult_contact flex flex-align-items-center">
                            <div class="detail_price_sub_obj">联系方式：</div>
                            <input name="customer_tel">
                        </label>
                        <input type="hidden" name="goods_id" value="{{$goods_info->id}}">
                        <label class="consult_submit flex flex-align-items-center flex-jfcontent-center">
                            提交
                        </label>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/js/jquery.imagezoom.js"></script>
    <script type="text/javascript" src="/js/jquery_form.js"></script>
    <script type="text/javascript" src="/vendor/layer/layer.js"></script>
    <script type="text/javascript" src="/vendor/pagination/lib/jquery.pagination.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".jqzoom").imagezoom();
            $("#thumblist li a").click(function () {
                //增加点击的li的class:tb-selected，去掉其他的tb-selecte
                $(this).parents("li").addClass("tb-selected").siblings().removeClass("tb-selected");
                //赋值属性
                $(".jqzoom").attr('src', $(this).find("img").attr("mid"));
                $(".jqzoom").attr('rel', $(this).find("img").attr("big"));
            });
            $(".detail_head h3").click(function () {
                $(this).addClass('current').siblings().removeClass('current');
                $('.detail_container>div:eq(' + $(this).index() + ')').removeClass('unshow').siblings().addClass('unshow');
            });
            $(".plus").click(function () {
                $(".goods_number").text(parseInt($(".goods_number").text()) + 1);
            });
            $(".minus").click(function () {
                $(".goods_number").text(parseInt($(".goods_number").text()) > 1 ? (parseInt($(".goods_number").text()) - 1) : parseInt($(".goods_number").text()));
            });
            $(".consult_submit").click(function () {
                if ($("[name=content]").val() == "") {
                    layer.msg('请填写咨询内容');
                    return false;
                }
                if ($("[name=customer_name]").val() == "") {
                    layer.msg('请填写姓名');
                    return false;
                }
                if ($("[name=customer_tel]").val() == "") {
                    layer.msg('请填写联系方式');
                    return false;
                }
                var reg = /^[1][0-9]{10}$/;
                if (!reg.test($("[name=customer_tel]").val())) {
                    layer.msg('请填写正确的手机号');
                    return false;
                }
                $("#inquiryForm").ajaxSubmit({
                    dataType: 'json',
                    success: function (response) {
                        layer.msg(response.msg);
                        $("[name=content]").val('');
                        $("[name=customer_name]").val('');
                        $("[name=customer_tel]").val('');
                    }
                })
            });
        })
        var pageSize = 2;
        var initPagination = function () {
            var num_entries = $("#hiddenresult .consult_item").length;
            // 创建分页
            $("#Pagination").pagination(num_entries, {
                num_edge_entries: 1, //边缘页数
                num_display_entries: 4, //主体页数
                callback: pageselectCallback,
                items_per_page: pageSize,//每页显示1项
                prev_text: '上一页',
                next_text: '下一页'
            });
        }();
        function pageselectCallback(page_index, jq) {
            $("#detail_consult_show").empty();
            var new_content = '';
            for (var i = (page_index * pageSize); i < ((page_index + 1) * pageSize); i++) {
                new_content = $("#hiddenresult .consult_item:eq(" + i + ")").clone();
                $("#detail_consult_show").append(new_content);
            }
            return false;
        }
        $('#goods_qrcode').mousemove(function () {
            $('#goods_qrcode_img').removeClass('hiddenresult');
        });
        $('#goods_qrcode').mouseout(function () {
            $('#goods_qrcode_img').addClass('hiddenresult');
        });
    </script>
@stop
