<div id="wrapper" class="m-wrapper">
    <div id="container" class="m-container">
        <!--头部，相册选择和格式选择-->

        <div id="uploader" class="m-uploader">
            <div class="queueList">
                <div id="dndArea" class="placeholder">
                    <div id="filePicker"></div>
                    <p>或将照片拖到这里，单次最多可选300张</p>
                </div>
            </div>
            <div class="statusBar" style="display:none;">
                <div class="progress">
                    <span class="text">0%</span>
                    <span class="percentage"></span>
                </div><div class="info"></div>
                <div class="btns">
                    <div id="filePicker2" class="m-addbutton"></div><input type="hidden" id="successfiles"><div class="uploadBtn">开始上传</div>
                </div>
            </div>
        </div>
    </div>
</div>
