@extends('layouts.wap',['foot'=>3,'title'=>'检测流程'])
@section('style')
    <style>
        .bd img{
            max-width: 100%;
        }
    </style>
@endsection
@section('body')
    <div class="wrap">
        @include('layouts.wap_head',['sub_title'=>'检测流程'])
        <div class="tabs" id="news_detail_tab">
            {{--<div class="tabs_nav">
                <a class="tab_nav_a active">热门产品</a>
                <a class="tab_nav_a">行业动态</a>
                <a class="tab_nav_a">技术文章</a>
            </div>--}}
            <div class="tabs_content">
                <div class="tab_plan" style="display: block;">
                    <div class="news_detail">
                        {!! !empty($cate_article)?$cate_article->content:'' !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


