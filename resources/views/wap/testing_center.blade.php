@extends('layouts.wap',['foot'=>3,'title'=>'检测中心'])
@section('style')
<style>
    .item{
        cursor: pointer;
    }
</style>
@endsection
@section('body')
    <div class="wrap news_index">
        @include('layouts.wap_head',['sub_title'=>'检测中心'])
        <div class="tabs" id="news_list_tab">
            <div class="lin" style="text-align: center">
                <span><h1>检测服务项目</h1></span>
            </div>
            <div class="item" style="text-align: center;">
                <a href="{{url('mobile/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_SHJC,\App\NewsCategory::$DEFAULT_CATE_SHJCLC])}}">
                    @foreach($cate_article_list as $v)
                        @if($v->id == \App\NewsCategory::$DEFAULT_CATE_SHJC)
                            <img style="width: 80%;" src="{{$v->getFullPicturePath()}}">
                        @endif
                    @endforeach
                    <div style="margin-top: 15px;font-size: 18px;text-align: center;color: #000;">生化检测</div>
                </a>
            </div>
            <div class="item" style="text-align: center;">
                <a href="{{url('mobile/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_MYJC,\App\NewsCategory::$DEFAULT_CATEMYJCLC])}}">
                    @foreach($cate_article_list as $v)
                        @if($v->id == \App\NewsCategory::$DEFAULT_CATE_MYJC)
                            <img style="width: 80%;" src="{{$v->getFullPicturePath()}}">
                        @endif
                    @endforeach
                    <div style="margin-top: 15px;font-size: 18px;text-align: center;color: #000;">免疫检测</div>
                </a>
            </div>
            <div class="item" style="text-align: center;">
                <a href="{{url('mobile/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_KTZB,\App\NewsCategory::$DEFAULT_CATEKYZBLC])}}">
                    @foreach($cate_article_list as $v)
                        @if($v->id == \App\NewsCategory::$DEFAULT_CATE_KTZB)
                            <img style="width: 80%;" src="{{$v->getFullPicturePath()}}">
                        @endif
                    @endforeach
                    <div style="margin-top: 15px;font-size: 18px;text-align: center;color: #000;">抗体制备</div>
                </a>
            </div>
            <div class="item" style="text-align: center;">
                <a href="{{url('mobile/testing_sub',[\App\NewsCategory::$DEFAULT_CATE_SJHDZ,\App\NewsCategory::$DEFAULT_CATESJHDZLC])}}">
                    @foreach($cate_article_list as $v)
                        @if($v->id == \App\NewsCategory::$DEFAULT_CATE_SJHDZ)
                            <img style="width: 80%;" src="{{$v->getFullPicturePath()}}">
                        @endif
                    @endforeach
                    <div style="margin-top: 15px;font-size: 18px;text-align: center;color: #000;">试剂盒定制</div>
                </a>
            </div>
        </div>
@endsection
@section('script')
    <script>
        /*var pageNumber = 1;
        var pageSize = 10;
        var loading = false;
        var hasMore = true;
        var cate_id = 0;
        function getList(){
            loading = true;
            loading_show();
            $.ajax({
                url:"{{url('mobile/news')}}",
                type:"POST",
                data:{pageNumber:pageNumber,pageSize:pageSize,cate_id:cate_id},
                dataType:'html',
                success: function (res) {
                    if(res == ""){
                        hasMore = false;
                    }else{
                        $(".tab_plan .news_list").append(res);
                    }
                },
                complete: function (res) {
                    loading = false;
                    loading_hide();
                }
            })
        }
        getList();
        //tab切换
        $("#news_list_tab").find(".tab_nav_a").click(function(){
             $(this).addClass('active').siblings().removeClass('active');
             var os = $(this).attr('os');
             window.scrollTo(0,0);
             if(cate_id != parseInt(os)){
                 cate_id = parseInt(os);
                 pageNumber = 1;
                 loading = false;
                 hasMore = true;
                $(".tab_plan .news_list").html('');
                getList();
             }
         });*/
        //滚动条事件
        $(".news_index").scroll(function(){
            var h = $(this).height();//div可视区域的高度
            var sh = $(this)[0].scrollHeight;//滚动的高度，$(this)指代jQuery对象，而$(this)[0]指代的是dom节点
            var st =$(this)[0].scrollTop;//滚动条的高度，即滚动条的当前位置到div顶部的距离
            if(h+st>=sh  && !loading && hasMore){
                //上面的代码是判断滚动条滑到底部的代码
                pageNumber++;
                getList();
            }
        })
    </script>
@endsection
