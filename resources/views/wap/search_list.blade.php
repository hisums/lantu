@foreach($list as $item)
    <div class="item" style="width: 95%;" onclick="javascript:window.location.href ='{{url('mobile/product_detail',[$item['goods_id'],$item['id']])}}'">
        <div class="title" style="text-align: left;">{{$item['getGoods']->goods_name}}</div>
        <br>
        <div class="img" style="float: left;">
            {{--<span class="top">TOP 1</span>--}}
            <img src="{{$item['getGoods']->getFullPicturePath()}}">
        </div>
        <div class="info" style="float: right;width: 50%">
            <p style="text-align: left;">品牌:{{$item['getGoods']['goods_vendor']}}  &nbsp;&nbsp;</p>
            <p style="text-align: left;">货号:{{$item['goods_number']}}</p>
            <p style="text-align: left;">规格:{{$item['goods_spec']}}</p>
            <br>
            <p style="text-align: left; color: #edb970;"><strong>￥{{$item['price']}}</strong></p>
        </div>
       {{-- <p class="name">{{$item['getGoods']['goods_name']}}({{$item['goods_spec']}})</p>
        <p class="num">￥{{$item['price']}}</p>--}}
    </div>
@endforeach
