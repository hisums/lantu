@extends('layouts.wap',['foot'=>3,'title'=>'资讯中心'])
@section('style')
<style>
    .item{
        cursor: pointer;
    }
    .tab_nav_a{
        width: 20%;
        margin: 0 5px;
    }
</style>
@endsection
@section('body')
    <div class="wrap news_index">
        @include('layouts.wap_head',['sub_title'=>'资讯中心'])
        <div class="tabs" id="news_list_tab">
            <div class="tabs_nav">
                <a class="tab_nav_a active" os="0">所有资讯</a>
                <a class="tab_nav_a" os="{{\App\NewsCategory::$DEFAULT_CATE_CXHD}}">促销活动</a>
                <a class="tab_nav_a" os="{{\App\NewsCategory::$DEFAULT_CATE_CPFK}}">行业动态</a>
                <a class="tab_nav_a" os="{{\App\NewsCategory::$DEFAULT_CATE_YNJD}}">技术文章</a>
            </div>
            <div class="tabs_content">
                <div class="tab_plan" style="display: block;">
                    <div class="news_list">
                        {{--<div class="item">
                            <div class="img"><img src="/wap/images/img2.jpg" alt=""></div>
                            <div class="txt">
                                <h2>新陈代谢分析试剂盒</h2>
                                <p>抗原与免疫动物的种属差异越远越好。动物必须适龄、健壮、无感染、体重合乎要求。不同动物对同一免疫原有不同的免疫应答表现。因此针对不同性质的免疫原医学.教育.网编辑整理，选用相应的动物。蛋白质抗原大部分动物皆适合；甾类激素多用家免；酶类多用豚鼠。……<a class="more" href="news_detail.html">[详情]</a></p>
                            </div>
                        </div>--}}

                    </div>
                    {{--<div class="tab_plan">行业动态</div>
                    <div class="tab_plan">技术文章</div>--}}
                </div>
            </div>
        </div>
@endsection
@section('script')
    <script>
        var pageNumber = 1;
        var pageSize = 10;
        var loading = false;
        var hasMore = true;
        var cate_id = 0;
        function getList(){
            loading = true;
            loading_show();
            $.ajax({
                url:"{{url('mobile/news')}}",
                type:"POST",
                data:{pageNumber:pageNumber,pageSize:pageSize,cate_id:cate_id},
                dataType:'html',
                success: function (res) {
                    if(res == ""){
                        hasMore = false;
                    }else{
                        $(".tab_plan .news_list").append(res);
                    }
                },
                complete: function (res) {
                    loading = false;
                    loading_hide();
                }
            })
        }
        getList();
        //tab切换
        $("#news_list_tab").find(".tab_nav_a").click(function(){
             $(this).addClass('active').siblings().removeClass('active');
             var os = $(this).attr('os');
             window.scrollTo(0,0);
             if(cate_id != parseInt(os)){
                 cate_id = parseInt(os);
                 pageNumber = 1;
                 loading = false;
                 hasMore = true;
                $(".tab_plan .news_list").html('');
                getList();
             }
         });
        //滚动条事件
        $(".news_index").scroll(function(){
            var h = $(this).height();//div可视区域的高度
            var sh = $(this)[0].scrollHeight;//滚动的高度，$(this)指代jQuery对象，而$(this)[0]指代的是dom节点
            var st =$(this)[0].scrollTop;//滚动条的高度，即滚动条的当前位置到div顶部的距离
            if(h+st>=sh  && !loading && hasMore){
                //上面的代码是判断滚动条滑到底部的代码
                pageNumber++;
                getList();
            }
        })
    </script>
@endsection
