@extends('layouts.wap',['foot'=>1,'title'=>'蓝图生物科技'])
@section('style')
    <style>
        .item{
            cursor: pointer;
        }
        .banner_box{
            position: relative;
            top: 45px;
        }
        .search{
            position: absolute;
            left: 10%;
            top: 55px;
            z-index:100;
            width: 80%;
            display:flex;
        }
        .search form{
            width: 100%;
        }
        .search input{
            border:solid 1px #777;
            border-radius:10px;
            width: 100%;
            height: 30px;
        }
        .list{
            width: 100%;
        }
        .left{
            width: 48%;
            margin: 1%;
            float: left;
        }
        .right{
            width: 48%;
            margin: 1%;
            float: left;
        }
        .about_us{
            padding: 0 20px;
            width: 98%;
        }
        .about_us .left{
            width: 58%;
            margin: 1%;
            float: left;
        }
        .about_us .right{
            width: 38%;
            margin: 1%;
            float: left;
        }
        .swiper-container{
            width:100% !important;
        }
        .search div{
            position: absolute;
            right: 5px;
            top: 5px;
        }
    </style>
    <link rel="stylesheet" href="/js/swiper/css/swiper.min.css">
@endsection
@section('body')
    <div class="wrap index" style="margin-bottom: 30px">
        <div class="header">
            <img src="/images/wap/header.png" width="100%" alt="">
        </div>
        <div class="banner" style="margin-bottom: 45px;margin-top: 20px">
            <div class="banner_box swiper-container">
                <div class="swiper-wrapper">
                    @foreach($banners as $banner)
                        <a href="{{url('mobile/news_detail',[$banner->id])}}" class="banner swiper-slide"><img src="{{$banner->getFullPicturePath()}}"></a>
                    @endforeach
                </div>
            </div>
            <div class="search">
                <form action="{{url('mobile/search')}}" id="searchForm"><input type="text" name="keywords" placeholder="搜索商品" ></form>
                <div id="searchIcon"><img src="/images/wap/search.png" style="width: 20px; height: 20px; float: right;z-index: 999;" alt=""></div>
            </div>
        </div>
        <div class="list" style="overflow:hidden; padding: 20px 0 5px; margin: 15px 0">
            <div class="left">
                <a href="{{url('mobile/product_category')}}"><img src="/images/wap/product.png" style="width: 100% ;height: 100%" alt=""></a>
            </div>
            <div class="right">
                <a href="{{url('mobile/testing_center')}}"><img src="/images/wap/testing_center.png" style="width: 100% ;height: 100%" alt=""></a>
            </div>
            <div class="left">
                <a href="{{url('mobile/news')}}"><img src="/images/wap/article_list.png" style="width: 100% ;height: 100%" alt=""></a>
            </div>
            <div class="right">
                <a href="{{url('http://bbs.lantubio.com/index.php?m=bbs')}}"><img src="/images/wap/bbs.png" style="width: 100% ;height: 100%" alt=""></a>
            </div>
        </div>
        <div class="about_us" style="overflow:hidden;">
            <p style="text-align: center;color:#777 ;font-size: larger;">联系我们</p>
            <div class="left" style="margin-top: 40px; ">
                <h3 style="color: #247dd9;">关于蓝图</h3>
                <br>
                <br>
                <p>黄石市蓝图生物科技有限公司</p>
                <br>
                <p>电话:0714-6338558</p>
                <br>
                <p>传真:0714-6338558</p>
                <br>
                <p>邮箱:355902222@qq.com</p>
                <br>
                <p>qq:355902222</p>
                <br>
                <p>湖北省黄石市黄石港区磁湖路41号</p>
                <br>
                <p><span>备案号:</span>鄂ICP备18013916号-1</p>
            </div>
            <div class="right" style="margin-top: 40px; ">
                <h3 style="color: #247dd9;">蓝图官方二维码</h3>
                <br>
                <br>
                <p>微信扫描二维码关注我们</p>
                <br>
                <br>
                <img style="width: 120px;height: 120px" src="/images/wap/wechat.png">
            </div>
        </div>
       {{-- <div class="footer">
            <div style="width: 25%;float: left"><a href="{{url('mobile')}}"><img src="/wap/img/icon_home_h.png" alt=""></a><br>首页</div>
            <div style="width: 25%;float: left"><a href="{{url('mobile/product')}}"><img src="/wap/img/icon_pro.png" alt=""></a><br>产品</div>
            <div style="width: 25%;float: left"><a href="{{url('mobile/news')}}"><img src="/wap/img/icon_news.png" alt=""></a><br>资讯</div>
            <div style="width: 25%;float: left"><a href="{{url('mobile/about_us')}}"><img src="/wap/img/icon_my.png" alt=""></a><br>关于我们</div>
        </div>--}}
    </div>

@endsection
@section('script')
<script src="/js/swiper/js/swiper.min.js"></script>
<script>
    var banner_slider = new Swiper('.banner_box', {
        speed:500,
        loop : true,
        autoplay: {
            delay: 2000,
            stopOnLastSlide: false,
            disableOnInteraction: false,
        }
    });
    $("#searchIcon").click(function () {
        $("#searchForm").submit();
    });
</script>
@endsection
