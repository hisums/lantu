@extends('layouts.wap',['foot'=>2,'title'=>'产品分类'])
@section('body')
    <div class="wrap about">
        @include('layouts.wap_head',['sub_title'=>'产品分类'])
        <div class="bg">
            <div class="class_list">
                <a class="title on" href="{{url('mobile/product',[$cate_id])}}">全部分类</a>
                @foreach($list as $item)
                    @if(\App\GoodsCategory::cateSubCount($item->id) > 0)
                        <a href="{{url('mobile/product_category',[$item->id])}}">{{$item->goods_name}} <i class="icon_arrow"></i></a>
                    @else
                        <a href="{{url('mobile/product',[$item->id])}}">{{$item->goods_name}} <i class="icon_arrow"></i></a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection