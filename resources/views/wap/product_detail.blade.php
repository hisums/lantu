@extends('layouts.wap',['foot'=>2,'title'=>'产品详情'])
@section('body')
    <div class="wrap">
        @include('layouts.wap_head',['sub_title'=>'产品详情'])
        <div class="product_detail">
            <img src="{{$info['goods_info']->getFullPicturePath()}}" alt="" class="product_detail_img">
            <h2 class="h2">{{$info['goods_info']->goods_name}}</h2>
            <p class="p">商品价格 <strong class="orange">￥{{$info['spec_info']->price}}</strong></p>
            <div class="info">
                <div class="span tit">
                    <strong>名称:</strong>{{$info['goods_info']->goods_name}}
                </div>
                <div class="span">
                    <strong>货号:</strong>{{$info['spec_info']->goods_number}}
                </div>
                <div class="span">
                    <strong>品牌名称:</strong>{{$info['goods_info']->goods_vendor}}
                </div>
                <div class="span">
                    <strong>货期:</strong>@foreach(\App\GoodsSpecs::$GOODS_DELIVERY_TIME as $dt)
                                            @if($dt['key'] == $info['spec_info']->delivery_time)
                                                {{$dt['text']}}
                                            @endif
                                        @endforeach
                </div>
                <div class="span">
                    <strong>规格:</strong>
                    @foreach($info['goods_info']->spec as $sp)
                        <a href="{{url('mobile/product_detail',[$info['goods_info']->id,$sp->id])}}"
                           @if($sp->id == $info['spec_info']->id) class="select" @endif>{{$sp->goods_spec}}</a>
                    @endforeach
                </div>
            </div>


            <div class="tabs" id="product_detail_tab">
                <div class="tabs_nav">
                    <a class="tab_nav_a active" style="width: 33%">产品参数</a>
                    <a class="tab_nav_a" style="width: 33%">产品描述</a>
                    <a class="tab_nav_a" style="width: 33%">文档下载</a>
                </div>
                <div class="tabs_content">
                    <div class="tab_plan" style="display: block;">
                        <div class="product_detail_txt">
                            @foreach($info['goods_info']->params as $pa)
                                <p>{{$pa->params_name}}：{{$pa->params_value}}</p>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab_plan">

                        <div class="product_detail_txt">
                            {!! $info['goods_info']->goods_content  !!}
                        </div>
                    </div>
                    <div class="tab_plan">

                        <div class="product_detail_txt">
                            @foreach($fileInfo as $v)
                                <p>
                                    <a href="{{$v->getFullPath()}}">{{$v->name}}</a>
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $("#product_detail_tab").find(".tab_nav_a").click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        $("#product_detail_tab .tabs_content .tab_plan:eq("+$(this).index()+")").show().siblings().hide();
    });
</script>
@endsection