@extends('layouts.wap',['foot'=>4,'title'=>'关于我们'])
@section('body')
    <div class="wrap about">
        @include('layouts.wap_head',['sub_title'=>'关于我们'])
        <div class="bg">
            <div class="banner"><img src="/wap/img/logo.png" alt=""></div>
            <div class="link">
                <div class="img">
                    <a href="https://weibo.com/6516530126/profile?topnav=1&wvr=6&is_all=1" class="sina"><img src="/wap/img/sina.png" alt=""></a>
                    <a href="https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzI2NzEzODQ4MA==&scene=124#wechat_redirect" class="wechar"><img src="/wap/img/wechar.png" alt=""></a>
                    <a href="http://bbs.lantubio.com/index.php?m=bbs" class="bbs"><img src="/wap/img/bbs.png" alt=""></a>
                </div>

                <div class="txt">
                    <a href="https://weibo.com/6516530126/profile?topnav=1&wvr=6&is_all=1" class="sina">官方微博</a>
                    <a href="https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzI2NzEzODQ4MA==&scene=124#wechat_redirect" class="wechar">微信公众号</a>
                    <a href="http://bbs.lantubio.com/index.php?m=bbs" class="bbs">蓝图论坛</a>
                </div>
            </div>

            <div class="hd">联系我们</div>

            <div class="form_group">
                <label class="form_label">联系人:</label>
                <span class="input_txt">陈经理</span>
            </div>
            <div class="form_group">
                <label class="form_label">手机号:</label>
                <span class="input_txt">13559590085</span>
            </div>
            <div class="form_group">
                <label class="form_label">固定电话:</label>
                <span class="input_txt">0714-6338558</span>
            </div>
            <div class="form_group">
                <label class="form_label">QQ号:</label>
                <span class="input_txt">355902222</span>
            </div>
            <div class="form_group">
                <label class="form_label">邮箱地址:</label>
                <span class="input_txt">355902222@qq.com</span>
            </div>
            <div class="form_group">
                <label class="form_label">地址:</label>
                <span class="input_txt">湖北省黄石市黄石港区磁湖路41号</span>
            </div>

            <div class="about_txt">
                <h6>蓝图生物简介</h6>
                {!! $about_us->content !!}
            </div>

        </div>
    </div>
@endsection

