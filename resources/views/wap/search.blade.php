@extends('layouts.wap',['foot'=>2,'title'=>'产品列表'])
@section('style')
    <style>
        .item{
            cursor: pointer;
        }
    </style>
@endsection
@section('body')
    <div class="wrap product">
        @include('layouts.wap_head',['sub_title'=>'产品列表'])
        <div class="tabs" id="productTab">
            {{--<div class="tabs_nav">
                <a class="tab_nav_a active">热门产品</a>
            </div>--}}
            <div class="tabs_content">
                <div class="tab_plan" style="display: block;">
                    <div class="list clearfix">
                        {{--<div class="item">
                            <div class="img">
                                <span class="top">TOP 1</span>
                                <img src="/wap/images/img1.jpg" alt="">
                            </div>
                            <p class="name">人C肽(C-P)酶联免疫</p>
                            <p class="num">￥2810.00</p>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    var pageNumber = 1;
    var pageSize = 10;
    var loading = false;
    var hasMore = true;
    var keywords = "{{$keywords}}";
    function getList(){
        loading = true;
        loading_show();
        $.ajax({
            url:"{{url('mobile/search')}}",
            type:"POST",
            data:{pageNumber:pageNumber,pageSize:pageSize,keywords:keywords},
            dataType:'html',
            success: function (res) {
                if(res == ""){
                    hasMore = false;
                }else{
                    $(".tab_plan .list").append(res);
                }
            },
            complete: function (res) {
                loading = false;
                loading_hide();
            }
        })
    }
    getList();
    //tab切换
    /*$("#orderTab").find(".tab_nav_a").click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        var os = $(this).attr('os');
        window.scrollTo(0,0);
        if(order_status != parseInt(os)){
            order_status = parseInt(os);
            pageNumber = 1;
            loading = false;
            hasMore = true;
            $(".tab_plan .list").html('');
            getList();
        }
    });*/
    //滚动条事件
    $(".product").scroll(function(){
        var h = $(this).height();//div可视区域的高度
        var sh = $(this)[0].scrollHeight;//滚动的高度，$(this)指代jQuery对象，而$(this)[0]指代的是dom节点
        var st =$(this)[0].scrollTop;//滚动条的高度，即滚动条的当前位置到div顶部的距离
        if(h+st>=sh  && !loading && hasMore){
            //上面的代码是判断滚动条滑到底部的代码
            pageNumber++;
            getList();
        }
    })
</script>
@endsection
