@foreach($list as $item)
    <div class="item" onclick="javascript:window.location.href ='{{url('mobile/news_detail',[$item['id']])}}'">
        <div class="img"><img src="{{$item['getFullPicturePath']}}" alt=""></div>
        <div class="txt">
            <h2>{{$item['subject']}}</h2>
            <p>{{strip_tags($item['content'])}}<a class="more" href="{{url('mobile/news_detail',[$item['id']])}}">[详情]</a></p>
        </div>
    </div>
@endforeach
