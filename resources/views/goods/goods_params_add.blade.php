<button style="margin: 20px 0 0 20px" class="layui-btn layui-btn-normal"
        lay-filter="{{makeElUniqueName('returnToSpecs')}}"><i
            class="layui-icon">&#xe654; </i> 返回
</button>

<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 属性名称</label>
        <div class="layui-input-block">
            <input type="hidden" value="{{isset($goods_attr)?$goods_attr->id:''}}" name="{{makeElUniqueName('id')}}">
            <input type="hidden" value="{{$goods_id}}"  name="{{makeElUniqueName('goods_id')}}">
            <input type="text"
                   name="{{makeElUniqueName('params_name')}}" value="{{isset($goods_attr)?$goods_attr->params_name:''}}"
                   required lay-verify="required"
                   placeholder="属性名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 属性值</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('params_value')}}" value="{{isset($goods_attr)?$goods_attr->params_value:''}}"
                   required lay-verify="required"
                   placeholder="属性值，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('sort_order')}}" value="{{isset($goods_attr)?$goods_attr->sort_order:''}}"
                   required lay-verify="required"
                   placeholder="排序，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('goods_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
    layui.use(['form', 'validator', 'uploadUtil', 'reditorUtil'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;
        var reditorUtil = layui.reditorUtil;
        var goodsId = {{$goods_id}};
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();
        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('goods_save')}})', function (data) {
            var index = layer.load(1);
            var url = '/goods/goods/addAttributeSpecs';
            var postParam = {
                goods_id: data.field['{{makeElUniqueName('goods_id')}}'],
                params_name: data.field['{{makeElUniqueName('params_name')}}'],
                params_value: data.field['{{makeElUniqueName('params_value')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}']
            };
            var id = data.field['{{makeElUniqueName('id')}}'];
            if (id != '') {
                //修改
                url = '/goods/goods/editAttribute';
                postParam.id = id;
            }
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose(function (dialogBox) {
                            $.get('/goods/goods/attribute/' + goodsId, {}, function (str) {
                                dialogBox.html(str);
                            });
                        });
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('returnToSpecs')}}\']').on('click', function () {
            var index = layer.load(1);
            $.ajax({
                contentType: "application/json",
                type: 'get',
                url: "/goods/goods/attribute/" + goodsId,
                success: function (outResult) {
                    var popLayerUtil = layui.popLayerUtil;
                    layer.close(index);
                    popLayerUtil.onClose(function (dialogBox) {
                        dialogBox.html(outResult);
                    });
                },
            });
        });
    });
</script>
