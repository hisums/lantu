<button style="margin: 20px 0 0 20px" class="layui-btn layui-btn-normal"
        lay-filter="{{makeElUniqueName('returnToSpecs')}}"><i
            class="layui-icon">&#xe654; </i> 返回
</button>

<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品规格</label>
        <div class="layui-input-block">
            <input type="hidden" value="{{isset($goods_specs)?$goods_specs->id:''}}" name="{{makeElUniqueName('id')}}">
            <input type="hidden" value="{{$goods_id}}" name="{{makeElUniqueName('goods_id')}}">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->goods_spec:''}}"
                   name="{{makeElUniqueName('goods_spec')}}"
                   required lay-verify="required"
                   placeholder="商品规格，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品货号或者SKU码</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->goods_number:''}}"
                   name="{{makeElUniqueName('goods_number')}}"
                   required lay-verify="required"
                   placeholder="商品货号或者SKU码，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品库存</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->inventory:''}}"
                   name="{{makeElUniqueName('inventory')}}"
                   required lay-verify="required"
                   placeholder="商品库存，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 销售价格</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->price:''}}"
                   name="{{makeElUniqueName('price')}}"
                   required lay-verify="required"
                   placeholder="销售价格，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 原始价格</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->origin_price:''}}"
                   name="{{makeElUniqueName('origin_price')}}"
                   required lay-verify="required"
                   placeholder="原始价格，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 进货价格</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->cost_price:''}}"
                   name="{{makeElUniqueName('cost_price')}}"
                   required lay-verify="required"
                   placeholder="进货价格，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 货期</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('delivery_time')}}" lay-search required lay-verify="required">
                @foreach (\App\GoodsSpecs::$GOODS_DELIVERY_TIME as $v)
                    <option {{isset($goods_specs) && $goods_specs->delivery_time == $v['key']?'selected="selected"':''}} value="{{$v['key']}}">{{ $v['text'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 销售单位</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->unit:''}}" name="{{makeElUniqueName('unit')}}"
                   required lay-verify="required"
                   placeholder="销售单位，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 最低起购数量</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->min_buy_amount:''}}"
                   name="{{makeElUniqueName('min_buy_amount')}}"
                   required lay-verify="required"
                   placeholder="最低起购数量，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->sort_order:''}}"
                   name="{{makeElUniqueName('sort_order')}}"
                   required lay-verify="required"
                   placeholder="排序，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 是否上架</label>
        <div class="layui-input-block">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}" value="{{\App\GoodsSpecs::$GOODS_SPECS_ON}}"
                   {{isset($goods_specs) && $goods_specs->display_flag == \App\GoodsSpecs::$GOODS_SPECS_ON?'checked':''}} {{!isset($goods_specs)?'checked':''}} class="layui-input"
                   title="是">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}"
                   value="{{\App\GoodsSpecs::$GOODS_SPECS_OFF}}"
                   {{isset($goods_specs) && $goods_specs->display_flag == \App\GoodsSpecs::$GOODS_SPECS_OFF?'checked':''}} class="layui-input"
                   title="否">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品橱窗图片</label>
        <div style="display: flex;width: 600px;flex-wrap: wrap;" class="layui-input-block">
            <div style="width: 180px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture1')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath1():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id1')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic1:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic1')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id1')}}">
                </div>
            </div>
            <div style="width: 180px;left:10px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture2')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath2():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id2')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic2:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic2')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id2')}}">
                </div>
            </div>
            <div style="width: 180px;left: 20px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture3')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath3():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id3')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic3:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic3')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id3')}}">
                </div>
            </div>
            <div style="width: 180px;top: 10px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture4')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath4():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id4')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic4:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic4')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id4')}}">
                </div>
            </div>
            <div style="width: 180px;left:10px;top:10px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture5')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath5():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id5')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic5:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic5')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id5')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('goods_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
    layui.use(['form', 'validator', 'uploadUtil', 'reditorUtil'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;
        var reditorUtil = layui.reditorUtil;
        var goodsId = {{$goods_id}};
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();

        var descIndex = reditorUtil.doInitEditor({elemId: '{{makeElUniqueName('goods_content')}}'});
        uploadUtil.doUpload({
            success: function (fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('goods_spec_pic1')}}') {
                    $('#{{makeElUniqueName('picture1')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id1')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic2')}}') {
                    $('#{{makeElUniqueName('picture2')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id2')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic3')}}') {
                    $('#{{makeElUniqueName('picture3')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id3')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic4')}}') {
                    $('#{{makeElUniqueName('picture4')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id4')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic5')}}') {
                    $('#{{makeElUniqueName('picture5')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id5')}}\']').val(fileId);
                }
            }
        });
        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('goods_save')}})', function (data) {
            var index = layer.load(1);
            var url = '/goods/goods/addGoodsSpecs';
            var postParam = {
                goods_id: data.field['{{makeElUniqueName('goods_id')}}'] || 1,
                goods_spec: data.field['{{makeElUniqueName('goods_spec')}}'],
                goods_number: data.field['{{makeElUniqueName('goods_number')}}'],
                inventory: data.field['{{makeElUniqueName('inventory')}}'],
                price: data.field['{{makeElUniqueName('price')}}'],
                cost_price: data.field['{{makeElUniqueName('cost_price')}}'],
                origin_price: data.field['{{makeElUniqueName('origin_price')}}'],
                unit: data.field['{{makeElUniqueName('unit')}}'],
                min_buy_amount: data.field['{{makeElUniqueName('min_buy_amount')}}'],
                goods_spec_pic1: data.field['{{makeElUniqueName('goods_picture_file_id1')}}'],
                goods_spec_pic2: data.field['{{makeElUniqueName('goods_picture_file_id2')}}'],
                goods_spec_pic3: data.field['{{makeElUniqueName('goods_picture_file_id3')}}'],
                goods_spec_pic4: data.field['{{makeElUniqueName('goods_picture_file_id4')}}'],
                goods_spec_pic5: data.field['{{makeElUniqueName('goods_picture_file_id5')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
                display_flag: data.field['{{makeElUniqueName('display_flag')}}'],
                delivery_time: data.field['{{makeElUniqueName('delivery_time')}}'],
            };
            var id = data.field['{{makeElUniqueName('id')}}'];
            if (id != '') {
                //修改
                url = '/goods/goods/editSpecs';
                postParam.id = id;
            }
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose(function (dialogBox) {
                            $.get('/goods/goods/specs/' + goodsId, {}, function (str) {
                                dialogBox.html(str);
                            });
                        });
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('returnToSpecs')}}\']').on('click', function () {
            var index = layer.load(1);
            $.ajax({
                contentType: "application/json",
                type: 'get',
                url: "/goods/goods/specs/" + goodsId,
                success: function (outResult) {
                    var popLayerUtil = layui.popLayerUtil;
                    layer.close(index);
                    popLayerUtil.onClose(function (dialogBox) {
                        dialogBox.html(outResult);
                    });
                },
            });
        });
    });
</script>
