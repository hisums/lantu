<button style="margin-left: 20px;margin-top: 20px;;" class="layui-btn layui-btn-normal layui-btn-warning"
        lay-filter="{{makeElUniqueName('addSpecs')}}"><i class="layui-icon">&#xe654;</i> 新增属性
</button>
<table lay-even class="layui-table">
    <style>
        input {
            width: 200px !important;
        }
    </style>
    <colgroup>
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
    </colgroup>
    <thead>
    <tr>
        <th>商品规格</th>
        <th>商品货号或者SKU码</th>
        <th>商品库存(单个型号库存)</th>
        <th>销售价格</th>
        <th>原始价格</th>
        <th>进货价格</th>
        <th>销售单位</th>
        <th>最低起购数量</th>
        <th>显示顺序</th>
        <th>是否显示</th>
        <th>货期</th>
        <th>图片</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    @foreach($goods_specs as $goods_spec)
        <tr>
            <td>
                <div style="width: 200px;">{{$goods_spec['goods_spec']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['goods_number']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['inventory']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['price']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['origin_price']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['cost_price']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['unit']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['min_buy_amount']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['sort_order']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['display_flag_text']}}</div>
            </td>
            <td>
                <div style="width: 200px;">{{$goods_spec['delivery_time_text']}}</div>
            </td>
            <td>
                <img src="{{$goods_spec['getFullPicturePath1']}}"/>
            </td>
            <td>
                <div style="width: 200px;">
                    <button id="{{$goods_spec['id']}}" class="layui-btn layui-btn-normal layui-btn-warning"
                            lay-filter="{{makeElUniqueName('editSpecs')}}"><i class="layui-icon">&#xe654;</i> 编辑
                    </button>
                    <button id="{{$goods_spec['id']}}" class="layui-btn layui-btn-normal layui-btn-warning"
                            lay-filter="{{makeElUniqueName('removeSpecs')}}"><i class="layui-icon">&#xe654;</i> 删除
                    </button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>
    layui.use([], function () {
        var $ = layui.jquery;
        var goodsId = {{$goods_id}};
        $('.layui-btn[lay-filter=\'{{makeElUniqueName('addSpecs')}}\']').on('click', function () {
            var index = layer.load(1);
            $.ajax({
                contentType: "application/json",
                type: 'get',
                url: "/goods/goods/addGoodsSpecsIndex/" + goodsId,
                success: function (outResult) {
                    var popLayerUtil = layui.popLayerUtil;
                    layer.close(index);
                    popLayerUtil.onClose(function (dialogBox) {
                        dialogBox.html(outResult);
                    });
                },
            });
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('removeSpecs')}}\']').on('click', function () {
            var that = this;
            layer.confirm('确定删除该商品？', {
                btn: ['确定', '放弃'],
                icon: 3
            }, function () {
                var index = layer.load(1);
                var id = $(that).attr('id');
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: "/goods/goods/delSpecs/" + id,
                    success: function (outResult) {
                        layer.close(index);
                        var popLayerUtil = layui.popLayerUtil;
                        if (outResult.Success) {
                            layer.msg(outResult.Message, {icon: 6});
                            //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                            //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                            //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                            popLayerUtil.onClose(function (dialogBox) {
                                $.get('/goods/goods/specs/' + goodsId, {}, function (str) {
                                    dialogBox.html(str);
                                });
                            });
                        } else {
                            layer.msg(outResult.Message, {icon: 5});
                        }
                    }
                });
            }, function () {
            });


        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('editSpecs')}}\']').on('click', function () {
            var index = layer.load(1);
            var id = $(this).attr('id');
            $.ajax({
                contentType: "application/json",
                type: 'get',
                url: "/goods/goods/editSpecsIndex/" + id,
                success: function (outResult) {
                    var popLayerUtil = layui.popLayerUtil;
                    layer.close(index);
                    popLayerUtil.onClose(function (dialogBox) {
                        dialogBox.html(outResult);
                    });
                },
                onClose: function (callback) {
                    callback.call(this, $('#{{makeElUniqueName('editGoodsSpecs')}}'));
                }
            });
        });
    })


</script>