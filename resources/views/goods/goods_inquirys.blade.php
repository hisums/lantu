<div class="layui-form layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">商品名称</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="商品名称" name="{{makeElUniqueName('goods_name')}}" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label">商品厂家</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="商品厂家" name="{{makeElUniqueName('goods_vendor')}}" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label">是否显示</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('display_flag')}}">
                    <option value="">(所有商品)</option>
                    <option value="">(所有商品)</option>
                    @foreach (\App\GoodsInquirys::$DISPLAY_FLAG_MAP as $item)
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">商品分类</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('goods_category')}}">
                    <option value="">(所有分类)</option>
                    @foreach ($all_cates as $cate)
                        <option value='{{$cate['id']}}'>{{$cate->getGoodsPath()}}</option>
                    @endforeach
                </select>
            </div>
            <label class="layui-form-label">是否已处理</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('process_status')}}">
                    <option value="">(所有商品)</option>
                    <option value="">(所有商品)</option>
                    @foreach (\App\GoodsInquirys::$PROCESS_STATUS_MAP as $item)
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px"></label>
            <div class="layui-input-inline layui-long-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_goods')}}"><i
                            class="layui-icon">
                        &#xe615;</i> 搜索
                </button>
            </div>
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbGoods')}}"></div>
<script>
    layui.use(['jfTable', 'form', 'addressUtil', 'dateRangeUtil'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var jfTable = layui.jfTable;
        var form = layui.form();
        var addressUtil = layui.addressUtil;
        var dateRangeUtil = layui.dateRangeUtil;

        form.render();
        layui.define(function (exports) {
            var obj = {
                showMore: function (id) {
                    $.get('/goods/goods/inquirysDetail/' + id, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('editGoodsIndex')}}',
                                title: '咨询内容',
                                type: 1,
                                content: str,
                                area: ['400px', '300px']
                            }),
                            onClose: function () {
                                layui.togoodsQueryFuncs.refreshTableGrid();
                            }
                        });
                    });
                },
                changeShow:function (id){
                    var index = layer.load(1);
                    $.get('/goods/goods/inquirysOperateShow/' + id, {}, function (outResult) {
                        layer.close(index);
                        if (outResult.Success) {
                            layer.msg(outResult.Message, {icon: 6});
                            layui.togoodsQueryFuncs.refreshTableGrid();
                        } else {
                            layer.msg(outResult.Message, {icon: 5});
                        }
                    });
                },
                doEdit: function (id) {
                    var index = layer.load(1);
                    $.get('/goods/goods/inquirysOperate/' + id, {}, function (outResult) {
                        layer.close(index);
                        if (outResult.Success) {
                            layer.msg(outResult.Message, {icon: 6});
                            layui.togoodsQueryFuncs.refreshTableGrid();
                        } else {
                            layer.msg(outResult.Message, {icon: 5});
                        }
                    });
                },
                refreshTableGrid: function () {
                    $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val('');
                    $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
                }
            };
            exports('togoodsQueryFuncs', obj);
        });

        $("#{{makeElUniqueName('tbGoods')}}").jfTable({
            url: '/goods/goods/inquirysQuery',
            pageSize: 5,
            page: true,
            skip: true,
            first: '首页',
            last: '尾页',
            columns: [{
                text: '操作',
                name: 'id',
                width: 200,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if (dataItem.process_status == {{\App\GoodsInquirys::$PROCESS_STATUS_UNFINISH}}) {
                        var html = '<a class="layui-btn layui-btn-small layui-btn-warning" onclick="layui.togoodsQueryFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe618;</i> 标记为已处理</a>';
                        return html;
                    } else {
                        return '<span style="color: green;">已处理</span>';
                    }
                }
            }, {
                text: '咨询人姓名',
                name: 'usersGet.nick_name',
                width: 100,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if (!value) {
                        return dataItem.customer_name;
                    }
                    return value;
                }
            }, {
                text: '咨询人电话',
                name: 'customer_tel',
                width: 170,
                align: 'center',
            }, {
                text: '咨询时间',
                name: 'inquiry_time',
                width: 170,
                align: 'center'
            }, {
                text: '咨询内容(点击查看详情)',
                name: 'content',
                width: 300,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    var html = '<a style="cursor: pointer;width:250px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" class="layui-btn-small layui-btn-warning" onclick="layui.togoodsQueryFuncs.showMore(' + dataItem.id + ')">' + value.substring(0,20) + '...</a>';
                    return html;
                }
            }, {
                text: '是否显示(点击修改)',
                name: 'display_flag_text',
                width: 150,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    var html = '';
                    if (dataItem.display_flag == {{\App\GoodsInquirys::$DISPLAY_FLAG_ON}}) {
                        html = '<a style="cursor: pointer;"onclick="layui.togoodsQueryFuncs.changeShow(' + dataItem.id + ')"><span style="color:green;">' + value + '</span></a>';
                    } else if (dataItem.display_flag == {{\App\GoodsInquirys::$DISPLAY_FLAG_OFF}}) {
                        html = '<a style="cursor: pointer;"onclick="layui.togoodsQueryFuncs.changeShow(' + dataItem.id + ')"><span style="color:red;">' + value + '</span></a>';
                    }
                    return html;
                }
            }, {
                text: '商品类别',
                name: 'goodsCategoryGet.goods_name',
                width: 120,
                align: 'center'
            }
            ],
            method: 'post',
            queryParam: {
                goods_name: $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val(),
                goods_vendor: $('input[name=\'{{makeElUniqueName('goods_vendor')}}\']').val(),
                display_flag: $('select[name=\'{{makeElUniqueName('display_flag')}}\']').val(),
                goods_category: $('select[name=\'{{makeElUniqueName('goods_category')}}\']').val(),
                process_status: $('select[name=\'{{makeElUniqueName('process_status')}}\']').val(),
            },
            toolbarClass: 'layui-btn-small',
            onBeforeLoad: function (param) {
                return $.extend(param, {
                    goods_name: $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val(),
                    goods_vendor: $('input[name=\'{{makeElUniqueName('goods_vendor')}}\']').val(),
                    display_flag: $('select[name=\'{{makeElUniqueName('display_flag')}}\']').val(),
                    goods_category: $('select[name=\'{{makeElUniqueName('goods_category')}}\']').val(),
                    process_status: $('select[name=\'{{makeElUniqueName('process_status')}}\']').val(),
                });
            },
            onLoadSuccess: function (data) {
                return data;
            },
            dataFilter: function (data) {
                return data;
            }
        });


        $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_goods')}}\']').on('click', function () {
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });
    });
</script>
