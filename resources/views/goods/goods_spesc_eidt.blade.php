<button style="margin: 20px 0 0 20px" class="layui-btn layui-btn-normal"
        lay-filter="{{makeElUniqueName('returnToSpecs')}}"><i
            class="layui-icon">&#xe654; </i> 返回
</button>

<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label">商品分类</label>
        <div class="layui-input-block">
            <input type="hidden" value="{{$goods_ids}}" name="{{makeElUniqueName('goods_ids')}}">
            <select name="{{makeElUniqueName('goods_category_id')}}" lay-search required lay-verify="required">
                <option value="0">不更改商品分类</option>
                @foreach ($leaf_cate as $cate)
                    <option value='{{$cate->id}}'>{{ $cate->getGoodsPath() }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">商品规格</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('goods_spec')}}"
                   placeholder="不填写不更改商品规格" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">商品货号或者SKU码</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('goods_number')}}"
                   placeholder="不填写不更改商品货号或者SKU码" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">商品库存</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('inventory')}}"
                   placeholder="不填写不更改商品库存" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">销售价格</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('price')}}"
                   placeholder="不填写不更改销售价格" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">原始价格</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('origin_price')}}"
                   placeholder="不填写不更改原始价格" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">进货价格</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('cost_price')}}"
                   placeholder="不填写不更改进货价格" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">货期</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('delivery_time')}}" lay-search required lay-verify="required">
                <option value="0" selected="selected">不更改货期</option>
                <option value="1">现货</option>
                <option value="2">7天</option>
                <option value="3">15天</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">销售单位</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('unit')}}"
                   placeholder="不填写不更改销售单位" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">最低起购数量</label>
        <div class="layui-input-block">
            <input type="text"
                   name="{{makeElUniqueName('min_buy_amount')}}"
                   placeholder="不填写不更改最低起购数量" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">排序</label>
        <div class="layui-input-block">
            <input type="text" value="{{isset($goods_specs)?$goods_specs->sort_order:''}}"
                   name="{{makeElUniqueName('sort_order')}}"
                   placeholder="不填写不更改排序" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">是否上架</label>
        <div class="layui-input-block">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}" value="0" checked="checked" class="layui-input" title="不更改">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}" value="{{\App\GoodsSpecs::$GOODS_SPECS_ON}}"
                     class="layui-input"
                   title="是">
            <input type="radio" name="{{makeElUniqueName('display_flag')}}"
                   value="{{\App\GoodsSpecs::$GOODS_SPECS_OFF}}"
                  class="layui-input"
                   title="否">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">商品橱窗图片</label>
        <div style="display: flex;width: 600px;flex-wrap: wrap;" class="layui-input-block">
            <div style="width: 180px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture1')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath1():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id1')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic1:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic1')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id1')}}">
                </div>
            </div>
            <div style="width: 180px;left:10px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture2')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath2():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id2')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic2:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic2')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id2')}}">
                </div>
            </div>
            <div style="width: 180px;left: 20px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture3')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath3():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id3')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic3:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic3')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id3')}}">
                </div>
            </div>
            <div style="width: 180px;top: 10px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture4')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath4():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id4')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic4:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic4')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id4')}}">
                </div>
            </div>
            <div style="width: 180px;left:10px;top:10px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture5')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath5():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id5')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic5:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic5')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id5')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('goods_spec_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
    layui.use(['form', 'validator', 'uploadUtil', 'reditorUtil'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;
        var reditorUtil = layui.reditorUtil;
        var goodsId = {{$goods_ids}};
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();

        var descIndex = reditorUtil.doInitEditor({elemId: '{{makeElUniqueName('goods_content')}}'});
        uploadUtil.doUpload({
            success: function (fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('goods_spec_pic1')}}') {
                    $('#{{makeElUniqueName('picture1')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id1')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic2')}}') {
                    $('#{{makeElUniqueName('picture2')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id2')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic3')}}') {
                    $('#{{makeElUniqueName('picture3')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id3')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic4')}}') {
                    $('#{{makeElUniqueName('picture4')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id4')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic5')}}') {
                    $('#{{makeElUniqueName('picture5')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id5')}}\']').val(fileId);
                }
            }
        });
        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('goods_spec_save')}})', function (data) {
            var index = layer.load(1);
            var url = '/goods/goods/editGoodsSpecsBatch';
            var postParam = {
                goods_ids: data.field['{{makeElUniqueName('goods_ids')}}'] || 1,
                goods_category:data.field['{{makeElUniqueName('goods_category_id')}}'],
                goods_spec: data.field['{{makeElUniqueName('goods_spec')}}'],
                goods_number: data.field['{{makeElUniqueName('goods_number')}}'],
                inventory: data.field['{{makeElUniqueName('inventory')}}'],
                price: data.field['{{makeElUniqueName('price')}}'],
                cost_price: data.field['{{makeElUniqueName('cost_price')}}'],
                origin_price: data.field['{{makeElUniqueName('origin_price')}}'],
                unit: data.field['{{makeElUniqueName('unit')}}'],
                min_buy_amount: data.field['{{makeElUniqueName('min_buy_amount')}}'],
                goods_spec_pic1: data.field['{{makeElUniqueName('goods_picture_file_id1')}}'],
                goods_spec_pic2: data.field['{{makeElUniqueName('goods_picture_file_id2')}}'],
                goods_spec_pic3: data.field['{{makeElUniqueName('goods_picture_file_id3')}}'],
                goods_spec_pic4: data.field['{{makeElUniqueName('goods_picture_file_id4')}}'],
                goods_spec_pic5: data.field['{{makeElUniqueName('goods_picture_file_id5')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
                display_flag: data.field['{{makeElUniqueName('display_flag')}}'],
                delivery_time: data.field['{{makeElUniqueName('delivery_time')}}'],
            };
            var id = data.field['{{makeElUniqueName('id')}}'];
            if (id != '') {
                //修改
                url = '/goods/goods/editGoodsSpecsBatch';
                postParam.id = id;
            }
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose(function (dialogBox) {
                            $.get('/goods/goods/specs/' + goodsId, {}, function (str) {
                                dialogBox.html(str);
                            });
                        });
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('returnToSpecs')}}\']').on('click', function () {
            var index = layer.load(1);
            $.ajax({
                contentType: "application/json",
                type: 'get',
                url: "/goods/goods/specs/" + goodsId,
                success: function (outResult) {
                    var popLayerUtil = layui.popLayerUtil;
                    layer.close(index);
                    popLayerUtil.onClose(function (dialogBox) {
                        dialogBox.html(outResult);
                    });
                },
            });
        });
    });
</script>



