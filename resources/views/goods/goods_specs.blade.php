<button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('addGoodsSpecsIndex')}}"><i
            class="layui-icon">&#xe654; </i> 新增
</button>
<div id="{{makeElUniqueName('tbSpecsGoods')}}"></div>
<script>
    layui.use(['jfTable', 'form', 'addressUtil', 'dateRangeUtil'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var jfTable = layui.jfTable;
        var form = layui.form();
        var addressUtil = layui.addressUtil;
        var dateRangeUtil = layui.dateRangeUtil;
        var goodsId = {{$goods_id}};
        form.render();
        layui.define(function (exports) {
            var obj = {
                doEdit: function (id) {
                    $.get('/goods/goods/editSpecsIndex/' + id, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('editGoodsSpecsIndex')}}',
                                title: '商品规格修改',
                                type: 1,
                                content: str,
                                area: ['800px', '570px']
                            }),
                            onClose: function () {
                                layui.togoodsQueryFuncs.refreshTableGrid();
                            }
                        });
                    });
                },
                doDelete: function (id) {
                    layer.confirm('确定删除该商品规格吗？', {
                        btn: ['确定', '放弃'],
                        icon: 3
                    }, function () {
                        var index = layer.load(1);
                        $.ajax({
                            contentType: "application/json",
                            type: 'get',
                            url: '/goods/goods/delSpecs/' + id,
                            success: function (outResult) {
                                layer.close(index);
                                if (outResult.Success) {
                                    layer.msg(outResult.Message, {icon: 6});
                                    layui.togoodsQueryFuncs.refreshTableGrid();
                                } else {
                                    layer.msg(outResult.Message, {icon: 5});
                                }
                            },
                            error: function (error) {
                                layer.close(index);
                                layui.validator.processValidateError(error);
                            }
                        });
                    }, function () {
                    });
                },
                refreshTableGrid: function () {
                    $("#{{makeElUniqueName('tbSpecsGoods')}}").jfTable("reload");
                }
            };
            exports('togoodsQueryFuncs', obj);
        });

        $("#{{makeElUniqueName('tbSpecsGoods')}}").jfTable({
            url: '/goods/goods/specsQuery',
            pageSize: 5,
            page: true,
            skip: true,
            first: '首页',
            last: '尾页',
            columns: [{
                text: '操作',
                name: 'id',
                width: 200,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    var html = '<a class="layui-btn layui-btn-small layui-btn-warning" onclick="layui.togoodsQueryFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe618;</i> 编辑</a>';
                    html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.togoodsQueryFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                    return html;
                }
            }, {
                text: '商品型号',
                name: 'goods_model',
                width: 100,
                align: 'center',
            }, {
                text: '商品规格',
                name: 'goods_spec',
                width: 170,
                align: 'center',
            }, {
                text: '商品货号或者SKU码',
                name: 'goods_number',
                width: 170,
                align: 'center',
                /*formatter: function (value, dataItem, index) {
                 return '<img style="height: 170px;width: 170px;" src="'+dataItem.getFullPicturePath+'">';
                 }*/
            }, {
                text: '商品库存(单个型号库存)',
                name: 'inventory',
                width: 170,
                align: 'center',
            }, {
                text: '销售价格',
                name: 'price',
                width: 120,
                align: 'center',
            }, {
                text: '原始价格',
                name: 'origin_price',
                width: 120,
                align: 'center',
            }, {
                text: '进货价格',
                name: 'cost_price',
                width: 120,
                align: 'center',
                /*formatter: function (value, dataItem, index) {
                 if(dataItem.list_flag == ){
                 return '<span style="color:green">' + value + '</span>';
                 }else if(dataItem.list_flag == ){
                 return '<span style="color:red">' + value + '</span>';
                 }
                 }*/
            }, {
                text: '销售单位',
                name: 'unit',
                width: 120,
                align: 'center',
            }, {
                text: '最低起购数量',
                name: 'min_buy_amount',
                width: 120,
                align: 'center',
            },{
                text: '最低起购数量',
                name: 'min_buy_amount',
                width: 120,
                align: 'center',
            },{
                text: '商品型号图片',
                name: 'goods_spec_pic1',
                width: 120,
                align: 'center',
                formatter: function (value, dataItem, index) {
                 return '<img style="height: 170px;width: 170px;" src="'+dataItem.getFullPicturePath+'">';
                 }
            },{
                text: '显示顺序',
                name: 'sort_order',
                width: 120,
                align: 'center',
            },{
                text: '显示标志',
                name: 'display_flag',
                width: 120,
                align: 'center',
            }


            ],
            method: 'post',
            queryParam: {
                goods_model: $('input[name=\'{{makeElUniqueName('goods_model')}}\']').val(),
                goods_spec: $('input[name=\'{{makeElUniqueName('goods_spec')}}\']').val(),
                goods_number: $('select[name=\'{{makeElUniqueName('goods_number')}}\']').val(),
                display_flag: $('select[name=\'{{makeElUniqueName('display_flag')}}\']').val(),
                cost_price_min: $('input[name=\'{{makeElUniqueName('cost_price_min')}}\']').val(),
                cost_price_max: $('input[name=\'{{makeElUniqueName('cost_price_max')}}\']').val(),
            },
            toolbarClass: 'layui-btn-small',
            onBeforeLoad: function (param) {
                return $.extend(param, {
                    goods_model: $('input[name=\'{{makeElUniqueName('goods_model')}}\']').val(),
                    goods_spec: $('input[name=\'{{makeElUniqueName('goods_spec')}}\']').val(),
                    goods_number: $('select[name=\'{{makeElUniqueName('goods_number')}}\']').val(),
                    display_flag: $('select[name=\'{{makeElUniqueName('display_flag')}}\']').val(),
                    cost_price_min: $('input[name=\'{{makeElUniqueName('cost_price_min')}}\']').val(),
                    cost_price_max: $('input[name=\'{{makeElUniqueName('cost_price_max')}}\']').val(),
                });
            },
            onLoadSuccess: function (data) {
                return data;
            },
            dataFilter: function (data) {
                return data;
            }
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('addGoodsSpecsIndex')}}\']').on('click', function () {
            $.get('/goods/goods/addGoodsSpecsIndex/'+goodsId, {}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createGoods')}}',
                        title: '新增商品型号',
                        type: 1,
                        content: str,
                        area: ['800px', '600px']
                    }),
                    onClose: function () {
                        layui.togoodsQueryFuncs.refreshTableGrid();
                    }
                });
            });
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_goods_specs')}}\']').on('click', function () {
            $("#{{makeElUniqueName('tbSpecsGoods')}}").jfTable("reload");
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('returnGoodsList')}}\']').on('click', function () {
            var url = '/goods/goods/index';
            $.ajax({
                contentType: "application/json",
                type: 'get',
                url: url,
                success: function (outResult) {
                    console.log(outResult);
                    $('.layui-show').html(outResult);
                }
            })
        });
    });
</script>
