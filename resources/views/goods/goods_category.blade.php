<div class="layui-default-tree-left">
    <ul id="{{makeElUniqueName('left-navi')}}"></ul>
</div>
<div class="layui-default-tree-navi">
    <div class="layui-field-box">
        <div class="layui-form-item" style="margin:0;margin-top:15px;">
            <div class="layui-inline">
                <label class="layui-form-label"></label>
                <div class="layui-input-inline layui-short-input">
                    <input type="hidden" name="{{makeElUniqueName('parent_id')}}">
                </div>
                <label class="layui-form-label" style="width:200px">分类名称</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="text" placeholder="分类名称" name="{{makeElUniqueName('goods_name')}}" autocomplete="off"
                           class="layui-input">
                </div>
                <div class="layui-input-inline" style="width:auto">
                    <button class="layui-btn" lay-filter="{{makeElUniqueName('search_group')}}"><i class="layui-icon">
                            &#xe615;</i> 搜索
                    </button>
                    &nbsp;
                    <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_cate')}}"><i
                                class="layui-icon">&#xe654; </i> 新增
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="{{makeElUniqueName('tbGroup')}}">
    </div>
</div>
<script>
    layui.use(['jfTable', 'tree','validator'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var jfTable = layui.jfTable;

        layui.define(function (exports) {
            var obj = {
                doEdit: function (catId) {
                    console.log(catId);
                    $.get('/goods/category/editIndex/' + catId, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('editDepartOrg')}}',
                                title: '修改分类名称',
                                type: 1,
                                content: str,
                                area: ['800px', '460px']
                            }),
                            onClose: function () {
                                layui.depGroupFuncs.refreshNaviTree();
                            }
                        });
                    });
                },
                doDelete: function (catID) {
                    layer.confirm('确定删除该分类？', {
                        btn: ['确定', '放弃'],
                        icon: 3
                    }, function () {
                        var index = layer.load(1);
                        $.ajax({
                            contentType: "application/json",
                            type: 'post',
                            url: '/goods/category/del',
                            data: JSON.stringify({
                                id: catID
                            }),
                            success: function (outResult) {
                                layer.close(index);
                                if (outResult.Success) {
                                    layer.msg(outResult.Message, {icon: 6});
                                    layui.depGroupFuncs.refreshNaviTree();
                                } else {
                                    layer.msg(outResult.Message, {icon: 5});
                                }
                            },
                            error: function (error) {
                                layer.close(index);
                                layui.validator.processValidateError(error);
                            }
                        });
                    }, function () {
                    });
                },
                refreshTableGrid: function () {
                    $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val('');
                    $("#{{makeElUniqueName('tbGroup')}}").jfTable("reload");
                },
                initNaviTree: function () {
                    this.doInitNaviTree(layui.depGroupFuncs.initTableGrid);
                },
                refreshNaviTree: function () {
                    $('#{{makeElUniqueName('left-navi')}}').find('li').each(function () {
                        $(this).remove();
                    });
                    this.doInitNaviTree(layui.depGroupFuncs.refreshTableGrid);
                },
                doInitNaviTree: function (callback) {
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'get',
                        url: '/goods/category/index/gettree',
                        success: function (outResult) {
                            console.log(outResult);
                            layer.close(index);
                            if (outResult.length > 0) {
                                $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val('');
                            }
                            layui.tree({
                                elem: '#{{makeElUniqueName('left-navi')}}',
                                skin: 'shihuang',
                                nodes: outResult,
                                click: function (node) {
                                    $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(node.id);
                                    layui.depGroupFuncs.refreshTableGrid();
                                }
                            });
                            callback.call(this);
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                },
                initTableGrid: function () {
                    $("#{{makeElUniqueName('tbGroup')}}").jfTable({
                        url: '/goods/category/query',
                        pageSize: 5,
                        page: true,
                        skip: true,
                        first: '首页',
                        last: '尾页',
                        columns: [{
                            text: '操作',
                            name: 'id',
                            width: 300,
                            align: 'center',
                            formatter: function (value, dataItem, index) {
                                var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.depGroupFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.depGroupFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                                return html;
                            }
                        }, {
                            text: '分类名称',
                            name: 'goods_name',
                            width: 500,
                            align: 'center',
                        }, {
                            text: '排序号',
                            name: 'sort_order',
                            width: 50,
                            align: 'center'
                        }],
                        method: 'get',
                        queryParam: {
                            name: $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val(),
                            parentId: $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(),
                        },
                        toolbarClass: 'layui-btn-small',
                        onBeforeLoad: function (param) {
                            return $.extend(param, {
                                name: $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val(),
                                parentId: $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(),
                            });
                        },
                        onLoadSuccess: function (data) {
                            return data;
                        },
                        dataFilter: function (data) {
                            return data;
                        }
                    });
                }
            };
            exports('depGroupFuncs', obj);
        });

        layui.depGroupFuncs.initNaviTree();

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_group')}}\']').on('click', function () {
            $("#{{makeElUniqueName('tbGroup')}}").jfTable("reload");
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_cate')}}\']').on('click', function () {
            var parentId = $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val();
            if (parentId == '' || parentId == undefined) {
                parentId = {{\App\GoodsCategory::$TOP_CATE}};
            }
            $.get('/goods/category/addIndex/' + parentId, {}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createGoodsCate')}}',
                        title: '新增分类',
                        type: 1,
                        content: str,
                        area: ['600px', '360px']
                    }),
                    onClose: function () {
                        layui.depGroupFuncs.refreshNaviTree();
                    }
                });
            });
        });
    });
</script>
