
<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('id')}}" value="{{ isset($goods)?$goods->id:'' }}">
            <input type="text" name="{{makeElUniqueName('goods_name')}}"
                   value="{{ isset($goods)?$goods->goods_name:'' }}" required lay-verify="required"
                   placeholder="商品名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品品牌</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('goods_vendor')}}"
                   value="{{ isset($goods)?$goods->goods_vendor:'' }}" required lay-verify="required"
                   placeholder="商品厂家，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 显示优先级</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}"
                   value="{{ isset($goods)?$goods->sort_order:'' }}" required lay-verify="required"
                   placeholder="显示优先级，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 是否上架</label>
        <div class="layui-input-block">
            <input type="radio" name="{{makeElUniqueName('list_flag')}}" value="{{ \App\Goods::$GOODS_LIST_FLAG_ON }}"
                   {{isset($goods) && $goods->list_flag == \App\Goods::$GOODS_LIST_FLAG_ON?'checked':''}} {{!isset($goods)?'checked':''}}  class="layui-input"
                   title="是">
            <input type="radio" name="{{makeElUniqueName('list_flag')}}" value="{{ \App\Goods::$GOODS_LIST_FLAG_OFF }}"
                   {{isset($goods) && $goods->list_flag == \App\Goods::$GOODS_LIST_FLAG_OFF?'checked':''}} class="layui-input"
                   title="否">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品分类</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('goods_category_id')}}" lay-search required lay-verify="required">
                @foreach ($leaf_cate as $cate)
                    <option {{isset($goods) && $goods->goods_category_id == $cate->id?'selected="selected"':''}} value='{{$cate->id}}'>{{ $cate->getGoodsPath() }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 关联文件1</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('goods_file_id')}}" lay-search required lay-verify="required">
                <option value=''>请选择关联文件</option>
                @foreach ($all_files as $sfile)
                    <option {{isset($goods) && @$file_ids[0] == $sfile->id?'selected="selected"':''}} value='{{$sfile->id}}'>{{ $sfile->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 关联文件2</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('goods_file_id2')}}" lay-search >
                <option value=''>请选择关联文件</option>
                @foreach ($all_files as $sfile)
                    <option {{isset($goods) && @$file_ids[1] == $sfile->id?'selected="selected"':''}} value='{{$sfile->id}}'>{{ $sfile->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 关联文件3</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('goods_file_id3')}}" lay-search>
                <option value=''>请选择关联文件</option>
                @foreach ($all_files as $sfile)
                    <option {{isset($goods) && @$file_ids[2] == $sfile->id?'selected="selected"':''}} value='{{$sfile->id}}'>{{ $sfile->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 关联文件4</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('goods_file_id4')}}" lay-search>
                <option value=''>请选择关联文件</option>
                @foreach ($all_files as $sfile)
                    <option {{isset($goods) && @$file_ids[3] == $sfile->id?'selected="selected"':''}} value='{{$sfile->id}}'>{{ $sfile->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 关联文件5</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('goods_file_id5')}}" lay-search>
                <option value=''>请选择关联文件</option>
                @foreach ($all_files as $sfile)
                    <option {{isset($goods) && @$file_ids[4] == $sfile->id?'selected="selected"':''}} value='{{$sfile->id}}'>{{ $sfile->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品橱窗图片</label>
        <div class="layui-input-block">
            <div class="layui-big-upload-box">
                <img id="{{makeElUniqueName('picture')}}"
                     src="{{ isset($goods)?$goods->getFullPicturePath():'/images/no-pic-back.png' }}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id')}}"
                       value="{{ isset($goods)?$goods->goods_picture:'' }}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_picture')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id')}}">
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top: 120px;" class="layui-form-item">
        <label class="layui-form-label">商品详情</label>
        {{--<div class="layui-input-block">
            <textarea name="{{makeElUniqueName('goods_content')}}" id="{{makeElUniqueName('goods_content')}}"
                      class="layui-textarea">{{ isset($goods)?$goods->goods_content:'' }}</textarea>
        </div>--}}
        <div class="layui-input-block">
            <script id="container" name="content" type="text/plain">{!! isset($goods)?$goods->goods_content:'' !!}</script>
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('goods_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>
<!-- ueditor配置文件 -->
<script type="text/javascript" src="/vendor/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/vendor/ueditor/ueditor.all.js"></script>
<script>
    var ue = UE.getEditor('container');


    layui.use(['form', 'validator', 'uploadUtil', 'reditorUtil'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;
        var reditorUtil = layui.reditorUtil;
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();
        var descIndex = reditorUtil.doInitEditor({elemId: '{{makeElUniqueName('goods_content')}}'});
        uploadUtil.doUpload({
            success: function (fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('goods_picture')}}') {
                    $('#{{makeElUniqueName('picture')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id')}}\']').val(fileId);
                }
            }
        });
        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('goods_save')}})', function (data) {
            var content = ue.getContent();
            var index = layer.load(1);
            var url = '/goods/goods/add';
            var postParam = {
                goods_name: data.field['{{makeElUniqueName('goods_name')}}'],
                goods_vendor: data.field['{{makeElUniqueName('goods_vendor')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
                goods_category_id: data.field['{{makeElUniqueName('goods_category_id')}}'],
                goods_file_id: data.field['{{makeElUniqueName('goods_file_id')}}'],
                goods_file_id2: data.field['{{makeElUniqueName('goods_file_id2')}}'],
                goods_file_id3: data.field['{{makeElUniqueName('goods_file_id3')}}'],
                goods_file_id4: data.field['{{makeElUniqueName('goods_file_id4')}}'],
                goods_file_id5: data.field['{{makeElUniqueName('goods_file_id5')}}'],
                goods_picture: data.field['{{makeElUniqueName('goods_picture_file_id')}}'],
                list_flag: data.field['{{makeElUniqueName('list_flag')}}'],
                goods_content: content
            };
            var goodsId = data.field['{{makeElUniqueName('id')}}'];
            if (goodsId != '') {
                //修改
                url = '/goods/goods/edit';
                postParam.id = goodsId;
            }
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        layer.close(popLayerUtil.index);
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose();
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });
    });
</script>
