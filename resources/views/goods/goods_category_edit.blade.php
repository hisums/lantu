<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 分类名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('cate_id')}}" value="{{ $cat_id}}">
            <input type="text" name="{{makeElUniqueName('goods_name')}}" value="{{ $goods->goods_name}}"
                   required lay-verify="required" placeholder="分类名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 上级分类</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('parent_id')}}" lay-search required lay-verify="required">
                @foreach ($prev_cates as $prev_cate)
                    <option value='{{$prev_cate->id}}' {{$goods->parent_id == $prev_cate->id?'selected':''}}>{{ $prev_cate->getGoodsPath() }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ $goods->sort_order}}"
                   required placeholder="排序号，必填（*）" lay-verify="required|number" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('cate_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
    layui.use(['form', 'validator'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();

        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('cate_save')}})', function (data) {
            var index = layer.load(1);
            var url = '/goods/category/edit';
            var postParam = {
                goods_name: data.field['{{makeElUniqueName('goods_name')}}'],
                cate_id: data.field['{{makeElUniqueName('cate_id')}}'],
                parent_id: data.field['{{makeElUniqueName('parent_id')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}']
            };

            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        layer.close(popLayerUtil.index);
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose();
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });
    });
</script>
