<div class="layui-form layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">商品名称</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="商品名称" name="{{makeElUniqueName('goods_name')}}" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label">商品厂家</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="商品厂家" name="{{makeElUniqueName('goods_vendor')}}" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label">是否上架</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('list_flag')}}">
                    <option value="0">(所有商品)</option>
                    @foreach (\App\Goods::$GOODS_LIST_FLAG_MAP as $item)
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">商品分类</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('goods_category')}}">
                    <option value="">(所有分类)</option>
                    @foreach ($all_cates as $cate)
                        <option value='{{$cate['id']}}'>{{$cate->getGoodsPath()}}</option>
                    @endforeach
                </select>
            </div>
            <label class="layui-form-label">商品分值</label>
            <div class="layui-input-inline layui-short-input">
                <input class="layui-input" placeholder="最低分值" name="{{makeElUniqueName('goods_score_min')}}">
            </div>
            <label class="layui-form-label">----到----</label>
            <div class="layui-input-inline layui-short-input">
                <input class="layui-input" placeholder="最高分值" name="{{makeElUniqueName('goods_score_max')}}">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px"></label>
            <div style="width: 650px!important;" class="layui-input-inline layui-long-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_goods')}}"><i
                            class="layui-icon">
                        &#xe615;</i> 搜索
                </button>
                &nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('addGoodsIndex')}}"><i
                            class="layui-icon">&#xe654; </i> 新增
                </button>
                &nbsp;
                <button class="layui-btn layui-btn-warm" lay-filter="{{makeElUniqueName('addGoodsBatch')}}"><i
                            class="layui-icon">&#xe61d; </i> 批量导入
                </button>
                &nbsp;
                <button class="layui-btn layui-btn-warning" lay-filter="{{makeElUniqueName('editGoodsBatch')}}"><i
                            class="layui-icon">&#xe618; </i> 批量编辑
                </button>
                <button class="layui-btn layui-btn-danger" lay-filter="{{makeElUniqueName('delGoodsBatch')}}">
                    <i
                            class="layui-icon">&#xe640; </i> 批量删除
                </button>
            </div>
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    {{--<div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px"></label>
            <div style="width: 650px!important;" class="layui-input-inline layui-long-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('pagecount5')}}">5</button>
                &nbsp;
                <button class="layui-btn" lay-filter="{{makeElUniqueName('pagecount10')}}">10</button>
                &nbsp;
                <button class="layui-btn" lay-filter="{{makeElUniqueName('pagecount25')}}">25</button>

                <button class="layui-btn" lay-filter="{{makeElUniqueName('pagecount50')}}">50</button>

                <button class="layui-btn" lay-filter="{{makeElUniqueName('pagecount100')}}">100</button>
            </div>
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>--}}
</div>
<div id="{{makeElUniqueName('tbGoods')}}"></div>
<script>
    layui.use(['jfTable', 'form', 'addressUtil', 'dateRangeUtil'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var jfTable = layui.jfTable;
        var form = layui.form();
        var addressUtil = layui.addressUtil;
        var dateRangeUtil = layui.dateRangeUtil;
        var pageSize = 5;

        form.render();
        layui.define(function (exports) {
            var obj = {
                doEdit: function (goodsId) {
                    $.get('/goods/goods/editIndex/' + goodsId, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('editGoodsIndex')}}',
                                title: '商品修改',
                                type: 1,
                                content: str,
                                area: ['800px', '570px']
                            }),
                            onClose: function () {
                                layui.togoodsQueryFuncs.refreshTableGrid();
                            }
                        });
                    });
                },
                specsList: function (goodsId) {
                    $.get('/goods/goods/specs/' + goodsId, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('editGoodsSpecs')}}',
                                title: '商品规格',
                                type: 1,
                                content: str,
                                area: ['1000px', '570px']
                            }),
                            onClose: function (callback) {
                                callback.call(this, $('#{{makeElUniqueName('editGoodsSpecs')}}'));
                            }
                        });
                    });
                },
                paramList: function (goodsId) {
                    $.get('/goods/goods/attribute/' + goodsId, {}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('goodsParamsList')}}',
                                title: '商品属性',
                                type: 1,
                                content: str,
                                area: ['930px', '570px']
                            }),
                            onClose: function (callback) {
                                callback.call(this, $('#{{makeElUniqueName('goodsParamsList')}}'));
                            }
                        });
                    });
                },
                doDelete: function (goodsId) {
                    layer.confirm('确定删除该商品？', {
                        btn: ['确定', '放弃'],
                        icon: 3
                    }, function () {
                        var index = layer.load(1);
                        $.ajax({
                            contentType: "application/json",
                            type: 'get',
                            url: '/goods/goods/del/' + goodsId,
                            success: function (outResult) {
                                layer.close(index);
                                if (outResult.Success) {
                                    layer.msg(outResult.Message, {icon: 6});
                                    layui.togoodsQueryFuncs.refreshTableGrid();
                                } else {
                                    layer.msg(outResult.Message, {icon: 5});
                                }
                            },
                            error: function (error) {
                                layer.close(index);
                                layui.validator.processValidateError(error);
                            }
                        });
                    }, function () {
                    });
                },
                refreshTableGrid: function () {
                    $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val('');
                    $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
                }
            };
            exports('togoodsQueryFuncs', obj);
        });

        $("#{{makeElUniqueName('tbGoods')}}").jfTable({
            url: '/goods/goods/query',
            pageSize: pageSize,
            page: true,
            skip: true,
            first: '首页',
            last: '尾页',
            columns: [{
                text: '<span style="cursor: pointer" id="sAll">全选</span>',
                name: 'id',
                width: 40,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    return '<input type="checkbox" name="{{makeElUniqueName('goods_ids')}}" value="' + value + '">';
                }
            },{
                text: '操作',
                name: 'id',
                width: 230,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    var html = '<div style="margin-bottom: 10px"><a class="layui-btn layui-btn-small layui-btn-warning" onclick="layui.togoodsQueryFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe618;</i> 编辑</a>';
                    html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.togoodsQueryFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a></div>';
                    html += '<div>&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.togoodsQueryFuncs.specsList(' + value + ')"><i class="layui-icon">&#xe62d;</i> 规格列表</a>';
                    html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-warm" onclick="layui.togoodsQueryFuncs.paramList(' + value + ')"><i class="layui-icon">&#xe631;</i> 属性列表</a></div>';
                    return html;
                }
            }, {
                text: '商品名称',
                name: 'goods_name',
                width: 120,
                align: 'center',
            }, {
                text: '商品厂家',
                name: 'goods_vendor',
                width: 140,
                align: 'center',
            }, {
                text: '商品橱窗图片',
                name: 'goods_picture',
                width: 170,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    return '<img style="height: 170px;width: 170px;" src="' + dataItem.getFullPicturePath + '">';
                }
            }, {
                text: '排序',
                name: 'sort_order',
                width: 80,
                align: 'center',
            }, {
                text: '商品类别',
                name: 'goodsCategoryGet.0.goods_name',
                width: 120,
                align: 'center',
            }, {
                text: '是否上架',
                name: 'list_flag_text',
                width: 80,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if (dataItem.list_flag == {{\APP\Goods::$GOODS_LIST_FLAG_ON}}) {
                        return '<span style="color:green">' + value + '</span>';
                    } else if (dataItem.list_flag == {{\APP\Goods::$GOODS_LIST_FLAG_OFF}}) {
                        return '<span style="color:red">' + value + '</span>';
                    }
                }
            }
            ],
            method: 'post',
            queryParam: {
                goods_name: $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val(),
                goods_vendor: $('input[name=\'{{makeElUniqueName('goods_vendor')}}\']').val(),
                list_flag: $('select[name=\'{{makeElUniqueName('list_flag')}}\']').val(),
                goods_category: $('select[name=\'{{makeElUniqueName('goods_category')}}\']').val(),
                goods_score_min: $('input[name=\'{{makeElUniqueName('goods_score_min')}}\']').val(),
                goods_score_max: $('input[name=\'{{makeElUniqueName('goods_score_max')}}\']').val(),
            },
            toolbarClass: 'layui-btn-small',
            onBeforeLoad: function (param) {
                return $.extend(param, {
                    goods_name: $('input[name=\'{{makeElUniqueName('goods_name')}}\']').val(),
                    goods_vendor: $('input[name=\'{{makeElUniqueName('goods_vendor')}}\']').val(),
                    list_flag: $('select[name=\'{{makeElUniqueName('list_flag')}}\']').val(),
                    goods_category: $('select[name=\'{{makeElUniqueName('goods_category')}}\']').val(),
                    goods_score_min: $('input[name=\'{{makeElUniqueName('goods_score_min')}}\']').val(),
                    goods_score_max: $('input[name=\'{{makeElUniqueName('goods_score_max')}}\']').val(),
                });
            },
            onLoadSuccess: function (data) {
                return data;
            },
            dataFilter: function (data) {
                return data;
            }
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('addGoodsIndex')}}\']').on('click', function () {
            $.get('/goods/goods/addIndex', {}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createGoods')}}',
                        title: '新增商品',
                        type: 1,
                        content: str,
                        area: ['800px', '600px']
                    }),
                    onClose: function () {
                        layui.togoodsQueryFuncs.refreshTableGrid();
                    }
                });
            });
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('addGoodsBatch')}}\']').on('click', function () {
            $.get('/goods/goods/addGoodsBatchIndex', {}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('addGoodsByFile')}}',
                        title: '批量导入商品',
                        type: 1,
                        content: str,
                        area: ['800px', '600px']
                    }),
                    onClose: function () {
                        layui.togoodsQueryFuncs.refreshTableGrid();
                    }
                });
            });
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('editGoodsBatch')}}\']').on('click', function () {
            var ids = [];
            $('input[name={{makeElUniqueName('goods_ids')}}]:checked').each(function (k, v) {
                ids.push($(v).val())
            });
            if (ids.length == 0) {
                layer.msg('请选择要编辑的商品!');
                return false;
            }
            $.post('/goods/goods/editGoodsBatch', {ids}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('editGoodsSpecBatch')}}',
                        title: '批量编辑商品规格',
                        type: 1,
                        content: str,
                        area: ['800px', '600px']
                    }),
                    onClose: function () {
                        layui.togoodsQueryFuncs.refreshTableGrid();
                    }
                });
            });
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('pagecount5')}}\']').on('click', function () {
            pageSize = 5;
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });
        $('.layui-btn[lay-filter=\'{{makeElUniqueName('pagecount10')}}\']').on('click', function () {
            pageSize = 10;
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });
        $('.layui-btn[lay-filter=\'{{makeElUniqueName('pagecount25')}}\']').on('click', function () {
            pageSize = 25;
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });
        $('.layui-btn[lay-filter=\'{{makeElUniqueName('pagecount50')}}\']').on('click', function () {
            pageSize = 50;
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });
        $('.layui-btn[lay-filter=\'{{makeElUniqueName('pagecount100')}}\']').on('click', function () {
            pageSize = 100;
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });
        $("#{{makeElUniqueName('tbGoods')}}").on('click', '#sAll', function () {
            $('input[name={{makeElUniqueName('goods_ids')}}]').each(function (k, v) {
                $(v).prop('checked', true);
            })
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_goods')}}\']').on('click', function () {
            $("#{{makeElUniqueName('tbGoods')}}").jfTable("reload");
        });
        $('.layui-btn[lay-filter=\'{{makeElUniqueName('delGoodsBatch')}}\']').on('click', function () {
            layer.confirm('确定删除这些商品吗？', {
                btn: ['确定', '放弃'],
                icon: 3
            }, function () {
                var ids = [];
                $('input[name={{makeElUniqueName('goods_ids')}}]:checked').each(function (k, v) {
                    ids.push($(v).val())
                });
                if (ids.length == 0) {
                    layer.msg('请选择要删除的商品!');
                    return false;
                }
                var index = layer.load(1);
                $.ajax({
                    url: '/goods/delGoodsBatch',
                    data: {ids},
                    dataType: "json",
                    type: 'post',
                    success: function (outResult) {
                        layer.close(index);
                        if (outResult.Success) {
                            layer.msg(outResult.Message, {icon: 6});
                            layui.togoodsQueryFuncs.refreshTableGrid();
                        } else {
                            layer.msg(outResult.Message, {icon: 5});
                        }
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            }, function () {
            });
        });
    });
</script>
