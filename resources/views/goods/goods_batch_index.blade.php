<style>
    .skin{
        background-color: #ffffff;
        width: 300px !important;
        height:300px !important;
    }
    .layui-upload-file-img{

    }
</style>
<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品分类</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('goods_category_id')}}" lay-search required lay-verify="required">
                @foreach ($leaf_cate as $cate)
                    <option value='{{$cate->id}}'>{{ $cate->getGoodsPath() }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品橱窗图片</label>
        <div class="layui-input-block">
            <div class="layui-big-upload-box">
                <img id="{{makeElUniqueName('picture')}}"
                     src="{{ isset($goods)?$goods->getFullPicturePath():'/images/no-pic-back.png' }}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id')}}"
                       value="{{ isset($goods)?$goods->goods_picture:'' }}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_picture')}}" class="layui-upload-file-img"
                           id="{{makeElUniqueName('picture_id')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 商品详情图片</label>
        <div style="display: flex;width: 600px;flex-wrap: wrap;" class="layui-input-block">
            <div style="width: 180px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture1')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath1():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id1')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic1:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic1')}}" class="layui-upload-file-img"
                           id="{{makeElUniqueName('picture_id1')}}">
                </div>
            </div>
            <div style="width: 180px;left:10px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture2')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath2():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id2')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic2:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic2')}}" class="layui-upload-file-img"
                           id="{{makeElUniqueName('picture_id2')}}">
                </div>
            </div>
            <div style="width: 180px;left: 20px;" class="layui-big-upload-box">
                <img style="width: 180px;" id="{{makeElUniqueName('picture3')}}"
                     src="{{isset($goods_specs)?$goods_specs->getFullPicturePath3():''}}">
                <input type="hidden" name="{{makeElUniqueName('goods_picture_file_id3')}}"
                       value="{{isset($goods_specs)?$goods_specs->goods_spec_pic3:''}}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('goods_spec_pic3')}}" class="layui-upload-file-img"
                           id="{{makeElUniqueName('picture_id3')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="btns layui-form-item">
        <label class="layui-form-label">源文件</label>
        <div id="load_file_name" class="layui-input-block">
            <input type="file" name="excelUp" class="layui-upload-file" lay-title="选择文件">
        </div>
        <input type="hidden" name="{{makeElUniqueName('load_file_path_name')}}" value="">
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('goods_batch_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
    layui.use(['form', 'validator', 'reditorUtil', 'upload','uploadUtil'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var reditorUtil = layui.reditorUtil;
        var upload = layui.upload;
        var uploadUtil = layui.uploadUtil;
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();
        uploadUtil.doUpload({
            elem:'.layui-upload-file-img',
            success: function (fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('goods_picture')}}') {
                    $('#{{makeElUniqueName('picture')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic1')}}') {
                    $('#{{makeElUniqueName('picture1')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id1')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic2')}}') {
                    $('#{{makeElUniqueName('picture2')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id2')}}\']').val(fileId);
                }
                if (fileKey == '{{makeElUniqueName('goods_spec_pic3')}}') {
                    $('#{{makeElUniqueName('picture3')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('goods_picture_file_id3')}}\']').val(fileId);
                }
            }
        });


        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('goods_batch_save')}})', function (data) {
            var index = layer.load(1);
            var url = '/goods/goods/addGoodsBatch';
            var postParam = {
                goods_category_id: data.field['{{makeElUniqueName('goods_category_id')}}'],
                load_file_name: data.field['{{makeElUniqueName('load_file_path_name')}}'],
                goods_picture:data.field['{{makeElUniqueName('goods_picture_file_id')}}'],
                goods_spec_pic1: data.field['{{makeElUniqueName('goods_picture_file_id1')}}'],
                goods_spec_pic2: data.field['{{makeElUniqueName('goods_picture_file_id2')}}'],
                goods_spec_pic3: data.field['{{makeElUniqueName('goods_picture_file_id3')}}']
            };
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        layer.close(popLayerUtil.index);
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose();
                    } else {
                        layer.close(popLayerUtil.index);
                        popLayerUtil.onClose();
                        layer.msg(outResult.Message, {icon: 5}, function () {
                            var html='';
                            for(var i=0; i<outResult.ResultData.length;i++){
                                html += outResult.ResultData[i].goods_name + '上传失败!<br/>';
                            }
                            layer.open({
                                type: 1,
                                title: false,
                                closeBtn: 0,
                                shadeClose: true,
                                skin: 'skin',
                                area: ['300px', '300px'],
                                content: html
                            });
                        })(outResult);

                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });

        var loadIndex;
        //上传
        upload({
            url: '/file/test',
            ext: 'xls',
            before: function (opt) {
                loadIndex = layer.load(1);
            },
            success: function (opt) {
                layer.close(loadIndex);
                if (opt.Success) {
                    console.log(opt);
                    $('#load_file_name').html('<div style="min-height: 36px;line-height: 36px">' + opt.ResultData.old_name + '</div>');
                    $("input[name={{makeElUniqueName('load_file_path_name')}}]").val(opt.ResultData.file_name);
                } else {
                    layer.msg(opt.Message, {icon: 5});
                }
            }
        })
    });
</script>
