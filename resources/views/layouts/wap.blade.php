<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
    <meta http-equiv="Expires" content="0" />
    <title>{{$title?:'蓝图生物科技'}}</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="renderer" content="webkit">
    <link rel="stylesheet" href="/wap/css/common.css">
    <script src="/wap/js/jquery.min.js"></script>
    @yield('style')
    <script>
        var sUserAgent=navigator.userAgent;
        var mobileAgents=['Android','iPhone','Symbian','WindowsPhone','iPod','BlackBerry','Windows CE'];
        var goUrl = 0;
        for( var i=0;i<mobileAgents.length;i++){
            if(sUserAgent.indexOf(mobileAgents[i]) > -1){
                goUrl = 1;
                break;
            }
        }
        if (goUrl == 1){
            var url = window.location.pathname;
            var route = url.split('/').splice(1);
            var value = window.location.search;
            if(route[1] == 'register'){
                u = "{{url('web/register')}}"
                loaction = u;
            }
        }else{
            var url = window.location.pathname;
            var route = url.split('/').splice(1);
            var value = window.location.search;
            if(route[1] == 'register'){
                u = "{{url('web/register')}}"
                loaction = u;
            }
            if(route[1] == "product"){
                var cate_id = route[2];
                u = "{{url('web/goods/list',['cate_id'=>'cate_ids',])}}";
                u = u.replace("cate_ids",cate_id);
                location = u;
            }else if(route[1] == "product_detail"){
                var id = route[2];
                var spe_id = route[3];
                if(spe_id){
                    u = "{{url('web/goods/detail',['id'=>'ids','spe_id'=>'spe_ids'])}}";
                    u = u.replace('ids',id).replace('spe_ids',spe_id);
                }else{
                    u = "{{url('web/goods/detail',['id'=>'ids'])}}";
                    u = u.replace('ids',id);
                }

                location = u;
            }else if(route[1] == "news"){
                var cate_id = route[2];
                cate_id = cate_id?cate_id:0;
                u = "{{url('web/article/article_list',['cate_id'=>'cate_ids'])}}";
                u = u.replace('cate_ids',cate_id);
                location = u;
            }else if(route[1] == "about_us"){
                location = "{{url('web/article/aboutUs')}}";
            }else if(route[1] == "testing_center"){
                location = "{{url('web/article/testing_center')}}";
            }else if(route[1] == "testing_sub"){
                var cate_id = route[2];
                var cate_sub_id = route[3];
                u = "{{url('web/article/testing_sub',['cate_id'=>'cate_ids','cate_sub_id'=>'cate_sub_ids'])}}";
                u = u.replace('cate_ids',cate_id).replace('cate_sub_ids',cate_sub_id);
                location = u;
            }else if(value){
                value = value.split('=').pop();
                u ="{{url('web/goods/list/0/2?keywords=value')}}";
                u = u.replace('value',value);
                location = u;
            }else if(route[1] == "register"){
                location = "{{url('web/register')}}"
            }else{
                location ="{{url('/')}}";
            }
        }
    </script>
</head>
<body>
@yield('body')
@if(!empty($foot))
<div class="tabbar">
    <a href="{{url('mobile')}}" class="tabbar_item {{$foot == 1 ? 'active':''}}"><span class="icon icon_home"></span><p>首页</p></a>
    <a href="{{url('mobile/product_category')}}" class="tabbar_item {{$foot == 2 ? 'active':''}}"><span class="icon icon_pro"></span><p>产品</p></a>
    <a href="{{url('mobile/news')}}" class="tabbar_item {{$foot == 3 ? 'active':''}}"><span class="icon icon_news"></span><p>资讯</p></a>
    <a href="{{url('mobile/about_us')}}" class="tabbar_item {{$foot == 4 ? 'active':''}}"><span class="icon icon_my"></span><p>关于我们</p></a>
</div>
@endif
</body>
<script src="/wap/js/jquery.lazyload.min.js"></script>
<script src="/js/layer_mobile/layer.js"></script>
<script src="/wap/js/common.js"></script>
<script>
    $(".lazy-load").lazyload();
</script>
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?ade3da5855c8a8b7296e253088c90d44";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
@yield('script')
</html>