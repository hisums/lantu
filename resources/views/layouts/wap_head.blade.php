<style>
    .top{
        width: 100%;
        background-color: #f0f0f0;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 999;
        border-bottom: #F0F0F0;
    }
    .top .back_to_index{
        width: 40px;
        height: 40px;
        float: left;
        padding: 5px;
    }
    .top .back_to_index img{
        width: 30px;
        height: 30px;
    }
    .top .title{
        height: 40px;
        text-align: center;
        width: calc(100% - 80px);
        width: -moz-calc(100% - 80px);
        width: -webkit-calc(100% - 80px);
        float: left;
    }
    .top .title .title_name{
        height: 100%;
        width: 100%;
        font-size: 16px;
        font-weight: bold;
        line-height: 40px;
    }
    #searchForm input{
        width: 100%;
        height: 40px;
    }
    .top .search-icon{
        width: 40px;
        height: 40px;
        float: right;
        padding: 5px;
    }
    .top .search-icon img{
        width: 30px;
        height: 30px;
    }
    .top_align{
        width: 100%;
        height:40px;
    }
</style>
<div class="top">
    <a href="javascript:history.go(-1)" class="back_to_index">
        <img src="/wap/img/back.png">
    </a>
    <div class="title">
        <div class="title_name" style="display: block;">{{@$sub_title?:'首页'}}</div>
        <div class="search layui-form-label" style="display: none;">
            <form id="searchForm" action="{{url('mobile/search')}}"><input type="text" id="keywords" name="keywords" placeholder="搜索商品"></form>
        </div>
    </div>
    <div class="search-icon"><img src="/wap/img/search.png"></div>
</div>
<div class="top_align"></div>
<script>
    $(function(){
        $('.search-icon').click(function(){
            if($("#keywords").val()){
                $("#searchForm").submit();
            }else{
                $('.title div').toggle();
            }
        });
    })
</script>