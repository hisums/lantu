<div class="layui-form layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px">文档名称</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="文档名称" name="{{makeElUniqueName('name')}}" autocomplete="off"
                       class="layui-input">
            </div>

            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
    <br/>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label" style="width:130px"></label>
            <div class="layui-input-inline layui-long-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_file')}}"><i
                            class="layui-icon">
                        &#xe615;</i> 搜索
                </button>
                &nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('addFile')}}"><i
                            class="layui-icon">&#xe654; </i> 新增
                </button>
            </div>
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('fileList')}}"></div>
<script>
    layui.use(['jfTable', 'form', 'addressUtil', 'dateRangeUtil', 'laydate', 'jquery', 'validator'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var jfTable = layui.jfTable;
        var form = layui.form();
        var addressUtil = layui.addressUtil;
        var dateRangeUtil = layui.dateRangeUtil;
        var laydate = layui.laydate;
        form.render();
        layui.define(function (exports) {
            var obj = {

                doDelete: function (id) {
                    console.log(id);
                    layer.confirm('确定删除该商品？', {
                        btn: ['确定', '放弃'],
                        icon: 3
                    }, function () {
                        var index = layer.load(1);
                        $.ajax({
                            dataType: "json",
                            type: 'post',
                            url: '/file/del',
                            data:{id:id},
                            success: function (outResult) {
                                layer.close(index);
                                if (outResult.Success) {
                                    layer.msg(outResult.Message, {icon: 6});
                                    layui.filesFunc.refreshTableGrid();
                                } else {
                                    layer.msg(outResult.Message, {icon: 5});
                                }
                            },
                            error: function (error) {
                                layer.close(index);
                                layui.validator.processValidateError(error);
                            }
                        });
                    }, function () {
                    });
                },
                refreshTableGrid: function () {
                    $('input[name=\'{{makeElUniqueName('name')}}\']').val('');
                    $("#{{makeElUniqueName('fileList')}}").jfTable("reload");
                }
            };
            exports('filesFunc', obj);
        });

        $("#{{makeElUniqueName('fileList')}}").jfTable({
            url: '/file/query',
            pageSize: 10,
            page: true,
            skip: true,
            first: '首页',
            last: '尾页',
            columns: [{
                text: '操作',
                name: 'id',
                width: 200,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    var  html = '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.filesFunc.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                    return html;
                }
            }, {
                text: '文件名',
                name: 'name',
                width: 500,
                align: 'center'
            }, {
                text: '查看文件',
                name: 'getFullPath',
                width: 150,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    return '<a href="'+value+'">查看文件</a>'
                }
            }
            ],
            method: 'post',
            queryParam: {
                name: $('input[name=\'{{makeElUniqueName('name')}}\']').val()
            },
            toolbarClass: 'layui-btn-small',
            onBeforeLoad: function (param) {
                return $.extend(param, {
                    name: $('input[name=\'{{makeElUniqueName('name')}}\']').val()
                });
            },
            onLoadSuccess: function (data) {
                return data;
            },
            dataFilter: function (data) {
                return data;
            }
        });


        $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_file')}}\']').on('click', function () {
            $("#{{makeElUniqueName('fileList')}}").jfTable("reload");
        });

        $('.layui-btn[lay-filter=\'{{makeElUniqueName('addFile')}}\']').on('click', function () {
            $.get('/file/addIndex', {}, function (str) {
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('fileAdd')}}',
                        title: '添加文件',
                        type: 1,
                        content: str,
                        area: ['800px', '600px']
                    }),
                    onClose: function () {
                        layui.filesFunc.refreshTableGrid();
                    }
                });
            });
        });
    });
</script>
