<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 关联文件</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('goods_file_id')}}" lay-search required lay-verify="required">
                <option value=''>请选择关联文件</option>
                @foreach ($all_files as $sfile)
                    <option {{isset($reagent_info) && $reagent_info->file_id == $sfile->id?'selected="selected"':''}} value='{{$sfile->id}}'>{{ $sfile->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 图片</label>
        <div class="layui-input-block">
            <div class="layui-big-upload-box">
                <img id="{{makeElUniqueName('picture')}}"
                     src="{{isset($reagent_info)?$reagent_info->getFullPath():''}}"
                >
                <input type="hidden" name="{{makeElUniqueName('reagent_picture_file_id')}}"
                       value="{{isset($reagent_info)?$reagent_info->reagent_img:''}}"
                >
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('reagent_picture')}}" class="layui-upload-file"
                           id="{{makeElUniqueName('picture_id')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item layui-form-center" style="margin-top:20px">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('reagent_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
    layui.use(['form', 'validator', 'uploadUtil', 'reditorUtil'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;
        var reditorUtil = layui.reditorUtil;
        var id = {{$id}};
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        form.render();

        var descIndex = reditorUtil.doInitEditor({elemId: '{{makeElUniqueName('goods_content')}}'});
        uploadUtil.doUpload({
            success: function (fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('reagent_picture')}}') {
                    $('#{{makeElUniqueName('picture')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('reagent_picture_file_id')}}\']').val(fileId);
                }
            }
        });
        //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
        form.on('submit({{makeElUniqueName('reagent_save')}})', function (data) {
            var index = layer.load(1);
            var url = '/reagent/edit';
            var postParam = {
                file_id: data.field['{{makeElUniqueName('goods_file_id')}}'],
                reagent_img: data.field['{{makeElUniqueName('reagent_picture_file_id')}}'],
                id: id
            };
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: url,
                data: JSON.stringify(postParam),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        layer.close(popLayerUtil.index);
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose();
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });
    });
</script>
