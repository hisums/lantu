<div class="layui-form layui-layer-outerbox">
    <form action="#" method="post" enctype="multipart/form-data" id="file_upload">
        <div id="uploader" class="wu-example">
            <!--用来存放文件信息-->
            <div id="thelist" class="uploader-list"></div>
            <div class="btns">
                <div class="layui-btn" lay-filter="{{makeElUniqueName('chooseFile')}}"><i
                            class="layui-icon">
                        &#xe615;</i> 选择文件
                </div>

                <input id="file" type="file" name="file_name[]" multiple="multiple" hidden>

            </div>
        </div>
        <div class="layui-form-item file_list">

        </div>
        <div class="layui-form-item layui-form-center" style="margin-top:20px">
            <button style="margin-left: -76px;" class="layui-btn" lay-submit
                    lay-filter="{{makeElUniqueName('file_save')}}">保存
            </button>
        </div>
    </form>
    <div class="layui-form-item layui-form-center"
         style="margin-top: 20px;position: relative;left: 72px;top: -57px;width: 15%">
        <button class=" layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>
<script>
    layui.use(['form', 'validator', 'uploadUtil', 'reditorUtil', 'layer'], function () {
        var form = layui.form();
        var $ = layui.jquery;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;
        var reditorUtil = layui.reditorUtil;
        var layer = layui.layer
        //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
        $('.layui-btn[lay-filter=\'{{makeElUniqueName('chooseFile')}}\']').on('click', function () {
            $("#file").click();
        });
        var file_index = 0;
        var allFile;
        var allFileName;
        function upload1(){
            if(file_index<allFile.length){
                var reader = new FileReader();
                reader.onload = function (res) {
                    var index = layer.load(1);
                    $.ajax({
                        url: '/file/upload',
                        data: {file: res.target.result, filename: allFile[file_index].name, index: file_index},
                        dataType: 'json',
                        type: 'post',
                        async:false,
                        success: function (res) {
                            if (!res.Success) {
                                $('.file_name:eq(' + res.ResultData.index + ')').html(res.ResultData.filename + '<p style="color: red;">　' + res.Message + '</p>');
                                layer.msg(res.Message, function () {
                                })
                            } else {
                                getAjaxReturn(res.ResultData);
                            }
                        },
                        error: function () {

                        },
                        complete:function(){
                            layer.close(index);
                            file_index++;
                            upload1();
                        }
                    });

                }
                reader.readAsDataURL(allFile[file_index]);
            }else{
                file_index = 0;
            }
        }
        $("#file").change(function (res) {
            allFile = res.currentTarget.files;
            var html = '';
            allFileName = [];
            for (var i = 0; i < allFile.length; i++) {
                allFileName.push(allFile[i].name);
                html += '<label style="width: unset" class="layui-form-label file_name">' + allFile[i].name + '</label>'
            }
            /*console.log(allFile);
            console.log(allFileName);*/
            $('.file_list').append(html);
            upload1();
            /*for (var j = 0; j < allFile.length; j++) {
                var reader = new FileReader();
                reader.onload = function (res) {
                    uploadFile(res.target.result, allFile);
                };
                reader.readAsDataURL(allFile[j]);
            }*/

        });
        var totalFile = [];
        var allDataFilePath = [];

        function uploadFile(base64File, allFile) {
            totalFile.push(base64File);
            if (totalFile.length == allFile.length) {
                var index = layer.load(1);
                for (var s = 0; s < totalFile.length; s++) {
                    $.ajax({
                        url: '/file/upload',
                        data: {file: totalFile[s], filename: allFile[s].name, index: s},
                        dataType: 'json',
                        type: 'post',
                        async:false,
                        success: function (res) {
                            if (!res.Success) {
                                $('.file_name:eq(' + res.ResultData.index + ')').html(res.ResultData.filename + '<p style="color: red;">　' + res.Message + '</p>');
                                layer.msg(res.Message, function () {
                                })
                            } else {
                                getAjaxReturn(res.ResultData);
                            }
                        },
                        error: function () {

                        }
                    });
                }
                layer.close(index);
            }
        }

        function getAjaxReturn(data) {
            allDataFilePath.push(data);
        }

        form.on('submit({{makeElUniqueName('file_save')}})', function () {
            if(allDataFilePath.length == 0){
                layer.msg('未上传文件', {icon: 5});
                return false;
            }
            var index = layer.load(1);
            $.ajax({
                contentType: "application/json",
                type: 'post',
                url: '/file/add',
                data: JSON.stringify(allDataFilePath),
                success: function (outResult) {
                    layer.close(index);
                    if (outResult.Success) {
                        layer.msg(outResult.Message, {icon: 6});
                        layer.close(popLayerUtil.index);
                        //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                        //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                        //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                        popLayerUtil.onClose();
                    } else {
                        layer.msg(outResult.Message, {icon: 5});
                    }
                },
                error: function (error) {
                    layer.close(index);
                    layui.validator.processValidateError(error);
                }
            });
            return false;
        });
    })


</script>
