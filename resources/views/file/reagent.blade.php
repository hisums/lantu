<div class="layui-form layui-field-box">

</div>
<div id="{{makeElUniqueName('reagent')}}"></div>
<script>
    layui.use(['jfTable', 'form', 'addressUtil', 'dateRangeUtil', 'validator'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var jfTable = layui.jfTable;
        var form = layui.form();
        var addressUtil = layui.addressUtil;
        var dateRangeUtil = layui.dateRangeUtil;

        form.render();
        layui.define(function (exports) {
            var obj = {
                doEdit: function (id) {
                    $.post('/reagent/editIndex', {id: id}, function (str) {
                        var popLayerUtil = layui.popLayerUtil;
                        popLayerUtil.doPopUp({
                            index: layer.open({
                                id: '{{makeElUniqueName('editReagent')}}',
                                title: '编辑',
                                type: 1,
                                content: str,
                                area: ['800px', '600px']
                            }),
                            onClose: function () {
                                layui.reagentFunc.refreshTableGrid();
                            }
                        });
                    });
                },
                refreshTableGrid: function () {
                    $('input[name=\'{{makeElUniqueName('name')}}\']').val('');
                    $("#{{makeElUniqueName('reagent')}}").jfTable("reload");
                }
            };
            exports('reagentFunc', obj);
        });

        $("#{{makeElUniqueName('reagent')}}").jfTable({
            url: '/reagent/query',
            pageSize: 5,
            page: true,
            skip: true,
            first: '首页',
            last: '尾页',
            columns: [{
                text: '操作',
                name: 'id',
                width: 200,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    return '<a class="layui-btn layui-btn-small layui-btn-warning" onclick="layui.reagentFunc.doEdit(' + value + ')"><i class="layui-icon">&#xe618;</i> 编辑</a>';
                }
            }, {
                text: '名称',
                name: 'name',
                width: 170,
                align: 'center'
            }, {
                text: '文件名称',
                name: 'serviceFile.name',
                width: 170,
                align: 'center'
            }, {
                text: '图片',
                name: 'reagent_img',
                width: 170,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    return '<img style="height: 170px;width: 170px;" src="' + dataItem.getFullPath + '">';
                }
            }, {
                text: '查看文件',
                name: 'file_id',
                width: 150,
                align: 'center',
                formatter: function (value, dataItem, index) {
                    if(value){
                        return '<a href="{{\App\ServiceFiles::$FILEFULLPATH}}'+ dataItem.serviceFile.file_id +'">查看文件</a>';
                    }else{
                        return '暂无关联文件';
                    }
                }
            }
            ],
            queryParam: {

            },
            method: 'post',
            toolbarClass: 'layui-btn-small',
            onBeforeLoad: function (param) {
                return $.extend(param, {

                });
            },
            onLoadSuccess: function (data) {
                return data;
            },
            dataFilter: function (data) {
                return data;
            }
        });

    });
</script>
