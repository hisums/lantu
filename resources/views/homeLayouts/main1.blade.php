<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{$title}}</title>

    <link href="/css/home/index.css" rel="stylesheet" type="text/css"/>
    <link href="/css/home/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script>
        var sUserAgent=navigator.userAgent;
        var mobileAgents=['Android','iPhone','Symbian','WindowsPhone','iPod','BlackBerry','Windows CE'];
        var goUrl = 0;
        for( var i=0;i<mobileAgents.length;i++){
            if(sUserAgent.indexOf(mobileAgents[i]) > -1){
                goUrl = 1;
                break;
            }
        }
        /*if (goUrl == 1){
            var url = window.location.pathname;
            var route = url.split('/').splice(1);
            if(route[1] == 'article'){
                if(route[2] == 'article_list'  ||route[2] == 'activity' ||route[2] == 'article_list_all'){
                    var id = route[3];
                    u = "{{url('mobile/news',['id'=>'ids'])}}";
                    u = u.replace("ids",id);
                    location = u;
                }else if(route[2] == 'testing_center'){
                    location = "{{url('mobile/testing_center')}}";
                }else if(route[2] == 'activityDetail' || route[2] == 'articleDetail'){
                    var id = route[3];
                    u = "{{url('mobile/news_detail',['id'=>'ids'])}}";
                    u = u.replace("ids",id);
                    location = u;
                }else if(route[2] == 'aboutUs' || route[2] == 'contactUs'){
                    location = "{{url('mobile/about_us')}}";
                }else if(route[2] == 'testing_sub'){
                    var cate_id = route[3];
                    var cate_sub_id = route[4];
                    u = "{{url('mobile/testing_sub',['cate_id' =>'cate_ids','cate_sub_id' => 'cate_sub_ids'])}}";
                    u = u.replace("cate_ids",cate_id).replace("cate_sub_ids",cate_sub_id);
                    location = u;
                }
            }else if(route[1] == 'goods'){
                if(route[2] == 'list'){
                    if(!route[3]){
                        location = "{{url('mobile/product')}}";
                    }else{
                        if(!route[4]){
                            var cate_id = route[3];
                            u = "{{url('mobile/product',['cate_id'=>'cate_ids'])}}";
                            u = u.replace('cate_ids',cate_id);
                            location = u;
                        }else{
                            var value = window.location.search;
                            value = value.split('=').pop();
                            u = "{{ url('mobile/search?keywords=value') }}";
                            u = u.replace("value",value);
                            location = u;
                        }
                    }
                }else if(route[2] == 'detail'){
                    var id = route[3];
                    var spe_id = route[4];
                    if(spe_id){
                        u = "{{url('mobile/product_detail',['id'=>'ids','spe_id'=>'spe_ids'])}}";
                        u = u.replace("ids",id).replace("spe_ids",spe_id);
                    }else{
                        u = "{{url('mobile/product_detail',['id'=>'ids'])}}";
                        u = u.replace("ids",id);
                    }
                    location = u;
                }
            }else if(route[1] = 'register'){
                location = "{{url('mobile/register')}}";
            }else{
                location ="{{url('mobile')}}";
            }*/
        }
    </script>
</head>
<style>
    html {
        min-width: 1360px;
    }

    input {
        outline: none;
    }

    select {
        outline: none;
    }

    .hideTab {
        display: none;
    }

    .right-fixed-box {
        position: fixed;
        height: 250px;
        width: 50px;
        background-color: rgba(0, 0, 0, 0.7);
        z-index: 10;
        right: 0;
        top: 330px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }

    .icon-div-height {
        height: 60px;
        width: 100%;
    }

    .right-fixed-box-detail {
        position: fixed;
        height: 150px;
        width: 150px;
        z-index: 10;
        right: 50px;
        top: 380px;
        background-color: rgba(0, 0, 0, 0.7);
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }

    .common-right-fixed-box-detail {
        color: #ffffff;
        width: 80%;
        font-size: 15px;
    }

    #qq-api-btn {
        height: 30px;
        width: 85%;
        border: 1px solid #77A6EC;
        color: #77A6EC;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        margin-top: 10px;
        cursor: pointer;
    }

    li a {
        color: #ffffff;
    }

    .company-qrcode {
        position: absolute;
        width: 150px;
        height: 150px;
        background-color: #ffffff;
        margin-top: 15px;
        margin-left: -45px;
    }

    .hidden-company-qrcode {
        display: none;
    }

    .triangle {
        width: 0;
        height: 0;
        border-right: 10px solid transparent;
        border-bottom: 10px solid #ffffff;
        border-left: 10px solid transparent;
        position: absolute;
        margin-top: 5px;
        margin-left: 18px;
    }
</style>
<body>


<div style="width: 100%;" class="tb flex flex-jfcontent-center">
    <div class="dl flex">
        <div class="dlwz flex">
            <div class="dlwz1">欢迎来到蓝图生物！</div>
            @if(session('userInfo'))
                <div class="dlwz2">{{session('userInfo')['name']}}</div>
                <div class="dlwz3 logout">[<a>注销</a>]</div>
            @else
                <div class="dlwz2">[<a href="{{url('web/login')}}">请登录</a>]</div>
                <div class="dlwz3">[<a href="{{url('web/register')}}">注册</a>]</div>
            @endif
        </div>
        <div class="dianhua flex flex-jfcontent-space-between">
            <div class="dianhua1">客服热线：</div>
            <div class="dianhua2">0714-6338558</div>

        </div>
    </div>
</div>

<div style="width: 100%" class="tb1 flex flex-jfcontent-center">
    <div class="tb11 flex">
        <a href="{{url('web/index')}}" class="logo"><img class="logoImg" src="/images/home/logo.png"/></a>
        <div class="sousuo">
            <div class="search-goods">
                <form class="flex flex-jfcontent-space-between flex-align-items-center"
                      action="{{url('web/goods/list',[0,2])}}" method="get" id="searchForm" onsubmit="setCookie('productListPage','',-1);">
                    <input type="text" name="keywords" class="search-goods-input" placeholder="请输入搜索产品目录或者产品名称">
                    <img class="icon1" src="/images/home/searchLogo.png">
                </form>
            </div>
        </div>

        {{--<div class="icon">
            <div class="dingdan"><img class="ddicon" src="/images/home/orderLogo.png"/>
                <div class="text-dd">我的订单</div>
            </div>

            <div class="gwc"><img class="gwcicon" src="/images/home/car.png"/>

                <div class="text-gwc">购物车</div>
            </div>
        </div>--}}
    </div>
</div>

</div>
<div class="zj">
    <div class="dh flex flex-jfcontent-center">
        <div class="dh-sub">
            <div class="guanggao"><img src="/images/home/lantuLogo.png"/></div>
            <div class="all-cate"><img src="/images/home/listLogo.png"><span>全部分类</span>
                <div {{isset($show) && $show?'style=display:block;!important':''}} class="fenlei hideTab">
                    <div class="box2">
                        <ul class="left">
                            @foreach($cates as $k=>$v)
                                <li class="ys"><a href="{{url('web/goods/list',['cate_id'=>$v['id']])}}">{{$v['name']}}</a>
                                    <div class="sec_section">
                                        <div class="sec_section_sub_box">
                                            @foreach($v['children'] as $k1=>$v1)
                                            <div class="sec_section_sub flex">
                                                <a href="{{url('web/goods/list',['cate_id'=>$v1['id']])}}" class="sec_section_sub_text flex-jfcontent-end">{{$v1['name']}}</a>
                                                @if(count($v1['children']) != 0)
                                                <img src="/images/home/right.png" class="tab-icon">
                                                @endif
                                                <div class="thrid_section_box flex-wrap">
                                                    @foreach($v1['children'] as $k2=>$v2)
                                                    <a href="{{url('web/goods/list',['cate_id'=>$v2['id']])}}" class="thrid_section flex-align-items-center flex-jfcontent-center">{{$v2['name']}}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    {{--@foreach($v['children'] as $k1=>$v1)--}}
                                        {{--<ul>--}}
                                            {{--<li class="ha_1"><a--}}
                                                        {{--href="{{url('web/goods/list',['cate_id'=>$v1['id']])}}">{{$v1['name']}}</a>--}}
                                                {{--@foreach($v1['children'] as $k2=>$v2)--}}
                                                    {{--<ul>--}}
                                                        {{--<li>--}}
                                                            {{--<a href="{{url('web/goods/list',['cate_id'=>$v2['id']])}}">{{$v2['name']}}</a>--}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                {{--@endforeach--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--@endforeach--}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="sub-tab">
                <div>
                    <div id="menu_left" class="menu">
                        <ul id="menu-homemenu" class="top-menu-main">
                            <li id="menu-item-18"
                                class="top-menu menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-18">
                                <a href="{{ url('web/index') }}">首页</a></li>
                            <li id="menu-item-20"
                                class="top-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-20">
                                <a href="{{url('web/goods/list')}}">产品中心</a>
                            </li>
                            <li id="menu-item-21"
                                class="top-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-21">
                                <a href="{{url('web/article/testing_center')}}">检测中心</a>
                                {{--<ul class="sub-menu">
                                    <li id="menu-item-31"
                                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-31">
                                        <a href="{{url('web/article/testing_center')}}">中心简介</a>
                                    </li>
                                    <li id="menu-item-30"
                                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-30">
                                        <a href="{{url('web/article/testing_detection')}}">检测流程</a>
                                    </li>
                                </ul>--}}
                            </li>
                            <li id="menu-item-22"
                                class="top-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-22">
                                <a href="http://bbs.lantubio.com/index.php?m=bbs">实验论坛</a>
                                {{--<ul class="sub-menu">
                                    <li id="menu-item-709"
                                        class="menu-item menu-item-type-post_type menu-item-object-post menu-item-709">
                                        <a href="http://bbs.lantubio.com/index.php?c=cate&fid=1">ELISA实验</a></li>
                                    <li id="menu-item-708"
                                        class="menu-item menu-item-type-post_type menu-item-object-post menu-item-708">
                                        <a href="http://bbs.lantubio.com/index.php?c=cate&fid=3">CLIA实验</a></li>
                                    <li id="menu-item-708"
                                        class="menu-item menu-item-type-post_type menu-item-object-post menu-item-708">
                                        <a href="http://bbs.lantubio.com/index.php?c=cate&fid=4">生化实验</a></li>
                                    <li id="menu-item-708"
                                        class="menu-item menu-item-type-post_type menu-item-object-post menu-item-708">
                                        <a href="http://bbs.lantubio.com/index.php?c=cate&fid=5">缓冲液配制</a></li>
                                </ul>--}}
                            </li>
                            <li id="menu-item-23"
                                class="top-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-23">
                                <a href="{{ url('web/article/article_list') }}">资讯中心</a>
                                {{--<ul class="sub-menu">
                                    <li id="menu-item-138"
                                        class="menu-item menu-item-type-post_type menu-item-object-post menu-item-138">
                                        <a href="{{ url('web/article/activity') }}">促销活动</a></li>
                                    --}}{{-- <li id="menu-item-137"
                                         class="menu-item menu-item-type-post_type menu-item-object-post menu-item-137">
                                         <a href="{{ url('web/article/article_list_all',[\App\NewsCategory::$DEFAULT_CATE_CPFK]) }}">产品反馈</a></li>--}}{{--
                                    <li id="menu-item-136"
                                        class="menu-item menu-item-type-post_type menu-item-object-post menu-item-136">
                                        <a href="{{ url('web/article/article_list') }}">技术文章</a></li>
                                </ul>--}}
                            </li>
                            <li id="menu-item-24"
                                class="top-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-24">
                                <a href="{{url('web/article/aboutUs')}}">联系我们</a>
                                {{--<ul class="sub-menu">
                                    <li id="menu-item-56"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56">
                                        <a href="{{url('web/article/aboutUs')}}">蓝图介绍</a></li>
                                    <li id="menu-item-55"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55">
                                        <a href="{{url('web/article/jobOfficer')}}">招聘启事</a></li>
                                    <li id="menu-item-55"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55">
                                        <a href="{{url('web/article/contactUs')}}">联系方式</a></li>
                                    <li id="menu-item-55"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a
                                                href="http://www.lst-pharm.com/online-join-apply">公司荣誉</a></li>
                                    <li id="menu-item-55"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a
                                                href="http://www.lst-pharm.com/online-join-apply">专利资质</a></li>
                                </ul>--}}
                            </li>
                        </ul>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="sub-title"></div>
        </div>
    </div>
</div>
<div class="right-fixed-box-detail flex flex-jfcontent-center hideTab">
    <div class="QQ-connect-detail common-right-fixed-box-detail flex flex-direction-col flex-jfcontent-center hideTab">
        <div style="height: 40px;" class="flex flex-align-items-center">经销咨询</div>
        <div style="height: 40px;" class="flex flex-align-items-center">在线沟通，请点我</div>
        <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=355902222&site=qq&menu=yes">
            <div id="qq-api-btn" class="flex flex-align-items-center qq-api-btn flex-jfcontent-center">在线咨询</div>
        </a>
    </div>
    <div class="phone-connect-detail common-right-fixed-box-detail flex flex-direction-col flex-jfcontent-center hideTab">
        <div style="height: 40px;" class="flex flex-align-items-center">客服热线</div>
        <div style="height: 40px;" class="flex flex-align-items-center">0714-6338558</div>
    </div>
    <div class="wechat-connect-detail common-right-fixed-box-detail flex flex-align-items-center flex-direction-col flex-align-items-center hideTab">
        <img style="width: 90%;margin-top:10px;margin-bottom: 5px" src="/images/home/wechat-qrcode.jpg">
        <div>扫一扫关注我们</div>
    </div>
</div>
<div class="right-fixed-box flex flex-direction-col flex-align-items-center flex-jfcontent-center">
    <div class="QQ-connect icon-div-height flex flex-align-items-center flex-jfcontent-center">
        <img src="/images/home/qq.png">
    </div>
    <div class="phone-connect icon-div-height flex flex-align-items-center flex-jfcontent-center">
        <img src="/images/home/phone.png">
    </div>
    <div class="wechat-connect icon-div-height flex flex-align-items-center flex-jfcontent-center">
        <img src="/images/home/wechati.png">
    </div>
</div>
@yield('content')
<div class="wy">
    <div class="wyjz flex">
        <div class="lxfs">
            <div class="ls">联系方式</div>
            <ul class="lxfs_1">
                @foreach($about as $v)
                <li class="lxfs_2">>{{$v->name}}: {{$v->content}}</li>
                @endforeach
                <li class="lxfs_2">>地址: 湖北省黄石市黄石港区磁湖路41号 </li>
            </ul>
        </div>
        <div class="fenduan"></div>
        <div class="sjrk">
            <div class="lj">社交入口</div>
            <ul class="xiaotubiao" style="list-style:none;width:300px;">
                <li style="display:inline-block">
                    <a id="company-website-qrcode" href="#" style="width:60px;height:60px">
                        <img src="/images/home/wechat.png"/>
                    </a>
                    <div class="triangle hidden-company-qrcode"></div>
                    <div class="company-qrcode flex-align-items-center flex-jfcontent-center hidden-company-qrcode">
                        <img style="width: 120px;height: 120px" src="/images/home/lantu-company-qrcode.png">
                    </div>
                </li>
                <li style="display:inline-block">
                    <a href="https://weibo.com/6516530126/profile?topnav=1&wvr=6&is_all=1" style="width:60px;height:60px; margin-left:10px;">
                        <img src="/images/home/sina.png"/>
                    </a>
                </li>
                <li style="display:inline-block">
                    <a href="https://www.zhihu.com/people/biolantu.com/activities" style="width:60px;height:60px; margin-left:10px;">
                        <div style="width: 50px;height: 50px;background-color: #1B61DE;border-radius: 5px;margin-left: 10px;"><img src="/images/home/zhihu.png"/></div>
                    </a>
                </li>
                <li style="display:inline-block">
                    <a href="https://www.linkedin.com/in/biolantu/" style="width:60px;height:60px; margin-left:10px;">
                        <img src="/images/home/in.png"/>
                    </a>
                </li>
            </ul>


        </div>
        <div class="fenduan"></div>

        <div class="qtfw flex">
            <div class="lxfs sy-width">
                <div class="sylb">实验类别</div>
                <ul class="lxfs_1">
                    <li class="lxfs_3"><a href="{{url('web/goods/list')}}">>产品中心</a></li>
                    <li class="lxfs_3"><a href="{{url('web/article/testing_center')}}">>检测中心</a></li>
                    <li class="lxfs_3"><a href="http://bbs.lantubio.com/index.php?m=bbs&c=forumlist">>实验论坛</a></li>
                    <li class="lxfs_3"><a href="{{ url('web/article/article_list') }}">>资讯中心</a></li>
                    <li class="lxfs_3"><a href="{{url('web/article/aboutUs')}}">>联系我们</a></li>
                </ul>
            </div>
            <div class="lxfs sy-width">
                <div class="sylb">公司信息</div>
                <ul class="lxfs_1">
                    <li class="lxfs_3"><a href="{{url('web/article/aboutUs')}}">>公司介绍</a></li>
                    <li class="lxfs_3"><a href="{{url('web/article/jobOfficer')}}">>招聘启事</a></li>
                    <li class="lxfs_3">>公司荣誉</li>
                    <li class="lxfs_3">>专利资质</li>
                </ul>
            </div>

            {{-- <div class="lxfs sy-width">
                 <div class="sylb">我的账户</div>
                 <ul class="lxfs_1">
                     <li class="lxfs_3">>账户注册</li>
                     <li class="lxfs_3">>订购历史</li>
                     <li class="lxfs_3">>我的收藏</li>
                     <li class="lxfs_3">>付款结果</li>
                     <li class="lxfs_3">>订单跟踪</li>
                     <li class="lxfs_3">>咨询帮助</li>
                 </ul>
             </div>--}}
        </div>
    </div>
</div>
</div>
<div class="wy1" style="display:flex; justify-content:center; align-items:center; color:#fff;">
    <div style="margin:0 20px;">
        <p><span>备案号:</span>鄂ICP备18013916号-1</p>
    </div>
</div>
</body>
</html>
<script src="/vendor/layui/layui.js" charset="utf-8"></script>
<script>
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    $('.logout').click(function () {
        $.ajax({
            url: "{{url('web/logout')}}",
            success: function () {
                window.location.reload();
            }
        })
    });
    $(".icon1").click(function () {
        setCookie('productListPage','',-1);
        setCookie('productListPageSize','',-1);
        $("#searchForm").submit();
    });
    layui.use(['jquery'], function () {
        var $ = layui.jquery;
        $('.all-cate').mousemove(function () {
            $('.fenlei').removeClass('hideTab');
        });

        $('.all-cate').mouseout(function () {
            $('.fenlei').addClass('hideTab');
        });

        $('.icon-div-height').mousemove(function () {
            $('.right-fixed-box-detail').removeClass('hideTab');
        });

        $('.icon-div-height').mouseout(function () {
            $('.right-fixed-box-detail').addClass('hideTab');
        });

        $('.right-fixed-box-detail').mousemove(function () {
            $('.right-fixed-box-detail').removeClass('hideTab');
        });
        $('.right-fixed-box-detail').mouseout(function () {
            $('.right-fixed-box-detail').addClass('hideTab');
        });

        $('.QQ-connect').mousemove(function () {
            $('.QQ-connect-detail').removeClass('hideTab');
            $('.phone-connect-detail').addClass('hideTab');
            $('.wechat-connect-detail').addClass('hideTab');
        });
        $('.phone-connect').mousemove(function () {
            $('.QQ-connect-detail').addClass('hideTab');
            $('.phone-connect-detail').removeClass('hideTab');
            $('.wechat-connect-detail').addClass('hideTab');
        });
        $('.wechat-connect').mousemove(function () {
            $('.QQ-connect-detail').addClass('hideTab');
            $('.phone-connect-detail').addClass('hideTab');
            $('.wechat-connect-detail').removeClass('hideTab');
        });
        $('#company-website-qrcode').mouseout(function () {
            $('.triangle').addClass('hidden-company-qrcode');
            $('.company-qrcode').addClass('hidden-company-qrcode');
        });
        $('#company-website-qrcode').mouseover(function () {
            $('.triangle').removeClass('hidden-company-qrcode');
            $('.company-qrcode').removeClass('hidden-company-qrcode');
        })
    });
</script>
