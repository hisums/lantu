@extends('homeLayouts.main')
@section('content')
    <style>
        .center-main-body {
            width: 1200px;
            position: relative;
            margin-left: -600px;
            left: 50%;
        }

        .center-main-body-left {
            width: 200px;
            margin-top: 50px;
        }

        .center-main-body-left-margin-top {
            width: 200px;
            margin-top: 480px;
        }

        .connect-us-content-detail {
            width: 90%;
            font-size: 13px;
        }

        .connect-us-sub {
            margin-top: 10px;
            font-size: 13px;
            width: 94%;
            border-bottom: 1px solid #999999;
        }

        .connect-us-sub div {
            width: 95%;
            height: 30px;
        }

        .connect-us {
            background-color: #ffffff;
            width: 100%;
            border-bottom: 1px solid #eeeeee;
        }

        .connect-us-title {
            width: 100%;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            height: 40px;
            background-color: #457AC6;
            color: #ffffff;
            letter-spacing: 4px;
            font-size: 18px;
        }

        .connect-us-content {
            height: 60px;
        }

        .product-list {
            width: 100%;
            margin-top: 30px;
            margin-bottom: 50px;
        }

        .product-list-sub {
            width: 90%;
            margin-top: 15px;
        }

        .ad {
            margin-top: 30px;
        }

        .ad-img {
            margin-top: 10px;
            margin-bottom: 10px;
            width: 100%;
        }

        .center-main-body-right {
            width: 950px;
            margin-bottom: 50px;
        }

        .piece-navigate {
            height: 30px;
            font-size: 14px;
            text-align: left;
            line-height: 40px;
            margin-bottom: 20px;
        }
    </style>
    <div class="center-main-body flex flex-jfcontent-space-between">
        <div class="center-main-body-left {{isset($show) && $show?'center-main-body-left-margin-top':''}}">
            <div class="connect-us flex flex-direction-col flex-align-items-center">
                <div class="connect-us-title flex flex-align-items-center flex-jfcontent-center">
                    联系我们
                </div>
                <div class="connect-us-sub flex flex-direction-col flex-align-items-center ">
                    @foreach($about as $v)
                        <div class="flex-align-items-center">{{$v->name}}：{{$v->content}}</div>
                    @endforeach
                    {{--<div class="flex-align-items-center">QQ ：355902222</div>
                    <div class="flex-align-items-center">邮箱：355902222@qq.com</div>--}}
                </div>
                <div class="connect-us-content flex flex-align-items-center flex-jfcontent-center">
                    <div class="connect-us-content-detail">
                        Note:为了方便,您也可以在我们网站右侧点击在线客服咨询!
                    </div>
                </div>
            </div>
            <div class="product-list  flex flex-direction-col flex-align-items-center">
                <div class="connect-us-title flex flex-align-items-center flex-jfcontent-center">
                    产品目录
                </div>
                @foreach($reagent as $v)
                    <div class="product-list-sub flex flex-align-items-center flex-jfcontent-center">
                        <a href="{{\App\ServiceFiles::$FILEFULLPATH.$v['serviceFile']['file_id']}}"><img
                                    style="width: 100%" src="{{$v['getFullPath']}}"></a>
                    </div>
                @endforeach
                {{--<div class="product-list-sub flex flex-align-items-center">
                    <img style="width: 100%" src="/images/home/phygene_products_catalog.gif">
                </div>
                <div class="product-list-sub flex flex-align-items-center">
                    <img style="width: 100%" src="/images/home/phygene_products_catalog_2.gif">
                </div>--}}
            </div>
            {{--<div class="ad flex flex flex-direction-col">
                <div style="letter-spacing: 1px;"
                     class="connect-us-title flex flex-align-items-center flex-jfcontent-center">
                    FAVOURABLE
                </div>

                <a href="{{url('web/register')}}" class="ad-detail flex flex-align-items-center flex-jfcontent-center">
                    <img class="ad-img" src="/images/home/demo-bn-side.jpg">
                </a>
            </div>--}}
        </div>
        <div class="center-main-body-right">
            <div class="piece-navigate">
                @if(isset($act))
                    <a style="color: #000;" href="{{url('web/index')}}">首页</a>>>

                        <a style="color: #000;" href="{{url('web/article/activity')}}">促销活动</a>{{isset($activity_title)?'>>'.$activity_title:''}}

                @else
                    <a style="color: #000;" href="{{url('web/index')}}">首页</a>>><a style="color: #000;"
                                                                                   href="{{url('web/goods/list')}}">产品中心</a>{{!empty($cate_id)?getUpCate($cate_id):''}}
                @endif

                {{--首页>>{{$title}}{{isset($sec_title)?'>>'.$sec_title:''}}--}}
            </div>
            <div>
                @yield('subContent')
            </div>
        </div>
    </div>
@stop