layui.define(['form','jquery', 'laypage'], function (exports) {
    var $ = layui.jquery;
    var layer = layui.layer;
    var element = layui.element();

    var obj = {
        initProvince: function (options) {
            var form = layui.form();
            var provinceSelect = options.element;
            var defaultValue = options.defaultValue?options.defaultValue:0;
            var hasEmptyItem = options.hasEmptyItem?options.hasEmptyItem:true;

            provinceSelect.find('option').each(function(){
                $(this).remove();
            });

            if (hasEmptyItem) {
                provinceSelect.append($('<option>').attr('value', '0').text('(请选择省份)'));
            }

            $.ajax({
                type: 'get',
                url: '/backstage/api/province/getall',
                success: function (data) {
                    for (var i=0; i<data.length; i++) {
                        var province = data[i];
                        if (province.id == defaultValue) {
                            provinceSelect.append($('<option>').attr('value', province.id).attr('selected', true).text(province.name));
                        } else {
                            provinceSelect.append($('<option>').attr('value', province.id).text(province.name));
                        }
                    }

                    form.render('select');

                    if (options.onComplete) {
                        options.onComplete.call(this, provinceSelect);
                    }
                    if (options.onChange) {
                        var layfilter = provinceSelect.attr('lay-filter');
                        form.on('select(' + layfilter + ')', function(data){
                            options.onChange.call(this, data.value, data.elem, data.othis);
                        });
                    }
                },
                error: function (e) {
                    var message = e.responseText;
                    layer.msg(message, { icon: 2 });
                }
            });
        },
        initCityByProvince: function(options) {
            var form = layui.form();
            var citySelect = options.element;
            var defaultValue = options.defaultValue?options.defaultValue:0;
            var hasEmptyItem = options.hasEmptyItem?options.hasEmptyItem:true;

            citySelect.find('option').each(function(){
                $(this).remove();
            });

            if (hasEmptyItem) {
                citySelect.append($('<option>').attr('value', '0').text('(请选择城市)'));
            }

            if (options.provinceId != '') {
                $.ajax({
                    type: 'get',
                    url: '/backstage/api/city/getbyprovince/' + options.provinceId,
                    success: function (data) {

                        for (var i=0; i<data.length; i++) {
                            var city = data[i];
                            if (city.id == defaultValue) {
                                citySelect.append($('<option>').attr('value', city.id).attr('selected', true).text(city.name));
                            } else {
                                citySelect.append($('<option>').attr('value', city.id).text(city.name));
                            }
                        }

                        form.render('select');

                        if (options.onComplete) {
                            options.onComplete.call(this, citySelect);
                        }
                        if (options.onChange) {
                            var layfilter = citySelect.attr('lay-filter');
                            form.on('select(' + layfilter + ')', function(data){
                                options.onChange.call(this, data.value, data.elem, data.othis);
                            });
                        }
                    },
                    error: function (e) {
                        var message = e.responseText;
                        layer.msg(message, { icon: 2 });
                    }
                });
            } else {
                form.render('select');

                if (options.onComplete) {
                    options.onComplete.call(this, citySelect);
                }
                if (options.onChange) {
                    var layfilter = citySelect.attr('lay-filter');
                    form.on('select(' + layfilter + ')', function(data){
                        options.onChange.call(this, data.value, data.elem, data.othis);
                    });
                }
            }
        },
        initDistrictByCity: function(options) {
            var form = layui.form();

            var districtSelect = options.element;
            var defaultValue = options.defaultValue?options.defaultValue:0;
            var hasEmptyItem = options.hasEmptyItem?options.hasEmptyItem:true;

            districtSelect.find('option').each(function(){
                $(this).remove();
            });

            if (hasEmptyItem) {
                districtSelect.append($('<option>').attr('value', '0').text('(请选择区县)'));
            }

            if (options.cityId != '') {
                $.ajax({
                    type: 'get',
                    url: '/backstage/api/district/getbycity/' + options.cityId,
                    success: function (data) {
                        for (var i=0; i<data.length; i++) {
                            var city = data[i];
                            if (city.id == defaultValue) {
                                districtSelect.append($('<option>').attr('value', city.id).attr('selected', true).text(city.name));
                            } else {
                                districtSelect.append($('<option>').attr('value', city.id).text(city.name));
                            }
                        }

                        form.render('select');

                        if (options.onComplete) {
                            options.onComplete.call(this, districtSelect);
                        }
                        if (options.onChange) {
                            var layfilter = districtSelect.attr('lay-filter');
                            form.on('select(' + layfilter + ')', function(data){
                                options.onChange.call(this, data.value, data.elem, data.othis);
                            });
                        }
                    },
                    error: function (e) {
                        var message = e.responseText;
                        layer.msg(message, { icon: 2 });
                    }
                });
            } else {
                form.render('select');

                if (options.onComplete) {
                    options.onComplete.call(this, districtSelect);
                }
                if (options.onChange) {
                    var layfilter = districtSelect.attr('lay-filter');
                    form.on('select(' + layfilter + ')', function(data){
                        options.onChange.call(this, data.value, data.elem, data.othis);
                    });
                }
            }
        },
        twoLevelAddressSelect: function(options) {
            var addressUtil = layui.addressUtil;

            addressUtil.initProvince({
                element: options.provinceSelect,
                defaultValue: options.defaultProvince?options.defaultProvince:0,
                hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true,
                onComplete: function(provinceSelect) {
                    addressUtil.initCityByProvince({
                        element: options.citySelect,
                        provinceId: provinceSelect.val(),
                        defaultValue: options.defaultCity?options.defaultCity:0,
                        hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true
                    });
                },
                onChange: function(val, ele, othis) {
                    addressUtil.initCityByProvince({
                        element: options.citySelect,
                        provinceId: val,
                        defaultValue: options.defaultCity?options.defaultCity:0,
                        hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true
                    });
                }
            });
        },
        threeLevelAddressSelect: function(options) {
            var addressUtil = layui.addressUtil;

            addressUtil.initProvince({
                element: options.provinceSelect,
                defaultValue: options.defaultProvince?options.defaultProvince:0,
                hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true,
                onComplete: function(provinceSelect) {
                    addressUtil.initCityByProvince({
                        element: options.citySelect,
                        provinceId: provinceSelect.val(),
                        defaultValue: options.defaultCity?options.defaultCity:0,
                        hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true,
                        onComplete: function(citySelect) {
                            addressUtil.initDistrictByCity({
                                element: options.districtSelect,
                                cityId: citySelect.val(),
                                defaultValue: options.defaultDistrict?options.defaultDistrict:0,
                                hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true
                            });
                        },
                        onChange: function(val, ele, othis) {
                            addressUtil.initDistrictByCity({
                                element: options.districtSelect,
                                cityId: val,
                                defaultValue: options.defaultDistrict?options.defaultDistrict:0,
                                hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true
                            });
                        }
                    });
                },
                onChange: function(val, ele, othis) {
                    addressUtil.initCityByProvince({
                        element: options.citySelect,
                        provinceId: val,
                        defaultValue: options.defaultCity?options.defaultCity:0,
                        hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true,
                        onComplete: function(citySelect) {
                            addressUtil.initDistrictByCity({
                                element: options.districtSelect,
                                cityId: citySelect.val(),
                                defaultValue: options.defaultDistrict?options.defaultDistrict:0,
                                hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true
                            });
                        },
                        onChange: function(val, ele, othis) {
                            addressUtil.initDistrictByCity({
                                element: options.districtSelect,
                                cityId: val,
                                defaultValue: options.defaultDistrict?options.defaultDistrict:0,
                                hasEmptyItem: options.hasEmptyItem?options.hasEmptyItem:true
                            });
                        }
                    });
                }
            });
        }
    };
    exports('addressUtil', obj);
});
