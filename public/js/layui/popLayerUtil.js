layui.define('layer', function(exports){
    var obj = {
        doPopUp: function(options) {
            this.index = options.index;
            this.onClose = options.onClose;
        }
    };
    exports('popLayerUtil', obj);
});
