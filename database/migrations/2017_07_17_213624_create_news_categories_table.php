<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * 新闻分类表
        */
        Schema::create('news_categories', function ($table) {
            $table->increments('id')->unsigned()->comment('新闻分类id');

            $table->string('name', 100)->nullable()->comment('新闻分类名称');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('显示顺序');

            $table->string('news_cate_picture', 255)->nullable()->comment('新闻分类主图片');
            $table->tinyInteger('display_flag')->nullable()->comment('显示标志：1=显示，2=不显示');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_categories');
    }
}
