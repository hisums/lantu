<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_groups', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name', 100)->nullable()->comment('用户组名');
            $table->string('code', 100)->nullable()->comment('用户组编号');
            $table->string('business_code', 100)->nullable()->comment('营业执照编号');
            $table->tinyInteger('group_type')->default(1)->nullable()->comment('用户组类型：1=外部企业单位，2=内部组织机构，3=内部部门，4=个人组');
            $table->tinyInteger('active_status')->default(0)->comment('用户组有效状态：1=未生效，2=已生效，3=已禁用');
            $table->bigInteger('parent_id')->unsigned()->nullable()->comment('父用户组ID');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('同级排序号');

            $table->bigInteger('manager_id')->unsigned()->nullable()->comment('主管理员id');
            $table->foreign('manager_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_groups', function ($table) {
            $table->dropForeign(['manager_id']);
        });
        Schema::dropIfExists('user_groups');
    }
}
