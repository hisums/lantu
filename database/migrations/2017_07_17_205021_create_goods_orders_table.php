<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
        * 商品订单表：
        *      订单是主从结构，主表是存储一般交易信息，从表存储商品信息
        *
        *    只要进入商品结算页，就会产生订单，订单是从购物车里转储数据过来
        */
        Schema::create('goods_orders', function ($table) {
            $table->bigIncrements('id')->unsigned()->comment('订单id');
            //订单号在业务上有两个作用，方便用户和客服查单
            //用来进行微信支付或者支付宝等在线支付的回调
            $table->string('order_no', 40)->nullable()->unique()->comment('订单号，根据系统规则自动产生的流水号');
            //
            $table->bigInteger('user_id')->unsigned()->comment('购买用户id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->decimal('price', 10, 2)->nullable()->comment('支付价格');

            $table->dateTime('order_time')->nullable()->comment('下单时间');

            //交易方式：1=在线支付，选择此种方式，用户需要使用第三方支付完成交易
            //         2=现金转账，选择此种方式，需要开启第二种工作流，由后台财务对账支付流水，客服确认已支付方可发货
            //         3=到付，理论上应该不会支持此种方式，先预留，留待客户确认
            $table->tinyInteger('transaction_method')->unsigned()->nullable()->comment('交易方式：1=在线支付，2=现金转账，3=到付');
            $table->tinyInteger('online_pay_type')->unsigned()->nullable()->comment('在线支付方式：1=微信支付，2=支付宝支付，3=银联支付');
            $table->string('transaction_no', 100)->nullable()->comment('交易流水号，对于第三方支付平台就是交易号');

            //订单状态：四种状态，其中发货需要填写运单号，由客服在后台录入
            //                   完成状态也是由客服同客户确认，然后手工关闭，系统预留一个待关闭订单列表，到了一定时间安排客服回访客户，确认签收则手动关闭订单
            $table->tinyInteger('status')->unsigned()->nullable()->comment('订单状态：1=待支付，2=已支付，3=已发货，4=已完成');

            $table->string('customer_name', 30)->nullable()->comment('客户名称');
            $table->string('customer_tel', 30)->nullable()->comment('客户联系电话');
            $table->string('customer_address', 255)->nullable()->comment('客户地址');

            $table->string('express_no', 100)->nullable()->comment('物流单号');

            //如果客户下单时要求开发票，需要提供税号（纳税人识别号）
            $table->tinyInteger('invoice_type')->nullable()->comment('客户要求开具发票类型：1=个人，2=增值税普通发票，3=增值税专用发票');
            $table->string('tax_no', 100)->nullable()->comment('税号');
            $table->string('invoicex_title', 100)->nullable()->comment('发票抬头，个人是无需抬头');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_orders');
    }
}
