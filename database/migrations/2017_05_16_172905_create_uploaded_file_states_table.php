<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadedFileStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploaded_file_states', function (Blueprint $table) {
            $table->string('file_id', 200)->nullable()->comment('上传文件ID');
            $table->tinyInteger('state')->unsigned()->default(0)->nullable()->comment('状态：0=临时文件，1=有效文件');
            $table->string('batch_upload_origin_name', 200)->nullable()->comment('批量上传文件的原始图片ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploaded_file_states');
    }
}
