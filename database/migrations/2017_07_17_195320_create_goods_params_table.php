<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        *  商品参数表，表示商品的详细参数，用于在页面上逐条显示
        */
        Schema::create('goods_params', function ($table) {
            $table->increments('id')->unsigned()->comment('商品参数id');
            $table->string('params_name', 100)->nullable()->comment('参数名称');
            $table->string('params_value', 255)->nullable()->comment('参数值');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('显示顺序');
            $table->integer('goods_id')->unsigned()->nullable()->comment('商品ID');
            $table->foreign('goods_id')->references('id')->on('goods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods_params', function ($table) {
            $table->dropForeign(['goods_id']);
        });
        Schema::dropIfExists('goods_params');
    }
}
