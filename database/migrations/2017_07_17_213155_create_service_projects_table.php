<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * 检测服务项目表
        */
        Schema::create('service_projects', function ($table) {
            $table->increments('id')->unsigned()->comment('服务项目id');

            $table->string('name', 100)->nullable()->comment('服务项目名称');
            $table->string('content', 8000)->nullable()->comment('服务项目内容，富文本');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('显示顺序');

            $table->string('service_picture', 255)->nullable()->comment('服务项目主图片');
            $table->tinyInteger('display_flag')->nullable()->comment('显示标志：1=显示，2=不显示');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_projects');
    }
}
