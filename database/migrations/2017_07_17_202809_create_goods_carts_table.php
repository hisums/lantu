<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * 所有用户下的订单都要首先添加到购物车里，然后在结算页面进行结算
        */
        Schema::create('goods_carts', function ($table) {
            //购物车需要保存用户的session id联合goods_id,goods_spec_id作为主键，如果已经登录，则需要同时存储session id和user id
            $table->string('session_id', 100)->comment('用户session id');

            //购物车是针对商品规格存储的，同一个商品如果规格一致，那么购物车数量是累加
            //     即：比如A商品有一个规格型号B，客户首先把B添加了一条到数据库购物车里
            //         当该客户再次购买B型号的A商品时，只是数量累加，不要新增购物车记录
            //         当用户购买的是A商品下的另外一个规格C，则需要添加针对C规格的购物车
            //      购物车有时效性，超过时间购物车将无法继续下单结算，由系统设置，默认为1天
            $table->integer('goods_id')->unsigned()->comment('商品ID');
            $table->foreign('goods_id')->references('id')->on('goods');
            $table->integer('goods_spec_id')->unsigned()->comment('商品规格ID');
            $table->foreign('goods_spec_id')->references('id')->on('goods_specs');

            //一般来说，当客户登录之后点击购买，那么将会保存用户id到购物车里
            //如果用户没有登录购买，那么不需要存储用户id，但是一定要将用户本地的session id给保存
            $table->bigInteger('user_id')->unsigned()->comment('用户id，可为空');
            $table->foreign('user_id')->references('id')->on('users');

            $table->decimal('price', 10, 2)->nullable()->comment('购买价格');
            $table->decimal('amount', 10, 2)->nullable()->comment('购买数量');

            $table->dateTime('purchase_time')->nullable()->comment('下单时间');

            $table->primary(['session_id', 'goods_id', 'goods_spec_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_carts');
    }
}
