<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_order_details', function ($table) {
            $table->bigIncrements('id')->unsigned()->comment('订单明细id');

            $table->bigInteger('order_id')->unsigned()->comment('订单ID');
            $table->foreign('order_id')->references('id')->on('goods_orders');

            $table->integer('goods_id')->unsigned()->comment('商品ID');
            $table->foreign('goods_id')->references('id')->on('goods');
            $table->integer('goods_spec_id')->unsigned()->comment('商品规格ID');
            $table->foreign('goods_spec_id')->references('id')->on('goods_specs');

            $table->decimal('price', 10, 2)->nullable()->comment('购买价格');
            $table->decimal('amount', 10, 2)->nullable()->comment('购买数量');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_order_details');
    }
}
