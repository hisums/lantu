<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReagentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reagents', function ($table) {
            $table->increments('id')->unsigned()->comment('主键');

            $table->integer('file_id')->unsigned()->nullable()->comment('文档主键');

            $table->string('name')->nullable()->comment('项目名称');
            $table->string('reagent_img')->nullable()->comment('前台展示图片');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reagents');
    }
}
