<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGoodsSpecsChangePriceToVarchar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goods_specs', function (Blueprint $table) {
            $table->string('price', 50)->change();
            $table->string('origin_price', 50)->change();
            $table->string('cost_price', 50)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods_specs', function (Blueprint $table) {
            //
        });
    }
}
