<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsInquirysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * 用户咨询表
        */
        Schema::create('goods_inquirys', function ($table) {
            $table->bigIncrements('id')->unsigned()->comment('商品咨询id');
            $table->string('customer_name', 100)->nullable()->comment('咨询人姓名');
            $table->string('customer_tel', 30)->nullable()->comment('咨询人电话');

            $table->bigInteger('user_id')->unsigned()->nullable()->comment('咨询人Id，可为空，即用户没有登录是匿名则不记录user_id');
            $table->foreign('user_id')->references('id')->on('users');

            //显示时需要按咨询时间倒序
            $table->dateTime('inquiry_time')->nullable()->comment('咨询时间');

            $table->string('content', 500)->nullable()->comment('咨询内容');
            //后台管理员可以设置该咨询是否可以显示，防止一些恶意者刷屏
            //系统后台要能够自动识别出一些敏感词，用户咨询时将敏感词屏蔽
            $table->tinyInteger('display_flag')->nullable()->comment('显示标志：1=显示，2=不显示');

            $table->tinyInteger('process_status')->nullable()->default(1)->comment('处理状态：1=未处理，2=已处理');
            $table->bigInteger('process_user_id')->unsigned()->nullable()->comment('处理人ID，可为空，处理后状态同时变更');
            $table->foreign('process_user_id')->references('id')->on('users');

            $table->integer('goods_id')->unsigned()->nullable()->comment('商品ID');
            $table->foreign('goods_id')->references('id')->on('goods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods_inquirys', function ($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['process_user_id']);
            $table->dropForeign(['goods_id']);
        });
        Schema::dropIfExists('goods_inquirys');
    }
}
