<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDashboardConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard_configs', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->comment('用户id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->tinyInteger('platform_id')->unsigned()->nullable()->comment('公众号平台类型：1=中华志愿者微信小程序，2=中华志愿者微信公众号');

            $table->string('openid', 100)->nullable()->comment('公众号openid');

            $table->string('avatar', 255)->nullable()->comment('公众号里的用户头像');

            $table->string('nickname', 100)->nullable()->comment('公众号里的用户昵称');

            $table->primary(['user_id', 'platform_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_configs');
    }
}
