<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * 商品目录表，分层级
        *      如： 蛋白
        *             |- 天然蛋白
        *             |- 重组蛋白
        */
        Schema::create('goods_categories', function ($table) {
            $table->increments('id')->unsigned()->comment('商品分类id');
            $table->string('goods_name', 100)->nullable()->comment('商品名称');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('排序号');
            $table->integer('parent_id')->unsigned()->nullable()->comment('父级分类');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_categories');
    }
}
