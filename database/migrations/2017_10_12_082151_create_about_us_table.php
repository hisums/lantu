<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us', function (Blueprint $table) {
            $table->increments('id')->unsigned()->comment('主键');
            $table->string('name')->nullable()->comment('联系方式名称');
            $table->string('content')->nullable()->comment('联系方式');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('显示顺序');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us');
    }
}
