<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * 服务文档表
        */
        Schema::create('service_files', function ($table) {
            $table->increments('id')->unsigned()->comment('文档id');
            $table->string('name', 100)->nullable()->comment('文档名称');
            $table->string('file_id', 255)->nullable()->comment('文档ID');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('显示顺序');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_files');
    }
}

