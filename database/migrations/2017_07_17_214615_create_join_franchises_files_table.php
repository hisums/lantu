<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJoinFranchisesFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * 加盟申请表
        */
        Schema::create('join_franchises', function ($table) {
            $table->increments('id')->unsigned()->comment('加盟申请id');

            $table->string('customer_name', 100)->nullable()->comment('申请人');
            $table->string('customer_tel', 30)->nullable()->comment('申请人联系电话');
            $table->string('file_id', 255)->nullable()->comment('申请人文档ID');

            $table->dateTime('request_time')->nullable()->comment('申请时间');

            $table->tinyInteger('process_status')->default(1)->nullable()->comment('申请处理状态：1=未处理，2=已处理');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('join_franchises');
    }
}
