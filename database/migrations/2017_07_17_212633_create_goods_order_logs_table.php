<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsOrderLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_order_logs', function ($table) {
            $table->bigIncrements('id')->unsigned()->comment('订单日志id');

            $table->bigInteger('order_id')->unsigned()->comment('订单ID');
            $table->foreign('order_id')->references('id')->on('goods_orders');

            $table->tinyInteger('operation_type')->unsigned()->nullable()->comment('操作类型：1=已下单，2=已支付，3=已发货，4=已完成');
            $table->dateTime('operation_time')->nullable()->comment('操作时间');

            $table->bigInteger('operation_user_id')->unsigned()->comment('操作用户id，前端不显示，后台需显示用于稽查');
            $table->foreign('operation_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_order_logs');
    }
}
