<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseImgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //基本图片表
        //      用来存放一些运营级别的图片广告数据
        //      比如首页banner，后面随着app不断增加，会有一些其它的运营级图片
        //      由于微信小程序有尺寸限制，因此一般都要把图片放到服务器上，否则容易造成尺寸过大，无法正常发布
        Schema::create('base_imgs', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->tinyInteger('img_type')->default(1)->nullable()->comment('图片类型：1=首页banner图');

            $table->string('picture', 200)->nullable()->comment('图片');

            //对于有一些板块，涉及到关联城市，比如有的城市要显示A banner，有的要显示B banner，则需要区分城市
            //如果城市栏位为空，则表示所有城市都显示同样的内容
            //如果某些类型，比如首页banner图，既有特定城市的图片，也有通用的（即city_id为null）的图片，那么优先级是先找特定城市的
            //如果找不到特定城市的，再找通用的
            $table->integer('city_id')->nullable()->unsigned()->comment('关联城市id');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('排序号');

            $table->string('link_url', 255)->nullable()->comment('链接URL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('base_imgs', function ($table) {
            $table->dropForeign(['city_id']);
        });
        Schema::dropIfExists('base_imgs');
    }
}
