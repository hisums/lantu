<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_files', function ($table) {
            $table->increments('id')->unsigned()->comment('主键');

            $table->integer('file_id')->unsigned()->comment('文档主键');
            $table->foreign('file_id')->references('id')->on('service_files');

            $table->integer('goods_id')->unsigned()->comment('商品主键');
            $table->foreign('goods_id')->references('id')->on('goods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods_files', function ($table) {
            $table->dropForeign(['goods_id']);
            $table->dropForeign(['file_id']);
        });
        Schema::dropIfExists('goods_files');
    }
}
