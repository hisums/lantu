<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsSpecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_specs', function ($table) {
            $table->increments('id')->unsigned()->comment('商品规格id');
            $table->string('goods_model', 100)->nullable()->comment('商品型号');
            $table->string('goods_spec', 100)->nullable()->comment('商品规格');
            $table->string('goods_number', 50)->nullable()->comment('商品货号或者SKU码');
            //商品库存为0表示该规格型号缺货，如果某个商品下的所有型号都缺货，则表示该商品已断货
            $table->integer('inventory')->nullable()->comment('商品库存');
            //一般商品都分为进货价，原价，销售价（平台上的正式卖价）
            $table->decimal('price', 10, 2)->nullable()->comment('销售价格');
            $table->decimal('origin_price', 10, 2)->nullable()->comment('原始价格');
            $table->decimal('cost_price', 10, 2)->nullable()->comment('进货价格（成本价格）');
            $table->string('unit', 100)->nullable()->comment('销售单位');
            $table->decimal('min_buy_amount', 10, 2)->nullable()->comment('最低起购数量');
            //商品型号图片，对应于商品详情页左边的商品轮播图，最大五张
            $table->string('goods_spec_pic1', 255)->nullable()->comment('商品型号图片一');
            $table->string('goods_spec_pic2', 255)->nullable()->comment('商品型号图片二');
            $table->string('goods_spec_pic3', 255)->nullable()->comment('商品型号图片三');
            $table->string('goods_spec_pic4', 255)->nullable()->comment('商品型号图片四');
            $table->string('goods_spec_pic5', 255)->nullable()->comment('商品型号图片五');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('显示顺序');
            $table->tinyInteger('display_flag')->nullable()->comment('显示标志：1=显示，2=不显示');

            $table->integer('goods_id')->unsigned()->nullable()->comment('商品ID');
            $table->foreign('goods_id')->references('id')->on('goods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods_specs', function ($table) {
            $table->dropForeign(['goods_id']);
        });
        Schema::dropIfExists('goods_specs');
    }
}
