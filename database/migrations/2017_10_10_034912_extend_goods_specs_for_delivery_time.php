<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendGoodsSpecsForDeliveryTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goods_specs', function (Blueprint $table) {
            $table->tinyInteger('delivery_time')->default(1)->unsigned()->nullable()->comment('货期 1:现货 2:7天 3:15天');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods_specs', function (Blueprint $table) {
            $table->dropColumn(['delivery_time']);
        });
    }
}
