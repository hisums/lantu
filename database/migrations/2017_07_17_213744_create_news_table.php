<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        * 新闻信息表
        */
        Schema::create('news', function ($table) {
            $table->increments('id')->unsigned()->comment('新闻id');

            $table->string('subject', 100)->nullable()->comment('新闻标题');
            $table->text('content')->nullable()->comment('新闻详情，富文本');
            $table->dateTime('create_time')->nullable()->comment('创建时间');

            $table->tinyInteger('display_flag')->nullable()->comment('显示标志：1=未发布，2=已发布');
            $table->tinyInteger('is_recommend')->default(0)->comment('是否推荐：1=是，0=否');

            $table->integer('news_cate_id')->unsigned()->comment('新闻分类ID');
            $table->foreign('news_cate_id')->references('id')->on('news_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
