<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_permissions', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100)->nullable()->comment('权限名称');
            $table->string('icon', 200)->nullable()->comment('图标，只可以用iconfont');
            $table->tinyInteger('permission_type')->default(2)->nullable()->comment('权限类型：1=顶部导航模块，2=菜单组，3=菜单，4=功能权限，5=api权限');
            $table->integer('parent_id')->unsigned()->nullable()->comment('父级权限');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('同级排序号');
            $table->tinyInteger('is_default')->unsigned()->default(0)->nullable()->comment('是否默认：0=否，1=是，只有一个菜单会是默认的');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_permissions');
    }
}
