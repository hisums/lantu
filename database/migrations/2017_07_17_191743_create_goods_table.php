<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function ($table) {
            $table->increments('id')->unsigned()->comment('商品id');
            $table->string('goods_name', 100)->nullable()->comment('商品名称');
            $table->string('goods_vendor', 100)->nullable()->comment('商品厂家');
            $table->string('goods_picture', 255)->nullable()->comment('商品橱窗图片');
            $table->text('goods_content')->nullable()->comment('商品详情，富文本');
            //商品分值，即评价数，默认1-5级，根据用户反馈来取平均值确定
            //鉴于本项目用户属性，这个值直接由蓝图自己打分
            $table->tinyInteger('goods_score')->unsigned()->nullable()->comment('商品分值：1-5级');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('显示优先级：1-10');
            $table->tinyInteger('list_flag')->nullable()->comment('上架标志：1=已上架，2=未上架');
            $table->integer('goods_category_id')->unsigned()->nullable()->comment('商品分类ID，只能是叶子级的商品分类');
            $table->foreign('goods_category_id')->references('id')->on('goods_categories');
            $table->string('goods_qrcode')->nullable()->comment('商品二维码');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods', function ($table) {
            $table->dropForeign(['goods_category_id']);
        });
        Schema::dropIfExists('goods');
    }
}
