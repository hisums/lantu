<?php

use Illuminate\Database\Seeder;

use App\Services\UserGroupService;
use App\Services\UserService;
use App\BaseDictionary;
use App\UserGroup;
use App\Role;
use App\UserRole;
use App\RoleMenuPermission;
use App\MenuPermission;
use App\User;
use App\GroupUser;
use App\NewsCategory;
use App\Reagent;

class NormalGodUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createUserGroup();
        $this->createNewsCate();
        $this->defaultReagent();
        $this->about();
    }

    private function createUserGroup()
    {
        $userGroupService = new UserGroupService();
        $userService      = new UserService();

        $rootOrg = $userGroupService->createUserGroup([
            'name'          => config('system.default_platform_org'),
            'code'          => $userGroupService->getNewGroupCode(),
            'group_type'    => UserGroup::$GROUP_TYPE_INTERNAL_ORGANIZATION,
            'active_status' => UserGroup::$GROUP_STATUS_ACTIVE,
            'sort_order'    => 0,
        ]);
    }

    private function createNewsCate()
    {
        $allDefaultCate = config('system.default_news_cate');
        for ($i = 0; $i < count($allDefaultCate); $i++) {
            NewsCategory::create([
                'name'         => $allDefaultCate[$i],
                'sort_order'   => $i + 1,
                'display_flag' => NewsCategory::$DISPLAY_FLAG_ON
            ]);
        }
    }

    private function defaultReagent()
    {
        $reagent = config('system.default_reagent');
        foreach ($reagent as $v) {
            Reagent::create([
                'name' => $v
            ]);
        }

    }

    private function about()
    {
        $aboutService = new \App\Services\AboutService();
        $aboutService->aboutSeeder();
    }


}
