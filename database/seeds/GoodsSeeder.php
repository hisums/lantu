<?php

use Illuminate\Database\Seeder;
use App\MenuPermission;
use App\Services\MenuPermissionService;
use App\Services\RouteService;
use App\Route;

class GoodsSeeder extends Seeder
{
    private $menuPermissionService = null;
    private $routeService = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->menuPermissionService = new MenuPermissionService();
        $this->routeService          = new RouteService();

        $operationModule = $this->menuPermissionService->createMenuPermission([
            'name'            => '商城管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            'parent_id'       => null,
            'sort_order'      => 1,
        ]);

        $this->createGoods($operationModule);
        $this->createContent($operationModule);
        $this->league($operationModule);
        $this->fileManager($operationModule);
        $this->otherManage($operationModule);
    }

    private function createGoods($operationModule)
    {
        $goodsManager = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品管理',
            'icon'            => '&#xe612;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id'       => $operationModule->id,
            'sort_order'      => 0,
        ]);

        $goodsCategoryIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品分类展示',
                'code'       => 'goods_category',
                'route'      => '/goods/category/index',
                'action'     => 'Goods\GoodsController@cateIndex',
                'slug'       => 'goods.category.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsManager->id,
            'sort_order'      => 1,
        ]);

        $depGroupGetTree = $this->menuPermissionService->createMenuPermission([
            'name'            => '获取右侧分类树',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '获取右侧分类树路由',
                'code'       => 'goods_category_get_tree',
                'route'      => '/goods/category/index/gettree',
                'action'     => 'Goods\GoodsController@cateIndexTrees',
                'slug'       => 'goods.category.index.gettree',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsCategoryQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '查询商品分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '查询商品分类',
                'code'       => 'goods_category_query',
                'route'      => '/goods/category/query',
                'action'     => 'Goods\GoodsController@cateQuery',
                'slug'       => 'goods.category.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsCategoryAddIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '新增商品分类页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '新增商品分类页面',
                'code'       => 'goods_category_add_index',
                'route'      => '/goods/category/addIndex/{parentId}',
                'action'     => 'Goods\GoodsController@cateAddIndex',
                'slug'       => 'goods.category.add.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsCategoryAdd = $this->menuPermissionService->createMenuPermission([
            'name'            => '新增商品分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '新增商品分类',
                'code'       => 'goods_category_add',
                'route'      => '/goods/category/add',
                'action'     => 'Goods\GoodsController@cateAdd',
                'slug'       => 'goods.category.add',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsCategoryEditIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '修改商品分类页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '修改商品分类',
                'code'       => 'goods_category_edit_index',
                'route'      => '/goods/category/editIndex/{cateId}',
                'action'     => 'Goods\GoodsController@cateEditIndex',
                'slug'       => 'goods.category.edit.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsCategoryEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '修改商品分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '修改商品分类',
                'code'       => 'goods_category_edit',
                'route'      => '/goods/category/edit',
                'action'     => 'Goods\GoodsController@cateEdit',
                'slug'       => 'goods.category.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsCategoryDel = $this->menuPermissionService->createMenuPermission([
            'name'            => '删除商品分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '删除商品分类',
                'code'       => 'goods_category_del',
                'route'      => '/goods/category/del',
                'action'     => 'Goods\GoodsController@cateDel',
                'slug'       => 'goods.category.del',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品列表',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品列表展示',
                'code'       => 'goods_index',
                'route'      => '/goods/goods/index',
                'action'     => 'Goods\GoodsController@goodsIndex',
                'slug'       => 'goods.goods.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsManager->id,
            'sort_order'      => 1,
        ]);

        $goodsQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品列表查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品列表查询',
                'code'       => 'goods_index_query',
                'route'      => '/goods/goods/query',
                'action'     => 'Goods\GoodsController@goodsIndexQuery',
                'slug'       => 'goods.goods.index.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsAddIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品添加页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品添加页面',
                'code'       => 'goods_index_add_index',
                'route'      => '/goods/goods/addIndex',
                'action'     => 'Goods\GoodsController@goodsAddIndex',
                'slug'       => 'goods.goods.index.add.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsAddBatchIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品批量添加页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品批量添加页面',
                'code'       => 'goods_batch_index_add_index',
                'route'      => '/goods/goods/addGoodsBatchIndex',
                'action'     => 'Goods\GoodsController@addGoodsBatchIndex',
                'slug'       => 'goods.goods.index.add.batch.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsAddBatch = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品批量添加',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品批量添加',
                'code'       => 'goods_batch_index_add',
                'route'      => '/goods/goods/addGoodsBatch',
                'action'     => 'Goods\GoodsController@addGoodsBatch',
                'slug'       => 'goods.goods.index.add.batch',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsAdd = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品添加',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品添加',
                'code'       => 'goods_index_add',
                'route'      => '/goods/goods/add',
                'action'     => 'Goods\GoodsController@goodsAdd',
                'slug'       => 'goods.goods.index.add',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsEditIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品编辑页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品编辑页面',
                'code'       => 'goods_index_edit_index',
                'route'      => '/goods/goods/editIndex/{id}',
                'action'     => 'Goods\GoodsController@goodsEditIndex',
                'slug'       => 'goods.goods.index.edit.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品编辑',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品编辑',
                'code'       => 'goods_index_edit',
                'route'      => '/goods/goods/edit',
                'action'     => 'Goods\GoodsController@goodsEdit',
                'slug'       => 'goods.goods.index.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsDel = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品删除',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品删除',
                'code'       => 'goods_index_del',
                'route'      => '/goods/goods/del/{id}',
                'action'     => 'Goods\GoodsController@goodsDel',
                'slug'       => 'goods.goods.index.del',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecs = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品规格',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品规格展示',
                'code'       => 'goods_specs',
                'route'      => '/goods/goods/specs/{goodsId}',
                'action'     => 'Goods\GoodsController@goodsSpecs',
                'slug'       => 'goods.goods.specs',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecsQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品规格列表查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品规格列表查询',
                'code'       => 'goods_specs_query',
                'route'      => '/goods/goods/specsQuery',
                'action'     => 'Goods\GoodsController@goodsSpecsQuery',
                'slug'       => 'goods.goods.specs.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsSpecs->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecsAddIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品规格添加页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品规格添加页面',
                'code'       => 'goods_index_add_goods_specs_index',
                'route'      => '/goods/goods/addGoodsSpecsIndex/{id}',
                'action'     => 'Goods\GoodsController@goodsSpecsAddIndex',
                'slug'       => 'goods.goods.index.add.specs',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsSpecs->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecsAdd = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品规格添加',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品规格添加',
                'code'       => 'goods_index_add_goods_specs',
                'route'      => '/goods/goods/addGoodsSpecs',
                'action'     => 'Goods\GoodsController@goodsSpecsAdd',
                'slug'       => 'goods.goods.add.specs',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsSpecs->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecsEditIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品规格编辑页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品规格编辑页面',
                'code'       => 'goods_index_edit_specs_index',
                'route'      => '/goods/goods/editSpecsIndex/{id}',
                'action'     => 'Goods\GoodsController@goodsEditSpecsIndex',
                'slug'       => 'goods.goods.index.edit.specs',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsSpecs->id,
            'sort_order'      => 1,
        ]);

        $goodsEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品规格编辑',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品规格编辑',
                'code'       => 'goods_index_edit_goods_specs',
                'route'      => '/goods/goods/editSpecs',
                'action'     => 'Goods\GoodsController@goodsSpecsEdit',
                'slug'       => 'goods.goods.index.specs',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsSpecs->id,
            'sort_order'      => 1,
        ]);

        $goodsDel = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品规格删除',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品规格删除',
                'code'       => 'goods_specs_del',
                'route'      => '/goods/goods/delSpecs/{id}',
                'action'     => 'Goods\GoodsController@goodsSpecsDel',
                'slug'       => 'goods.goods.index.del.specs',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsSpecs->id,
            'sort_order'      => 1,
        ]);


        $goodsAttribute = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品属性',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品属性展示',
                'code'       => 'goods_attribute',
                'route'      => '/goods/goods/attribute/{goodsId}',
                'action'     => 'Goods\GoodsController@goodsAttribute',
                'slug'       => 'goods.goods.attribute',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsIndex->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecsQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品属性列表查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品属性列表查询',
                'code'       => 'goods_attribute_query',
                'route'      => '/goods/goods/attributeQuery',
                'action'     => 'Goods\GoodsController@goodsAttributeQuery',
                'slug'       => 'goods.goods.attribute.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsAttribute->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecsAddIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品属性添加页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品属性添加页面',
                'code'       => 'goods_index_add_goods_attribute_index',
                'route'      => '/goods/goods/addGoodsAttributeIndex/{id}',
                'action'     => 'Goods\GoodsController@goodsAttributeAddIndex',
                'slug'       => 'goods.goods.index.add.attribute',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsAttribute->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecsAdd = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品属性添加',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品属性添加',
                'code'       => 'goods_index_add_goods_attribute',
                'route'      => '/goods/goods/addAttributeSpecs',
                'action'     => 'Goods\GoodsController@goodsAttributeAdd',
                'slug'       => 'goods.goods.add.attribute',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsAttribute->id,
            'sort_order'      => 1,
        ]);

        $goodsSpecsEditIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品属性编辑页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品规格编辑页面',
                'code'       => 'goods_index_edit_attribute_index',
                'route'      => '/goods/goods/editAttributeIndex/{id}',
                'action'     => 'Goods\GoodsController@goodsEditAttributeIndex',
                'slug'       => 'goods.goods.index.edit.attribute',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsAttribute->id,
            'sort_order'      => 1,
        ]);

        $goodsEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品属性编辑',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品属性编辑',
                'code'       => 'goods_index_edit_goods_attribute',
                'route'      => '/goods/goods/editAttribute',
                'action'     => 'Goods\GoodsController@goodsAttributeEdit',
                'slug'       => 'goods.goods.index.attribute',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsAttribute->id,
            'sort_order'      => 1,
        ]);

        $goodsDel = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品属性删除',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品属性删除',
                'code'       => 'goods_attribute_del',
                'route'      => '/goods/goods/delAttribute/{id}',
                'action'     => 'Goods\GoodsController@goodsAttributeDel',
                'slug'       => 'goods.goods.index.del.attribute',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsAttribute->id,
            'sort_order'      => 1,
        ]);

        $goodsInquirys = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品咨询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品咨询列表',
                'code'       => 'goods_inquirys_list',
                'route'      => '/goods/goods/inquirys',
                'action'     => 'Goods\GoodsController@goodsInquirys',
                'slug'       => 'goods.goods.inquirys.list',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsManager->id,
            'sort_order'      => 1,
        ]);

        $goodsInquirysQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品咨询查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品咨询查询',
                'code'       => 'goods_inquirys_query',
                'route'      => '/goods/goods/inquirysQuery',
                'action'     => 'Goods\GoodsController@goodsInquirysQuery',
                'slug'       => 'goods.goods.inquirys.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsInquirys->id,
            'sort_order'      => 1,
        ]);

        $goodsInquirysContent = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品咨询详情',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品咨询详情',
                'code'       => 'goods_inquirys_detail',
                'route'      => '/goods/goods/inquirysDetail/{id}',
                'action'     => 'Goods\GoodsController@goodsInquirysDetail',
                'slug'       => 'goods.goods.inquirys.detail',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsInquirys->id,
            'sort_order'      => 1,
        ]);

        $goodsInquirysOperate = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品咨询标记处理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品咨询标记处理',
                'code'       => 'goods_inquirys_operate',
                'route'      => '/goods/goods/inquirysOperate/{id}',
                'action'     => 'Goods\GoodsController@goodsInquirysOperate',
                'slug'       => 'goods.goods.inquirys.operate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsInquirys->id,
            'sort_order'      => 1,
        ]);

        $goodsInquirysIsShow = $this->menuPermissionService->createMenuPermission([
            'name'            => '商品咨询是否显示',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '商品咨询是否显示',
                'code'       => 'goods_inquirys_show',
                'route'      => '/goods/goods/inquirysOperateShow/{id}',
                'action'     => 'Goods\GoodsController@goodsInquirysShow',
                'slug'       => 'goods.goods.inquirys.show',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $goodsInquirys->id,
            'sort_order'      => 1,
        ]);
    }

    private function createContent($operationModule)
    {
        $contentManager = $this->menuPermissionService->createMenuPermission([
            'name'            => '文章管理',
            'icon'            => '&#xe621;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id'       => $operationModule->id,
            'sort_order'      => 0,
        ]);

        $newsCategoryIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '文章分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '文章分类展示',
                'code'       => 'news_category',
                'route'      => '/news/category/index',
                'action'     => 'News\NewsController@cateIndex',
                'slug'       => 'news.category.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $contentManager->id,
            'sort_order'      => 1,
        ]);

        $newsCategoryQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '查询文章分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '查询文章分类',
                'code'       => 'news_category_query',
                'route'      => '/news/category/query',
                'action'     => 'News\NewsController@cateQuery',
                'slug'       => 'news.category.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $newsCategoryAddIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '新增文章分类页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '新增文章分类页面',
                'code'       => 'news_category_add_index',
                'route'      => '/news/category/addIndex',
                'action'     => 'News\NewsController@cateAddIndex',
                'slug'       => 'news.category.add.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $newsCategoryAdd = $this->menuPermissionService->createMenuPermission([
            'name'            => '新增文章分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '新增文章分类',
                'code'       => 'news_category_add',
                'route'      => '/news/category/add',
                'action'     => 'News\NewsController@cateAdd',
                'slug'       => 'news.category.add',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $newsCategoryEditIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '修改文章分类页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '修改文章分类页面',
                'code'       => 'news_category_edit_index',
                'route'      => '/news/category/editIndex/{cateId}',
                'action'     => 'News\NewsController@cateEditIndex',
                'slug'       => 'news.category.edit.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $newsCategoryEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '修改文章分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '修改文章分类',
                'code'       => 'news_category_edit',
                'route'      => '/news/category/edit',
                'action'     => 'News\NewsController@cateEdit',
                'slug'       => 'news.category.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $newsCategoryDel = $this->menuPermissionService->createMenuPermission([
            'name'            => '删除文章分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '删除文章分类',
                'code'       => 'news_category_del',
                'route'      => '/news/category/del/{id}',
                'action'     => 'News\NewsController@cateDel',
                'slug'       => 'news.category.del',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsCategoryIndex->id,
            'sort_order'      => 1,
        ]);

        $newsIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '文章列表',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '文章列表',
                'code'       => 'news',
                'route'      => '/news/index',
                'action'     => 'News\NewsController@newsIndex',
                'slug'       => 'news.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $contentManager->id,
            'sort_order'      => 1,
        ]);

        $newsQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '查询文章',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '查询文章',
                'code'       => 'news_query',
                'route'      => '/news/query',
                'action'     => 'News\NewsController@newsQuery',
                'slug'       => 'news.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsIndex->id,
            'sort_order'      => 1,
        ]);

        $newsAddIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '新增文章页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '新增文章页面',
                'code'       => 'news_add_index',
                'route'      => '/news/addIndex',
                'action'     => 'News\NewsController@newsAddIndex',
                'slug'       => 'news.add.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsIndex->id,
            'sort_order'      => 1,
        ]);

        $newsAdd = $this->menuPermissionService->createMenuPermission([
            'name'            => '新增文章',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '新增文章',
                'code'       => 'news_add',
                'route'      => '/news/add',
                'action'     => 'News\NewsController@newsAdd',
                'slug'       => 'news.add',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsIndex->id,
            'sort_order'      => 1,
        ]);

        $newsEditIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '修改文章页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '修改文章页面',
                'code'       => 'news_edit_index',
                'route'      => '/news/editIndex/{cateId}',
                'action'     => 'News\NewsController@newsEditIndex',
                'slug'       => 'news.edit.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsIndex->id,
            'sort_order'      => 1,
        ]);

        $newsEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '修改文章',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '修改文章',
                'code'       => 'news_edit',
                'route'      => '/news/edit',
                'action'     => 'News\NewsController@newsEdit',
                'slug'       => 'news.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsIndex->id,
            'sort_order'      => 1,
        ]);

        $newsDel = $this->menuPermissionService->createMenuPermission([
            'name'            => '删除文章',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '删除文章',
                'code'       => 'news_del',
                'route'      => '/news/del/{id}',
                'action'     => 'News\NewsController@newsDel',
                'slug'       => 'news.del',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $newsIndex->id,
            'sort_order'      => 1,
        ]);
    }

    private function league($operationModule)
    {
        $leagueManager = $this->menuPermissionService->createMenuPermission([
            'name'            => '加盟管理',
            'icon'            => '&#xe613;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id'       => $operationModule->id,
            'sort_order'      => 0,
        ]);

        $leagueIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '加盟申请列表',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '加盟申请列表',
                'code'       => 'league',
                'route'      => '/league/index',
                'action'     => 'League\LeagueController@leagueIndex',
                'slug'       => 'league.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $leagueManager->id,
            'sort_order'      => 1,
        ]);

        $leagueIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '加盟申请查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '加盟申请查询',
                'code'       => 'league',
                'route'      => '/league/query',
                'action'     => 'league\leagueController@leagueQuery',
                'slug'       => 'league.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $leagueIndex->id,
            'sort_order'      => 1,
        ]);

        $dealLeague = $this->menuPermissionService->createMenuPermission([
            'name'            => '处理申请',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '处理申请',
                'code'       => 'league.deal',
                'route'      => '/league/deal',
                'action'     => 'league\leagueController@dealLeague',
                'slug'       => 'league.deal',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $leagueIndex->id,
            'sort_order'      => 1,
        ]);
    }

    private function fileManager($operationModule)
    {
        $fileManager = $this->menuPermissionService->createMenuPermission([
            'name'            => '文件管理',
            'icon'            => '&#xe61d;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id'       => $operationModule->id,
            'sort_order'      => 0,
        ]);

        $fileIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '文件列表',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '文件列表',
                'code'       => 'file',
                'route'      => '/file/index',
                'action'     => 'File\FileController@fileIndex',
                'slug'       => 'file.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileManager->id,
            'sort_order'      => 1,
        ]);

        $fileQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '文件列表查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '文件列表查询',
                'code'       => 'file',
                'route'      => '/file/query',
                'action'     => 'File\FileController@fileQuery',
                'slug'       => 'file.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);

        $fileAddIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '添加&修改文件页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '添加&修改文件页面',
                'code'       => 'file.add.index',
                'route'      => '/file/addIndex',
                'action'     => 'File\FileController@addIndex',
                'slug'       => 'file.add.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);

        $fileUpload = $this->menuPermissionService->createMenuPermission([
            'name'            => '上传文件',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '添加文件',
                'code'       => 'file.upload',
                'route'      => '/file/upload',
                'action'     => 'File\FileController@upload',
                'slug'       => 'file.upload',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);

        $fileAdd = $this->menuPermissionService->createMenuPermission([
            'name'            => '添加文件',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '添加文件',
                'code'       => 'file.add',
                'route'      => '/file/add',
                'action'     => 'File\FileController@add',
                'slug'       => 'file.add',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);

        $fileEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '修改文件',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '修改文件',
                'code'       => 'file.edit',
                'route'      => '/file/edit',
                'action'     => 'File\FileController@edit',
                'slug'       => 'file.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);

        $fileDel = $this->menuPermissionService->createMenuPermission([
            'name'            => '删除文件',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '删除文件',
                'code'       => 'file.del',
                'route'      => '/file/del',
                'action'     => 'File\FileController@del',
                'slug'       => 'file.del',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);

        $reagentIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '试剂文件管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '试剂文件管理',
                'code'       => 'reagent.index',
                'route'      => '/reagent/index',
                'action'     => 'File\FileController@reagentIndex',
                'slug'       => 'reagent.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileManager->id,
            'sort_order'      => 1,
        ]);

        $reagentQuery = $this->menuPermissionService->createMenuPermission([
            'name'            => '试剂文件查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '试剂文件查询',
                'code'       => 'reagent.query',
                'route'      => '/reagent/query',
                'action'     => 'File\FileController@reagentQuery',
                'slug'       => 'reagent.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);

        $reagentEditIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '试剂文件编辑页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '试剂文件编辑页面',
                'code'       => 'reagent.edit.index',
                'route'      => '/reagent/editIndex',
                'action'     => 'File\FileController@reagentEditIndex',
                'slug'       => 'reagent.edit.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);

        $reagentEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '试剂文件编辑',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '试剂文件编辑',
                'code'       => 'reagent.edit',
                'route'      => '/reagent/edit',
                'action'     => 'File\FileController@reagentEdit',
                'slug'       => 'reagent.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $fileIndex->id,
            'sort_order'      => 1,
        ]);
    }

    private function otherManage($operationModule)
    {
        $otherManage = $this->menuPermissionService->createMenuPermission([
            'name'            => '其他管理',
            'icon'            => '&#xe632;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id'       => $operationModule->id,
            'sort_order'      => 0,
        ]);

        $aboutIndex = $this->menuPermissionService->createMenuPermission([
            'name'            => '关于我们',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '关于我们',
                'code'       => 'about',
                'route'      => '/about/index',
                'action'     => 'News\AboutController@aboutIndex',
                'slug'       => 'about.index',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $otherManage->id,
            'sort_order'      => 1,
        ]);

        $aboutEdit = $this->menuPermissionService->createMenuPermission([
            'name'            => '关于我们信息修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id'        => $this->routeService->createRoute([
                'name'       => '关于我们信息修改',
                'code'       => 'about.edit',
                'route'      => '/about/edit',
                'action'     => 'News\AboutController@aboutEdit',
                'slug'       => 'about.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method'     => Route::$ROUTE_METHOD_POST,
                'sort_order' => 1,
            ])->id,
            'parent_id'       => $aboutIndex->id,
            'sort_order'      => 1,
        ]);


    }
}
