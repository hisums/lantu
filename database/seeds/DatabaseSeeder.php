<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DefaultSuperAdminSeeder::class);
        $this->call(DefaultModuleSeeder::class);
        $this->call(DefaultGoodsCategorySeeder::class);

        $this->call(NormalGodUserSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(CityDistrictInitSeeder::class);
        $this->call(GoodsSeeder::class);
    }
}
