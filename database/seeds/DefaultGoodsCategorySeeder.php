<?php

use Illuminate\Database\Seeder;
use App\Services\GoodsCategoryService;

class DefaultGoodsCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $goodsService = new GoodsCategoryService();
        $goodsService->createTopCategory();
    }
}
