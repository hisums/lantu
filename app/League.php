<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/2
 * Time: 15:28
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    protected $table = 'join_franchises';
    public $timestamps = false;

    protected $fillable = [
        'customer_name', 'customer_tel', 'file_id','request_time','process_status'
    ];

    public static $PROCESS_STATUS_FINISH = 2;
    public static $PROCESS_STATUS_UNFINISH = 1;

    public static $PROCESS_STATUS_MAP = [
        ['key' => 1, 'text' => '未处理'],
        ['key' => 2, 'text' => '已处理'],
    ];

}