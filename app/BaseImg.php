<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lib\FileService\FastDFS;

class BaseImg extends Model
{
    public static $BASE_IMAGE_TYPE_MAINPAGE_BANNER = 1;

    public static $BASE_IMAGE_TYPE_MAP = [
        ['key' => 1, 'text' => '首页banner']
    ];

    public $timestamps = false;

    protected $fillable = [
        'img_type', 'picture', 'city_id', 'sort_order', 'link_url'
    ];

    public function getFullPicturePath()
    {
        return FastDFS::getFullUrl($this->picture);
    }

    public function getThumbnail()
    {
        if (strpos($this->picture, '.png') != FALSE) {
            return $this->getFullPicturePath().'!t350x350.png';
        } else if (strpos($this->picture, '.jpg') != FALSE) {
            return $this->getFullPicturePath().'!t350x350.jpg';
        } else if (strpos($this->picture, '.jpeg') != FALSE) {
            return $this->getFullPicturePath().'!t350x350.jpeg';
        } else if (strpos($this->picture, '.gif') != FALSE) {
            return $this->getFullPicturePath().'!t350x350.gif';
        }
        return '';
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public static function getBaseImageTypeKeys()
    {
        return collect(self::$BASE_IMAGE_TYPE_MAP)->pluck('key')->toArray();
    }
}
