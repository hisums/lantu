<?php
function getUpCate($cate_id)
{
    $cateInfo = \App\GoodsCategory::where('id',$cate_id)->first();

    echo '>>'.$cateInfo->getGoodsPathInHome();
}

function curlGet($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}