<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/18
 * Time: 9:52
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class GoodsCategory extends Model
{
    public static $TOP_CATE = 1;


    protected $table = 'goods_categories';
    public $timestamps = false;

    protected $fillable = [
        'goods_name', 'sort_order', 'parent_id'
    ];

    public function getGoodsPath()
    {
        if (!empty($this->parent)) {
            return $this->parent->getGoodsPath() . '>>' . $this->goods_name;
        } else {
            return $this->goods_name;
        }
    }

    public function parent()
    {
        return $this->belongsTo('App\GoodsCategory', 'parent_id');
    }

    public function getGoodsPathInHome()
    {
        if (!empty($this->parent) && $this->parent->id != 1) {
            return $this->parent->getGoodsPathInHome() . '>><a style="color:#000" href="' . url('web/goods/list', $this->id) . '">' . $this->goods_name . '</a>';
        } else {
            return '<a style="color:#000" href="' . url('web/goods/list', $this->id) . '">'.$this->goods_name. '</a>';
        }
    }

    /**
     * 全局静态方法 获取分类下级的数量
     * @author  hkw <hkw925@qq.com>
     * @param $cate_id
     * @return int
     */
    public static function cateSubCount($cate_id){
        return GoodsCategory::where('parent_id',$cate_id)->count();
    }
}