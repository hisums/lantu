<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/1
 * Time: 14:46
 */

namespace App;


use App\Lib\FileService\FastDFS;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    public $timestamps = false;

    protected $fillable = [
        'subject', 'content', 'create_time', 'display_flag', 'news_cate_id', 'show_img', 'sort_order' , 'is_recommend'
    ];

    public static $DISPLAY_FLAG_ON = 1;
    public static $DISPLAY_FLAG_OFF = 2;

    public static $DISPLAY_FLAG_MAP = [
        ['key' => 1, 'text' => '显示'],
        ['key' => 2, 'text' => '不显示 ']
    ];
    public static $RECOMMEND_YES = 1;
    public static $RECOMMEND_NO = 0;

    public static $RECOMMEND_MAP = [
        ['key' => 1, 'text' => '推荐'],
        ['key' => 0, 'text' => '默认']
    ];

    public function newsCategory()
    {
        return $this->belongsTo('App\NewsCategory', 'news_cate_id', 'id');
    }

    public function newsCategoryGet()
    {
        return $this->newsCategory()->first();
    }

    public function getFullPicturePath()
    {
        if (!empty($this->show_img)) {
            return FastDFS::getFullUrl($this->show_img);
        }
        return '/images/no-pic-back.png';
    }
}