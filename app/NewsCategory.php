<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/1
 * Time: 14:44
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Lib\FileService\FastDFS;

class NewsCategory extends Model
{
    protected $table = 'news_categories';
    public $timestamps = false;

    protected $fillable = [
        'name', 'sort_order', 'news_cate_picture', 'display_flag'
    ];
    //首页轮播
    public static $BANNER_ID = 1;
    //促销活动
    public static $DEFAULT_CATE_CXHD = 2;
    //咨询中心
    public static $DEFAULT_CATE_JSWZ = 3;
    //管理岗位招聘
    public static $DEFAULT_CATE_GLGWZP = 4;
    //技术岗位招聘
    public static $DEFAULT_CATE_JSGWZP = 5;
    //其他岗位招聘
    public static $DEFAULT_CATE_QTGWZP = 6;
    //产品反馈
    public static $DEFAULT_CATE_CPFK = 7;
    //技术文章
    public static $DEFAULT_CATE_YNJD = 8;
    //生化检测
    public static $DEFAULT_CATE_SHJC = 9;
    //免疫检测
    public static $DEFAULT_CATE_MYJC = 10;
    //抗体制备
    public static $DEFAULT_CATE_KTZB = 11;
    //试剂盒定制
    public static $DEFAULT_CATE_SJHDZ = 12;
    //中心简介
    public static $DEFAULT_CATE_ZXJJ = 13;
    //检测流程
    public static $DEFAULT_CATE_JCLC = 14;
    //生化检测流程
    public static $DEFAULT_CATE_SHJCLC = 15;
    //免疫检测流程
    public static $DEFAULT_CATEMYJCLC = 16;
    //抗体制备流程
    public static $DEFAULT_CATEKYZBLC = 17;
    //试剂盒定制流程
    public static $DEFAULT_CATESJHDZLC = 18;
    //蓝图介绍
    public static $DEFAULT_CATELTJS = 19;
    //联系我们
    public static $DEFAULT_CATELXWM = 20;
    //实验展示图片
    public static $DEFAULT_CATESYZSTP = 21;

    public static $DISPLAY_FLAG_ON = 1;
    public static $DISPLAY_FLAG_OFF = 2;

    public static $DISPLAY_FLAG_MAP = [
        ['key' => 1, 'text' => '显示'],
        ['key' => 2, 'text' => '不显示']
    ];

    public function getFullPicturePath()
    {
        if (!empty($this->news_cate_picture)) {
            return FastDFS::getFullUrl($this->news_cate_picture);
        }
        return '/images/no-pic-back.png';
    }
}
