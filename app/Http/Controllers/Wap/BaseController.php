<?php

namespace App\Http\Controllers\Wap;

use App\Services\AboutService;
use App\Services\GoodsCategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    private $categoryService = null;
    public $about = null;
    public $all_cate = null;

    /**
     * 控制模板title与分类是否展开
     * @param GoodsCategoryService $categoryService
     */

    public function __construct(GoodsCategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
        $this->all_cate        = $this->categoryService->getInitCategory([])[0]['children'];
        $aboutService = new AboutService();
        $this->about           = $aboutService->getData();
    }
}
