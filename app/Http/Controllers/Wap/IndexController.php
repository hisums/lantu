<?php

namespace App\Http\Controllers\Wap;

use App\NewsCategory;
use App\News;
use App\Services\AboutService;
use App\Services\GoodsCategoryService;
use App\Services\GoodsService;
use App\Services\NewsService;
use App\Services\FileService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends BaseController
{
    private $categoryService = null;
    private $newsService = null;
    private $goodsService = null;
    private $aboutService = null;
    private $fileService = null;

    public function __construct(GoodsCategoryService $categoryService,
                                NewsService $newsService,
                                GoodsService $goodsService,
                                AboutService $aboutService,
                                FileService $fileService,
                                NewsCategory $newsCategory,
                                News $news
    )
    {
        parent::__construct($categoryService);
        $this->newsService     = $newsService;
        $this->categoryService = $categoryService;
        $this->goodsService = $goodsService;
        $this->aboutService = $aboutService;
        $this->fileService          = $fileService;
    }

    public function index(){
        return view('wap.index',[
            'goodsIndex'=>$this->goodsService->getIndexGoods(),
            'indexNews'=>$this->newsService->getIndexNews(),
            'about_us'=>$this->newsService->getAboutUs(),
            'banners'=>$this->newsService->getBanner()
        ]);
    }

    public function product(Request $request){
        if ($request->ajax()) {
            $input = $request->all();
            $cate_id = $request->cate_id?:1;
            $input['cate_id'] = $cate_id;
            $list  = $this->goodsService->specList($input);
            //dump($list['list']);exit;
            return view('wap.product_list', ['list' => $list['list']]);
        }else{
            $cate_id = $request->cate_id?:1;
            return view('wap.product',['cate_id'=>$cate_id]);
        }
    }

    public function news(Request $request){
        if ($request->ajax()) {
            $input = $request->all();
            $list  = $this->newsService->getWapNews($input);
            return view('wap.news_list', ['list' => $list['list']]);
        }else{
            return view('wap.news');
        }
    }

    public function newsDetail(Request $request){
        $info = $this->newsService->articleDetail($request->id);
        return view('wap.news_detail',['info'=>$info]);
    }

    public function aboutUs(){
        return view('wap.about_us',['about_us'=>$this->newsService->getAboutUs()]);
    }

    public function productDetail(Request $request){
        if($request->spe_id){
            $info        = $this->goodsService->goodsInfo($request->id, $request->spe_id);
        }else{
            $info        = $this->goodsService->goodsInfo($request->id);
        }
        $fileInfo    = $this->fileService->getFileDetail($request->id);
        return view('wap.product_detail',['info'=>$info,'fileInfo'=>$fileInfo]);
    }

    public function testing_center(Request $request){
        $center          = $this->newsService->testingArticleList(NewsCategory::$DEFAULT_CATE_ZXJJ);
        $cateArticleList = $this->newsService->testing_center_cate();
        return view('wap.testing_center', ['cate_article_list' => $cateArticleList]);
    }

    public function testing_sub(Request $request)
    {
        $cateSubId = $request->cate_sub_id;
        $cateId    = $request->cate_id;
        //检测流程主文章
        $cateArticle = News::where('news_cate_id', $cateSubId)->orderBy('sort_order', 'asc')->first();
        return view('wap.testing_sub', ['cate_article'     => $cateArticle,]);
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $input = $request->all();
            //dd($input);
            $list  = $this->goodsService->specListSearch($input);
            //dump($list['list']);
            return view('wap.search_list', ['list' => $list['list']]);
        }else{
            $input    = $request->all();
            $keywords = $input['keywords'];
            return view('wap.search',['keywords'=>$keywords]);
        }
    }

    public function productCategory(Request $request){
        $cate_id = $request->cate_id?:1;
        $list = $this->categoryService->getChildCate1($cate_id);
        return view('wap.product_category',['list'=>$list,'cate_id'=>$cate_id]);
    }

    public function register()
    {
        return view('home.register1', [
            'title' => '注册',
            'show'  => false,
            'cates' => $this->all_cate,
            'about'    => $this->about,
        ]);
    }

    public function doRegister(Request $request)
    {
        $input                = $request->all();
        $input['true_verify'] = Session::get('verify');
        $this->registerValidate($input);
        $this->userService->homeRegister($input);
        return ResponseUtil::jsonResponse(true, 0, '注册成功!', '');
    }
}
