<?php
namespace App\Http\Controllers\News;

use App\News;
use App\NewsCategory;
use App\Services\NewsService;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Lib\Util\ResponseUtil;
use Validator;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/1
 * Time: 14:34
 */
class NewsController extends Controller
{
    private $newsService = null;


    public function __construct(NewsService $newsService
    )
    {
        $this->newsService = $newsService;
    }

    /*
     * @author: hy
     * @name: 新闻分类展示
     * @method:GET
     * @param: $request
     * @route:/news/category/index
     */
    public function cateIndex(Request $request)
    {
        return view('news.newsCate');
    }

    /*
     * @author: hy
     * @name: 查询新闻分类
     * @method:POST
     * @param: $request
     * @route:/news/category/query
     */
    public function cateQuery(Request $request)
    {
        $input = $request->all();
        return $this->newsService->getNewsCate($input);
    }

    /*
     * @author: hy
     * @name: 新增新闻分类页面
     * @method:GET
     * @param: $request
     * @route:/news/category/addIndex
     */
    public function cateAddIndex(Request $request)
    {
        return view('news.newsCateAdd');
    }

    /*
     * @author: hy
     * @name: 新增新闻分类
     * @method:POST
     * @param: $request
     * @route:/news/category/add
     */
    public function cateAdd(Request $request)
    {
        $input = $request->all();
        $this->cateAddValidate($input);
        $this->newsService->newsCateAdd($input);
        return ResponseUtil::jsonResponse(true, 0, '添加成功', '');
    }

    /*
     * @author: hy
     * @name: 修改新闻分类页面
     * @method:GET
     * @param: $request
     * @route:/news/category/editIndex/{cateId}
     */
    public function cateEditIndex(Request $request)
    {
        $id   = $request->cateId;
        $cate = NewsCategory::where('id', $id)->first();
        return view('news.newsCateAdd', [
            'cate_info' => $cate
        ]);
    }

    /*
     * @author: hy
     * @name: 修改新闻分类
     * @method:POST
     * @param: $request
     * @route:/news/category/edit
     */
    public function cateEdit(Request $request)
    {
        $input = $request->all();
        $this->cateAddValidate($input);
        $this->newsService->newsCateEdit($input);
        return ResponseUtil::jsonResponse(true, 0, '修改成功', '');
    }

    /*
     * @author: hy
     * @name: 删除新闻分类
     * @method:GET
     * @param: $request
     * @route:/news/category/del/{id}
     */
    public function cateDel(Request $request)
    {
        $id          = $request->id;
        $input['id'] = $id;
        $this->cateDelValidate($input);
        $this->newsService->newsCateDel($id);
        return ResponseUtil::jsonResponse(true, 0, '删除成功', '');
    }

    public function cateAddValidate(Array $input)
    {
        return Validator::make($input, [
            'name'       => [
                'required',
                'max:50'
            ],
            'sort_order' => [
                'required',
                'between:0,255',
                'integer',
            ],
        ], [
            'name.required'       => '分类名必须',
            'name.max'            => '名称最大不得超过:max个字符',
            'sort_order.required' => '排序必须',
            'sort_order.between'  => '排序值必须在0~255之间',
            'sort_order.integer'  => '排序值必须为整数',
        ])->validate();
    }

    public function cateDelValidate(Array $input)
    {
        return Validator::make($input, [
            'id' => [
                'required',
                'numeric'
            ],
        ], [
            'id.required' => '必须传入分类ID',
            'id.numeric'  => '分类ID格式不正确，必须为数字',
        ])->after(function ($validator) use ($input) {
            if (!empty($input['id'])) {
                $allDefaultCate = config('system.default_news_cate');
                $allDefaultCateIds = array_keys($allDefaultCate);
                if(in_array($input['id'],$allDefaultCateIds)){
                    $validator->errors()->add('newsCategory.illegal', '该分类不可删除');
                }
                $res = $this->newsService->issetNews($input['id']);
                if ($res) {
                    $validator->errors()->add('newsCategory.illegal', '该分类存在其它关联,不可删除');
                }
            }
        })->validate();
    }

    /*
     * @author: hy
     * @name: 新闻列表
     * @method:GET
     * @param: $request
     * @route:/news/index
     */
    public function newsIndex(Request $request)
    {
        $cate = $this->newsService->getNewsCateInNews();
        return view('news.news', [
            'newsCates' => $cate
        ]);
    }

    /*
     * @author: hy
     * @name: 查询新闻
     * @method:POST
     * @param: $request
     * @route:/news/query
     */
    public function newsQuery(Request $request)
    {
        $input = $request->all();
        return $this->newsService->getNews($input);
    }

    /*
     * @author: hy
     * @name: 新增新闻页面
     * @method:GET
     * @param: $request
     * @route:/news/addIndex
     */
    public function newsAddIndex(Request $request)
    {
        $cate = $this->newsService->getNewsCateInNews();
        return view('news.newsAdd', [
            'newsCates' => $cate
        ]);
    }

    /*
     * @author: hy
     * @name: 新增新闻
     * @method:POST
     * @param: $request
     * @route:/news/add
     */
    public function newsAdd(Request $request)
    {
        $input = $request->all();
        $this->newsAddValidate($input);
        $this->newsService->newsAdd($input);
        return ResponseUtil::jsonResponse(true, 0, '添加成功', '');
    }

    /*
     * @author: hy
     * @name: 修改新闻页面
     * @method:GET
     * @param: $request
     * @route:/news/editIndex/{cateId}
     */
    public function newsEditIndex(Request $request)
    {
        $id       = $request->cateId;
        $newsInfo = News::where('id', $id)->first();
        $cate     = $this->newsService->getNewsCateInNews();
        return view('news.newsAdd', [
            'newsCates' => $cate,
            'newsInfo'  => $newsInfo
        ]);
    }

    /*
     * @author: hy
     * @name: 修改新闻
     * @method:POST
     * @param: $request
     * @route:/news/edit
     */
    public function newsEdit(Request $request)
    {
        $input = $request->all();
        $this->newsAddValidate($input);
        $this->newsService->newsEdit($input);
        return ResponseUtil::jsonResponse(true, 0, '修改成功', '');
    }

    /*
     * @author: hy
     * @name: 删除新闻
     * @method:GET
     * @param: $request
     * @route:/news/del/{id}
     */
    public function newsDel(Request $request)
    {
        $id = $request->id;
        $this->newsService->newsDel(['id' => $id]);
        return ResponseUtil::jsonResponse(true, 0, '删除成功', '');
    }

    public function newsAddValidate(Array $input)
    {
        return Validator::make($input, [
            'subject' => [
                'required',
                'max:50'
            ],
            'content' => [
                'required'
            ],
        ], [
            'subject.required' => '新闻标题必须',
            'subject.max'      => '新闻标题不得超过50个字符',
            'content.max'      => '新闻内容不得超过4000个字符',
            'content.required' => '新闻内容必须',
        ])->after(function ($validator) use ($input) {

        })->validate();
    }


}