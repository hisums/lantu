<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/12
 * Time: 16:09
 */

namespace App\Http\Controllers\News;

use App\Lib\Util\ResponseUtil;
use App\Services\AboutService;
use Illuminate\Http\Request;

class AboutController
{
    private $aboutService = null;

    public function __construct(AboutService $aboutService)
    {
        $this->aboutService = $aboutService;
    }


    public function aboutIndex()
    {
        return view('news.about',[
            'list'=>$this->aboutService->getData()
        ]);
    }

    public function aboutEdit(Request $request)
    {
        $input = $request->all();
        $this->aboutService->editContent($input);
        return ResponseUtil::jsonResponse(true, 0, '保存成功', '');
    }


}