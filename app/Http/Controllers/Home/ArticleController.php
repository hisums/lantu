<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/4
 * Time: 11:58
 */

namespace App\Http\Controllers\Home;


use App\News;
use App\NewsCategory;
use App\Services\FileService;
use App\Services\GoodsCategoryService;
use App\Services\NewsService;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends BaseController
{
    private $categoryService = null;
    private $newsCategoryService = null;
    private $newsService = null;
    private $fileService = null;

    public function __construct(GoodsCategoryService $categoryService,
                                NewsCategory $newsCategoryService,
                                NewsService $newsService,
                                FileService $fileService
    )
    {
        parent::__construct($categoryService);
        $this->newsCategoryService = $newsCategoryService;
        $this->categoryService     = $categoryService;
        $this->newsService         = $newsService;
        $this->fileService         = $fileService;
    }

    public function activityList()
    {
        $actList = $this->newsService->actList();
        $reagent = $this->fileService->getReagentInfoNoPage();
        return view('home.consulting.activity', [
            'title'    => '促销活动',
            'show'     => true,
            'cates'    => $this->all_cate,
            'about'    => $this->about,
            'act_list' => $actList,
            'reagent'  => $reagent,
            'act'      => 'activity'
        ]);
    }

    public function activityDetail(Request $request)
    {
        $id        = $request->id;
        $actDetail = $this->newsService->actDetail($id);
        $reagent   = $this->fileService->getReagentInfoNoPage();
        return view('home.consulting.activityDetail', [
            'title'          => $actDetail->subject,
            'show'           => false,
            'cates'          => $this->all_cate,
            'about'          => $this->about,
            'act_detail'     => $actDetail,
            'reagent'        => $reagent,
            'act'            => 'activity',
            'activity_title' => $actDetail->subject ? $actDetail->subject : '活动'
        ]);
    }


    /**
     * @description:文章列表
     * @author: hkw <hkw925@qq.com>
     * @param null $cate_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article_list($cate_id = null)
    {
        if(!$cate_id){
            $article_list = $this->newsService->allArticleList();
        }else{
            $article_list = $this->newsService->articleList($cate_id);
        }
        return view('home.article.article_list', [
            'title'       => '资讯中心',
            'cates'       => $this->all_cate,
            'about'       => $this->about,
            'article'     => $article_list,
            'common_data' => $this->commonRecommend(),
            'cate_id'     => $cate_id
        ]);
    }

    public function article_list_all(Request $request)
    {
        $cate_id      = $request->cate_id;
        $cate_name    = NewsCategory::where('id', $cate_id)->value('name');
        $article_list = $this->newsService->articleList($cate_id);
        return view('home.article.article_list', [
            'title'       => $cate_name,
            'cates'       => $this->all_cate,
            'about'       => $this->about,
            'article'     => $article_list,
            'common_data' => $this->commonRecommend(),
            'cate_id'     => $cate_id
        ]);
    }

    public function article_detail(Request $request)
    {
        $id          = $request->id;
        $articleInfo = $this->newsService->articleDetail($id);
        $title       = NewsCategory::where('id', $articleInfo->news_cate_id)->value('name');
        return view('home.article.article_detail', [
            'title'        => $title,
            'cates'        => $this->all_cate,
            'about'        => $this->about,
            'article_info' => $articleInfo,
            'common_data'  => $this->commonRecommend()
        ]);
    }

    //今日推荐  总排行数据
    public function commonRecommend()
    {
        $todayRecommend = $this->newsService->todayRecommend();
        $ranking        = $this->newsService->ranking();
        return [
            'today_recommend' => $todayRecommend,
            'ranking'         => $ranking,
        ];
    }


    public function about_us()
    {
        $aboutUs = News::where('news_cate_id', NewsCategory::$DEFAULT_CATELTJS)->orderBy('sort_order', 'asc')->first();
        $connectUs = News::where('news_cate_id', NewsCategory::$DEFAULT_CATELXWM)->orderBy('sort_order', 'asc')->first();
        return view('home.article.aboutUs', [
            'title'    => '蓝图介绍',
            'about'    => $this->about,
            'cates'    => $this->all_cate,
            'about_us' => $aboutUs,
            'connect_us' => $connectUs
        ]);
    }

    public function contact_us()
    {
        $connectUs = News::where('news_cate_id', NewsCategory::$DEFAULT_CATELXWM)->orderBy('sort_order', 'asc')->first();
        return view('home.article.contact_us', [
            'title'      => '联系我们',
            'about'      => $this->about,
            'cates'      => $this->all_cate,
            'connect_us' => $connectUs
        ]);
    }

    public function job_officer()
    {
        $managerData = $this->newsService->articleList(NewsCategory::$DEFAULT_CATE_GLGWZP);
        $tecData     = $this->newsService->articleList(NewsCategory::$DEFAULT_CATE_JSGWZP);
        $otherData   = $this->newsService->articleList(NewsCategory::$DEFAULT_CATE_QTGWZP);
        return view('home.article.jobOfficer', [
            'title'        => '招聘信息',
            'cates'        => $this->all_cate,
            'about'        => $this->about,
            'manager_data' => $managerData,
            'tec_data'     => $tecData,
            'other_data'   => $otherData,
        ]);
    }
}
