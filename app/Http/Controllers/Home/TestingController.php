<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/9
 * Time: 15:27
 */

namespace App\Http\Controllers\Home;

use App\News;
use App\Services\GoodsCategoryService;
use App\NewsCategory;
use App\Services\NewsService;
use Illuminate\Http\Request;

class TestingController extends BaseController
{
    private $categoryService = null;
    private $newsCategoryService = null;
    private $newsService = null;

    public function __construct(GoodsCategoryService $categoryService,
                                NewsCategory $newsCategoryService,
                                NewsService $newsService
    )
    {
        parent::__construct($categoryService);
        $this->newsCategoryService = $newsCategoryService;
        $this->categoryService     = $categoryService;
        $this->newsService         = $newsService;
    }

    public function testing_center()
    {
        $center          = $this->newsService->testingArticleList(NewsCategory::$DEFAULT_CATE_ZXJJ);
        $cateArticleList = $this->newsService->testing_center_cate();
        return view('home.testing.testing_center', [
            'title'             => '检测中心',
            'show'              => false,
            'cates'             => $this->all_cate,
            'about'             => $this->about,
            'center'            => !$center->isEmpty() ? $center[0] : '',
            'cate_article_list' => $cateArticleList
        ]);
    }

    public function testing_detection()
    {
        $detection = $this->newsService->testingArticleList(NewsCategory::$DEFAULT_CATE_JCLC);
        $testImg   = $this->newsService->testingArticleList(NewsCategory::$DEFAULT_CATESYZSTP);
        return view('home.testing.testing_detection', [
            'title'     => '检测流程',
            'show'      => false,
            'cates'     => $this->all_cate,
            'about'     => $this->about,
            'detection' => !$detection->isEmpty() ? $detection[0] : '',
            'test_img'  => $testImg
        ]);
    }


    public function testing_sub(Request $request)
    {
        $cateSubId = $request->cate_sub_id;
        $cateId    = $request->cate_id;
        $cateName  = NewsCategory::where('id', $cateSubId)->value('name');
        //检测流程主文章
        $cateArticle = News::where('news_cate_id', $cateSubId)->orderBy('sort_order', 'asc')->first();
        //检测流程相关文章
        $testingSubList = $this->newsService->testingArticleList($cateId);
        return view('home.testing.testing_sub', [
            'title'            => $cateName,
            'show'             => false,
            'cates'            => $this->all_cate,
            'about'            => $this->about,
            'testing_sub_list' => $testingSubList,
            'cate_article'     => $cateArticle,
            'cate_id'          => $cateId,
            'cate_sub_id'      => $cateSubId
        ]);
    }
}