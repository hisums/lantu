<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/7
 * Time: 11:30
 */

namespace App\Http\Controllers\Home;


use App\Lib\Util\ResponseUtil;
use App\Services\UserService;
use App\Services\GoodsCategoryService;
use Gregwar\Captcha\CaptchaBuilder;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Session;
use Validator;

class LoginController extends BaseController
{
    private $categoryService = null;
    private $userService = null;

    public function __construct(UserService $userService,
                                GoodsCategoryService $categoryService
    )
    {
        parent::__construct($categoryService);
        $this->categoryService = $categoryService;
        $this->userService     = $userService;
    }

    public function login()
    {
        return view('home.login', [
            'title' => '登录',
            'show'  => false,
            'cates' => $this->all_cate,
            'about'    => $this->about,
        ]);
    }

    public function doLogin(Request $request)
    {

        $input                = $request->all();
        $input['true_verify'] = Session::get('verify');
        $this->loginValidate($input);
        Session::put('userInfo', Auth::user()->toArray());
        Session::save();
        return ResponseUtil::jsonResponse(true, 0, '登录成功!', '');
    }

    public function loginValidate($input)
    {
        return Validator::make($input, [
            'name'     => [
                'required'
            ],
            'password' => [
                'required'
            ],
            'verify'   => [
                'required',
            ]
        ], [
            'name.required'     => '请输入用户名',
            'password.required' => '请输入密码',
            'verify.required'   => '请输入验证码',
        ])->after(function ($validator) use ($input) {
            if (!empty($input['verify'])) {
                if ($input['verify'] != $input['true_verify']) {
                    $validator->errors()->add('verify.error', '验证码错误!');
                }
            }
            if (!empty($input['name']) && !empty($input['password'])) {
                if (!Auth::attempt(['name' => $input['name'], 'password' => $input['password']], 1)) {
                    $validator->errors()->add('name.error', '用户名或密码错误!');
                }
            }
        })->validate();
    }


    public function captcha(Request $request)
    {

        //生成验证码图片的Builder对象，配置相应属性

        $builder = new CaptchaBuilder;
        //可以设置图片宽高及字体
        $builder->build($width = 100, $height = 32, $font = null);
        //获取验证码的内容
        $phrase = $builder->getPhrase();
        //把内容存入session
        Session::put('verify', $phrase);
        Session::save();
        //生成图片
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-Type: image/jpeg');
        $builder->output();
        exit(0);
    }

    public function logout()
    {
        Session::forget('userInfo');
    }

    public function register()
    {
        return view('home.register', [
            'title' => '注册',
            'show'  => false,
            'cates' => $this->all_cate,
            'about'    => $this->about,
        ]);
    }

    public function registerValidate(Array $input)
    {
        return Validator::make($input, [
            'name'     => [
                'required',
                'between:6,18',
                'unique:users,name'
            ],
            'password' => [
                'required',
                'between:6,20'
            ],
            'verify'   => [
                'required',
            ]
        ], [
            'name.required'     => '请输入用户名',
            'name.between'      => '用户名长度有误!',
            'name.unique'       => '用户名已存在!',
            'password.required' => '请输入密码',
            'password.between'  => '密码只能在6-22位之间',
            'verify.required'   => '请输入验证码',
        ])->after(function ($validator) use ($input) {
            if (!empty($input['verify'])) {
                if ($input['verify'] != $input['true_verify']) {
                    $validator->errors()->add('verify.error', '验证码错误!');
                }
            }
        })->validate();
    }

    public function doRegister(Request $request)
    {
        $input                = $request->all();
        $input['true_verify'] = Session::get('verify');
        $this->registerValidate($input);
        $this->userService->homeRegister($input);
        return ResponseUtil::jsonResponse(true, 0, '注册成功!', '');
    }
}