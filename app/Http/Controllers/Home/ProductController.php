<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/6 0006
 * Time: 下午 3:26
 */
namespace App\Http\Controllers\Home;

use App\Services\FileService;
use App\Services\GoodsCategoryService;
use App\Services\GoodsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use sngrl\SphinxSearch\SphinxSearch;

class ProductController extends BaseController
{
    private $goodsService = null;
    private $goodsCategoryService = null;
    private $fileService = null;

    public function __construct(GoodsCategoryService $categoryService, GoodsService $goodsService, FileService $fileService)
    {
        parent::__construct($categoryService);
        $this->fileService          = $fileService;
        $this->goodsCategoryService = $categoryService;
        $this->goodsService         = $goodsService;
    }

    /**
     * @description:商品详情
     * @author: hkw <hkw925@qq.com>
     * @param $id
     * @param $spe_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function goodsDetail($id, $spe_id = 0)
    {
        $info        = $this->goodsService->goodsInfo($id, $spe_id);
        $inquiryList = $this->goodsService->inquiryList($id);
        $fileInfo    = $this->fileService->getFileDetail($id);
        $reagent     = $this->fileService->getReagentInfoNoPage();
        return view('home.product.productDetail', [
            'title'       => '产品详情',
            'cates'       => $this->all_cate,
            'about'       => $this->about,
            'goods_info'  => $info['goods_info'],
            'spec_info'   => $info['spec_info'],
            'inquiryList' => $inquiryList,
            'file_info'   => $fileInfo,
            'reagent'     => $reagent,
        ]);
    }

    /**
     * @description:商品咨询提交
     * @author: hkw <hkw925@qq.com>
     * @param Request $request
     * @return string
     */
    public function addInquiry(Request $request)
    {
        $this->validate($request, [
            'content'       => 'required',
            'customer_name' => 'required',
            'customer_tel'  => 'required',
        ], [
            'content.required'       => '请输入咨询内容',
            'customer_name.required' => '请输入姓名',
            'customer_tel.required'  => '请输入联系方式',
        ]);
        if ($this->goodsService->addInquiry($request->input())) {
            return json_encode(['code' => 0, 'msg' => '咨询成功']);
        } else {
            return json_encode(['code' => 1, 'msg' => '咨询失败']);
        }
    }

    /**
     * @description:商品列表
     * @author: hkw <hkw925@qq.com>
     * @param int $cate_id
     * @param int $order_type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function goodsList($cate_id = 0, $order_type = 2)
    {
        //dd($this->goodsService->goodsSpecList()[0]->goods);exit;
        $cate_info = $this->goodsCategoryService->getCateInfo($cate_id);
        $keywords  = request()->input('keywords');
        //dd($keywords);
        $reagent   = $this->fileService->getReagentInfoNoPage();
        if(request()->ajax()){
            $input = request()->all();
            //dump($input);
            $keywords = $input['keywords'];
            //$inputs['pageSize'] = $input['pageSize'];
            //$inputs['pageNumber'] = $input['pageNumber'];
            $specList  = $this->goodsService->goodsSpecList($cate_id, $order_type, $keywords,$input);
            return $specList;
        }else{
            return view('home.product.productList', [
                'cate_id'    => $cate_id,
                'order_type' => $order_type,
                'title'      => $cate_info ? $cate_info->goods_name : '全部产品',
                'cates'      => $this->all_cate,
                'about'      => $this->about,
                'reagent'    => $reagent,
                'keywords'   => $keywords,
                'show'       => true,
            ]);
        }
        /*$specList  = $this->goodsService->goodsSpecList($cate_id, $order_type, $keywords);

        //dd($specList);
        return view('home.product.productList', [
            'cate_id'    => $cate_id,
            'order_type' => $order_type,
            'title'      => $cate_info ? $cate_info->goods_name : '全部产品',
            'cates'      => $this->all_cate,
            'about'      => $this->about,
            'specList'   => $specList, // 返回精确查找
            'reagent'    => $reagent,
            'show'       => true,
        ]);*/
    }

}