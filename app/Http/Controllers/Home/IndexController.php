<?php
namespace App\Http\Controllers\Home;

use App\Services\GoodsCategoryService;
use App\Services\NewsService;
use sngrl\SphinxSearch\SphinxSearch;


/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/7
 * Time: 9:02
 */
class IndexController extends BaseController
{
    private $categoryService = null;
    private $newsService = null;

    public function __construct(GoodsCategoryService $categoryService,
                                NewsService $newsService
    )
    {
        parent::__construct($categoryService);
        $this->newsService     = $newsService;
        $this->categoryService = $categoryService;
    }


    public function index()
    {
        $banner   = $this->newsService->getBanner();
        return view('home.index', [
            'title'  => '蓝图生物科技有限公司',
            'show'   => true,
            'cates'  => $this->all_cate,
            'about'    => $this->about,
            'banner' => $banner
        ]);
    }

    public function aboutUs(){
        return view('home.aboutUs',['title'=>'关于我们']);
    }

}