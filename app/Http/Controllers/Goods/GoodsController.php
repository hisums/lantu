<?php
namespace App\Http\Controllers\Goods;

use App\Goods;
use App\GoodsCategory;
use App\GoodsFiles;
use App\GoodsInquirys;
use App\GoodsParams;
use App\GoodsSpecs;
use App\Lib\Util\ResponseUtil;
use App\ServiceFiles;
use App\Services\GoodsCategoryService;
use App\Services\GoodsParamsService;
use App\Services\GoodsSpecsService;
use App\Services\GoodsService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use App\Services\FileService;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/18
 * Time: 9:49
 */
class GoodsController extends Controller
{

    private $goodsCategoryService = null;
    private $goodsService = null;
    private $goodsSpecsService = null;
    private $goodsParamsService = null;
    private $fileService = null;


    public function __construct(GoodsCategoryService $goodsCategoryService,
                                GoodsService $goodsService,
                                GoodsSpecsService $goodsSpecsService,
                                GoodsParamsService $goodsParamsService,
                                FileService $fileService
    )
    {
        $this->goodsParamsService   = $goodsParamsService;
        $this->goodsCategoryService = $goodsCategoryService;
        $this->goodsService         = $goodsService;
        $this->goodsSpecsService    = $goodsSpecsService;
        $this->fileService          = $fileService;
    }

    /*
     * @author: hy
     * @name: 分类主页
     * @param: $request
     */
    public function cateIndex(Request $request)
    {
        return view('goods.goods_category');
    }

    /*
     * @author: hy
     * @name: 分类数据查询
     * @param: $request
     */
    public function cateQuery(Request $request)
    {
        $input = $request->all();
        return $this->goodsCategoryService->getCate($input);
    }

    /*
     * @author: hy
     * @name: 分类添加页面
     * @param: $request
     */
    public function cateAddIndex(Request $request)
    {
        $parentId = $request->parentId;

        if (!empty($parentId) && $parentId != 'null') {
            $parent = GoodsCategory::findOrFail($parentId);
        } else {
            $parent = null;
        }
        $prevCate = $this->goodsCategoryService->getUpCate($parent);
        return view('goods.goods_category_create', [
            'parent_id'  => $parentId,
            'prev_cates' => $prevCate
        ]);
    }

    /*
     * @author: hy
     * @name: 分类添加
     * @param: $request
     */
    public function cateAdd(Request $request)
    {
        $input = $request->all();
        $this->goodsCategoryService->cateAdd($input);
        return ResponseUtil::jsonResponse(true, 0, '添加成功', '');
    }

    /*
     * @author: hy
     * @name: 左侧分类树
     * @param: $request
     */
    public function cateIndexTrees(Request $request)
    {
        $input = $request->all();
        return $this->goodsCategoryService->getInitCategory($input);
    }

    /*
     * @author: hy
     * @name: 分类编辑页面
     * @param: $request
     */
    public function cateEditIndex(Request $request)
    {
        $id      = $request->cateId;
        $goods   = GoodsCategory::findOrFail($id);
        $parent  = null;
        $allCate = $this->goodsCategoryService->getAllCate();
        return view('goods.goods_category_edit', [
            'cat_id'     => $id,
            'goods'      => $goods,
            'prev_cates' => $allCate
        ]);
    }

    /*
     * @author: hy
     * @name: 分类编辑
     * @param: $request
     */
    public function cateEdit(Request $request)
    {
        $input = $request->all();
        $this->cateEditValidate($input);
        $this->goodsCategoryService->cateEdit($input);
        return ResponseUtil::jsonResponse(true, 0, '修改成功', '');
    }

    /*
     * @author: hy
     * @name: 分类编辑验证
     * @param: $request
     */
    public function cateEditValidate($input)
    {
        return Validator::make($input, [
            'cate_id'    => [
                'required',
                'numeric'
            ],
            'parent_id'  => [
                'required',
                'numeric'
            ],
            'goods_name' => [
                'required',
            ]
        ], [
            'cate_id.required'    => '必须传入分类ID',
            'activity_id.numeric' => '分类ID格式不正确，必须为数字',
            'parent_id.required'  => '必须传入父分类ID',
            'parent_id.numeric'   => '父分类ID格式不正确，必须为数字',
            'goods_name.required' => '分类名必填',
        ])->after(function ($validator) use ($input) {
            if (!empty($input['cate_id']) && !empty($input['parent_id'])) {
                $list  = $this->goodsCategoryService->getAllCate();
                $child = $this->goodsCategoryService->getAllChild($list, $input['cate_id']);
                array_push($child, $input['cate_id']);
                if (in_array($input['parent_id'], $child)) {
                    $validator->errors()->add('goodsCategory.illegal', '父级分类选择有误');
                }
            }
        })->validate();
    }

    /*
     * @author: hy
     * @name: 分类删除
     * @param: $request
     */
    public function cateDel(Request $request)
    {
        $input = $request->all();
        $this->cateDelValidate($input);
        $this->goodsCategoryService->delCate($input);
        return ResponseUtil::jsonResponse(true, 0, '删除成功', '');
    }

    /*
     * @author: hy
     * @name: 分类删除验证
     * @param: $request
     */
    public function cateDelValidate(Array $input)
    {
        return Validator::make($input, [
            'id' => [
                'required',
                'numeric'
            ],
        ], [
            'id.required' => '必须传入分类ID',
            'id.numeric'  => '分类ID格式不正确，必须为数字',
        ])->after(function ($validator) use ($input) {
            if (!empty($input['id'])) {
                $list  = $this->goodsCategoryService->getAllCate();
                $child = $this->goodsCategoryService->getAllChild($list, $input['id']);
                if (!empty($child)) {
                    $validator->errors()->add('goodsCategory.illegal', '该分类存在子分类,不可删除');
                }
                $res = $this->goodsCategoryService->issetGoods($input['id']);
                if ($res) {
                    $validator->errors()->add('goodsCategory.illegal', '该分类存在其它关联,不可删除');
                }
            }
        })->validate();
    }

    /*
     * @author: hy
     * @name: 商品管理首页
     * @param: $request
     */
    public function goodsIndex(Request $request)
    {
        $allCate = $this->goodsCategoryService->getAllCate();
        return view('goods.goods_index', [
            'all_cates' => $allCate
        ]);
    }

    /*
     * @author: hy
     * @name: 商品列表查询
     * @param: $request
     */
    public function goodsIndexQuery(Request $request)
    {
        $input = $request->all();
        //dd($input);
        return $this->goodsService->getGoods($input);
    }

    /*
     * @author: hy
     * @name: 商品添加页面
     * @param: $request
     */
    public function goodsAddIndex(Request $request)
    {
        $leafCate = $this->goodsCategoryService->getLeaf();
        $allFiles = $this->fileService->getAllFile();
        return view('goods.goods_add', [
            'leaf_cate' => $leafCate,
            'all_files' => $allFiles,
        ]);
    }

    /*
     * @author: hy
     * @name: 商品添加
     * @param: $request
     */
    public function goodsAdd(Request $request)
    {
        $input = $request->all();
        $this->goodsAddValidate($input);
        $this->goodsService->goodsAdd($input);
        return ResponseUtil::jsonResponse(true, 0, '添加成功', '');
    }

    /*
     * @author: hy
     * @name: 商品添加验证
     * @param: $request
     */
    public function goodsAddValidate(Array $input)
    {
        return Validator::make($input, [
            'goods_name'        => [
                'required',
                'max:50'
            ],
            'goods_vendor'      => [
                'required'
            ],
            'sort_order'        => [
                'required',
                'between:1,10',
                'integer'
            ],
            'list_flag'         => [
                'required',
                'between:1,2',
                'integer'
            ],
            'goods_category_id' => [
                'required',
                'integer'
            ],
        ], [
            'goods_name.required'        => '商品名字必须',
            'goods_vendor.required'      => '商品厂家必须',
            'list_flag.required'         => '请选择上架信息',
            'goods_category_id.required' => '请选择商品分类',
            'sort_order.required'        => '请填写排序信息',
        ])->after(function ($validator) use ($input) {
            if (!empty($input['goods_category_id'])) {
                $listId = $this->goodsCategoryService->getLeafId();
                if (!in_array($input['goods_category_id'], $listId->toArray())) {
                    $validator->errors()->add('goods.illegal', '分类选择有误');
                }
            }
        })->validate();
    }

    /*
     * @author: hy
     * @name: 商品编辑页面
     * @param: $request
     */
    public function goodsEditIndex(Request $request)
    {
        $id       = $request->id;
        $goods    = Goods::findOrFail($id);
        $file_ids  = GoodsFiles::where('goods_id', $id)->pluck('file_id');
        $leafCate = $this->goodsCategoryService->getLeaf();
        $allFiles = $this->fileService->getAllFile();
        return view('goods.goods_add', [
            'goods'     => $goods,
            'leaf_cate' => $leafCate,
            'all_files' => $allFiles,
            'file_ids'   => $file_ids
        ]);
    }

    /*
     * @author: hy
     * @name: 商品编辑
     * @param: $request
     */
    public function goodsEdit(Request $request)
    {
        $input = $request->all();
        $this->goodsAddValidate($input);
        $this->goodsService->goodsEdit($input);
        return ResponseUtil::jsonResponse(true, 0, '修改成功', '');
    }

    /*
     * @author: hy
     * @name: 商品删除
     * @param: $request
     */
    public function goodsDel(Request $request)
    {
        $input['id'] = $request->id;
        $this->goodsService->goodsDel($input);
        return ResponseUtil::jsonResponse(true, 0, '删除成功', '');
    }

    /*
     * @author: hy
     * @name: 商品批量删除
     * @param: $request
     */
    public function delGoodsBatch(Request $request)
    {
        $input = $request->all();
        $this->goodsService->delGoodsBatch($input);
        return ResponseUtil::jsonResponse(true, 0, '删除成功', '');
    }

    /*
     * @author: hy
     * @method:get
     * @name: 商品规格
     * @param: $request
     */
    public function goodsSpecs(Request $request)
    {
        $input['goods_id'] = $request->goodsId;
        //var_dump($input);
        $goodsSpecs        = $this->goodsSpecsService->getGoodsSpecs($input);
        //dd($goodsSpecs);
        return view('goods.goods_specs1', [
            'goods_specs' => $goodsSpecs,
            'goods_id'    => $input['goods_id']
        ]);
    }

    /*
     * @author: hy
     * @name: 商品规格列表查询
     * @method:post
     * @param: $request
     */
    public function goodsSpecsQuery(Request $request)
    {
        $input = $request->all();

    }

    /*
     * @author: hy
     * @name: 商品规格添加页面
     * @method:get
     * @param: $request
     */
    public function goodsSpecsAddIndex(Request $request)
    {
        $goodsId = $request->id;
        return view('goods.goods_spesc_add', [
            'goods_id' => $goodsId
        ]);
    }

    public function goodsSpecsAddValidate($input)
    {
        return Validator::make($input, [
            'goods_spec'     => [
                'required',
            ],
            'goods_number'   => [
                'required',
            ],
            'inventory'      => [
                'required',
                'integer',
                'min:0'
            ],
            'price'          => [
                'required',
            ],
            'origin_price'   => [
                'required',
            ],
            'cost_price'     => [
                'required',
            ],
            'unit'           => [
                'required',
            ],
            'min_buy_amount' => [
                'required',
                'min:0',
            ],
            'sort_order'     => [
                'required',
                'integer',
            ],
            'display_flag'   => [
                'required',
                'between:1,2',
            ],
        ], [
            'goods_model.required'  => '商品型号必须',
            'goods_spec.required'   => '商品规格必须',
            'goods_number.required' => '商品货号必须',
            'inventory.integer'     => '商品库存必须为数字',
            'inventory.required'    => '商品库存必须',
            'price.required'        => '销售价格必须',
        ])->validate();
    }

    /*
     * @author: hy
     * @name: 商品规格添加
     * @method:post
     * @param: $request
     */
    public function goodsSpecsAdd(Request $request)
    {
        $input = $request->all();
        $this->goodsSpecsAddValidate($input);
        $this->goodsSpecsService->addGoodsSpecs($input);
        return ResponseUtil::jsonResponse(true, 0, '添加成功', '');
    }

    /*
     * @author: hy
     * @name: 商品规格编辑页面
     * @method:get
     * @param: $request
     */
    public function goodsEditSpecsIndex(Request $request)
    {
        $id         = $request->id;
        $goodsSpecs = GoodsSpecs::findOrFail($id);
        return view('goods.goods_spesc_add', [
            'goods_id'    => $goodsSpecs->goods_id,
            'goods_specs' => $goodsSpecs
        ]);
    }

    /*
     * @author: hy
     * @name: 商品规格编辑
     * @method:post
     * @param: $request
     */
    public function goodsSpecsEdit(Request $request)
    {
        $input = $request->all();
        $this->goodsSpecsAddValidate($input);
        $this->goodsSpecsService->editGoodsSpecs($input);
        return ResponseUtil::jsonResponse(true, 0, '修改成功', '');
    }

    /*
     * @author: hy
     * @name: 商品规格删除
     * @method:get
     * @param: $request
     */
    public function goodsSpecsDel(Request $request)
    {
        $id = $request->id;
        $this->goodsSpecsService->delGoodsSpecs($id);
        return ResponseUtil::jsonResponse(true, 0, '删除成功', '');
    }

    /*
     * @author: hy
     * @name: 商品属性
     * @method:get
     * @param: $request
     * @route:/goods/goods/attribute/{goodsId}
     */
    public function goodsAttribute(Request $request)
    {
        $goodsId     = $request->goodsId;
        $goodsParams = $this->goodsParamsService->getGoodsParams($goodsId);
        return view('goods.goods_params', [
            'goods_id'     => $goodsId,
            'goods_params' => $goodsParams
        ]);
    }

    /*
     * @author: hy
     * @name: 商品属性列表查询
     * @method:POST
     * @param: $request
     * @route:/goods/goods/attributeQuery
     */
    public function goodsAttributeQuery(Request $request)
    {

    }

    /*
     * @author: hy
     * @name: 商品属性添加页面
     * @method:get
     * @param: $request
     * @route:/goods/goods/addGoodsAttributeIndex/{id}
     */
    public function goodsAttributeAddIndex(Request $request)
    {
        $goodsId = $request->id;
        return view('goods.goods_params_add', [
            'goods_id' => $goodsId
        ]);
    }

    /*
     * @author: hy
     * @name: 商品属性添加
     * @method:post
     * @param: $request
     * @route:/goods/goods/addAttributeSpecs
     */
    public function goodsAttributeAdd(Request $request)
    {
        $input = $request->all();
        $this->goodsAttrAddValidate($input);
        $this->goodsParamsService->goodsAttributeAdd($input);
        return ResponseUtil::jsonResponse(true, 0, '添加成功', '');
    }

    /*
     * @author: hy
     * @name: 商品属性编辑页面
     * @method:GET
     * @param: $request
     * @route:/goods/goods/editAttributeIndex/{id}
     */
    public function goodsEditAttributeIndex(Request $request)
    {
        $id        = $request->id;
        $goodsAttr = GoodsParams::findOrFail($id);
        return view('goods.goods_params_add', [
            'goods_id'   => $goodsAttr->goods_id,
            'goods_attr' => $goodsAttr
        ]);
    }

    /*
     * @author: hy
     * @name: 商品属性编辑
     * @method:post
     * @param: $request
     * @route:/goods/goods/editAttribute
     */
    public function goodsAttributeEdit(Request $request)
    {
        $input = $request->all();
        $this->goodsAttrAddValidate($input);
        $this->goodsParamsService->goodsAttributeEdit($input);
        return ResponseUtil::jsonResponse(true, 0, '添加成功', '');
    }

    /*
     * @author: hy
     * @name: 商品属性删除
     * @method:GET
     * @param: $request
     * @route:/goods/goods/delAttribute/{id}
     */
    public function goodsAttributeDel(Request $request)
    {
        $id = $request->id;
        $this->goodsParamsService->goodsAttributeDel($id);
        return ResponseUtil::jsonResponse(true, 0, '删除成功', '');
    }

    public function goodsAttrAddValidate($input)
    {
        return Validator::make($input, [
            'params_name'  => [
                'required',
            ],
            'params_value' => [
                'required',
            ],
            'sort_order'   => [
                'required',
                'integer',
            ],
        ], [
            'params_name.required' => '商品属性名字必须',
            'goods_spec.required'  => '商品属性值必须',
            'sort_order.required'  => '排序必须',
            'sort_order.integer'   => '排序必须为数字',
        ])->validate();
    }

    /*
     * @author: hy
     * @name: 商品咨询
     * @method:GET
     * @param: $request
     * @route:/goods/goods/inquirysQuery
     */
    public function goodsInquirys()
    {
        $allCate = $this->goodsCategoryService->getAllCate();
        return view('goods.goods_inquirys', [
            'all_cates' => $allCate
        ]);
    }

    /*
     * @author: hy
     * @name: 商品咨询查询
     * @method:POST
     * @param: $request
     * @route:/goods/goods/inquirysQuery
     */
    public function goodsInquirysQuery(Request $request)
    {
        $input = $request->all();
        return $this->goodsService->getGoodsInquirys($input);
    }

    /*
    * @author: hy
    * @name: 商品咨询详情
    * @method:GET
    * @param: $request
    * @route:/goods/goods/inquirysDetail/{id}
    */
    public function goodsInquirysDetail(Request $request)
    {
        $id      = $request->id;
        $content = GoodsInquirys::where('id', '=', $id)->first();
        return $content['content'];
    }

    /*
    * @author: hy
    * @name: 商品咨询标记处理
    * @method:GET
    * @param: $request
    * @route:/goods/goods/inquirysOperate/{id}
    */
    public function goodsInquirysOperate(Request $request)
    {
        $id = $request->id;
        $this->goodsService->changeProcess($id);
        return ResponseUtil::jsonResponse(true, 0, '标记成功', '');
    }

    /*
    * @author: hy
    * @name: 商品咨询控制显示
    * @method:GET
    * @param: $request
    * @route:/goods/goods/inquirysOperateShow/{id}
    */
    public function goodsInquirysShow(Request $request)
    {
        $id = $request->id;
        $this->goodsService->changeInquirysShow($id);
        return ResponseUtil::jsonResponse(true, 0, '修改成功', '');
    }

    /*
     * @author: hy
     * @name: 商品批量添加页面
     * @method:GET
     * @param: $request
     * @route:/goods/goods/addGoodsBatchIndex
     */
    public function addGoodsBatchIndex()
    {
        $leafCate = $this->goodsCategoryService->getLeaf();
        return view('goods.goods_batch_index', [
            'leaf_cate' => $leafCate,
        ]);
    }

    /*
     * @author: hy
     * @name: 商品批量添加
     * @method:POST
     * @param: $request
     * @route:/goods/goods/addGoodsBatch
     */
    public function addGoodsBatch(Request $request)
    {
        $input = $request->all();
        $error = $this->goodsService->goodsBatchAdd($input);
        if (empty($error)) {
            return ResponseUtil::jsonResponse(true, 0, '批量添加成功', '');
        } else {
            return ResponseUtil::jsonResponse(false, 0, '部分数据关联文件名出错', $error);
        }

    }

    /**
     * @author: shuyougeng
     * @name: 商品批量修改页面
     * @method: POST
     * @param Request $request
     * @route: /goods/goods/editGoodsBatch
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editGoodsBatch(Request $request)
    {
        $goods_ids = $request->ids;
        $goods_ids = implode('/',$goods_ids);
        //dd($goods_ids);
        $leafCate = $this->goodsCategoryService->getLeaf();
        return view('goods.goods_spesc_eidt',[
            'goods_ids' => $goods_ids,
            'leaf_cate' => $leafCate,
        ]);
    }

    /**
     * @author: shuyougeng
     * @name: 商品批量修改
     * @method: POST
     * @param Request $request
     * @route: /goods/goods/editGoodsSpecBatch
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editGoodsSpecBatch(Request $request)
    {
        $input = $request->all();
        $error = $this->goodsService->goodsSpecBatchEdit($input);
        if (empty($error)) {
            return ResponseUtil::jsonResponse(true, 0, '批量规格修改成功', '');
        } else {
            return ResponseUtil::jsonResponse(false, 0, '部分数据关联文件名出错', $error);
        }
    }
}