<?php

namespace App\Http\Controllers\League;

use App\Services\LeagueService;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/2
 * Time: 15:25
 */
class LeagueController extends Controller
{
    private $leagueService = null;


    public function __construct(LeagueService $leagueService
    )
    {
        $this->leagueService = $leagueService;
    }

    /*
     * @author: hy
     * @name: 加盟申请列表
     * @method:GET
     * @param: $request
     * @route:/league/index
     */
    public function leagueIndex(Request $request)
    {
        return view('league.league');
    }

    /*
     * @author: hy
     * @name: 加盟申请查询
     * @method:POST
     * @param: $request
     * @route:/league/query
     */
    public function leagueQuery(Request $request)
    {
        $input = $request->all();
        return $this->leagueService->getApply($input);
    }

    public function downFile(Request $request)
    {
        $filePath = $request->filePath;
        return response()->download($filePath);
    }

}