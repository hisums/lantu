<?php

namespace App\Http\Middleware;

use Closure;

class HisumsVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->checkHisums()){
            return response('系统异常',502);
        }
        return $next($request);
    }

    private function checkHisums(){
        $hisums_verify = curlGet('http://project.hisums.cn/api/pm/verify/'.config('sms.hisums_verify.pname').'/'.config('sms.hisums_verify.token'));
        if($hisums_verify){
            if($hisums_verify_result = json_decode($hisums_verify,true)){
                if($hisums_verify_result['code'] == 1){
                    return false;
                }
            }else{
                return false;
            }
        }
        return true;
    }
}
