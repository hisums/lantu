<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/2
 * Time: 15:27
 */

namespace App\Services;

use App\League;
use App\Lib\Util\QueryPager;

class LeagueService
{
    public function getApply(Array $input,$paging = true)
    {
        $query = new League();


        $pager = new QueryPager($query);

        $pager->mapField('process_status', League::$PROCESS_STATUS_MAP);

        return $paging ? $pager->doPaginate($input, 'request_time') :
            $pager->queryWithoutPaginate($input, 'request_time');
    }
}