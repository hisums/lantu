<?php

namespace App\Services;

use App\BaseImg;
use App\Lib\Util\QueryPager;

class BaseImgService
{
    private function baseQuery()
    {
        return BaseImg::with('city')->select('id', 'img_type', 'picture', 'city_id', 'sort_order', 'link_url');
    }

    public function getBaseImages(Array $input, $paging = true)
    {
        $query = $this->baseQuery();

        if (!empty($input['cityId'])) {
            $query = $query->where('city_id', $input['cityId']);
        } else if ($input['cityId'] === 0) {
            $query = $query->whereNull('city_id');
        }

        if (!empty($input['imgType'])) {
            $query = $query->where('img_type', $input['imgType']);
        }

        $pager = new QueryPager($query);

        $pager->mapField('img_type', BaseImg::$BASE_IMAGE_TYPE_MAP);

        $pager->setRefectionMethodField('getFullPicturePath');
        $pager->setRefectionMethodField('getThumbnail');

        return $paging ? $pager->doPaginate($input, 'sort_order') :
            $pager->queryWithoutPaginate($input, 'sort_order');
    }
}
