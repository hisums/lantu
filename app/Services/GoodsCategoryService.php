<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/18
 * Time: 9:52
 */

namespace App\Services;

use App\Goods;
use DB;
use App\GoodsCategory;
use App\Lib\Util\QueryPager;

class GoodsCategoryService
{
    /*
     * @author: hy
     * @name: 创建顶级分类
     * @param: $request
     */
    public function createTopCategory()
    {
        $goods = GoodsCategory::create([
            'goods_name' => config('goods.init_goods_category_name'),
            'sort_order' => config('goods.init_sort_order'),
            'parent_id' => config('goods.init_parent_id'),
        ]);

        return $goods;
    }

    /*
     * @author: hy
     * @name: 分类添加
     * @param: $request
     */
    public function cateAdd(Array $input)
    {
        GoodsCategory::create([
            'goods_name' => $input['goods_name'],
            'parent_id'  => $input['parent_id'],
            'sort_order' => $input['sort_order']
        ]);
    }

    /*
     * @author: hy
     * @name: 获取数据 将数据做成树型结构
     * @param: $request
     */
    public function getInitCategory(Array $input)
    {
        $info = GoodsCategory::select(DB::raw('goods_name name'), 'id', 'parent_id', 'sort_order')
            ->orderBy('sort_order')->get()->toArray();
        return $this->getChildCate($info, null);
    }

    /*
     * @author: hy
     * @name: 获取数据 将数据做成树型结构
     * @param: $request
     */
    public function getChildCate($categories, $parentCategory)
    {
        $children = [];

        $categoryCount = count($categories);

        for ($i = 0; $i < $categoryCount; $i++) {
            $category = $categories[$i];

            if ($parentCategory === null) {
                if (empty($category['parent_id'])) {
                    $currentCate = $category;
                    array_push($children, $currentCate);
                    unset($categories[$i]);
                }
            } else {
                if ($category['parent_id'] == $parentCategory['id']) {
                    $currentCate = $category;
                    array_push($children, $currentCate);
                    unset($categories[$i]);
                }
            }
        }

        $result = [];

        $childrenCount = count($children);
        if ($childrenCount > 0) {
            $categories = array_values($categories);

            for ($i = 0; $i < $childrenCount; $i++) {
                $currentCate = $children[$i];

                array_push($result, array_merge($currentCate, [
                    'spread'   => true,
                    'children' => $this->getChildCate($categories, $currentCate)
                ]));
            }
        }

        return $result;
    }

    /*
     * @author: hy
     * @name: 获取数据
     * @param: $request
     */
    public function getCate(Array $input, $paging = true)
    {
        $query = new GoodsCategory();
        empty($input['parentId']) ? $input['parentId'] = GoodsCategory::$TOP_CATE : $input['parentId'];
        $query = $query->where('parent_id', $input['parentId']);
        $pager = new QueryPager($query);
        return $paging ? $pager->doPaginate($input, 'sort_order') :
            $pager->queryWithoutPaginate($input, 'sort_order');
    }

    /*
     * @author: hy
     * @name: 获取上级分类
     * @param: $request
     */
    public function getUpCate($parent)
    {
        if ($parent) {
            return GoodsCategory::where('id', $parent->id)
                ->orderBy('sort_order')->get();
        }
        return GoodsCategory::where('parent_id', GoodsCategory::$TOP_CATE)
            ->orderBy('sort_order')->get();
    }

    /*
     * @author: hy
     * @name: 修改分类
     * @param: $request
     */
    public function cateEdit(Array $input)
    {
        GoodsCategory::where('id', $input['cate_id'])->update([
            'goods_name' => $input['goods_name'],
            'parent_id'  => $input['parent_id'],
            'sort_order' => $input['sort_order']
        ]);
    }

    /*
     * @author: hy
     * @name: 获取所有分类
     * @param: $request
     */
    public function getAllCate()
    {
        return GoodsCategory::orderBy('parent_id', 'asc')->orderBy('sort_order', 'asc')->get();
    }

    /*
     * @author: hy
     * @name: 获取所有下级分类
     * @param: $request
     */
    public function getAllChild($list, $id)
    {
        static $arr = [];
        foreach ($list as $k => $v) {
            if ($v['parent_id'] == $id) {
                $arr[] = $v['id'];
                $this->getAllChild($list, $v['id']);
            }
        }
        return $arr;
    }

    /*
     * @author: hy
     * @name: 判断是否存在该分类的商品
     * @param: $request
     */
    public function issetGoods($cateId)
    {
        return Goods::where('goods_category_id', $cateId)->first();
    }

    /*
     * @author: hy
     * @name: 删除分类
     * @param: $request
     */
    public function delCate(Array $input)
    {
        GoodsCategory::where('id', $input['id'])
            ->delete();
    }

    /*
     * @author: hy
     * @name: 获取所有叶子分类
     * @param: $request
     */
    public function getLeaf()
    {
        //获取所有的parent_id 去重
        $allPId = GoodsCategory::groupBy('parent_id')->pluck('parent_id')->toArray();
        //查找所有不是parent_id 的 id
        $leftId = GoodsCategory::whereNotIn('id', $allPId)->pluck('id');
        return GoodsCategory::whereIn('id',$leftId)->get();
    }

    /*
     * @author: hy
     * @name: 获取所有叶子分类id
     * @param: $request
     */
    public function getLeafId()
    {
        //获取所有的parent_id 去重
        $allPId = GoodsCategory::groupBy('parent_id')->pluck('parent_id')->toArray();
        //查找所有不是parent_id 的 id
        return  GoodsCategory::whereNotIn('id', $allPId)->pluck('id');
    }

    /**
     * @description:根据分类id获取所有子分类id
     * @author: hkw <hkw925@qq.com>
     * @param $id
     * @return array
     */
    public function getChildId($id){
        static $ids = [];
        $ids[] = $id;
        $child = GoodsCategory::where('parent_id',$id)->pluck('id');
        if($child){
            foreach($child as $v){
                $this->getChildId($v);
            }
        }
        return $ids;
    }

    /**
     * @description:获取分类信息
     * @author: hkw <hkw925@qq.com>
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getCateInfo($id){
        return GoodsCategory::where('id',$id)->first();
    }

    public function getCateId(array $goods_name){
        return GoodsCategory::where('goods_name',$goods_name)->first();
    }
    public function getCateIds(array $goods_name){
        $id = [];
        foreach($goods_name as $k=>$v){
            if($k=0){
                $id[$k] = GoodsCategory::where(['goods_name'=>$v,'parent_id'=>0])->first()['id'];
                dump($id[$k]);exit;
            }else{
                $id[$k] = GoodsCategory::where(['goods_name'=>$v,'parent_id'=>$id[$k-1]])->first()['id'];
            }
        }
        return end($id);
    }

    /**
     * 获取下级分类 提供给移动端分类页
     * @author  hkw <hkw925@qq.com>
     * @param int $cate_id
     * @return \Illuminate\Support\Collection
     */
    public function getChildCate1($cate_id=0){
        return GoodsCategory::where('parent_id',$cate_id)->orderBy('sort_order')->get();
    }
}
