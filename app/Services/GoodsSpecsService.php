<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/25
 * Time: 14:23
 */

namespace App\Services;

use App\GoodsSpecs;
use App\Lib\Util\QueryPager;

class GoodsSpecsService
{
    public function getGoodsSpecs(Array $input)
    {
        $query = new GoodsSpecs();
        $query = $query->where('goods_id', $input['goods_id']);
        $pager = new QueryPager($query);
        $pager->mapField('display_flag', GoodsSpecs::$GOODS_SPECS_FLAG_MAP);
        $pager->mapField('delivery_time', GoodsSpecs::$GOODS_DELIVERY_TIME);
        $pager->setRefectionMethodField('getFullPicturePath1');
        $pager->setRefectionMethodField('getFullPicturePath2');
        $pager->setRefectionMethodField('getFullPicturePath3');
        $pager->setRefectionMethodField('getFullPicturePath4');
        $pager->setRefectionMethodField('getFullPicturePath5');

        return $pager->queryWithoutPaginate($input, 'sort_order');
    }

    public function addGoodsSpecs(Array $input)
    {
        GoodsSpecs::create([
            'goods_id'        => $input['goods_id'],
            'goods_spec'      => $input['goods_spec'],
            'goods_number'    => $input['goods_number'],
            'inventory'       => $input['inventory'],
            'price'           => $input['price'],
            'cost_price'      => $input['cost_price'],
            'origin_price'    => $input['origin_price'],
            'unit'            => $input['unit'],
            'min_buy_amount'  => $input['min_buy_amount'],
            'goods_spec_pic1' => $input['goods_spec_pic1'],
            'goods_spec_pic2' => $input['goods_spec_pic2'],
            'goods_spec_pic3' => $input['goods_spec_pic3'],
            'goods_spec_pic4' => $input['goods_spec_pic4'],
            'goods_spec_pic5' => $input['goods_spec_pic5'],
            'sort_order'      => $input['sort_order'],
            'display_flag'    => $input['display_flag'],
            'delivery_time'    => $input['delivery_time'],
        ]);
    }

    public function editGoodsSpecs(Array $input)
    {
        GoodsSpecs::where('id', $input['id'])->update([
            'goods_id'        => $input['goods_id'],
            'goods_spec'      => $input['goods_spec'],
            'goods_number'    => $input['goods_number'],
            'inventory'       => $input['inventory'],
            'price'           => $input['price'],
            'cost_price'      => $input['cost_price'],
            'origin_price'    => $input['origin_price'],
            'unit'            => $input['unit'],
            'min_buy_amount'  => $input['min_buy_amount'],
            'goods_spec_pic1' => $input['goods_spec_pic1'],
            'goods_spec_pic2' => $input['goods_spec_pic2'],
            'goods_spec_pic3' => $input['goods_spec_pic3'],
            'goods_spec_pic4' => $input['goods_spec_pic4'],
            'goods_spec_pic5' => $input['goods_spec_pic5'],
            'sort_order'      => $input['sort_order'],
            'display_flag'    => $input['display_flag'],
            'delivery_time'    => $input['delivery_time'],
        ]);
    }

    public function delGoodsSpecs($id)
    {
        GoodsSpecs::where('id', $id)->delete();
    }
}