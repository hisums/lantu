<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/12
 * Time: 16:22
 */

namespace App\Services;

use App\AboutUs;
use App\Lib\Util\QueryPager;

class AboutService
{
    public function aboutSeeder()
    {
        $seeder = config('system.default_about_data');
        foreach ($seeder as $v) {
            AboutUs::create([
                'name'       => $v['name'],
                'content'    => $v['content'],
                'sort_order' => $v['sort_order'],
            ]);
        }
    }

    //获取所有数据
    public function getData()
    {
        return AboutUs::orderBy('sort_order', 'asc')->get();
    }

    //修改内容
    public function editContent($input)
    {
        foreach($input as $v){
            AboutUs::where('id',$v['id'])->update([
                'content'=>$v['content']
            ]);
        }
    }
}