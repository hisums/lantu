<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/31
 * Time: 11:32
 */

namespace App\Services;

use App\GoodsParams;
use App\Lib\Util\QueryPager;

class GoodsParamsService
{
    public function getGoodsParams($goodsId)
    {
        $query = new GoodsParams();
        $query = $query->where('goods_id', $goodsId);
        $pager = new QueryPager($query);
        return $pager->queryWithoutPaginate(['goods_id' => $goodsId], 'sort_order');
    }

    public function goodsAttributeAdd(Array $input)
    {
        GoodsParams::create([
            'params_name'  => $input['params_name'],
            'params_value' => $input['params_value'],
            'sort_order'   => $input['sort_order'],
            'goods_id'     => $input['goods_id']
        ]);
    }

    public function goodsAttributeEdit(Array $input)
    {
        GoodsParams::where('id', '=', $input['id'])->update([
            'params_name'  => $input['params_name'],
            'params_value' => $input['params_value'],
            'sort_order'   => $input['sort_order'],
            'goods_id'     => $input['goods_id']
        ]);
    }

    public function goodsAttributeDel($id)
    {
        GoodsParams::where('id', '=', $id)->delete();
    }
}
