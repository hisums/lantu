<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/1
 * Time: 14:49
 */

namespace App\Services;

use App\Lib\Util\QueryPager;
use App\NewsCategory;
use App\News;

class NewsService
{
    public function getNewsCate(Array $input, $paging = true)
    {
        $query = new NewsCategory();

        if (!empty($input['name'])) {
            $query = $query->where('name', 'like', '%' . $input['name'] . '%');
        }

        if (!empty($input['display_flag'])) {
            $query = $query->where('display_flag', $input['display_flag']);
        }

        $pager = new QueryPager($query);

        $pager->mapField('display_flag', NewsCategory::$DISPLAY_FLAG_MAP);

        $pager->setRefectionMethodField('getFullPicturePath');
        return $paging ? $pager->doPaginate($input, 'sort_order') :
            $pager->queryWithoutPaginate($input, 'sort_order');
    }

    public function newsCateAdd(Array $input)
    {
        NewsCategory::create([
            'name'              => $input['name'],
            'sort_order'        => $input['sort_order'],
            'news_cate_picture' => $input['news_cate_picture'],
            'display_flag'      => $input['display_flag'],
        ]);
    }

    public function newsCateEdit(Array $input)
    {
        NewsCategory::where('id', $input['id'])->update([
            'name'              => $input['name'],
            'sort_order'        => $input['sort_order'],
            'news_cate_picture' => $input['news_cate_picture'],
            'display_flag'      => $input['display_flag'],
        ]);
    }

    public function newsCateDel($id)
    {
        NewsCategory::where('id', $id)->delete();
    }

    public function issetNews($id)
    {
        return News::where('news_cate_id', $id)->first();
    }

    public function getNews(Array $input, $paging = true)
    {
        $query = new News;

        if (!empty($input['subject'])) {
            $query = $query->where('subject', 'like', '%' . $input['subject'] . '%');
        }

        if (!empty($input['display_flag'])) {
            $query = $query->where('display_flag', $input['display_flag']);
        }


        if (!empty($input['news_cate_id'])) {
            $query = $query->whereHas('newsCategory', function ($query) use ($input) {
                $query->where('news_categories.id', $input['news_cate_id']);
            });
        }

        if (!empty($input['start_create_dt'])) {
            $query = $query->where('create_time', '>=', $input['start_create_dt']);
        }

        if (!empty($input['end_create_dt'])) {
            $query = $query->where('create_time', '<=', $input['end_create_dt']);
        }

        if (empty($input['sort_order']) || $input['sort_order'] == 1) {
            $query = $query->orderBy('sort_order', 'ASC');
        } else {
            $query = $query->orderBy('sort_order', 'DESC');
        }

        $pager = new QueryPager($query);

        $pager->mapField('display_flag', News::$DISPLAY_FLAG_MAP);
        $pager->mapField('is_recommend', News::$RECOMMEND_MAP);

        $pager->setRefectionMethodField('newsCategoryGet');

        return $paging ? $pager->doPaginate($input) :
            $pager->queryWithoutPaginate($input);
    }

    public function getWapNews(array $input){
        $query = new News();
        if (!empty($input['cate_id'])) {
            $query = $query->where('news_cate_id',$input['cate_id']);
        }else{
            $query = $query->whereIn('news_cate_id',[NewsCategory::$DEFAULT_CATE_CXHD,NewsCategory::$DEFAULT_CATE_CPFK,NewsCategory::$DEFAULT_CATE_YNJD]);
        }
        $query = $query->orderBy('sort_order', 'ASC');
        $pager = new QueryPager($query);
        $pager->setRefectionMethodField('getFullPicturePath');
        return $pager->doPaginate1($input);
    }

    public function getIndexNews(){
        return News::where('news_cate_id',NewsCategory::$DEFAULT_CATE_JSWZ)->first();
    }

    public function getAboutUs(){
        return News::where('news_cate_id',NewsCategory::$DEFAULT_CATELTJS)->first();
    }

    public function getNewsCateInNews()
    {
        return NewsCategory::where('display_flag', NewsCategory::$DISPLAY_FLAG_ON)->get();
    }

    public function newsAdd(Array $input)
    {
        date_default_timezone_set('PRC');
        News::create([
            'subject'      => $input['subject'],
            'content'      => $input['content'],
            'display_flag' => $input['display_flag'],
            'news_cate_id' => $input['news_cate_id'],
            'create_time'  => date('Y-m-d H:i', time()),
            'show_img'     => $input['show_img'],
            'sort_order'   => $input['sort_order'],
            'is_recommend' => $input['is_recommend'],
        ]);
    }

    public function newsEdit(Array $input)
    {
        News::where('id', $input['id'])->update([
            'subject'      => $input['subject'],
            'content'      => $input['content'],
            'display_flag' => $input['display_flag'],
            'news_cate_id' => $input['news_cate_id'],
            'show_img'     => $input['show_img'],
            'sort_order'   => $input['sort_order'],
            'is_recommend' => $input['is_recommend'],
        ]);
    }

    public function newsDel(Array $input)
    {
        News::where('id', $input['id'])->delete();
    }

    public function getBanner()
    {
        return News::select('*')
            ->where('display_flag', News::$DISPLAY_FLAG_ON)
            ->whereIn('news_cate_id', [NewsCategory::$DEFAULT_CATE_CXHD,NewsCategory::$DEFAULT_CATE_YNJD,NewsCategory::$DEFAULT_CATE_CPFK])
//            ->whereIn('news_cate_id', [NewsCategory::$BANNER_ID])
            ->orderBy('sort_order','asc')
            ->limit(5)->get();
    }

    public function actList()
    {
        return News::where('news_cate_id', NewsCategory::$DEFAULT_CATE_CXHD)->get();
    }

    public function actDetail($id)
    {
        return News::where('id', $id)->first();
    }

    public function allArticleList()
    {
        return News::whereIn('news_cate_id', [NewsCategory::$DEFAULT_CATE_JSWZ,NewsCategory::$DEFAULT_CATE_YNJD,NewsCategory::$DEFAULT_CATE_CPFK])->orderBy('sort_order', 'asc')->get();
//        return News::whereIn('news_cate_id', [NewsCategory::$BANNER_ID])->orderBy('sort_order', 'asc')->get();
    }

    public function articleList($cate)
    {
        return News::where('news_cate_id', $cate)->orderBy('sort_order', 'asc')->get();
    }

    public function testingArticleList($cate)
    {
        return News::where('news_cate_id', $cate)->limit(5)->orderBy('sort_order', 'asc')->get();
    }

    public function articleDetail($id)
    {
        return News::where('id', $id)->first();
    }

    //获取今日推荐
    public function todayRecommend()
    {
        $cate_arr = [
            NewsCategory::$DEFAULT_CATE_JSWZ,
            NewsCategory::$DEFAULT_CATE_CXHD,
            NewsCategory::$DEFAULT_CATE_CPFK,
            NewsCategory::$DEFAULT_CATE_YNJD,
        ];
        return News::whereIn('news_cate_id', $cate_arr)
            ->where('is_recommend', News::$RECOMMEND_YES)
            ->limit(5)
            ->orderBy('sort_order', 'asc')->get();
    }

    public function testing_center_cate()
    {
        $cateList = [
            NewsCategory::$DEFAULT_CATE_SHJC,
            NewsCategory::$DEFAULT_CATE_MYJC,
            NewsCategory::$DEFAULT_CATE_KTZB,
            NewsCategory::$DEFAULT_CATE_SJHDZ,
        ];
        return NewsCategory::whereIn('id',$cateList)->get();
    }

    //总排行
    public function ranking()
    {
        $cate_arr = [
            NewsCategory::$DEFAULT_CATE_JSWZ,
            NewsCategory::$DEFAULT_CATE_CPFK,
            NewsCategory::$DEFAULT_CATE_YNJD,
        ];
        return News::whereIn('news_cate_id', $cate_arr)
            ->limit(5)
            ->orderBy('sort_order', 'asc')->get();
    }


}