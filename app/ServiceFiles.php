<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/10
 * Time: 11:19
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class ServiceFiles extends Model
{
    protected $table = 'service_files';

    public static $FILEFULLPATH = '/storage/upload/';
    public static $MIME_TYPE = [
        'application/vnd.ms-excel'                                                  => 'xls',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'         => 'xlsx',
        'application/msword'                                                        => 'doc',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'   => 'docx',
        'application/vnd.ms-powerpoint'                                             => 'ppt',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
    ];

    public $timestamps = false;

    protected $fillable = [
        'name', 'file_id', 'sort_order'
    ];

    public function getFullPath()
    {
        if(!empty($this->file_id)){
            return '/storage/upload/'.$this->file_id;
        }
        return '';
    }




}