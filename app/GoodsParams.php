<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/31
 * Time: 9:04
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Lib\Util\QueryPager;

class GoodsParams extends Model
{
    protected $table = 'goods_params';
    public $timestamps = false;
    protected $fillable = ['params_name', 'params_value', 'sort_order', 'goods_id'];


}