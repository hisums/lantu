<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/12
 * Time: 16:22
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $table = 'about_us';

    public $timestamps = false;

    public static $PHONE_ID = 1;
    public static $CHUANZHEN_ID = 2;
    public static $QQ_ID = 3;
    public static $EMAIL_ID = 4;

    protected $fillable = [
        'name', 'content' ,'sort_order'
    ];
}