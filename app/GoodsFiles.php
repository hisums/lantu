<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/10
 * Time: 20:45
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class GoodsFiles extends Model
{
    protected $table = 'goods_files';
    public $timestamps = false;

    protected $fillable = [
        'goods_id', 'file_id'
    ];


}