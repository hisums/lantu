<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lib\Core\RouteSerializer;

class UploadedFileState extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'file_id';

    protected $fillable = [
        'file_id', 'state', 'batch_upload_origin_name'
    ];
}
