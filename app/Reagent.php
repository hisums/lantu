<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/19
 * Time: 9:23
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Lib\FileService\FastDFS;

class Reagent extends Model
{
    protected $table = 'reagents';
    public $timestamps = false;

    protected $fillable = [
        'file_id', 'name', 'reagent_img'
    ];

    public function serviceFile()
    {
        return $this->hasOne('App\ServiceFiles', 'id', 'file_id')->first();
    }

    public function getFullPath()
    {
        if (!empty($this->reagent_img)) {
            return FastDFS::getFullUrl($this->reagent_img);
        }
        return '/images/no-pic-back.png';
    }



}