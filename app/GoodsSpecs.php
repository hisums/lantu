<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/25
 * Time: 13:59
 */

namespace App;

use App\Lib\FileService\FastDFS;
use Illuminate\Database\Eloquent\Model;

class GoodsSpecs extends Model
{
    protected $table = 'goods_specs';
    public $timestamps = false;
    protected $fillable = [
        'goods_model',
        'goods_spec',
        'goods_number',
        'inventory',
        'price',
        'origin_price',
        'cost_price',
        'unit',
        'min_buy_amount',
        'goods_spec_pic1',
        'goods_spec_pic2',
        'goods_spec_pic3',
        'goods_spec_pic4',
        'goods_spec_pic5',
        'sort_order',
        'display_flag',
        'delivery_time',
        'goods_id',
    ];

    public static $GOODS_SPECS_ON = 1;
    public static $GOODS_SPECS_OFF = 2;


    public static $GOODS_SPECS_FLAG_MAP = [
        ['key' => 1, 'text' => '显示'],
        ['key' => 2, 'text' => '不显示'],
    ];

    public static $GOODS_DELIVERY_TIME = [
        ['key' => 1, 'text' => '现货'],
        ['key' => 2, 'text' => '7天'],
        ['key' => 3, 'text' => '15天'],
    ];

    public function getFullPicturePath1()
    {

        if (!empty($this->goods_spec_pic1)) {
            return FastDFS::getFullUrl($this->goods_spec_pic1);
        }
        return '/images/no-pic-back.png';
    }

    public function getFullPicturePath2()
    {

        if (!empty($this->goods_spec_pic2)) {
            return FastDFS::getFullUrl($this->goods_spec_pic2);
        }
        return '/images/no-pic-back.png';
    }

    public function getFullPicturePath3()
    {

        if (!empty($this->goods_spec_pic3)) {
            return FastDFS::getFullUrl($this->goods_spec_pic3);
        }
        return '/images/no-pic-back.png';
    }

    public function getFullPicturePath4()
    {

        if (!empty($this->goods_spec_pic4)) {
            return FastDFS::getFullUrl($this->goods_spec_pic4);
        }
        return '/images/no-pic-back.png';
    }

    public function getFullPicturePath5()
    {

        if (!empty($this->goods_spec_pic5)) {
            return FastDFS::getFullUrl($this->goods_spec_pic5);
        }
        return '/images/no-pic-back.png';
    }

    /**
     * @description:关联商品
     * @author: hkw <hkw925@qq.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goods()
    {
        return $this->belongsTo('App\Goods', 'goods_id', 'id');
    }

    public function getGoods(){
        return $this->goods()->first();
    }

    public function goodsPic(){
        return $this->getGoods()->getFullPicturePath();
    }
}