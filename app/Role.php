<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'description', 'group_id'
    ];

    public function permissions()
    {
        return $this->belongsToMany('App\MenuPermission', 'role_menu_permissions', 'role_id', 'permission_id');
    }
}
