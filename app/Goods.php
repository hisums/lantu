<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/20
 * Time: 14:11
 */

namespace App;

use App\Lib\FileService\FastDFS;
use Illuminate\Database\Eloquent\Model;

class Goods extends Model
{
    protected $table = 'goods';
    public $timestamps = false;
    protected $fillable = ['goods_name', 'goods_vendor', 'goods_picture', 'goods_content', 'goods_score', 'sort_order', 'list_flag', 'goods_category_id'];

    public static $GOODS_LIST_FLAG_ON = 1;
    public static $GOODS_LIST_FLAG_OFF = 2;

    public static $GOODS_LIST_FLAG_MAP = [
        ['key' => 1, 'text' => '已上架'],
        ['key' => 2, 'text' => '未上架']
    ];

    public static $GOODS_SCORE_MIN = 1;
    public static $GOODS_SCORE_MAX = 5;

    public function goodsCategory()
    {
        return $this->belongsTo('App\GoodsCategory', 'goods_category_id', 'id');
    }

    public function goodsCategoryGet()
    {
        return $this->goodsCategory()->get();
    }

    public function goodsInquirys()
    {
        return $this->belongsTo('App\GoodsInquirys', 'id', 'goods_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'goods_inquirys', 'goods_id', 'user_id');
    }

    public function usersGet()
    {
        return $this->users()->first();
    }

    public function goodsFile()
    {
        return $this->belongsToMany('App\ServiceFiles', 'goods_files', 'goods_id', 'file_id');
    }

    public function getFile()
    {
        return $this->goodsFile()->first();
    }

    public function goodsInquirysGet()
    {
        return $this->goodsInquirys()->get();
    }

    public function getFullPicturePath()
    {
        if (!empty($this->goods_picture)) {
            return FastDFS::getFullUrl($this->goods_picture);
        }
        return '/images/no-pic-back.png';
    }

    public function getFullQRCodePath()
    {
        if (!empty($this->goods_qrcode)) {
            return '/storage/app/public/qrcodes/'.$this->goods_qrcode;
        }
        return '/images/no-pic-back.png';
    }

    public function spec()
    {
        return $this->hasMany('App\GoodsSpecs', 'goods_id', 'id');
    }

    public function params()
    {
        return $this->hasMany('App\GoodsParams', 'goods_id', 'id')->orderBy('sort_order','asc');
    }
}