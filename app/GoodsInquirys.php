<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/31
 * Time: 15:43
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class GoodsInquirys extends Model
{
    protected $table = 'goods_inquirys';
    public $timestamps = false;
    protected $fillable = ['display_flag', 'process_status', 'process_user_id'];

    public static $PROCESS_STATUS_FINISH = 2;
    public static $PROCESS_STATUS_UNFINISH = 1;

    public static $PROCESS_STATUS_MAP = [
        ['key' => 1, 'text' => '未处理'],
        ['key' => 2, 'text' => '已处理']
    ];

    public static $DISPLAY_FLAG_ON = 1;
    public static $DISPLAY_FLAG_OFF = 2;

    public static $DISPLAY_FLAG_MAP = [
        ['key' => 1, 'text' => '显示'],
        ['key' => 2, 'text' => '不显示']
    ];

    public function goods()
    {
        return $this->belongsTo('App\Goods', 'goods_id', 'id');
    }

    public function goodsGet()
    {
        return $this->goods()->first();
    }

    public function goodsCategory()
    {
        return $this->goodsGet()->belongsTo('App\GoodsCategory', 'goods_category_id', 'id');
    }

    public function goodsCategoryGet()
    {
        return $this->goodsCategory()->first();
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function usersGet()
    {
        return $this->users()->first();
    }


}