<?php

return [
    'client_url' => '//captcha.luosimao.com/static/js/api.js',
    'client_site_key' => '8e44d22f684c0160e9880b04ca4570d5',

    'verify_url' => 'https://captcha.luosimao.com/api/site_verify',
    'verify_api_key' => '83e1652939ad1fa278f3853a30c54bf5',

    'enable' => env('ENABLE_CAPTCHA', false),
];
