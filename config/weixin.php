<?php

/**
 * Usage:  Config::get('constants.CONSTANT_NAME') or die('Config Error!');
 * 注意：如果get里面的文件并不存在，不会报错，而只会返回null
 */
return [
    'weixin_configs' => [
        [
            'key' => \App\AccessToken::$APP_TYPE_DEFAULT,
            'AppId' => '',
            'AppSecret' => ''
        ]
    ]
];
