<?php

return [

    'init_user_name' => env('INIT_USER_NAME', 'admin'),

    'init_user_nick_name' => env('INIT_USER_NICK_NAME', 'admin'),

    'init_user_mobile' => env('INIT_USER_MOBILE', '00000000000'),

    'init_user_password' => env('INIT_USER_PASSWORD', 'ADM_123'),

    'logo_image' => env('LOGO_IMAGE', '/images/logo.png'),

    'default_platform_org' => '恒森信息',

    'default_admin_role_name' => '超级管理员',


    'default_news_cate' => [
        '首页轮播图',
        '促销活动',
        '技术文章',
        '管理岗位招聘',
        '技术岗位招聘',
        '其他岗位招聘',
        '产品反馈',
        '疑难解答',
        '生化检测',
        '免疫检测',
        '抗体制备',
        '试剂盒定制',
        '中心简介',
        '检测流程',
        '生化检测流程',
        '免疫检测流程',
        '抗体制备流程',
        '试剂盒定制流程',
        '蓝图介绍',
        '联系我们',
        '实验展示图片',
    ],

    'default_reagent' => [
        '热销试剂',
        '耗材目录',
        '试剂目录'
    ],

    'default_about_data' => [
        [
            'name'    => '电话',
            'content' => '0595-27971666',
            'sort_order'=>1,
        ],
        [
            'name'    => '传真',
            'content' => '0595-27973666',
            'sort_order'=>2,
        ],
        [
            'name'    => 'QQ',
            'content' => '355902222',
            'sort_order'=>3,
        ],
        [
            'name'    => '邮箱',
            'content' => '2228104692@qq.com',
            'sort_order'=>4,
        ],
    ],

];
